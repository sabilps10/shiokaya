<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Question;
use App\Models\Answer;
use App\Models\Account;
use Log;
// VALIDATION: change the requests to match your own file names if you need form validation
//use App\Http\Requests\TagCrudRequest as StoreRequest;
//use App\Http\Requests\TagCrudRequest as UpdateRequest;

class QuizController extends Controller {

    public function index(Request $request, Question $question){
      $questions = $question->orderBy('id','asc')->get();

      return view('welcome',[
        'questions'=>$questions,
        'unique_code'=>md5(rand(0,99999).'|'.time())
      ]);
    }
    public function result(Request $request,
                          Question $question,
                          Account $account
                          ){
      $questions = $question->orderBy('id','asc')->get();
      $data = $request->all();
      $results = $this->calculate($questions, $data);
      $unique_code = md5(rand(0,99999).'|'.time());
      $saveData = [
        'account_id'=>0,
        'name'=>$data['name'],
        'email'=>$data['email'],
        'code'=>$unique_code,
        'phone_number'=>$data['phone_number'],
        'answers'=>$this->getAnswers($data),
        'results'=>$results
      ];

      $oldAccount = $account->where('email',$data['email'])->first();
      if(!isset($oldAccount->id)){
        $account_id = $this->createAccount([
          'name'=>$data['name'],
          'email'=>$data['email'],
          'phone_number'=>$data['phone_number']
        ]);

      }else{
        $account_id = $oldAccount->id;
      }

      $rs = $this->saveAnswer([
        'account_id'=>$account_id,
        'code'=>$unique_code,
        'content'=>serialize($saveData)
      ]);
      /*
      @TODO
      1. create account and retrieve account_id
      2. map the account_id into saveData
      */

      if($rs){
        return view('result',[
          'data'=>$data,
          'unique_code'=>$unique_code,
          'results'=>$results,
          
        ]);
      }else{
        return view('failed',[

        ]);
      }

    }
    private function createAccount($data){
      $account = new Account($data);
      $account->save();
      return $account->id;
    }
    private function saveAnswer($data){
      $account = new Answer($data);
      return $account->save();
    }
    private function getAnswers($data){
      $results = [];
      foreach($data as $name=>$val){
          if(preg_match("/a_/",$name)){
            $results[$name] = $val;
          }
      }
      return $results;
    }
    private function calculate($questions, $data){
        $results = [];
        for($i=0;$i< $questions->count(); $i++){
            $q = $questions[$i]->toArray();

            foreach($q as $name=>$val){
                if(preg_match("/n_/",$name)){
                  if(!isset($results[$name])) $results[$name] = 0;

                  if($q[$name]==$data['a_'.$q['id']]){
                 //     Log::info(json_encode($results));
                   // Log::info($name.'->'.$q[$name].'=='.$q['id']."#".$data['a_'.$q['id']]);

                    $results[$name]+=1;
                  }
                }
            }

        }
        return $results;
    }



}
