<?php namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;
use Illuminate\Http\Request;

// VALIDATION: change the requests to match your own file names if you need form validation
//use App\Http\Requests\TagCrudRequest as StoreRequest;
//use App\Http\Requests\TagCrudRequest as UpdateRequest;

class QuestionCrudController extends CrudController {

	public function setup() {
        $this->crud->setModel('App\Models\Question');
        $this->crud->setRoute(config('backpack.base.route_prefix')  . '/question');
        $this->crud->setEntityNameStrings('question', 'questions');

        $this->crud->setColumns(['id','question','a','b','c','d']);

        $this->crud->addField([
          	'name' => 'question',
          	'label' => 'Question'
          	]);

        $this->crud->addField([
          'name' => 'a',
          'label' => 'A'
        ]);

        $this->crud->addField([
          'name' => 'b',
          'label' => 'B'
        ]);

        $this->crud->addField([
          'name' => 'c',
          'label' => 'C'
        ]);

        $this->crud->addField([
          'name' => 'd',
          'label' => 'D'
        ]);

/*
V	,	Visual
A	,	Auditory
K	,	Kinesthetic
An	,	Analitic
Cre	,	Creator
Sta	,	Star
Sup	,	Supporter
Dea	,	Deal Maker
Tra	,	Trader
Acc	,	Accumulator
Lor	,	Lord
Mec	,	Mechanic
Dyn	,	Dynamo
Bla	,	Blaze
Tem	,	Tempo
Ste	,	Steel*/
        $this->crud->addField([
          'name' => 'n_v',
          'label' => 'Visual',
          'type' => 'select_from_array',
          'options' => ['f'=>'F','a' => 'A', 'b' => 'B', 'c' => 'C', 'd' => 'D'],
          'allows_null' => false,

        ]);

        $this->crud->addField([
          'name' => 'n_a',
          'label' => 'Auditory',
          'type' => 'select_from_array',
          'options' => ['f'=>'F','a' => 'A', 'b' => 'B', 'c' => 'C', 'd' => 'D'],
          'allows_null' => false,

        ]);

        $this->crud->addField([
          'name' => 'n_k',
          'label' => 'Kinesthetic',
          'type' => 'select_from_array',
          'options' => ['f'=>'F','a' => 'A', 'b' => 'B', 'c' => 'C', 'd' => 'D'],
          'allows_null' => false,

        ]);

        $this->crud->addField([
          'name' => 'n_an',
          'label' => 'Analytic',
          'type' => 'select_from_array',
          'options' => ['f'=>'F','a' => 'A', 'b' => 'B', 'c' => 'C', 'd' => 'D'],
          'allows_null' => false,

        ]);

        $this->crud->addField([
          'name' => 'n_cre',
          'label' => 'Creator',
          'type' => 'select_from_array',
          'options' => ['f'=>'F','a' => 'A', 'b' => 'B', 'c' => 'C', 'd' => 'D'],
          'allows_null' => false,

        ]);

        $this->crud->addField([
          'name' => 'n_sta',
          'label' => 'Star',
          'type' => 'select_from_array',
          'options' => ['f'=>'F','a' => 'A', 'b' => 'B', 'c' => 'C', 'd' => 'D'],
          'allows_null' => false,

        ]);

        $this->crud->addField([
          'name' => 'n_sup',
          'label' => 'Supporter',
          'type' => 'select_from_array',
          'options' => ['f'=>'F','a' => 'A', 'b' => 'B', 'c' => 'C', 'd' => 'D'],
          'allows_null' => false,

        ]);

        $this->crud->addField([
          'name' => 'n_dea',
          'label' => 'Deal Maker',
          'type' => 'select_from_array',
          'options' => ['f'=>'F','a' => 'A', 'b' => 'B', 'c' => 'C', 'd' => 'D'],
          'allows_null' => false,

        ]);


        $this->crud->addField([
          'name' => 'n_tra',
          'label' => 'Trader',
          'type' => 'select_from_array',
          'options' => ['f'=>'F','a' => 'A', 'b' => 'B', 'c' => 'C', 'd' => 'D'],
          'allows_null' => false,

        ]);

        $this->crud->addField([
          'name' => 'n_acc',
          'label' => 'Accumulator',
          'type' => 'select_from_array',
          'options' => ['f'=>'F','a' => 'A', 'b' => 'B', 'c' => 'C', 'd' => 'D'],
          'allows_null' => false,

        ]);

        $this->crud->addField([
          'name' => 'n_lor',
          'label' => 'Lord',
          'type' => 'select_from_array',
          'options' => ['f'=>'F','a' => 'A', 'b' => 'B', 'c' => 'C', 'd' => 'D'],
          'allows_null' => false,

        ]);


        $this->crud->addField([
          'name' => 'n_mec',
          'label' => 'Mechanic',
          'type' => 'select_from_array',
          'options' => ['f'=>'F','a' => 'A', 'b' => 'B', 'c' => 'C', 'd' => 'D'],
          'allows_null' => false,

        ]);

        $this->crud->addField([
          'name' => 'n_dyn',
          'label' => 'Dynamo',
          'type' => 'select_from_array',
          'options' => ['f'=>'F','a' => 'A', 'b' => 'B', 'c' => 'C', 'd' => 'D'],
          'allows_null' => false,

        ]);

        $this->crud->addField([
          'name' => 'n_bla',
          'label' => 'Blaze',
          'type' => 'select_from_array',
          'options' => ['f'=>'F','a' => 'A', 'b' => 'B', 'c' => 'C', 'd' => 'D'],
          'allows_null' => false,

        ]);

        $this->crud->addField([
          'name' => 'n_tem',
          'label' => 'Tempo',
          'type' => 'select_from_array',
          'options' => ['f'=>'F','a' => 'A', 'b' => 'B', 'c' => 'C', 'd' => 'D'],
          'allows_null' => false,

        ]);

        $this->crud->addField([
          'name' => 'n_ste',
          'label' => 'Steel',
          'type' => 'select_from_array',
          'options' => ['f'=>'F','a' => 'A', 'b' => 'B', 'c' => 'C', 'd' => 'D'],
          'allows_null' => false,

        ]);

    }

	public function store(Request $request)
	{
		return parent::storeCrud();
	}

	public function update(Request $request)
	{
		return parent::updateCrud();
	}
}
