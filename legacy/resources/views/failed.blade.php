<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="{{url('favicon.ico')}}">

    <title>Clock&Compass</title>

    <!-- Bootstrap core CSS -->
    <link href="{{url('bower_components/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{url('css/styles.css')}}" rel="stylesheet">
  </head>

  <body>
    <!--head-->
    <div class="head">
      <div class="container">
        <div class="row">
          <div class="col-md-12 center">
            <img src="{{url('images/logo-small.png')}}" class="logo"/>
          </div>
        </div>
      </div>


    <main role="main" class="container">
      <div class="container">
        <div class="row">
          <div class="col-md-12 center">
            <h3>Terjadi Kesalahan</h3>
            <p>Mohon maaf, jawaban kuisioner anda tidak berhasik disimpan. Silahkan coba kembali.</p>
            <div class="action">
              <a href="{{url('/')}}" class="btn btn-primary">Coba Lagi</a>
            </div>
          </div>
        </div>
      </div>

    </main><!-- /.container -->

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="{{url('bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{url('bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{url('bower_components/chart.js/dist/Chart.bundle.min.js')}}"></script>

  </body>
</html>
