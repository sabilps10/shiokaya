<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="{{url('favicon.ico')}}">

    <title>Clock&Compass</title>

    <!-- Bootstrap core CSS -->
    <link href="{{url('bower_components/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{url('css/styles.css')}}" rel="stylesheet">
  </head>

  <body>
    <!--head-->
    <div class="head">
      <div class="container">
        <div class="row">
          <div class="col-md-12 center">
            <img src="{{url('images/logo-small.png')}}" class="logo"/>
          </div>
        </div>
      </div>
      <!--
      <nav class="navbar navbar-expand-md navbar-dark bg-dark">
        <a class="navbar-brand" href="#"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarsExampleDefault">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
              <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
            </li>

          </ul>

        </div>
      </nav>
    -->
    </div> <!--/head-->

    <main role="main" class="container">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
              {!! Form::open(['url' => 'result','method'=>'post']) !!}
              <h4 class="center title" style="margin-top:60px">Selamat datang di kuisioner profil kemakmuran anda, sebagai dasar acuan anda untuk memilih peran anda dalam bisnis, hubungan, dan keputusan-keputusan dalam hidup anda</h4>
              <div class="register">
                <p>Silahkan isi form dibawah ini : </p>
                <div class="row">
                <div class="col-md-2">
                  <label>Nama Anda</label>
                </div>
                <div class="col-md-10">
                  <input type="text" name="name"/>
                </div>
                  <div class="col-md-2">
                  <label>E-Mail Anda</label>
                  </div>
                    <div class="col-md-10">
                  <input type="text" name="email"/>  </div>
                    <div class="col-md-2">
                  <label>Nomor HP Anda</label>
                  </div>
                    <div class="col-md-10">
                  <input type="text" name="phone_number"/>  </div>

                </div>
                <div class="action center">
                  <input type="button" name="btnNext" value="MULAI" class="btn btn-primary"/>
                </div>
              </div>
              <div class="questions hide">

<p>
Pilih HANYA Salah satu dari 4 pilihan (a,b,c,d) dari tiap nomor, yang menurut anda isinya paling mewakili atau mendekati diri anda.</p>


                  @for($i=0;$i<$questions->count();$i++)
                    <div class="items">
                      {{($i+1)}}. {!!$questions[$i]->question!!}
                    </div>
                    <div class="answers">
                      <ul>
                        <li><input type="radio" name="a_{{$questions[$i]->id}}" value="a" checked="checked"/> {!!$questions[$i]->a!!}</li>
                        <li><input type="radio" name="a_{{$questions[$i]->id}}" value="b"/> {!!$questions[$i]->b!!}</li>
                        <li><input type="radio" name="a_{{$questions[$i]->id}}" value="c"/> {!!$questions[$i]->c!!}</li>
                        <li><input type="radio" name="a_{{$questions[$i]->id}}" value="d"/> {!!$questions[$i]->d!!}</li>
                      </ul>
                    </div>
                  @endfor

                  <input type="hidden" name="code" value="{{$unique_code}}"/>
                  <div class="action center">
                    <input type="submit" name="btnSubmit" value="LIHAT HASIL" class="btn btn-primary"/>
                  </div>


              </div>
              {{ csrf_field() }}

                {!! Form::close() !!}
          </div>
        </div>
      </div>

    </main><!-- /.container -->

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="{{url('bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{url('bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script>
    $("input[name=btnNext]").click(function(){
      $(".register").addClass('hide');
      $(".questions").removeClass('hide').hide().fadeIn();
    });
    </script>
  </body>
</html>
