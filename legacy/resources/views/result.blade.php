<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="{{url('favicon.ico')}}">

    <title>ShioKaya</title>

    <!-- Bootstrap core CSS -->
    <link href="{{url('bower_components/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{url('css/styles.css')}}" rel="stylesheet">
  </head>

  <body>
    <!--head-->
    <div class="head">
      <div class="container">
        <div class="row">
          <div class="col-md-12 center">
            <img src="{{url('images/logo-small.png')}}" class="logo"/>
          </div>
        </div>
      </div>
    </div>

    <main role="main" class="container">
      <div class="container profile">
        <div class="row">
          <div class="col-md-6">
            <div class="chart" style="width:400px;height:400px;text-align:center;">
              <canvas id="myChart" width="250" height="250"></canvas>
            </div>
          </div>
          <div class="col-md-6">
            <div>
            <h4>{{$data['name']}}</h4>
            
            <div>
              {{$data['email']}}
            </div>
            <div>
              {{$data['phone_number']}}
            </div>
            </div>
            <hr size="1"/>
            <h4>Profile Anda</h4>
            
            <h5 class="profile-name" style="color:#cc0000"></h5>
            <div class="profile-desc"></div>

          </div>
        </div>
      </div>

    </main><!-- /.container -->

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="{{url('bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{url('bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{url('bower_components/chart.js/dist/Chart.bundle.min.js')}}"></script>
    <script>
    $("input[name=btnNext]").click(function(){
      $(".register").addClass('hide');
      $(".questions").removeClass('hide').hide().fadeIn();
    });
    var myProfile = "n_mec";
    var bignum = 0;
    var results = {!!json_encode($results)!!};

    var names = {
      "n_dea":"SOUTH-EAST/NEGOTIATOR/TENGGARA/DEAL MAKER",
      "n_sta":"NORTH-EAST/PROMOTOR/TIMUR LAUT/STAR",
      "n_sup":"EAST/MEDIATOR/TIMUR/SUPPORTER",
      "n_cre":"NORTH/INITIATOR/UTARA/CREATOR",
      "n_tra":"SOUTH/DISTRIBUTOR/SELATAN/TRADER",
      "n_acc":"SOUTH-WEST/COLLECTOR/BARAT DAYA/ACCUMULATOR",
      "n_lor":"WEST/REGULATOR/BARAT/LORD",
      "n_mec":"NORTH-WEST/INTEGRATOR/BARAT LAUT/MECHANIC"
    }
    results['n_cre'] += results['n_v'] + results['n_dyn'];
    results['n_dea'] += results['n_an'] + results['n_a'] + results['n_bla'] + results['n_tem'];
    results['n_sta'] += results['n_v'] + results['n_a'] + results['n_dyn'] + results['n_bla'];
    results['n_sup'] += results['n_a'] + results['n_bla'];
    //results['n_tra'] += results['n_an'] + results['n_tem'];

    results['n_acc'] += results['n_k'] + results['n_an'] + results['n_tem'] + results['n_ste'];
    results['n_lor'] += results['n_k'] + results['n_ste'];
    results['n_mec'] += results['n_v'] + results['n_k']+ results['n_dyn']+ results['n_ste'];


    for(var n in results){
      if(['n_cre','n_sta','n_sup','n_dea','n_tra','n_acc','n_lor','n_mec'].includes(n)){
        if(results[n] > bignum){
          bignum = results[n];
          myProfile = n;
        }
      }
    }

    console.log(results);

    $.get("profile.json?r="+Math.random()*199999,function(response){
      $(".profile-name").html(names[myProfile]);
      $(".profile-desc").html(response[myProfile]);
    });

    var ctx = $("#myChart");
    var data= {
        labels: ['North', 'North-East', 'East', 'South-East','South','South-West','West','North-West'],
        datasets: [{
            data: [
                    results['n_cre'],
                    results['n_sta'],
                    results['n_sup'],
                    results['n_dea'],
                    results['n_tra'],
                    results['n_acc'],
                    results['n_lor'],
                    results['n_mec']
                  ]
        }]
    }
    options = {
        scale: {
            // Hides the scale
            display: true
        },
        title: {
            display: true,
            text: 'Profile Chart'
        },
        legend: {
            display: false,

        }
    };
    var myRadarChart = new Chart(ctx, {
        type: 'radar',
        data: data,
        options: options
    });
    </script>
  </body>
</html>
