<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/dummy',function(){
  return view('result');
});
Route::post('/result','QuizController@result');
Route::get('/iukm/privacy-policy','iukmprivacy@privacy');
Route::get('/', 'QuizController@index');
