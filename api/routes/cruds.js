import CrudActions from "../libs/crud";
const cors = require('cors');
const express = require("express");
const router = express.Router();
const crudAction = new CrudActions({});
let crudable = [];

const allowCrud = (req,res,next)=>{
    if(crudable.includes(req.params.schema)) return next();
    return next(new Error('the end point is not available'))
}

router.all('*', cors());
router.options('/:schema', cors(),(req,res)=>{
    res.send('');
}) // enable pre-flight request for DELETE request

//retrieve all
router.get("/:schema", [allowCrud], function(req, res, next) {
    res.send({status:1})
  /*return crudAction
    .setPool(req.pool)
    .setSchema(req.params.schema)
    .getAll(req, res, next);*/
});

//lookup
router.get("/:schema/lookup",  [allowCrud], function(req, res, next) {
  return crudAction
    .setPool(req.pool)
    .setSchema(req.params.schema)
    .lookup(req, res, next);
});

//retrieve by id
router.get("/:schema/:id",  [allowCrud], function(req, res, next) {
  return crudAction
    .setPool(req.pool)
    .setSchema(req.params.schema)
    .get(req, res, next);
});

//update
router.post("/:schema/:id",  [allowCrud], function(req, res, next) {
  return crudAction
    .setPool(req.pool)
    .setSchema(req.params.schema)
    .update(req, res, next);
});

//delete
router.delete("/:schema/:id",  [allowCrud], function(req, res, next) {
  return crudAction
    .setPool(req.pool)
    .setSchema(req.params.schema)
    .delete(req, res, next);
});

router.crudable = (schemas) => {
    schemas.map((item)=>{
        crudable.push(item)
    })
}


module.exports = router;
