
const { log } = require("async");
const config = require("config");
const moment = require('moment-timezone');
const mysql2 = require("mysql2/promise");
const is_premium = require(`./helpers/is_premium`)

let pool = mysql2.createPool({
    connectionLimit: 10,
    host: config.get(`mysql.host`),
    port: config.get(`mysql.port`),
    user: config.get(`mysql.username`),
    password: config.get(`mysql.password`),
    database: config.get(`mysql.database`),
    timezone: config.get(`mysql.timezone`)
});

let cleanup = () => {
    pool.end();
    process.exit();
};

async function run() {
    try {
        // check jam server
        let jakartaTime = moment().tz('Asia/Jakarta').format('DD/MM/YYYY hh:mm:ss')
        let londonTime = moment().tz('Europe/London').format('DD/MM/YYYY hh:mm:ss')
        let currdate = moment().tz('Asia/Jakarta').format('YYYY-MM-DD hh:mm:ss')

        console.log(`#Timezone - ${moment.tz.guess()}`);
        console.log(`#Jakarta Time - ${jakartaTime}`);
        console.log(`#London Time - ${londonTime}`);
        console.log(`#Server Time - ${moment().format('DD/MM/YYYY hh:mm:ss')}`);
        console.log(`----------------------------------------------------------`);

        console.log(`is Premium Started`)
        console.log(`#START - ${jakartaTime}`)
        console.log(`----------------------------------------------------------`);

        let affiliates = await pool.query(`SELECT * 
        FROM affiliates 
        WHERE affiliate_status = ?`,
        [0])
        affiliates = affiliates[0]
        
        for (const item of affiliates) {
            let premium = await is_premium.premium(pool, item.account_id, true)

            if (premium === true) {
                await pool.query(`UPDATE affiliates 
                SET affiliate_status = ?, updated_at = ? WHERE id = ?`,
                [1, currdate, item.id])
            }
        }
        
        console.log(`----------------------------------------------------------`);
        console.log(`is Premium Ended`)
        console.log(`#END - ${jakartaTime}`)
        console.log(`----------------------------------------------------------`);

        cleanup()
    } catch (error) {
        console.log(error)
        cleanup()
    }
}

run()
process.on("SIGINT", cleanup);
process.on("SIGTERM", cleanup);