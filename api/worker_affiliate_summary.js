
const { log } = require("async");
const config = require("config");
const moment = require('moment-timezone');
const mysql2 = require("mysql2/promise");

let pool = mysql2.createPool({
    connectionLimit: 10,
    host: config.get(`mysql.host`),
    port: config.get(`mysql.port`),
    user: config.get(`mysql.username`),
    password: config.get(`mysql.password`),
    database: config.get(`mysql.database`),
    timezone: config.get(`mysql.timezone`)
});

let cleanup = () => {
    pool.end();
    process.exit();
};

async function run() {
    try {
        // check jam server
        let jakartaTime = moment().tz('Asia/Jakarta').format('DD/MM/YYYY hh:mm:ss')
        let londonTime = moment().tz('Europe/London').format('DD/MM/YYYY hh:mm:ss')
        let currdate = moment().tz('Asia/Jakarta').format('YYYY-MM-DD hh:mm:ss')

        console.log(`#Timezone - ${moment.tz.guess()}`);
        console.log(`#Jakarta Time - ${jakartaTime}`);
        console.log(`#London Time - ${londonTime}`);
        console.log(`#Server Time - ${moment().format('DD/MM/YYYY hh:mm:ss')}`);
        console.log(`----------------------------------------------------------`);

        console.log(`Affiliate Summary Started`)
        console.log(`#START - ${jakartaTime}`)
        console.log(`----------------------------------------------------------`);

        let clicks = await pool.query(`INSERT INTO affiliate_summary(affiliate_id,clicks) 
        SELECT affiliate_id,SUM(total) clicks
        FROM adlogs 
        WHERE logtype='CLICK' 
        GROUP BY affiliate_id 
        ON DUPLICATE KEY UPDATE clicks = VALUES(clicks);`)

        let impression = await pool.query(`INSERT INTO affiliate_summary(affiliate_id,impressions) 
        SELECT affiliate_id,SUM(total) 
        FROM adlogs 
        WHERE logtype='IMPRESSION' 
        GROUP BY affiliate_id 
        ON DUPLICATE KEY UPDATE impressions = VALUES(impressions);`)

        let buys = await pool.query(`INSERT INTO affiliate_summary (affiliate_id, buys) 
        SELECT affiliate_id, COUNT(a.id) buys 
        FROM transactions  a
        WHERE transaction_status = 1 AND transaction_type='SUBS'
        GROUP BY affiliate_id 
        ON DUPLICATE KEY UPDATE buys = VALUES(buys);`)
        
        let revenue = await pool.query(`INSERT INTO affiliate_summary (affiliate_id, revenue) 
        SELECT affiliate_id,SUM(amount) as revenue 
        FROM affiliate_revenues 
        GROUP BY affiliate_id 
        ON DUPLICATE KEY UPDATE revenue = VALUES(revenue);`)

        let withdraw = await pool.query(`INSERT INTO affiliate_summary (affiliate_id, withdrawn) 
        SELECT affiliate_id,SUM(amount) as withdrawn 
        FROM affiliate_withdrawals 
        WHERE withdraw_status = 1 
        GROUP BY affiliate_id 
        ON DUPLICATE KEY UPDATE withdrawn = VALUES(withdrawn);`)

        let total = await pool.query(`INSERT INTO affiliate_summary (affiliate_id, saldo) 
        select affiliate_id, 
        COALESCE(SUM(revenue),0) - COALESCE(SUM(withdrawn),0) saldo
        FROM affiliate_summary
        GROUP BY affiliate_id
        ON DUPLICATE KEY UPDATE saldo = VALUES(saldo);`)
        
        console.log(`----------------------------------------------------------`);
        console.log(`Affiliate Summary Ended`)
        console.log(`#END - ${jakartaTime}`)
        console.log(`----------------------------------------------------------`);

        cleanup()
    } catch (error) {
        console.log(error)
        cleanup()
    }
}

run()
process.on("SIGINT", cleanup);
process.on("SIGTERM", cleanup);