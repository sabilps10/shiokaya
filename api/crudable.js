const crudable = [
  "pretests",
  "texts",
  "accounts",
  "profesi",
  "contents",
  "industri",
  "pages",
  "blocks",
  "settings",
  "adcampaigns",
  "adcampaigns_assets",
  "payment_logs",
  "transactions",
  "vouchers",
  "roles",
  "voucher_catalogs"
];
module.exports = crudable;
