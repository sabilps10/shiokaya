**Slot Jawaban**

terdapat 7 slot jawaban. 

0 --- 1 --- 2 --- 3 --- 4 --- 5 --- 6

*Scoring:*

8 --- 4 --- 2 --- 0 --- 2 --- 4 --- 8

Contoh Irisan : 

Left (C1/R1) or Right (C3/R3) : 

8 --- 4 ---2 --- 0 --- 2 --- 4 --- 8

Middle (C2/R2) : 

 0 --- 2 --- 4 --- 8 --- 4 --- 2 --- 0 

An-A-V-K :

8 --- 4 --- 2 --- 1 --- 2 --- 4 --- 8

karena cuma ada 4 kriteria, jadi nilai hanya dialokasikan ke kolom C2 dan R2


**Wealth Diagram**
X | C1 | C2 | C3
---| --- | --- | ---
R1 | Mech | Creator | Star
R2 | Lord | * | Supporter
R3 | Accu | Trader| Dealmaker

---

X | C1 | C2 | C3
---| --- | --- | ---
R1 | C1R1 | C2R1 | C3R1
R2 | C1R2 | * | C3R2
R3 | C1R3 | Trader| C3R3

X | C1 | C2 | C3
---| --- | --- | ---
R1 | 0 | 1 | 2
R2 | 3 | 4 | 5
R3 | 6 | 7 | 8

---

**An-A-V-K**

X | C1 | C2 | C3
---| --- | --- | ---
R1 | * | V | *
R2 | An | * | A
R3 | * | K | *


**Type Pertanyaan**
1. Vertical -

- jika berada di paling ujung kiri
semua C1 dapat score 8
- jika berada di nomor 1,2,4,5 maka kita pakai rule scoring irisan. dimana kolom kiri(C1)/kanan(C3) dengan tengah(C2) akan mendapatkan nilai.

contoh :
user milih di posisi nomor 1

Maka kiri dapat nilai 4, tengah dapat nilai 2.



2. Horizontal
- jika berada di paling ujung kiri
semua R1 dapat score 8, dst
- jika berada di nomor 1,2,4,5 maka kita pakai rule scoring irisan. dimana kolom atas(R1)/bawah(R3) dengan tengah(R2) akan mendapatkan nilai.

contoh :
user milih di posisi nomor 1

Maka kiri dapat nilai 4, tengah dapat nilai 2.

3. AnA
- jika berada di paling ujung kiri
semua C1 dapat score 8, dst

4. VK

- jika berada di paling ujung kiri
semua R1 dapat score 8, dst


Dropdown jenis pertanyaan di halaman Input Pertanyaan : 

- horisontal
- vertikal
- An-A
- V-K