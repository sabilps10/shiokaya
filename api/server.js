import models from "./libs";
import CrudActions from "./libs/crud";
const crudAction = new CrudActions({});
const express = require("express");
const config = require("config");
const app = express();
const port = config.get("port");
const jwt = require("jsonwebtoken");
const moment = require("moment");
const async = require("async");
const path = require("path");
const bodyParser = require("body-parser");
const _ = require("underscore");

const rateLimit = require("express-rate-limit");
var mysql2 = require("mysql2/promise");

const multer = require("multer");
const sha1 = require("sha1");

const crudable = require("./crudable");
const crudOptions = require("./crudOptions");

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, config.get("upload_path"));
  },
  filename: function (req, file, cb) {
    let chunks = file.originalname.split(".");
    let ext = chunks[chunks.length - 1];
    cb(null, sha1(file.originalname) + "." + ext);
  },
});
const upload = multer({ storage: storage });

const limiter = rateLimit({
  windowMs: config.get("ratelimit.windowMs"), // 15 minutes
  max: config.get("ratelimit.max"), // limit each IP to 100 requests per windowMs
});
app.use(limiter);

//modules
const cruds = require("./routes/cruds");
const login = require("./libs/login");
const guest = require("./libs/guest");

const token = require("./token");
const register = require("./libs/register");
const confirm = require("./libs/confirmation");
const activation = require("./libs/activation");

const userHasAccess = (access, req) => {
  console.log('meta',req.meta);
  let roles = req.meta.roles;
  if (!roles) return;
  if (roles.length === 0) return;
  for (let k in access) {
    if (roles.includes(access[k])) return true;
  }
  return false;
};

app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);

app.use(bodyParser.json());

app.use(function (req, res, next) {
  res.setHeader("Access-Control-Allow-Origin", "*");

  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, OPTIONS, PUT, PATCH, DELETE"
  );
  res.setHeader(
    "Access-Control-Allow-Headers",
    "X-Requested-With,content-type,Authorization,authorization,Content-Type"
  );
  res.setHeader("Access-Control-Allow-Credentials", false);
  //intercepts OPTIONS method
  if ("OPTIONS" === req.method) {
    //respond with 200
    res.send(200);
    return;
  }
  //inject pool
  req.pool = models.getPool();
  req.crud = {
    fields: ["*"],
    joins: [],
    filters: [],
  };

  next();
});

app.use(express.static("public"));
app.set("view engine", "ejs");

function isAuthorized(req, res, next) {
  if (typeof req.headers.authorization === "undefined") {
    console.log("need login");
    throw new Error("need_login");
  }

  let chunks = req.headers.authorization.split(" ");
  let token = chunks[1];
  var payload = jwt.verify(token, config.get("JWT.secret"));

  const { sub, session_id, roles, results } = payload;
  req.meta = { sub, session_id, roles, results };
  //if (!_.includes(roles, "user")) throw new Error("access_denied");

  next();
}
function isAuthor(req, res, next) {
  const { roles } = req.meta;
  if (typeof roles === "undefined") throw new Error("access_denied");
  if (roles.includes("admin") || roles.includes("cms")) next();
  else throw new Error("access_denied");
}
function isAdmin(req, res, next) {
  const { roles } = req.meta;

  if (typeof roles === "undefined") throw new Error("access_denied");
  if (roles[0] !== "admin") throw new Error("access_denied");
  next();
}
function isPremium(req, res, next) {
  const { roles } = req.meta;

  if (typeof roles === "undefined") throw new Error("access_denied");
  if (roles[0] !== "premium") throw new Error("access_denied");
  next();
}
const allowCrud = (req, res, next) => {
  if (crudable.includes(req.params.schema)) return next();
  return next(new Error("the end point is not available"));
};
function errorHandler(err, req, res, next) {
  console.log("error", err.message);
  if (res.headersSent) {
    return next(err);
  }

  if (err.message === "need_login") {
    res.status(401);
    return res.send({ status: 401, message: "Please login" });
  }

  if (err.message === "jwt expired") {
    res.status(403);
    return res.send({ status: 403, message: "Session Expired, Please Login" });
  }

  res.status(400);
  res.send({
    status: 0,
    error: err.message,
  });
}

app.use("/file", express.static("uploads"));

//upload media
app.post("/media/upload", [upload.single("file")], (req, res) => {
  try {
    res.send({
      file_url: config.get("download_url") + "/" + req.file.filename,
    });
  } catch (err) {
    console.log(err.message);
    res.send(400);
  }
});

app.post("/guest/token", (req, res, next) => {
  guest.guest(models.getPool(), req, res, next);
});
app.post("/login", (req, res, next) => {
  login.login(models.getPool(), req, res, next);
});

// REGiSTER
app.post("/register", (req, res, next) => {
  register.register(models.getPool(), req, res, next);
});
app.get("/dashboard", models.dashboard.getSummary);

app.post("/affiliates/register", models.affiliates.register);
app.post("/email/submit", (req, res, next) => {
  register.submit(models.getPool(), req, res, next);
});
app.post("/confirm", [], (req, res, next) => {
  confirm.send(models.getPool(), req, res, next);
});

app.post("/activation/:id", [isAuthorized, isAdmin], (req, res, next) => {
  activation.activate(models.getPool(), req, res, next);
});
app.get("/page/content/home",models.webpage.home);
//app.use("/crud", [ isAuthorized ], cruds);
app.get("/crud/:schema", [allowCrud,isAuthorized], function (
  req,
  res,
  next
) {
  const { joins, filters, relationships, searchFilters, access } = crudOptions;
  if (typeof access[req.params.schema] !== "undefined") {
    if (!userHasAccess(access[req.params.schema], req))
      throw new Error("access_denied");
  }

  req.crud.joins =
    typeof joins[req.params.schema] !== "undefined"
      ? joins[req.params.schema]
      : [];

  req.crud.filters =
    typeof filters[req.params.schema] !== "undefined"
      ? filters[req.params.schema]
      : [];

  req.crud.relationships =
    typeof relationships[req.params.schema] !== "undefined"
      ? relationships[req.params.schema]
      : [];

  req.crud.searchFilters =
    typeof searchFilters[req.params.schema] !== "undefined"
      ? searchFilters[req.params.schema]
      : [];

  return crudAction
    .setPool(models.getPool())
    .setSchema(req.params.schema)
    .getAll(req, res, next);
});

app.get("/crud/:schema/lookup", [isAuthorized,allowCrud], function (req, res, next) {

  const { fields, joins, filters, relationships, searchFilters, access } = crudOptions;
  if (typeof access[req.params.schema] !== "undefined") {
    if (!userHasAccess(access[req.params.schema], req))
      throw new Error("access_denied");
  }

  req.crud.fields =
    typeof fields[req.params.schema] !== "undefined"
      ? fields[req.params.schema]
      : ["*"];
  req.crud.joins =
    typeof joins[req.params.schema] !== "undefined"
      ? joins[req.params.schema]
      : [];

  req.crud.filters =
    typeof filters[req.params.schema] !== "undefined"
      ? filters[req.params.schema]
      : [];

  req.crud.relationships =
    typeof relationships[req.params.schema] !== "undefined"
      ? relationships[req.params.schema]
      : [];

  req.crud.searchFilters =
    typeof searchFilters[req.params.schema] !== "undefined"
      ? searchFilters[req.params.schema]
      : [];


  return crudAction
    .setPool(models.getPool())
    .setSchema(req.params.schema)
    .lookup(req, res, next);
});

//retrieve by id
app.get("/crud/:schema/:id", [allowCrud, isAuthorized, isAdmin], function (
  req,
  res,
  next
) {
  const { joins, filters, relationships,access } = crudOptions;

  if (typeof access[req.params.schema] !== "undefined") {
    if (!userHasAccess(access[req.params.schema], req))
      throw new Error("access_denied");
  }

  req.crud.joins =
    typeof joins[req.params.schema] !== "undefined"
      ? joins[req.params.schema]
      : [];

  req.crud.filters =
    typeof filters[req.params.schema] !== "undefined"
      ? filters[req.params.schema]
      : [];

  req.crud.relationships =
    typeof relationships[req.params.schema] !== "undefined"
      ? relationships[req.params.schema]
      : [];

  return crudAction
    .setPool(req.pool)
    .setSchema(req.params.schema)
    .get(req, res, next);
});
//add
app.post("/crud/:schema", [allowCrud, isAuthorized], function (
  req,
  res,
  next
) {
  const { joins, filters, relationships,access } = crudOptions;

  if (typeof access[req.params.schema] !== "undefined") {
    if (!userHasAccess(access[req.params.schema], req))
      throw new Error("access_denied_role");
  }

  return crudAction
    .setPool(req.pool)
    .setSchema(req.params.schema)
    .add(req, res, next);
});
//update
app.post("/crud/:schema/:id", [allowCrud, isAuthorized], function (
  req,
  res,
  next
) {
  const { joins, filters, relationships,access } = crudOptions;

  if (typeof access[req.params.schema] !== "undefined") {
    if (!userHasAccess(access[req.params.schema], req))
      throw new Error("access_denied");
  }
  return crudAction
    .setPool(req.pool)
    .setSchema(req.params.schema)
    .update(req, res, next);
});

//delete
app.delete("/crud/:schema/:id", [allowCrud, isAuthorized, isAdmin], function (
  req,
  res,
  next
) {
  const { joins, filters, relationships,access } = crudOptions;

  if (typeof access[req.params.schema] !== "undefined") {
    if (!userHasAccess(access[req.params.schema], req))
      throw new Error("access_denied");
  }
  return crudAction
    .setPool(req.pool)
    .setSchema(req.params.schema)
    .delete(req, res, next);
});

//question
app.get("/question", [isAuthorized], models.question.getAllQuestions);
app.get("/questions", [isAuthorized], (req, res, next) => {
  models.question_group.getQuestions(models.question, req, res, next);
});
app.get("/question/:id", [isAuthorized], models.question.get);
app.post("/question", [isAuthorized], models.question.add);
app.post("/question/:id", [isAuthorized], models.question.update);
app.delete("/question/:id", [isAuthorized], models.question.delete);
app.post("/result", [isAuthorized], models.result.submit);
app.get("/result", [isAuthorized], models.result.retrieve);
app.post("/validate", [isAuthorized], models.result.validate);

//                                                                                                                                                             
app.get("/contents", [isAuthorized], models.content.getAll);
app.get("/contents/:id", [isAuthorized], models.content.get);
app.post("/contents", [isAuthorized], models.content.add);
app.post("/contents/:id", [isAuthorized], models.content.update);
app.delete("/contents/:id", [isAuthorized], models.content.delete);
app.get("/premium", [isAuthorized, isPremium], models.content.getPremium);
app.get(
  "/premium/latest",
  [isAuthorized, isPremium],
  models.content.getPremiumContent
);
app.get("/profile/:slug", [], models.content.getContent);
app.get(
  "/premium/:slug",
  [isAuthorized, isPremium],
  models.content.getPremiumBySlug
);
app.get("/account/lookup", [isAuthorized, isAdmin], models.account.lookup);
app.post("/account", [isAuthorized, isAdmin], models.account.addAccount);
app.post("/account/:id", [isAuthorized, isAdmin], models.account.updateAccount);
app.post("/payment/url", [], models.payment.get_url);
app.post("/payment/notify", [], models.payment.notify);
app.get("/payment/info", [], models.payment.info);
app.get("/payment/verify/:trx_id", [], models.payment.verify);

//question_group

app.get("/question-groups", [isAuthorized], models.question_group.getAll);
app.get(
  "/question-groups/lookup",
  [isAuthorized],
  models.question_group.lookup
);
app.get("/question-groups/:id", [isAuthorized], models.question_group.get);
app.post("/question-groups", [isAuthorized], models.question_group.add);
app.post("/question-groups/:id", [isAuthorized], models.question_group.update);
app.delete(
  "/question-groups/:id",
  [isAuthorized],
  models.question_group.delete
);

//payment confirmations backend
app.get("/confirmation", [isAuthorized], models.payment_confirmation.getAll);
app.get(
  "/confirmation/pending",
  [isAuthorized],
  models.payment_confirmation.getPendings
);
app.post(
  "/confirmation/pending",
  [isAuthorized],
  models.payment_confirmation.add
);
app.get(
  "/confirmation/pending/activate/:id",
  [isAuthorized, isAdmin],
  (req, res, next) => {
    activation.activate(models.getPool(), req, res, next);
  }
);

app.get(
  "/confirmation/pending/reactivate/:id",
  [isAuthorized, isAdmin],
  (req, res, next) => {
    activation.manual_activation(models.getPool(), req, res, next);
  }
);
app.get("/confirmation/:id", [isAuthorized], models.payment_confirmation.get);
app.post("/confirmation", [isAuthorized], models.payment_confirmation.add);
app.post(
  "/confirmation/:id",
  [isAuthorized],
  models.payment_confirmation.update
);
app.delete(
  "/confirmation/:id",
  [isAuthorized],
  models.payment_confirmation.delete
);

//pretests
app.get("/pretests", [isAuthorized], models.pretests.getData);
app.post("/pretests/submit", [isAuthorized], models.pretests.getContent);
app.get("/confirm", [isAuthorized], (req, res, next) => {
  res.render("index");
});

app.get("/affiliate/summary/:affiliate_id",
[isAuthorized], models.affiliates.summary);




app.get("/adcampaigns",[isAuthorized], models.affiliates.getCampaigns);

app.get("/vouchers",[isAuthorized],models.voucher_catalogs.getAll);
// Buy voucher
app.post("/voucher/buy",[isAuthorized], models.affiliates.buy_voucher)

// Landing 
app.get("/affiliate/landing/:link",[], models.affiliates.landing)

// is_premium 
app.get("/affiliates/is_premium",[isAuthorized], models.affiliates.is_premium)

// affiliate revenue 
app.get("/affiliates/revenue",[isAuthorized], models.affiliates.revenue)

// affiliate withdraw 
app.get("/affiliates/withdraw",[isAuthorized], models.affiliates.withdraw)
app.post("/affiliates/withdraw",[isAuthorized], models.affiliates.submit_withdraw)
app.post("/affiliates/withdraw/:affiliate_withdraw_id/approval/:withdraw_status",[isAuthorized, isAdmin], models.affiliates.update_status_withdraw)
app.post("/affiliates/update",[isAuthorized], models.affiliates.affiliate_update)
app.get("/affiliates",[isAuthorized], models.affiliates.affiliate_profile)
app.post("/affiliates/setting/update",[isAuthorized], models.affiliates.update)

// voucher stats
app.get("/vouchers/stats",[isAuthorized], models.affiliates.voucher_stats)


//misc
app.get("/", (req, res) => res.send({ status: 1 }));

app.use(errorHandler);

app.listen(port, () => console.log(`Ambu app listening on port ${port}!`));

function cleanup() {
  models.getPool().end();
  process.exit();
}

process.on("SIGINT", cleanup);
process.on("SIGTERM", cleanup);
