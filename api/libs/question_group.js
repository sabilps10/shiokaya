import Actions from "./actions";
import Engine from './engine';

class QuestionGroup extends Actions {
  constructor(props) {
    super({
      ...props,
      tableName: "question_groups"
    });
    this.engine = new Engine();
    this.getQuestions = this.getQuestions.bind(this);
    this.getQuestionGroups = this.getQuestionGroups.bind(this);
    this.mapQuestions = this.mapQuestions.bind(this);
    this.lookup = this.lookup.bind(this);
  }
  async lookup(req,res,next) {
    try{
      let data = await this.findAll({})
      return res.send({
        status:1,
        data
      })
    }catch(error){
      next(error)
    }
  }
  async getQuestionGroups(){
    let questions = await this.findAll({
      
    });
    return questions;
  }
  async mapQuestions(question, questions){
    let results = await questions.map(async q => {
        q.questions = await question.findAll({
          where: { group_id: q.id },
          start: 0,
          limit: 100
        });
        return q;
      });
      let data = await Promise.all(results);
      return data;
  }
  async getQuestions(model, req, res, next) {
    try {
      //retrieve all question groups
      let questions = await this.getQuestionGroups();
      let q = await this.mapQuestions(model, questions);

      //randomized the question
      let randomized = await q.map(async (qt)=>{
          let t = await this.engine.question(qt.questions);
          t.answer_type = qt.answer_type;
          let slots = [];
          if(t.slot_1 !== null) slots.push(t.slot_1);
          if(t.slot_2 !== null) slots.push(t.slot_2);
          if(t.slot_3 !== null) slots.push(t.slot_3);
          t.slots = slots;
          return t;
      })
      
      let data = await Promise.all(randomized);
      
      return res.send({
        status: 1,
        data
      });
    } catch (error) {
      next(error);
    }
  }
  
}

export default QuestionGroup;
