import Actions from "./actions";
import Account from "./account";
import AccountRole from "./account_role";

const axios = require("axios");
const config = require("config");
const moment = require("moment");
const sha1 = require("sha1");
axios.defaults.headers.post["Content-Type"] = "application/json";
const payment_endpoint = config.get("payment.endpoint");
const adserving_url = config.get("adserving_url");
const nodemailer = require("nodemailer");
const fs = require("fs");
const generate = require("../helpers/generate");
const is_premium = require("../helpers/is_premium");
const slugify = require('slugify');

let transport = nodemailer.createTransport({
  host: config.get("nodemailer.host"),
  port: config.get("nodemailer.port"),
  auth: {
    user: config.get("nodemailer.user"),
    pass: config.get("nodemailer.pass"),
  },
});

class Affiliates extends Actions {
  constructor(props) {
    super({
      ...props,
      tableName: "affiliates",
    });
    this.account = new Account(props);
    this.accountRole = new AccountRole(props);
    this.summary = this.summary.bind(this);
    this.register = this.register.bind(this);
    this.sendEmail = this.sendEmail.bind(this);
    this.getCampaigns = this.getCampaigns.bind(this);
    this.buy_voucher = this.buy_voucher.bind(this);
    this.landing = this.landing.bind(this);
    this.is_premium = this.is_premium.bind(this);
    this.revenue = this.revenue.bind(this);
    this.withdraw = this.withdraw.bind(this);
    this.submit_withdraw = this.submit_withdraw.bind(this);
    this.update_status_withdraw = this.update_status_withdraw.bind(this);
    this.affiliate_update = this.affiliate_update.bind(this);
    this.affiliate_profile = this.affiliate_profile.bind(this);
    this.update = this.update.bind(this);
    this.voucher_stats = this.voucher_stats.bind(this);
  }
  async update(req, res, next) {
    try {
      let account_id = req.meta.sub;
      const { name, deskripsi, photo_url, industry, photos } = req.body;
      let affiliate = await this.selectOne(
        `SELECT id FROM affiliates WHERE account_id=? LIMIT 1`,
        [account_id]
      );
      let params = {
        name, deskripsi, photo_url, industry,
      };
      if(photo_url){
        params.photo_url = photo_url;
      }
      params.link = affiliate.id +'-'+slugify(params.name);
      let rs = await this.save(affiliate.id, params,'updated_at');

      if (photos.length > 0) {
        
        let resetPhotos = await this.queryWithValues(
          `DELETE FROM affiliate_photos WHERE affiliate_id = ?`,
          [affiliate.id]
        );

        for (let i = 0; i < photos.length; i++) {
          console.log(`INSERT INTO affiliate_photos(affiliate_id,photo_url) VALUES(?,?);`,
          [affiliate.id])
          let photo = await this.queryWithValues(
            `INSERT INTO affiliate_photos(affiliate_id,photo_url) VALUES(?,?);`,
            [affiliate.id, photos[i]]
          );
        }
      }
      let status = 0;
      if(rs){
        status = 1;
      }

      return res.send({status});
      
    } catch (error) {
      console.log(error);
      return next(error);
    }
  }

  async summary(req, res, next) {
    try {
      let affiliate_id = parseInt(req.params.affiliate_id);
      let account_id = req.meta.sub;
      //make sure that the user is the same
      let user = await this.selectOne(
        `SELECT id,affiliate_status,name,deskripsi,photo_url,industry,link \FROM affiliates WHERE account_id=?`,
        [account_id]
      );
      if (user.id !== affiliate_id)
        throw new Error("you are not allowed to access these information");
      let summary = await this.selectOne(
        "SELECT * FROM affiliate_summary WHERE affiliate_id = ? ",
        [affiliate_id]
      );
        //fetch photos
      let photos = await this.select(`SELECT photo_url FROM affiliate_photos WHERE affiliate_id = ? LIMIT 10`,[
        affiliate_id
      ]);
      
      user.photos = photos.map((photo)=>{
        return photo.photo_url;
      })

      if (summary === null) {
        summary = {
          impressions: 0,
          clicks: 0,
          buys: 0,
        };
        let ins = await this.queryWithValues(
          `
        INSERT INTO affiliate_summary
        (affiliate_id,impressions,clicks,buys,created_at)
        VALUES(?,?,?,?,NOW())`,
          [affiliate_id, 0, 0, 0]
        );
      }
      /*
      select * from transactions a inner join payment_logs b on a.transaction_id = b.transaction_id where a.affiliate_id=8 AND transaction_status=1;
*/
      let sales = await this.selectOne(
        `SELECT COUNT(*) as total,SUM(transaction_amount) as gross from transactions WHERE affiliate_id = ? AND transaction_status = 1`,
        [affiliate_id]
      );

      let pendings = await this.selectOne(
        `SELECT COUNT(*) as total,SUM(transaction_amount) as gross from transactions WHERE affiliate_id = ? AND transaction_status = 0`,
        [affiliate_id]
      );
      
      let vouchers = await this.select(
        `SELECT a.created_at, a.id,a.code,a.expired_time,a.qty,a.discount,b.nama 
          FROM vouchers a INNER JOIN voucher_catalogs b ON a.voucher_id = b.id 
          WHERE a.issuer_id = ? AND voucher_status <> 0 ORDER BY a.id DESC LIMIT 100;`,
        [account_id]
      );

      for (let i = 0; i < vouchers.length; i++) {
        vouchers[
          i
        ].redeems = await this.selectOne(
          `SELECT COUNT(*) as total FROM voucher_redeems WHERE voucher_id = ?`,
          [vouchers[i].id]
        );
      }
      summary.sales = sales.total;
      summary.gross = sales.gross;
      summary.pendings = pendings.total;

      return res.send({
        status: 1,
        summary: summary,
        vouchers,
        affiliate_status: user.affiliate_status,
        affiliate: user,
      });
    } catch (error) {
      return next(error);
    }
  }
  async getCampaigns(req, res, next) {
    try {
      let affiliate_id = req.query.affiliate_id;
      let campaigns = await this.select(
        `SELECT * FROM adcampaigns ORDER BY id DESC LIMIT 1000`,
        []
      );
      let rs = campaigns.map(async (cmp) => {
        let banner = await this.select(
          `SELECT * FROM adcampaigns_assets WHERE campaign_id = ? LIMIT 100`,
          [cmp.id]
        );
        banner.map((b) => {
          b.script = `<a href="${adserving_url}/campaign/${cmp.campaign_id}/${b.id}/click?affiliate_id=${affiliate_id}">
          <img src="${adserving_url}/campaign/${cmp.campaign_id}/${b.id}/banner?affiliate_id=${affiliate_id}"/>
            </a>`;
        });
        cmp.assets = banner;
        return cmp;
      });
      let adcampaigns = await Promise.all(rs);
      return res.send({
        status: 1,
        adcampaigns,
      });
    } catch (error) {
      return next(error);
    }
  }
  async register(req, res, next) {
    try {
      let {
        name,
        email,
        password,
        phone,
        address,
        company,
        industry,
      } = req.body;

      let account = await this.selectOne(
        `SELECT id, COUNT(id) jml 
        FROM accounts 
        WHERE email like "%${email}%" 
        GROUP BY id;`
      )

      let accountId = account.id

      if (account.jml === 0) {
        //create account
        let account_id = await this.account.createAccount(
          {
            name,
            email,
            password,
            phone,
          },
          "affiliate"
        );

        accountId = account_id
      }else{
        let role = await this.selectOne(`SELECT id 
        FROM roles 
        WHERE name = ?`,
        ["affiliate"])

        await this.accountRole.insert({
          account_id : accountId,
          role_id : role.id
        })
      }

      let affiliate_no = `A-${accountId}-${moment().format("YYYYMMDD")}`;

      //insert affiliates profile
      let insertId = await this.insert(
        {
          name,
          email,
          phone,
          company,
          address,
          industry,
          affiliate_no,
          account_id: accountId,
        },
        [],
        "created_at"
      );

      if (insertId > 0) {
        //send Email
        let sent = await this.sendEmail({
          name,
          email,
          affiliate_no,
        });
        return res.send({ status: 1 });
      } else return res.send({ status: 0 });
    } catch (error) {
      return next("Affiliate:", error);
    }
  }

  async sendEmail(props) {
    const { name, email, affiliate_no } = props;

    let template = fs.readFileSync(
      "./templates/email/affiliate_register.html",
      "utf8"
    );

    template = template.replace(
      "{{date}}",
      moment().tz("UTC").format("DD MMMM, YYYY")
    );
    template = template.replace("{{name}}", name);
    template = template.replace("{{affiliate_no}}", affiliate_no);
    template = template.replace("{{email}}", email);

    let mailOptions = {
      from: config.get("email"),
      to: email,
      subject: "Welcome to Shiokaya Affiliation Programme",
      html: template,
    };

    let send = await transport.sendMail(mailOptions);
    return send;
  }

  async buy_voucher(req, res, next) {
    try {
      let { voucher_id, qty } = req.body;
      let kode_voucher = await generate.voucher_code();
      let expired_time = moment().add(1, "M").format("YYYY-MM-DD HH:mm:ss");
      let transaction_id = req.meta.sub + "-" + moment().unix();
      let price_id = 0;
      let transaction_type = "VOUCHER";
      let transaction = null;
      let ipaymu_transaction = null;
      let payload_ipaymu = null;

      // VOUCHER
      let voucher = await this.selectOne(
        `SELECT * FROM voucher_catalogs WHERE id=${voucher_id}`
      );

      for (let i = 0; i <= 10; i++) {
        if (i === 10) {
          next(new Error("maaf tidak dapat memproses permintaan anda!"));
          return;
        } else {
          let check_voucher = await this.selectOne(
            `SELECT count(id) total FROM vouchers WHERE code="${kode_voucher}"`
          );
          if (check_voucher.total > 0) {
            kode_voucher = await generate.voucher_code();
          } else {
            break;
          }
        }
      }

      let payload_voucher = [
        kode_voucher,
        expired_time,
        qty,
        moment().format("YYYY-MM-DD HH:mm:ss"),
        voucher.discount,
        req.meta.sub,
        0,
        voucher.id,
      ];

      if (!voucher.id || voucher.id === 0) {
        next(new Error("maaf tidak dapat memproses permintaan anda!"));
        return;
      }

      let insertVoucher = await this.queryWithValues(
        `INSERT INTO vouchers 
      (code,expired_time,qty,created_at,discount,issuer_id,voucher_status,voucher_id) 
      VALUES (?,?,?,?,?,?,?,?)`,
        payload_voucher
      );

      let id_voucher = insertVoucher[0].insertId;

      // TRANSACTION
      let affiliate = await this.selectOne(
        `SELECT * FROM affiliates WHERE account_id = ?`,
        [req.meta.sub]
      );
      let affiliate_id = affiliate.id;
      let transaction_amount = parseFloat(voucher.harga) * parseInt(qty);

      let payload_transaction = [
        transaction_id,
        req.meta.sub,
        0,
        moment().format("YYYY-MM-DD HH:mm:ss"),
        price_id,
        affiliate_id,
        id_voucher,
        transaction_amount,
        transaction_type,
      ];

      try {
        transaction = await this.queryWithValues(
          `INSERT INTO transactions 
          (transaction_id,account_id,transaction_status,created_at,price_id,affiliate_id,voucher_id,transaction_amount,transaction_type)
          VALUES (?,?,?,?,?,?,?,?,?)`,
          payload_transaction
        );
      } catch (error) {
        await this.queryWithValues(`DELETE FROM vouchers WHERE id = ?`, [
          id_voucher,
        ]);
        next(new Error("maaf tidak dapat memproses permintaan anda!"));
        return;
      }

      if (transaction[0].insertId === 0) {
        await this.queryWithValues(`DELETE FROM vouchers WHERE id = ?`, [
          id_voucher,
        ]);
        next(new Error("maaf tidak dapat memproses permintaan anda!"));
        return;
      } else {
        // GENERATE LINK IPAYMU
        payload_ipaymu = {
          transaction_id: transaction_id,
          product: "VOUCHER",
          quantity: 1,
          price: transaction_amount,
          comment: `Pembelian voucher ${voucher.nama}`,
        };
      }

      try {
        ipaymu_transaction = await axios.post(payment_endpoint, payload_ipaymu);
      } catch (error) {
        console.log(error);
        await this.queryWithValues(`DELETE FROM vouchers WHERE id = ?`, [
          id_voucher,
        ]);
        await this.queryWithValues(`DELETE FROM transactions WHERE id = ?`, [
          transaction[0].insertId,
        ]);
        next(new Error("maaf tidak dapat memproses permintaan anda!"));
        return;
      }

      let response = {
        status: 1,
        trx_id: transaction[0].insertId,
        price: transaction_amount,
        url: ipaymu_transaction.data.url,
        sessionId: ipaymu_transaction.data.sessionid,
      };

      res.send(response);
    } catch (error) {
      next(error);
    }
  }

  async landing(req, res, next) {
    try {
      let link = req.params.link;
      let content_1 = await this.selectOne(
        `SELECT * FROM blocks WHERE name = ?`,
        ["affiliate-landing-1"]
      );
      let content_2 = await this.selectOne(
        `SELECT * FROM blocks WHERE name = ?`,
        ["affiliate-landing-2"]
      );
      let content_3 = await this.selectOne(
        `SELECT * FROM blocks WHERE name = ?`,
        ["affiliate-landing-3"]
      );

      let affiliate = await this.selectOne(
        `SELECT * FROM affiliates WHERE link = ?`,
        [link]
      );
      let array = Object.keys(affiliate);
      affiliate = array.map((item) => {
        if (!affiliate[item]) {
          affiliate[item] = "";
        }
        return affiliate;
      });
      affiliate = affiliate[0];

      let affiliate_photos = await this.select(
        `SELECT * FROM affiliate_photos WHERE affiliate_id = ?`,
        [affiliate.id]
      );

      let response = {
        status: 1,
        content_1,
        content_2,
        content_3,
        affiliate,
        affiliate_photos,
      };

      res.send(response);
    } catch (error) {
      next(error);
    }
  }

  async is_premium(req, res, next) {
    try {
      let account_id = req.meta.sub;
      let status = 1;

      let premium = await is_premium.premium(this, account_id, false);

      if (premium === false) {
        status = 0;
      }

      let response = {
        status,
      };

      res.send(response);
    } catch (error) {
      next(error);
    }
  }

  async revenue(req, res, next) {
    try {
      let account_id = req.meta.sub;

      let { affiliate_id } = await this.selectOne(
        `SELECT id affiliate_id 
      FROM affiliates 
      WHERE account_id = ? `,
        [account_id]
      );

      let revenue = await this.select(
        `SELECT * 
      FROM affiliate_revenues 
      WHERE affiliate_id = ?`,
        [affiliate_id]
      );

      let response = {
        status: 1,
        revenue,
      };

      res.send(response);
    } catch (error) {
      next(error);
    }
  }

  async withdraw(req, res, next) {
    try {
      let account_id = req.meta.sub;

      let { affiliate_id } = await this.selectOne(
        `SELECT id affiliate_id 
      FROM affiliates 
      WHERE account_id = ? `,
        [account_id]
      );

      let withdraw = await this.select(
        `SELECT * 
      FROM affiliate_withdrawals 
      WHERE affiliate_id = ?`,
        [affiliate_id]
      );

      let response = {
        status: 1,
        withdraw,
      };

      res.send(response);
    } catch (error) {
      next(error);
    }
  }

  async submit_withdraw(req, res, next) {
    try {
      let account_id = req.meta.sub;
      let { amount } = req.body;
      let currdate = moment().format("YYYY-MM-DD HH:mm:ss");

      let { affiliate_id } = await this.selectOne(
        `SELECT id affiliate_id 
      FROM affiliates 
      WHERE account_id = ? `,
        [account_id]
      );

      let { revenue } = await this.selectOne(
        `SELECT SUM(amount) revenue
      FROM affiliate_revenues 
      WHERE affiliate_id = ?`,
        [affiliate_id]
      );

      if (parseFloat(amount) > parseFloat(revenue)) {
        await this.queryWithValues(
          `INSERT INTO affiliate_withdrawals 
        (affiliate_id, amount, withdraw_status, created_at) 
        VALUES (?,?,?,?)`,
          [affiliate_id, parseFloat(amount), 2, currdate]
        );
      } else {
        await this.queryWithValues(
          `INSERT INTO affiliate_withdrawals 
        (affiliate_id, amount, withdraw_status, created_at) 
        VALUES (?,?,?,?)`,
          [affiliate_id, parseFloat(amount), 0, currdate]
        );
      }

      let response = {
        status: 1,
      };

      res.send(response);
    } catch (error) {
      next(error);
    }
  }

  async update_status_withdraw(req, res, next) {
    try {
      let { affiliate_withdraw_id, withdraw_status } = req.params;
      let currdate = moment().format("YYYY-MM-DD HH:mm:ss");

      await this.queryWithValues(
        `UPDATE affiliate_withdrawals 
      SET withdraw_status = ?, updated_at = ? WHERE id = ? `,
        [withdraw_status, currdate, affiliate_withdraw_id]
      );

      let response = {
        status: 1,
      };

      res.send(response);
    } catch (error) {
      next(error);
    }
  }

  async affiliate_update(req, res, next) {
    try {
      let account_id = req.meta.sub;
      let currdate = moment().format("YYYY-MM-DD HH:mm:ss");
      req.body.updated_at = currdate;

      let affiliate = await this.selectOne(
        `SELECT id FROM affiliates WHERE account_id = ? `,
        [account_id]
      );

      if (affiliate) {
        let key = Object.keys(req.body)
          .map((item) => `${item} = ?`)
          .toString();
        let val = Object.values(req.body);
        val.push(affiliate.id);

        await this.queryWithValues(
          `UPDATE affiliates SET ${key} WHERE id = ?`,
          val
        );
      }

      let response = {
        status: 1,
      };

      res.send(response);
    } catch (error) {
      next(error);
    }
  }

  async affiliate_profile(req, res, next) {
    try {
      let account_id = req.meta.sub;

      let affiliate = await this.selectOne(
        `SELECT * 
      FROM affiliates 
      WHERE account_id = ? `,
        [account_id]
      );

      if (!affiliate) {
        next(new Error(`Maaf, anda belum terdaftar sebagai afiliasi`));
        return;
      }

      let response = {
        status: 1,
        affiliate,
      };

      res.send(response);
    } catch (error) {
      next(error);
    }
  }

  async voucher_stats(req, res, next) {
    try {
      
      let voucher = await this.select(
        `SELECT id, voucher_id, code, qty 
        FROM vouchers`
      );

      for (const item of voucher) {
        let redeem = await this.selectOne(`SELECT COUNT(voucher_id) jumlah_redeem
        FROM voucher_redeems WHERE voucher_id = ?`,[item.id])
        redeem.jumlah_redeem = redeem.jumlah_redeem ? parseInt(redeem.jumlah_redeem) : 0
        item.jumlah_redeem = redeem.jumlah_redeem
      }
      
      let response = {
        status: 1,
        voucher
      };

      res.send(response);
    } catch (error) {
      next(error);
    }
  }
}

export default Affiliates;
