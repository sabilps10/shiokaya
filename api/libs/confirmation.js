const config = require("config");
const moment = require("moment-timezone");
const sha1 = require("sha1");
const mysql = require("mysql2/promise");
const nodemailer = require("nodemailer");
const fs = require("fs");

let transport = nodemailer.createTransport({
  host: config.get("nodemailer.host"),
  port: config.get("nodemailer.port"),
  auth: {
    user: config.get("nodemailer.user"),
    pass: config.get("nodemailer.pass")
  }
});

module.exports = {
  async send(pool, req, res, next) {
      
    try {
      let session_id = typeof req.meta !== 'undefined' ? req.meta.session_id : null;
      let email = req.body.email;
      let nama_rekening = req.body.nama_rekening;
      let nomor_rekening = req.body.nomor_rekening;
      let tanggal_transfer = req.body.tanggal_transfer;
      let jumlah = req.body.jumlah;
      let nama_bank = req.body.nama_bank;
      let nomor_telp = req.body.nomor_telp;
      let cabang_bank = req.body.cabang_bank;


      let account = await pool.query("SELECT session_id FROM accounts where email = ? LIMIT 1",[email]);

      if(session_id === null) session_id = account[0][0].session_id;
      
      let insertAccount = await pool.query(
        `insert into payment_confirmations \ 
                                    (nomor_telp, nama_bank,cabang_bank,email,nama_rekening,nomor_rekening,tanggal_transfer,jumlah,created_at,confirm_status,session_id) \
                                    values(?,?,?,?,?,?,?,?,NOW(),0,?)`,
        [nomor_telp,nama_bank,cabang_bank,email, nama_rekening, nomor_rekening, tanggal_transfer, jumlah,session_id]
      );

      if (insertAccount[0].insertId) {
        if(parseInt(jumlah) > 50000){
          let template = fs.readFileSync(
            "./templates/email/confirmation_recieved.html",
            "utf8"
          );
          template = template.replace(
            "{{date}}",
            moment()
              .tz("UTC")
              .format("DD MMMM, YYYY")
          );
          template = template.replace("{{name}}", nama_rekening);
          template = template.replace("{{nomor_rekening}}", nomor_rekening);
          template = template.replace("{{email}}", email);
          template = template.replace("{{nama_bank}}", nama_bank);
          template = template.replace("{{tanggal_transfer}}", tanggal_transfer);
          template = template.replace("{{jumlah}}", "Rp. " + jumlah.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'));
          template = template.replace("{{cabang_bank}}", cabang_bank);
  
          let mailOptions = {
            from: config.get("email"),
            to: email,
            subject: "Terima kasih atas Konfirmasi Pembayaran Anda!",
            html: template
          };
    
          let send = await transport.sendMail(mailOptions);
          
        }
        res.send({ status: 1 });
      } else {
        res.send({ status: 0 });
      }
    } catch (error) {
      next(error);
    }
  }
};
