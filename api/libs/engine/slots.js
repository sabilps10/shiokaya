const slots = {
    C1: [0, 3, 6],
    C2: [1, 7],
    C3: [2, 5, 8],
    R1: [0, 1, 2],
    R2: [3, 5],
    R3: [6, 7, 8],
    An: [3],
    A: [5],
    V: [1],
    K: [7]
  };
  
export default slots;