import mapScore from "./mapScore";

const handleAnA = (question, answer) => {
    const { slots } = question;
    const [left, right] = slots;
    let [leftScore, rightScore] = [0, 0, 0];
    switch (answer) {
      case 1:
        leftScore = 4;
  
        leftScore = 0;
        break;
      case 2:
        leftScore = 2;
  
        leftScore = 0;
  
        break;
      case 3:
        leftScore = 1;
        leftScore = 1;
        break;
      case 4:
        leftScore = 0;
        leftScore = 2;
        break;
      case 5:
        leftScore = 0;
        leftScore = 4;
        break;
      case 6:
        leftScore = 8;
        break;
      default:
        leftScore = 8;
        break;
    }
    let results = [0, 0, 0, 0, 0, 0, 0, 0, 0];
    results = mapScore(results, left, leftScore);
    results = mapScore(results, right, rightScore);
    return results;
  };
  export default handleAnA;