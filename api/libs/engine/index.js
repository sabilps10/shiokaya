/**
 * engine/index.js
 * Usage :
 * const engine = new Engine();
 * //randomized questions
 * engine.shuffle(question_groups);
 * //calculate results
 * engine.results( {questions, answers} )
 */
import handleLeftRightScenario from "./leftright";
import handleTopDownScenario from "./topdown";
import handleAnA from "./ana";
import handleVK from "./vk";

const _ = require("lodash");

export default class Engine {
  question(data) {
    let q = _.shuffle(data)[0];
    return q;
  }

  shuffle(question_groups) {
    let shuffled = [];
    question_groups.map(item => {
      let q = this.question(item.questions);
      shuffled.push(q);
    });
    return shuffled;
  }

  results({ questions, answers }) {
    console.log(questions, answers);
    let _results = [0, 0, 0, 0, 0, 0, 0, 0, 0];
    console.log('->',_results);
    for(let index in questions){
      let q = questions[index];
      
   // questions.map((q, index) => {
      let answer = answers[index];
      let r = [];
      try {
        if (q.answer_type === "left-right") {
           r = handleLeftRightScenario(q, answer);
        }
        if (q.answer_type === "top-down") {
           r = handleTopDownScenario(q, answer);
        }
        if (q.answer_type === "ana") {
           r = handleAnA(q, answer);
        }
        if (q.answer_type === "vk") {
           r = handleVK(q, answer);
        }
        console.log('each score:', r);
        if(r){
          console.log('results now: ',_results);
          for (let i = 0; i < _results.length; i++) {
            if(typeof r[i] !== 'undefined') {
              console.log(_results[i], r[i],typeof r[i]);
            _results[i] += r[i];
            }
            
          }
        }
        
      } catch (e) {
        console.log('error',answer, e.message);
      }
   // });
    }
    return _results;
  }
}
