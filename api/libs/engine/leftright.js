import mapScore from "./mapScore";

const handleLeftRightScenario = (question, answer) => {
    const { slots } = question;
    const [left, middle, right] = slots;
    let [leftScore, middleScore, rightScore] = [0, 0, 0];
    switch (answer) {
      case 1:
        leftScore = 4;
        middleScore = 2;
        rightScore = 0;
        break;
      case 2:
        leftScore = 2;
        middleScore = 4;
        rightScore = 0;
        break;
      case 3:
        leftScore = 0;
        middleScore = 8;
        rightScore = 0;
        break;
      case 4:
        leftScore = 0;
        middleScore = 4;
        rightScore = 2;
        break;
      case 5:
        leftScore = 0;
        middleScore = 2;
        rightScore = 4;
        break;
      case 6:
        rightScore = 8;
        break;
      default:
        leftScore = 8;
        break;
    }
    let results = [0, 0, 0, 0, 0, 0, 0, 0, 0];
    results = mapScore(results, left, leftScore);
    results = mapScore(results, middle, middleScore);
    results = mapScore(results, right, rightScore);
    return results;
  };

  export default handleLeftRightScenario;