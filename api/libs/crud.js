import Actions from "./actions";

class CrudActions extends Actions {
  constructor(props) {
    super({
      ...props,
      tableName: "crud"
    });
    this.setPool = this.setPool.bind(this);
    this.setSchema = this.setSchema.bind(this);
    this.lookup = this.lookup.bind(this);
  }
  setPool(pool) {
    this.props.pool = pool;
    return this; //so we can chained it.
  }
  setSchema(name) {
    this.props.tableName = name;
    return this; //so we can chained it.
  }
  async lookup(req, res, next) {
   
    const { search } = typeof req.query !== 'undefined' ? req.query : {
      search:''
    }
    let searchConditions = typeof search !== 'undefined' ? {...this.formatLookupFilters(search, req)} : null
    
    try {
      let data = await this.findAll({
        select: req.crud.fields,
        limit: 100,
        searchConditions: searchConditions
      });
      return res.send({ status: 1, data });
    } catch (error) {
      next(error);
    }
  }
}

export default CrudActions;
