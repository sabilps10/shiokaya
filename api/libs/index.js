const mysql2 = require("mysql2/promise");
const config = require("config");
import Question from "./question";
import QuestionGroup from "./question_group";
import Result from "./result";
import Content from "./content";
import Confirmation from "./confirmation";
import PaymentConfirmation from "./payment_confirmation";
import Pretests from "./pretests";
import Payment from "./payment";
import Affiliates from "./affiliates";
import Dashboard from "./dashboard";
import Account from "./account";
import WebPage from "./webpage";
import VoucherCatalogs from "./voucher_catalogs";
let pool = mysql2.createPool({
  connectionLimit: 10,
  host: config.get("mysql.host"),
  user: config.get("mysql.username"),
  password: config.get("mysql.password"),
  database: config.get("mysql.database"),
  timezone: config.get("mysql.timezone")
});

const models = {
  getPool: () => pool,
  question: new Question({ pool }),
  question_group: new QuestionGroup({ pool }),
  result: new Result({ pool }),
  content: new Content({ pool }),
  payment_confirmation: new PaymentConfirmation({ pool }),
  pretests: new Pretests({pool}),
  payment: new Payment({pool}),
  affiliates: new Affiliates({pool}),
  dashboard: new Dashboard({pool}),
  account: new Account({pool}),
  webpage: new WebPage({pool}),
  voucher_catalogs: new VoucherCatalogs({pool}),
};

export default models;
