import Actions from "./actions";

class PaymentConfirmation extends Actions {
  constructor(props) {
    super({
      ...props,
      tableName: "payment_confirmations"
    });
    this.getPendings = this.getPendings.bind(this);
  }
  async getPendings(req,res,next){
    let {page, per_page, search} = req.query;
    page = parseInt(page)
    try{
      let result = await this.paginate({
        page, limit: per_page, searchConditions:{
          email:search,
          nama_rekening:search,
          nomor_rekening:search,
        }, 
        
      })
      let r = result.rows.map(async (row,index)=>{
          let account = await this.select("SELECT id,name,email,phone FROM accounts WHERE session_id = ? AND email is not null LIMIT 1", [row.session_id])
          row.account = account.length > 0 ? account[0] : null;
          console.log(row.session_id,row.account);
          if(row.account === null){
            row.profil = null;
            row.results = null;
            return row;
          }
          let profil = await this.select("SELECT * FROM results WHERE account_id=?",[row.account.id])
          row.results = profil.length > 0 ? profil[0] : null;
          return row;
      })
      result.rows = await Promise.all(r);
      return res.send({
        status: 1,
        data: result.rows,
        page: page,
        total: result.totalResults
      });
    }catch(error){
      next(error)
    }
  }
}


export default PaymentConfirmation;
