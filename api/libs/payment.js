import Actions from "./actions";
const axios = require("axios");
const config = require("config");
const moment = require("moment");
const sha1 = require("sha1");
axios.defaults.headers.post["Content-Type"] = "application/json";
const payment_endpoint = config.get("payment.endpoint");
const nodemailer = require("nodemailer");
const fs = require("fs");

let transport = nodemailer.createTransport({
  host: config.get("nodemailer.host"),
  port: config.get("nodemailer.port"),
  auth: {
    user: config.get("nodemailer.user"),
    pass: config.get("nodemailer.pass"),
  },
});

class Payment extends Actions {
  constructor(props) {
    super({
      ...props,
      tableName: "transactions",
    });
    this.get_url = this.get_url.bind(this);
    this.notify = this.notify.bind(this);
    this.activate = this.activate.bind(this);
    this.info = this.info.bind(this);
    this.verify = this.verify.bind(this);
  }
  async info(req, res, next) {
    try {
      let trx_id = req.query.trx_id;
      let pgw = await this.selectOne(
        `SELECT transaction_id,channel,status_code,via,total,va,uniqamount FROM payment_logs WHERE trx_id=? ORDER BY id DESC`,
        [trx_id]
      );
      let transaction = await this.selectOne(
        `SELECT transaction_id,transaction_status,transaction_amount as amount FROM transactions where transaction_id=?;`,
        [pgw.transaction_id]
      );
      return res.send({
        status: 1,
        pgw,
        transaction,
      });
    } catch (error) {
      return next(error);
    }
  }
  async verify(req, res, next) {
    try {
      let trx_id = req.params.trx_id;
      let pgw = await this.selectOne(
        `SELECT transaction_id,channel,status_code,via,total,va,uniqamount FROM payment_logs WHERE trx_id=? ORDER BY id DESC`,
        [trx_id]
      );
      let transaction = await this.selectOne(
        `SELECT transaction_id,transaction_status,transaction_amount as amount,transaction_type FROM transactions where transaction_id=?;`,
        [pgw.transaction_id]
      );
      return res.send({
        status: 1,
        pgw,
        transaction,
        type: transaction.transaction_type,
      });
    } catch (error) {
      return next(error);
    }
  }
  async get_url(req, res, next) {
    try {
      let session_id = req.body.session_id;
      let affiliate_id = req.body.affiliate_id;
      let promo = req.body.promo;
      let account = await this.select(
        "SELECT * FROM accounts where session_id=? LIMIT 1",
        [session_id]
      );
      let account_id = account[0].id;
      let transaction_id = account_id + "-" + moment().unix();
      let voucher = null;
      let price = await this.selectOne(
        "SELECT * FROM price ORDER BY id DESC LIMIT 1"
      );
      if (promo !== null && promo.length > 0) {
        voucher = await this.selectOne(
          `SELECT * FROM vouchers WHERE code=? LIMIT 1`,
          [promo]
        );
        if (voucher !== null) {
          let usage = await this.selectOne(
            `SELECT COUNT(id) as total FROM voucher_redeems WHERE voucher_id = ?`,
            [voucher.id]
          );
          if (usage.total >= voucher.qty) {
            voucher = null;
          } else {
            let redeem = await this.queryWithValues(
              `INSERT INTO voucher_redeems(voucher_id,account_id,created_at) VALUES(?,?,NOW());`,
              [voucher.id, account_id]
            );
          }
        }
      }
      let base_price = price !== null ? price.price : 200000;
      let transaction_amount =
        voucher !== null
          ? base_price - voucher.discount * base_price
          : base_price;

      let results = await this.selectOne(
        `SELECT COUNT(id) jml 
        FROM results 
        WHERE account_id = ?`,
        [account_id]
      )
      results = results.jml

      transaction_amount = results > 0 ? transaction_amount * 0.5 : transaction_amount

      let payload = {
        transaction_id,
        product: "Shiokaya Premium",
        quantity: 1,
        price: transaction_amount,
        comment: "one-time subscription",
      };
      console.log("payment-payload:", payload);

      let insertId = await this.insert(
        {
          account_id,
          transaction_id,
          transaction_status: transaction_amount > 0 ? 0 : 1,
          transaction_amount,
          voucher_id: voucher !== null ? voucher.id : 0,
          affiliate_id: affiliate_id !== null ? affiliate_id : 0,
          price_id: price !== null ? price.id : 0,
        },
        [],
        "created_at"
      );

      if (insertId > 0) {
       

        if (transaction_amount === 0) {
          //jika gratis langsung aktifkan
          let activate = await this.activate(account_id);
          if (activate) {
            console.log(
              moment().format("YYYY-MM-DD HH:mm:ss"),
              " - ",
              transaction_id,
              " - ACTIVATED"
            );
          }
          return res.send({
            status: 1,
            trx_id: insertId,
            price: transaction_amount,
           
          });
        }else{
          let response = await axios.post(payment_endpoint, {
            ...payload,
          });
          return res.send({
            status: 1,
            trx_id: insertId,
            price: transaction_amount,
            ...response.data,
          });
        }
       
      } else {
        return res.send({
          status: 0,
          message:
            "Maaf, Kami gagal menyiapkan transaksi untuk anda. Silahkan coba kembali !",
        });
      }
    } catch (error) {
      return next(error);
    }
  }
  async notify(req, res, next) {
    try {
      console.log("notify:", req.body);
      const { transaction_id } = req.body;

      console.log("CHECK logs : ", transaction_id);

      let transaction = await this.select(
        "SELECT status_code FROM payment_logs WHERE transaction_id = ? LIMIT 1",
        [transaction_id]
      );
      console.log(transaction_id, transaction);
      if (parseInt(transaction[0].status_code) === 1) {
        console.log(transaction_id);
        console.log(
          moment().format("YYYY-MM-DD HH:mm:ss"),
          " - ",
          transaction_id,
          " - COMPLETED"
        );
        let trans = await this.select(
          "SELECT * FROM transactions where transaction_id = ? ",
          [transaction_id]
        );
        const { id, account_id } = trans[0];

        if (trans[0].transaction_type === "VOUCHER") {
          let check_voucher = await this.selectOne(
            `SELECT COUNT(id) total FROM vouchers WHERE id = ? AND issuer_id = ?`,
            [trans[0].voucher_id, trans[0].account_id]
          );

          if (check_voucher.total === 1) {
            await this.queryWithValues(
              `UPDATE vouchers SET updated_at = ?, voucher_status = ? WHERE id = ?`,
              [moment().format("YYYY-MM-DD HH:mm:ss"), 1, trans[0].voucher_id]
            );

            await this.queryWithValues(
              "UPDATE transactions SET transaction_status = 1, updated_at = ? WHERE id = ?",
              [moment().format("YYYY-MM-DD HH:mm:ss"), id]
            );
          }

          let user = await this.selectOne(
            `SELECT * FROM accounts WHERE id = ? `,
            [trans[0].account_id]
          );

          let template = fs.readFileSync(
            "./templates/email/buy_voucher.html",
            "utf8"
          );
          template = template.replace("{{name}}", `${user.name}`);

          let mailOptions = {
            from: config.get("email"),
            to: user.email,
            subject: "Pembelian Voucher",
            html: template,
          };

          await transport.sendMail(mailOptions);
        } else {
          let update = await this.queryWithValues(
            "UPDATE transactions SET transaction_status = 1, updated_at = NOW() WHERE id = ?",
            [id]
          );
          let activate = await this.activate(account_id);
          if (activate) {
            console.log(
              moment().format("YYYY-MM-DD HH:mm:ss"),
              " - ",
              transaction_id,
              " - ACTIVATED"
            );
          }
        }
      } else {
        console.log(
          moment().format("YYYY-MM-DD HH:mm:ss"),
          " - ",
          transaction_id,
          " - FAILED -" + transaction[0].status_code
        );
      }
      return res.send({
        status: 1,
      });
    } catch (error) {
      console.log(error);
      next(error);
    }
  }
  async activate(account_id) {
    let plain_pass = Math.ceil(Math.random() * 9999999);
    let secret = sha1(moment().unix());
    let password = sha1(plain_pass + secret);
    let updateAccount = await this.queryWithValues(
      "UPDATE accounts SET password = ?,secret=? WHERE id=?",
      [password, secret, account_id]
    );
    let account = await this.select(
      "SELECT * FROM accounts WHERE id = ? LIMIT 1",
      [account_id]
    );
    const { name, email } = account[0];

    const PREMIUM = 3;
    let role = await this.queryWithValues(
      "INSERT INTO account_roles(account_id,role_id) VALUES(?,?) ",
      [account_id, PREMIUM]
    );
    let template = fs.readFileSync("./templates/email/activated.html", "utf8");
    template = template.replace(
      "{{date}}",
      moment().tz("UTC").format("DD MMMM, YYYY")
    );
    template = template.replace("{{name}}", name);
    template = template.replace("{{token}}", plain_pass);
    template = template.replace("{{email}}", email);

    let mailOptions = {
      from: config.get("email"),
      to: email,
      subject: "Selamat!, Akun Premium anda sudah aktif !",
      html: template,
    };
    //console.log("SEND EMAIL", mailOptions);
    let send = await transport.sendMail(mailOptions);
    return send;
  }
}

export default Payment;
