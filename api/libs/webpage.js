import Actions from "./actions";
import Account from "./account";

const axios = require("axios");
const config = require("config");
const moment = require("moment");
const sha1 = require("sha1");
axios.defaults.headers.post["Content-Type"] = "application/json";
const payment_endpoint = config.get("payment.endpoint");

const nodemailer = require("nodemailer");
const fs = require("fs");

let transport = nodemailer.createTransport({
  host: config.get("nodemailer.host"),
  port: config.get("nodemailer.port"),
  auth: {
    user: config.get("nodemailer.user"),
    pass: config.get("nodemailer.pass"),
  },
});

class WebPage extends Actions {
  constructor(props) {
    super({
      ...props,
      tableName: "affiliates",
    });
  
    this.home = this.home.bind(this);
  }
  async home(req,res,next){
      try{
        let rs = await this.select("SELECT * FROM blocks");
        let blocks = {}
        rs.map((block)=>{
            blocks[block.name] = block;
        })
        return res.send({
            status:1,
            blocks
        })
      }catch(error){
          return next(error);
      }
  }
  
}

export default WebPage;
