const _ = require('lodash');

module.exports = {
    async voucher_code() {
        try {
            let result = ''
            let characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
            let charactersLength = characters.length
            for (let i = 0; i < 8; i++) {
                result += characters.charAt(Math.floor(Math.random() * charactersLength))
            }
            result = _.shuffle(result).toString().replace(/,/g,"")
            return result
        } catch (error) {
            console.log(error);
            console.log("gagal generate kode voucher")
        }
    }
}