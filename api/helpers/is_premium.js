module.exports = {
    async premium(pool,account_id, worker) {
        try {
            let premium = false
            let prop = worker === false ? "selectOne" : "query"

            let results = await pool[prop](`SELECT count(id) jml_result 
            FROM results 
            WHERE account_id = ?`,
            [account_id])

            let transaction = await pool[prop](`SELECT count(id) jml_trx 
            FROM transactions 
            WHERE account_id = ?
            AND transaction_status = ? 
            AND transaction_type = ?`,
            [account_id, 1, 'SUBS'])

            if (worker === false) {
                if (results.jml_result > 0 && transaction.jml_trx > 0) {
                    premium = true
                }
            }else{
                if (results[0][0].jml_result > 0 && transaction[0][0].jml_trx > 0) {
                    premium = true
                }
            }
            
            return premium
        } catch (error) {
            console.log(error);
        }
    }
}