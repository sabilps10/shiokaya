const { log } = require("async");
const config = require("config");
const moment = require("moment-timezone");
const mysql2 = require("mysql2/promise");

let pool = mysql2.createPool({
  connectionLimit: 10,
  host: config.get(`mysql.host`),
  port: config.get(`mysql.port`),
  user: config.get(`mysql.username`),
  password: config.get(`mysql.password`),
  database: config.get(`mysql.database`),
  timezone: config.get(`mysql.timezone`),
});

let cleanup = () => {
  pool.end();
  process.exit();
};

async function run() {
  try {
    // check jam server
    let jakartaTime = moment().tz("Asia/Jakarta").format("DD/MM/YYYY hh:mm:ss");
    let londonTime = moment().tz("Europe/London").format("DD/MM/YYYY hh:mm:ss");
    let currdate = moment().tz("Asia/Jakarta").format("YYYY-MM-DD hh:mm:ss");

    console.log(`#Timezone - ${moment.tz.guess()}`);
    console.log(`#Jakarta Time - ${jakartaTime}`);
    console.log(`#London Time - ${londonTime}`);
    console.log(`#Server Time - ${moment().format("DD/MM/YYYY hh:mm:ss")}`);
    console.log(`----------------------------------------------------------`);

    console.log(`Affiliate Revenue Started`);
    console.log(`#START - ${jakartaTime}`);
    console.log(`----------------------------------------------------------`);

    await pool.query(`INSERT IGNORE INTO affiliate_revenues (affiliate_id, transaction_id, 
        amount, created_at, updated_at) 
        SELECT  a.affiliate_id, a.transaction_id, 
        (a.transaction_amount * b.commisions) amount,
        a.created_at,a.updated_at
        FROM transactions a 
        INNER JOIN affiliates b 
        ON a.affiliate_id = b.id AND b.affiliate_status = 1        
        WHERE a.transaction_status = 1 AND a.transaction_type='SUBS'`);

    console.log(`----------------------------------------------------------`);
    console.log(`Affiliate Revenue Ended`);
    console.log(`#END - ${jakartaTime}`);
    console.log(`----------------------------------------------------------`);

    cleanup();
  } catch (error) {
    console.log(error);
    cleanup();
  }
}

run();
process.on("SIGINT", cleanup);
process.on("SIGTERM", cleanup);
