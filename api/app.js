var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const config = require("config");
const jwt = require('jsonwebtoken');
const bodyParser = require("body-parser");
var indexRouter = require('../routes/index');
var usersRouter = require('../routes/users');


var app = express();


app.use((req,res,next)=>{
  req.poolPromised = app.get('poolPromised')
  next();
})

app.use(
  bodyParser.urlencoded({
    extended: true
  })
);
app.use(bodyParser.json());

app.use(function (req, res, next) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, OPTIONS, PUT, PATCH, DELETE"
  );
  res.setHeader(
    "Access-Control-Allow-Headers",
    "X-Requested-With,content-type,Authorization"
  );
  res.setHeader("Access-Control-Allow-Credentials", false);

  next();
});

function isAuthorized(req, res, next) {
  if (typeof req.headers.authorization === "undefined") {
    console.log("need login");
    throw new Error("need_login");
  }

  let chunks = req.headers.authorization.split(" ");
  let token = chunks[1];
  var payload = jwt.verify(token, config.get("JWT.secret"));
  req.meta = {
    user_id: payload.sub,
    companies: payload.companies,
    managed_groups: payload.managed_groups,
    managed_companies: payload.managed_companies,
    groups: payload.groups
  };

  next();
}

function errorHandler(err, req, res, next) {
  console.log("error", err.message);
  if (res.headersSent) {
    return next(err);
  }

  if (err.message === "need_login") {
    res.status(401)
    return res.send({ status: 401, message: "Please login" });
  }

  if (err.message === "jwt expired") {
    res.status(403);
    return res.send({ status: 403, message: "Session Expired, Please Login" });
  }

  res.status(400);
  res.send({
    status: 0,
    error: err.message
  });
}
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});
/*
// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});
*/
app.use(errorHandler);



module.exports = app;
