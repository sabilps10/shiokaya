import Api from "../Services/Api";


const api = new Api();

export default (data) => dispatch => {
    
    dispatch({
        type: 'ACCOUNT',
        data,
        payload:null
    })
    
    api.accounts(data).then((response)=>{
        
        if(typeof response.data !== 'undefined'){
            dispatch({
                type:"ACCOUNT_SUCCESS",
                payload: response
            })
        }else{
            dispatch({
                type:"ACCOUNT_ERROR",
                payload:null,
            })
        }
    })
}