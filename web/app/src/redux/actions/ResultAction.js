import Api from "../../Services/Api"; 

const api = new Api();

export default (data) => dispatch => {
    
    dispatch({
        type: 'RESULT',
        data
    })
    
    api.result(data).then((response)=>{
        
        if(typeof response.status !== 'undefined'){
            dispatch({
                type:"RESULT_SUCCESS",
                payload: response
            })
        }else{
            dispatch({
                type:"RESULT_ERROR",
                message: response.error
            })
        }
        
    })
  
}