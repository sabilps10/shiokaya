import Api from "../../Services/Api"; 

const api = new Api();

export default (data) => dispatch => {
    
    dispatch({
        type: 'GUEST',
        data
    })
    
    api.loginAsGuest(data).then((response)=>{
        
        if(typeof response.access_token !== 'undefined'){
            dispatch({
                type:"GUEST_SUCCESS",
                payload: response
            })
        }else{
            dispatch({
                type:"GUEST_ERROR",
                message: response.error
            })
        }
        
    })
  
}