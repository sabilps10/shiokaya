
let INITIAL_STATE = {
    data:null,
    payload:null,
    fetching:false,
    error:false,
    message:"",
}
export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case "CONFIRM":
      return {
        data: action.data,
        fetching:true,
        message:"",
    };
    case "CONFIRM_SUCCESS":
      return {
        payload: action.payload,
        fetching:false
    };
    case "CONFIRM_ERROR":
      return {
        data:null,
        error: true,
        fetching:false,
        payload:null,
    };
    default:
      return state;
  }
};
