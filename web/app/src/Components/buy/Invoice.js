import React, { useState } from "react";
import { Grid, CircularProgress, Button, TextField } from "@material-ui/core";
import {
  Radar,
  RadarChart,
  PolarAngleAxis,
  PolarGrid,
  PolarRadiusAxis,
  Legend
} from "recharts";

export default function Invoice(props) {
  const { data, confirm_url,toConfirm } = props;

  return (
    <div style={{ marginTop: 100 }}>
      <Grid
        container
        style={{
          maxWidth: 800,
          marginTop: 60,
          marginLeft: "auto",
          marginRight: "auto"
        }}
      >
        <Grid item xs={12} md={12}>
          <div style={{ margin: 30, textAlign: "left", fontSize: "120%" }}>
            <h1 id="-akses-halaman-detail-premium-anda-">
              HALAMAN DETAIL PREMIUM ANDA
            </h1>
            <p>
              Investasi anda adalah senilai Rp. 200,000 ( <del>Rp. 500,000</del>{" "}
              )
            </p>
            <p>
              Untuk 10 lembar detil informasi profil anda, yang anda bisa
              manfaatkan untuk kemajuan diri, pekerjaan dan bisnis anda.
            </p>
            <h2 id="-apa-saja-yang-akan-saya-peroleh-">
              <em>Apa saja yang akan saya peroleh ?</em>
            </h2>
            <ol>
              <li>
                Anda akan memperoleh hasil laporan detail tentang diri anda.
              </li>
              <li>
                Anda akan memperoleh laporan lengkap dalam bentuk infografis
                yang mudah dimengerti.
              </li>
              <li>
                Anda akan memperoleh akun khusus untuk anda memperoleh informasi
                tambahan spesifik profil anda (berdasarkan kondisi dunia bisnis
                terkini).
              </li>
            </ol>
            <p>Silahkan transfer pembayaran anda sebesar Rp. 200,000 ke :</p>

            <div class="hljs-keyword">CIMB NIAGA </div>
            <div> 800115318700 </div>
            <div>A/N Narapatih Inspiratama</div>

            <p>
              dan <a href="javascript:;" onClick={()=>{
                toConfirm();
              }}>klik tombol berikut</a> untuk
              mengkonfirmasikan pembayaran anda. Atau anda dapat Whatsapp Bukti transfer anda ke nomor +62-877-0088-4964
            </p>
          </div>
          <div style={{textAlign:'center'}}>
            <Button variant={"contained"} color={'primary'} onClick={()=>{
              toConfirm();
            }}>KONFIRMASI PEMBAYARAN</Button>
          </div>
        </Grid>
      </Grid>
    </div>
  );
}
