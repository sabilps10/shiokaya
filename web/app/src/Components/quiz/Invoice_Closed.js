import React, { useState } from "react";
import { Grid, CircularProgress, Button, TextField } from "@material-ui/core";
import {
  Radar,
  RadarChart,
  PolarAngleAxis,
  PolarGrid,
  PolarRadiusAxis,
  Legend
} from "recharts";

export default function InvoiceClosed(props) {
  const { data, confirm_url,toConfirm } = props;

  return (
    <div style={{ marginTop: 100 }}>
      <Grid
        container
        style={{
          maxWidth: 800,
          marginTop: 60,
          marginLeft: "auto",
          marginRight: "auto"
        }}
      >
        <Grid item xs={12} md={12}>
          <div style={{ margin: 30, textAlign: "left", fontSize: "120%" }}>
          <p>Kami ucapkan terima kasih kepada seluruh User Komunitas Sadar Potensi Diri yang telah mengikuti test Potensi Diri anda di Shiokaya.com</p>
<p><br />Mohon maaf dikarenakan tingginya hint user sehingga mengakibatkan kendala pada sistem kami saat ini, maka kami memutuskan menunda sementara transaksi pembayaran dari user untuk Shio Kaya Premium dari tanggal 10 April 2020 sampai dengan 12 April 2020.</p>
<p><br />Kami akan segera menyelesaikan permasalahan sistem ini, dan akan membuka kembali transaksi pembayaran pada hari Senin, tanggal 13 April 2020.</p>
<p><br />Jika ada pertanyaan atau anda membutuhkan bantuan lebih lanjut, silahkan hubungi kami di : </p>
<p>&nbsp;</p>
<p>Whatsapp : +62-877-0088-4964<br />Email : <a href="mailto:support@shiokaya.com">support@shiokaya.com</a><br /><br />Kami selalu siap memberikan pelayanan dan dukungan terbaik bagi anda untuk menemukan potensi diri dalam mencapai keberhasilan.<br /><br />Terima Kasih<br /><br />Customer Relation<br />Shiokaya.com</p>
            </div>
        </Grid>
      </Grid>
    </div>
  );
}
