import React from "react";
import { Redirect } from "react-router-dom";
import {
  Container,
  Grid,
  Button,
  TextField,
  CircularProgress,
  Snackbar,
  Link
} from "@material-ui/core";
import { Instagram } from "@material-ui/icons";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import loginAsGuestAction from "../redux/actions/loginAsGuestAction";
import LoginContainerStyles from "./styles/LoginContainerStyles";
import Header from "./quiz/Header";
import BigBackgroundImage from "../assets/img1.jpg";
import BigBackgroundImage2 from "../assets/img2.jpg";

import Footer from "./Footer";
import YouTube from 'react-youtube';


import CallAction from "../redux/actions/CallAction";

const qs = require('qs');

class LandingContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      token: "",
      fetching: false,
      success: false,
      session_id: "",
      snackbar: false,
      snackbarText: "",
      page: 1,
      content: null
    };
    this.initHomeContent = this.initHomeContent.bind(this);

  }
  componentDidMount() {
    const { location, call_action, callAction } = this.props;
    let token = localStorage.getItem("token");
    let chunk = location.search.split("?");
    let qparams = (chunk.length > 0) ? qs.parse(chunk[1]) : null;
    if (typeof qparams.promo !== 'undefined') {
      localStorage.setItem("promo", qparams.promo);
    }

    this.props.loginAsGuest();
    callAction(call_action, "home", {
      endpoint: "/page/content/home",
      scenario: 'get'
    })

    //production disini
   //document.location="http://shiokaya.id";
    //localStorage.clear();
  }
  componentDidUpdate(_props, _states) {
    const { guestLogin, call_action } = this.props;
    if (_props.guestLogin.payload !== guestLogin.payload) {
      this.setState({
        fetching: guestLogin.fetching
      });
      if (typeof guestLogin.payload === "undefined") return;
      if (guestLogin.payload !== null) {
        localStorage.setItem("token", guestLogin.payload.access_token);
        localStorage.setItem("session_id", guestLogin.payload.user.session_id);
        localStorage.setItem("name", guestLogin.payload.user.name);
        //this.props.history.push("/quiz");
      }
    }
    
    if (call_action.home !== _props.call_action.home) {
      this.initHomeContent(call_action.home);
    }
  }
  initHomeContent(payload) {
    if (typeof payload === 'undefined') return;
    if (payload === null) return;
    this.setState({
      content: payload
    })
  }
  render() {
    const { classes, history } = this.props;
    const { fetching, page, content } = this.state;
    if (content === null) return "";
    if (fetching)
      return (
        <div
          className={classes.root}
          style={{ textAlign: "center", margin: 100 }}
        >
          <CircularProgress />
        </div>
      );

    return (
      <div>
        <Header />
        <div
          style={{
            backgroundImage: "url(" + (page === 1 ? content.blocks['banner-depan'].image_url : content.blocks['banner-dalam'].image_url) + ")",
            marginTop: 50,
            textAlign: "center",
            height: window.innerHeight - 50,
            backgroundSize: "cover"
          }}
        >
          <div style={{ textAlign: "center" }}>
            <div
              style={{
                position: "absolute",
                bottom: 0,
                width: "100%",
                backgroundColor: "#000000",
                opacity: 0.5,
                height: "100%"
              }}
            ></div>
            <div
              style={{
                position: "absolute",
                bottom: 0,
                padding: 10,
                maxWidth: 700,
                margin: "0 auto",
                left:
                  window.innerWidth > 700 ? (window.innerWidth - 700) * 0.5 : 0
              }}
            >
              {page === 1 ? (<h1
                style={{
                  color: "white",
                  fontSize: "200%",
                  textShadow: "rgb(134, 130, 142) 3px 2px 4px"
                }}
                dangerouslySetInnerHTML={{ __html: content.blocks['banner-depan'].summary }}
              />
              ) : (<h1
                style={{
                  color: "white",
                  fontSize: "200%",
                  textShadow: "rgb(134, 130, 142) 3px 2px 4px"
                }}
                dangerouslySetInnerHTML={{ __html: content.blocks['banner-dalam'].summary }}
              />

                )}
            </div>
          </div>
        </div>
        <div
          className={classes.root}
          style={{
            padding: 15,
            paddingTop: 80,
            paddingBottom: 80,
            maxWidth: 640,

            marginLeft: "auto",
            marginRight: "auto"
          }}
        >
          {page === 1 ? (
            <Grid container spacing={3}>
              <Grid item xs={12}>
                <div style={{ marginBottom: 30, textAlign: "center" }}>
                  <YouTube videoId="B9Wz-RwYr_Y" opts={{
                    height: window.innerWidth < 640 ? '180' : '280',
                    width: window.innerWidth < 640 ? '340' : '640',
                  }} />
                </div>
                <div style={{ fontSize: "120%", lineHeight: "150%" }} dangerouslySetInnerHTML={{ __html: content.blocks['banner-depan'].content }} >

                </div>
                <div
                  style={{
                    textAlign: "center",
                    margin: 30
                  }}
                >
                  <Button
                    variant={"contained"}
                    color={"primary"}
                    size={"large"}
                    onClick={() => {
                      this.setState({
                        page: 2
                      });
                    }}
                  >
                    IKUT TEST
                  </Button>
                </div>
                <div style={{
                  width: 184,
                  margin: '0 auto'
                }}>
                  <h3 style={{ textAlign: "center" }}>Ikuti: </h3>
                  <Link target={"_blank"} href={'https://www.instagram.com/p/B-lvHthl5fY/'} style={{ position: 'relative' }}><Instagram />
                    <h4 style={{
                      display: 'inline',
                      margin: 0,
                      position: 'absolute',
                      top: -8,
                      left: 30,
                      fontWeight: 800,
                      fontSize: '120%',
                      width: 180
                    }}>Tentang Shiokaya</h4></Link>
                </div>
              </Grid>
            </Grid>
          ) : (
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <div style={{ fontSize: "120%", lineHeight: "150%" }}>
                    <h2>SELAMAT DATANG DI SHIOKAYA</h2>
                    <p>
                      Anda cukup mengalokasikan waktu selama 5 menit, mengisi 12
                      pertanyaan singkat shiokaya, untuk bisa memperoleh gambaran
                      umum tentang potensi model keberhasilan diri anda.
                  </p>
                    <p>
                      <strong>Shiokaya</strong> merupakan alat ukur untuk potensi
                    model keberhasilan, yaitu cuplikan informasi tentang
                    bagaimana anda bisa menggunakan potensi yang selama ini ada
                    di dalam diri anda, untuk bisa mencapai keberhasilan lebih
                    cepat dan akurat.
                  </p>
                    <p>
                      5 hal apa yang bisa anda lakukan setelah anda mendapatkan
                      gambaran umum anda melalui shiokaya ini:
                  </p>
                    <ol style={{
                      padding: 0,
                      paddingLeft: 15
                    }}>
                      <li>Melakukan penyesuaian dalam cara kerja anda</li>
                      <li>
                        Mengerti bagaimana anda bisa men-tune-up bisnis anda
                    </li>
                      <li>
                        Menyadari apa yang menjadi kekuatan maupun tantangan diri
                        anda
                    </li>
                      <li>
                        Mengerti model rekan atau karyawan yang anda butuhkan
                        untuk berhasil lebih cepat
                    </li>
                      <li>
                        Mampu untuk mencapai sukses dengan cara yang lebih
                        menyenangkan dalam mencapai tujuan anda
                    </li>
                    </ol>
                  </div>
                  <div
                    style={{
                      textAlign: "center",
                      margin: 30
                    }}
                  >
                    <Button
                      variant={"contained"}
                      color={"primary"}
                      size={"large"}
                      onClick={() => {
                        history.push("/test")
                      }}
                    >
                      MULAI TEST
                  </Button>
                  </div>
                </Grid>
              </Grid>
            )}
        </div>
        <Footer />
      </div>
    );
  }
}

const mapStateToProps = state => {
  const { guestLogin, call_action } = state;
  return {
    guestLogin,
    call_action
  };
};

const mapDispatchToProps = dispatch => ({
  loginAsGuest: data => dispatch(loginAsGuestAction(data)),
  callAction: (state, name, opts) => dispatch(CallAction(state, name, opts)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(LoginContainerStyles)(LandingContainer));
