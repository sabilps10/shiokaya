import React from "react";
import { Redirect } from "react-router-dom";
import { Container, Grid } from "@material-ui/core";
import AppBar from "@material-ui/core/AppBar";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import MaterialTable from "material-table";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Box from "@material-ui/core/Box";
import Snackbar from "@material-ui/core/Snackbar";
import CircularProgress from "@material-ui/core/CircularProgress";
import Menu from "./Menu";
import moment from "moment";
import DashboardStyle from "./styles/DashboardStyles";

/*actions*/
import UpcomingRecallAction from "../actions/upcomingRecall";
import UpcomingReminderAction from "../actions/upcomingReminder";
import LookupPatientAction from "../actions/lookupPatient";
import PatientListingAction from "../actions/patientListing";
import AddRecallAction from "../actions/AddRecall";
import AddReminderAction from "../actions/AddReminder";
import AddPatientAction from "../actions/AddPatient";
import EditRecallAction from "../actions/EditRecall";
import EditReminderAction from "../actions/EditReminder";
import GetDoctorActions from "../actions/DoctorList";
/*widgets*/
import AddRecall from "./AddRecall";
import EditRecall from "./EditRecall";
import Api from "../Services/Api";

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <Typography
      component="span"
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
      style={{ width: "100%" }}
    >
      <Box>{children}</Box>
    </Typography>
  );
}

const api = new Api();

class DashboardContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tabvalue: props.location.tab !== null ? props.location.tab : 0,
      selectedRow: null,
      edit_dialog: false,
      add_recall_dialog: false,
      add_reminder_dialog: false,
      goto_registrasi: false,
      selected_status: 0,
      selectedDate: moment().format("YYYY-MM-DD HH:mm:ss"),
      selected_patient: {},
      lookup_value: "",
      new_description: "",
      edit_type: "recall",
      snackbar: false,
      snackbarText: "",
      make_appointment: false,
    };
    this.handleChange = this.handleChange.bind(this);
    this.onEditDialogClosed = this.onEditDialogClosed.bind(this);
    this.onAddRecallDialogClosed = this.onAddRecallDialogClosed.bind(this);
    this.onAddReminderDialogClosed = this.onAddReminderDialogClosed.bind(this);
    this.renderAddDialog = this.renderAddDialog.bind(this);
    this.renderEditDialog = this.renderEditDialog.bind(this);
  }
  a11yProps(index) {
    return {
      id: `simple-tab-${index}`,
      "aria-controls": `simple-tabpanel-${index}`
    };
  }
  handleChange(evt, tabvalue) {
    //console.log(tabvalue);
    this.setState({
      tabvalue
    });
  }
  onEditDialogClosed() {
    this.setState({
      edit_dialog: false
    });
  }
  onAddRecallDialogClosed() {
    this.setState({
      add_recall_dialog: false
    });
  }
  onAddReminderDialogClosed() {
    this.setState({
      add_reminder_dialog: false
    });
  }
  
  componentDidMount() {
    const { upcomingRecall, upcomingReminder, getDoctors } = this.props;
    upcomingRecall({});
    upcomingReminder({});
    getDoctors();
  }
  componentDidUpdate(_props, _states) {
    const {
      upcoming_recall,
      upcoming_reminder,
      upcomingRecall,
      upcomingReminder,
      add_recall,
      add_reminder,
      patient_lookup,
      edit_recall,
      edit_reminder
    } = this.props;
    const {
      selectedRow,
      selected_patient,
    } = this.state;

    if (upcoming_recall.payload !== _props.upcoming_recall.payload) {
      console.log("UPCOMINGRECALL ", upcoming_recall.payload);
    }
    if (upcoming_reminder.payload !== _props.upcoming_reminder.payload) {
      console.log("UPCOMING REMINDER", upcoming_reminder.payload);
    }
    if (add_recall.payload !== _props.add_recall.payload) {
      if (add_recall.payload !== null) {
        this.setState({
          add_recall_dialog:false,
          add_reminder_dialog:false,
          snackbar: true,
          snackbarText:
            "Recall baru untuk `" +
            selected_patient.nomor +
            " - " +
            selected_patient.firstName +
            " " +
            selected_patient.lastName +
            "` Pasien dokter `"+ selected_patient.doctor.name +"` telah berhasil ditambahkan."
        });
        upcomingRecall({});
      }
    }

    if (add_reminder.payload !== _props.add_reminder.payload) {
      if (add_reminder.payload !== null) {
        this.setState({
          add_recall_dialog:false,
          add_reminder_dialog:false,
          snackbar: true,
          snackbarText:
            "Reminder baru untuk `" +
            selected_patient.nomor +
            " - " +
            selected_patient.firstName +
            " " +
            selected_patient.lastName +
            "` telah berhasil ditambahkan."
        });
        upcomingReminder({});
      }
    }
    if (edit_recall.payload !== _props.edit_recall.payload) {
      if (edit_recall.payload !== null) {
        this.setState({
          edit_dialog:false,
          snackbar: true,
          snackbarText:
            "Status Recall untuk `" +
            selectedRow.patient.nomor +
            " - " +
            selectedRow.patient.firstName +
            " " +
            selectedRow.patient.lastName +
            "` Pasien dokter `"+ selectedRow.doctor.name +"` telah berhasil diperbaharui."
        });
        upcomingRecall({});
      }
    }

    if (edit_reminder.payload !== _props.edit_reminder.payload) {
      if (edit_reminder.payload !== null) {
        this.setState({
          edit_dialog:false,
          snackbar: true,
          snackbarText:
            "Status Reminder untuk `" +
            selectedRow.patient.nomor +
            " - " +
            selectedRow.patient.firstName +
            " " +
            selectedRow.patient.lastName +
            "` telah berhasil diperbaharui."
        });
        upcomingReminder({});
      }
    }
  }
  renderAddDialog(props) {
    const {
      save_recall,
      type,
      save_reminder,
      upcomingRecall,
      upcomingReminder,
      get_doctors
    } = this.props;
    return (
      <AddRecall
        {...props}
        doctors={get_doctors.payload ? get_doctors.payload.data : []}
        onSubmit={data => {
          const {
            selected_patient,
            description,
            selectedDate,
            doctor_id
          } = data;
          let patient_id = selected_patient.id;

          if (type === "recall")
            save_recall({
              patient_id,
              doctor_id,
              description,
              reminder_date: selectedDate
            });
          else
            save_reminder({
              patient_id,
              doctor_id,
              description,
              reminder_date: selectedDate
            });

          this.setState({
            add_recall_dialog: false
          });

          
        }}
      />
    );
  }
  renderEditDialog(props) {
    const { selectedRow, editRecall, editReminder } = props;
    const {edit_type} = this.state;
    if (selectedRow === null) return "";
    return (
      <EditRecall
        {...props}
        onSubmit={(data) => {
          edit_type === "recall"
            ? editRecall(selectedRow.id, data)
            : editReminder(selectedRow.id, data);
        }}
      />
    );
  }

  render() {
    const { classes } = this.props;
    const { goto_registrasi, snackbar, snackbarText,selected_patient,make_appointment } = this.state;
    const { upcoming_recall, upcoming_reminder, patient_listing,editRecall,editReminder,location } = this.props;
    if (goto_registrasi)
      return (
        <Redirect
          to={{
            pathname: "/registrasi",
            state: { auth: true}
          }}
        />
      );

    if(make_appointment)
        return (
          <Redirect
            to={{
              pathname: "/book",
              state: { auth: true, selected_patient}
            }}
          />
        );
    return (
      <div className={classes.root}>
        <Menu selected={0} {...this.props} />
        <Container
          style={{
            flex: 1,
            flexDirection: "row"
          }}
        >
          <Grid container spacing={3}>
            <Grid item xs={8}>
              <div className={classes.topDesc}>
                Daftar Pasien Recall / Reminder. Harap menghubungi Pasien pada
                tanggal yang telah ditentukan
              </div>
            </Grid>
            <Grid item xs={4}>
              <div className={classes.topActionButtons}>
                <Button
                  color="primary"
                  variant="contained"
                  onClick={() => {
                    this.setState({
                      goto_registrasi: true
                    });
                  }}
                >
                  Registrasi Pasien Baru
                </Button>
              </div>
            </Grid>
          </Grid>
        </Container>
        <AppBar position="static">
          <Tabs
            value={this.state.tabvalue}
            onChange={this.handleChange}
            aria-label="Daftar"
          >
            <Tab label="Daftar Recall" {...this.a11yProps(0)} />
            <Tab label="Daftar Reminder" {...this.a11yProps(1)} />
            <Tab label="Daftar Pasien" {...this.a11yProps(2)} />
          </Tabs>
        </AppBar>
        <TabPanel value={this.state.tabvalue} index={0}>
          <MaterialTable
            columns={[
              {
                field: "reminder_date",
                title: "Tanggal",
                render: rowData =>
                  moment(rowData.reminder_date).format("DD/MM/YYYY")
              },
              {
                field: "name",
                title: "Pasien",
                render: rowData =>
                  rowData.patient.nomor +
                  " - " +
                  rowData.patient.firstName +
                  " " +
                  rowData.patient.lastName
              },
              {
                field: "doctor",
                title: "Doctor",
                render: rowData => rowData.doctor.name
              },
              {
                field: "phone",
                title: "Telp",
                render: rowData =>
                  rowData.patient.handphone +
                  " / " +
                  rowData.patient.phoneAreaCode +
                  "-" +
                  rowData.patient.phoneNumber
              },
              { title: "Deskripsi", field: "description" },
              {
                field: "reminder_status",
                title: "Status",
                render: rowData => {
                  switch (rowData.reminder_status) {
                    case 1:
                      return "Done/Confirmed";
                      break;
                    case 2:
                      return "Batal";
                      break;
                    default:
                      return "Pending";
                  }
                }
              }
            ]}
            data={
              upcoming_recall.payload !== null
                ? upcoming_recall.payload.data
                : []
            }
            title="Daftar Recall"
            onRowClick={(evt, selectedRow) => this.setState({ selectedRow })}
            options={{
              filtering: true,
              actionsColumnIndex: -1,
              headerStyle: {
                backgroundColor: "#dfe4ea",
                color: "#333"
              },
              rowStyle: rowData => ({
                backgroundColor:
                  this.state.selectedRow &&
                  this.state.selectedRow.tableData.id === rowData.tableData.id
                    ? "#EEE"
                    : "#FFF"
              })
            }}
            actions={[
              {
                icon: "edit",
                tooltip: "Edit",
                onClick: (event, rowData) => {
                  this.setState({
                    edit_dialog: true,
                    edit_type: "recall",
                    selectedRow: rowData
                  });
                }
              }
            ]}
          />
        </TabPanel>
        <TabPanel value={this.state.tabvalue} index={1}>
          <div style={{ maxWidth: "100%" }}>
            <MaterialTable
              columns={[
                {
                  field: "reminder_date",
                  title: "Tanggal",
                  render: rowData =>
                    moment(rowData.reminder_date).format("DD/MM/YYYY")
                },
                {
                  field: "name",
                  title: "Pasien",
                  render: rowData =>
                    rowData.patient.firstName + " " + rowData.patient.lastName
                },
                {
                  field: "phone",
                  title: "Telp",
                  render: rowData =>
                    rowData.patient.handphone +
                    " / " +
                    rowData.patient.phoneAreaCode +
                    "-" +
                    rowData.patient.phoneNumber
                },
                { title: "Deskripsi", field: "description" },
                {
                  field: "reminder_status",
                  title: "Status",
                  render: rowData => {
                    switch (rowData.reminder_status) {
                      case 1:
                        return "Done/Confirmed";
                        break;
                      case 2:
                        return "Cancelled";
                        break;
                      default:
                        return "Pending";
                    }
                  }
                }
              ]}
              data={
                upcoming_reminder.payload !== null
                  ? upcoming_reminder.payload.data
                  : []
              }
              title="Daftar Reminder"
              onRowClick={(evt, selectedRow) => this.setState({ selectedRow })}
              options={{
                filtering: true,
                actionsColumnIndex: -1,
                headerStyle: {
                  backgroundColor: "#dfe4ea",
                  color: "#333"
                },
                rowStyle: rowData => ({
                  backgroundColor:
                    this.state.selectedRow &&
                    this.state.selectedRow.tableData.id === rowData.tableData.id
                      ? "#EEE"
                      : "#FFF"
                })
              }}
              actions={[
                {
                  icon: "edit",
                  tooltip: "Edit",
                  onClick: (event, rowData) => {
                    this.setState({
                      edit_dialog: true,
                      edit_type: "reminder",
                      selectedRow: rowData
                    });
                  }
                }
              ]}
            />
          </div>
        </TabPanel>
        <TabPanel value={this.state.tabvalue} index={2}>
          <div style={{ maxWidth: "100%" }}>
            <MaterialTable
              columns={[
                {
                  field: "nomor",
                  title: "No."
                },
                {
                  field: "name",
                  title: "Pasien",
                  render: rowData => rowData.firstName + " " + rowData.lastName
                },
                { title: "No.Telp", field: "phoneNumber" },
                { title: "Locality", field: "nationality" },
                { title: "Type", field: "patient_type" }
              ]}
              data={query =>
                new Promise((resolve, reject) => {
                  api
                    .patient_listing(
                      {
                        search: query.search,
                        per_page: query.pageSize,
                        page: query.page
                      },
                      true
                    )
                    .then(result => {
                      resolve({
                        data: result.data,
                        page: result.page,
                        totalCount: result.total
                      });
                    });
                })
              }
              title="Daftar Pasien"
              onRowClick={(evt, selectedRow) => this.setState({ selectedRow })}
              options={{
                filtering: true,
                actionsColumnIndex: -1,
                headerStyle: {
                  backgroundColor: "#dfe4ea",
                  color: "#333"
                },
                rowStyle: rowData => ({
                  backgroundColor:
                    this.state.selectedRow &&
                    this.state.selectedRow.tableData.id === rowData.tableData.id
                      ? "#EEE"
                      : "#FFF"
                })
              }}
              actions={[
                {
                  icon: "edit",
                  tooltip: "Edit",
                  onClick: (event, rowData) => {
                    this.setState({
                      selected_patient: rowData
                    });
                  }
                },
                {
                  icon: "phone",
                  tooltip: "Recall",
                  onClick: (event, rowData) => {
                    this.setState({
                      add_recall_dialog: true,
                      selected_patient: rowData
                    });
                  }
                },
                {
                  icon: "alarm",
                  tooltip: "Reminder",
                  onClick: (event, rowData) => {
                    this.setState({
                      add_reminder_dialog: true,
                      selected_patient: rowData
                    });
                  }
                },
                {
                  icon: "date_range",
                  tooltip: "Appointment",
                  onClick: (event, rowData) => {
                    this.setState({
                      make_appointment: true,
                      selected_patient: rowData
                    });
                  }
                }
              ]}
            />
          </div>
        </TabPanel>

        {this.renderEditDialog({
          open: this.state.edit_dialog,
          handleClose: this.onEditDialogClosed,
          selectedRow: this.state.selectedRow,
          editRecall,
          editReminder,
          type: this.state.edit_type
        })}

        {this.state.add_recall_dialog
          ? this.renderAddDialog({
              open: this.state.add_recall_dialog,
              handleClose: this.onAddRecallDialogClosed,
              type: "recall",
              selected_patient: this.state.selected_patient
            })
          : this.renderAddDialog({
              open: this.state.add_reminder_dialog,
              handleClose: this.onAddReminderDialogClosed,
              type: "reminder",
              selected_patient: this.state.selected_patient
            })}

        <Snackbar
          anchorOrigin={{ vertical: "top", horizontal: "center" }}
          key={"snackbar"}
          open={snackbar}
          variant={"warning"}
          onClose={() => {
            this.setState({
              snackbar: false
            });
          }}
          ContentProps={{
            "aria-describedby": "message-id"
          }}
          message={snackbarText}
        />
      </div>
    );
  }
}

const mapStateToProps = state => {
  const {
    login,
    upcoming_recall,
    upcoming_reminder,
    patient_lookup,
    patient_listing,
    add_recall,
    add_reminder,
    add_patient,
    edit_recall,
    edit_reminder,
    edit_patient,
    get_doctors
  } = state;
  return {
    login,
    upcoming_recall,
    upcoming_reminder,
    patient_lookup,
    patient_listing,
    add_recall,
    add_reminder,
    add_patient,
    edit_recall,
    edit_reminder,
    edit_patient,
    get_doctors
  };
};

const mapDispatchToProps = dispatch => ({
  upcomingRecall: data => dispatch(UpcomingRecallAction(data)),
  upcomingReminder: data => dispatch(UpcomingReminderAction(data)),
  lookupPatient: data => dispatch(LookupPatientAction(data)),
  patientListing: data => dispatch(PatientListingAction(data)),
  save_recall: data => dispatch(AddRecallAction(data)),
  save_reminder: data => dispatch(AddReminderAction(data)),
  save_patient: data => dispatch(AddPatientAction(data)),
  editRecall: (id, data) => dispatch(EditRecallAction(id, data)),
  editReminder: (id, data) => dispatch(EditReminderAction(id, data)),
  getDoctors: data => dispatch(GetDoctorActions(data))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(DashboardStyle)(DashboardContainer));
