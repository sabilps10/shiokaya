const config = require('config');
const moment = require('moment-timezone');
const mysql2 = require("mysql2/promise");

let poolPromised = mysql2.createPool({
    connectionLimit: 10,
    host: config.get("mysql.host"),
    port: config.get("mysql.port"),
    user: config.get("mysql.user"),
    password: config.get("mysql.pass"),
    database: config.get("mysql.database"),
    timezone: config.get("mysql.timezone")
});

async function main() {
    try {
        // check jam server
        let jakartaTime = moment().tz('Asia/Jakarta').format('DD/MM/YYYY hh:mm:ss')
        let londonTime = moment().tz('Europe/London').format('DD/MM/YYYY hh:mm:ss')

        console.log(`#Timezone - ${moment.tz.guess()}`);
        console.log(`#Jakarta Time - ${jakartaTime}`);
        console.log(`#London Time - ${londonTime}`);
        console.log(`#Server Time - ${moment().format('DD/MM/YYYY hh:mm:ss')}`);
        console.log(`----------------------------------------------------------`);

        console.log(`Daily Sales Started`)
        console.log(`#START - ${jakartaTime}`)
        console.log(`----------------------------------------------------------`);

        let sqlGetDataSales = await poolPromised.format(`SELECT created_at tanggal, affiliate_id, COUNT(id) as total 
        FROM transactions WHERE transaction_status = 1 
        group by DATE(created_at), affiliate_id`)

        let sqlGetDataPending = await poolPromised.format(`SELECT created_at tanggal, affiliate_id, COUNT(id) as total 
        FROM transactions WHERE transaction_status = 0
        group by DATE(created_at), affiliate_id`)
        
        let getDataPending = await poolPromised.query(sqlGetDataPending)
        let dataPending = getDataPending[0].map(item => {
            item.tanggal = moment(item.tanggal).format('YYYY-MM-DD hh:mm:ss')
            return item
        })

        for (const item of dataPending) {
            let sqlInsertPending = await poolPromised.format(`INSERT INTO daily_pending_transactions (tanggal,affiliate_id,total) 
            VALUES(?,?,?) 
            ON DUPLICATE KEY UPDATE total = VALUES(total)`,
            [item.tanggal, item.affiliate_id, item.total])
            await poolPromised.query(sqlInsertPending)
        }
        
        let getDataSales = await poolPromised.query(sqlGetDataSales)
        let dataSales = getDataSales[0].map(item => {
            item.tanggal = moment(item.tanggal).format('YYYY-MM-DD hh:mm:ss')
            return item
        })

        for (const item of dataSales) {
            let sqlInsertSales = await poolPromised.format(`INSERT INTO daily_sales (tanggal,affiliate_id,total) 
            VALUES(?,?,?) 
            ON DUPLICATE KEY UPDATE total = VALUES(total)`,
            [item.tanggal, item.affiliate_id, item.total])
            await poolPromised.query(sqlInsertSales)
        }
        
        console.log(`----------------------------------------------------------`);
        console.log(`Daily Sales Ended`)
        console.log(`#END - ${jakartaTime}`)
        console.log(`----------------------------------------------------------`);

        poolPromised.end()
        process.exit(1);
    } catch (error) {
        poolPromised.end();
        console.log(error);
    }
}

main()