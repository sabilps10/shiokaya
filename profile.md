BARD
```
<h2 style="text-align: center;"><span style="color: #ff6600;">BARD</span></h2>
<p>Para pemilik shio ini merupakan tenaga pemasar yang berbakat serta promotor terbaik.</p>
<p>Mereka juga merupakan pribadi yang kuat, bersinar serta mampu menginspirasi orang lain dengan penyampaian yang mudah dimengerti secara konsisten.</p>
<p>Kalangan Bard biasanya sangat butuh perhatian untuk tetap menjaga energi positif dalam dirinya.<br /><br /><br /></p>
<table style="width: 100%; height: 71px; background-color: #f5f5f5;">
<tbody>
<tr>
<td style="width: 100px;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-BARD.png" width="100" height="100" /></td>
<td style="width: 497px;">Elemen Matahari melambangkan sumber kehidupan bagi seluruh kehidupan di bumi. Seorang yang mampu memberikan inspirasi serta semangat kepada sekitarnya untuk menyelesaikan persoalan yang dihadapi.</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<p>Bard memiliki karakteristik warna orange yang kuat dan hangat, yang memberikan rasa nyaman, dan kerap digunakan untuk menciptakan hawa produktif dan menghasilkan repetisi di lingkungan kerja yang membutuhkan produktivitas. &nbsp;</p>
<h2 style="text-align: center;"><br /><span style="color: #ff6600;">KEKUATAN BARD : PENGARUH</span><br /><br /></h2>
<table style="height: 78px; width: 100%; background: #f5f5f5; padding: 20; background-color: #ff6600;">
<tbody>
<tr>
<td style="width: 30%; padding: 20;">
<p><span style="color: #ffffff;"><strong>Sisi Produktif</strong></span></p>
</td>
<td style="width: 70%;">
<p><span style="color: #ffffff;">Ceria, ambisi, energik, sensualitas, menyenangkan, kenyamanan, bersahabat, percaya diri, harapan, kreativitas, merangsang emosi.</span></p>
</td>
</tr>
<tr>
<td style="width: 174px;">
<p><span style="color: #ffffff;"><strong>Sisi Tantangan</strong></span></p>
</td>
<td style="width: 416px;"><span style="color: #ffffff;">&nbsp;Mudah dijangkau, gaduh, merangsang, hiperaktif, perampasan, frustrasi, kesembronoan, kurangnya intelektualisme, ketidak dewasaan.</span></td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<h2 class="p1" style="text-align: center;"><span style="color: #ff6600;">SELF-LEADERSHIP</span></h2>
<p class="p1"><span class="s1">Pemimpin yang memiliki kemampuan untuk menularkan semangat dan kekuatan yang penuh dinamika, serta menjadi sumber energi bagi dirinya dan orang lain. Mampu untuk sabar dalam menjalankan suatu hal. Tajam, terarah dan mampu melihat segala hal secara menyeluruh (big picture).</span></p>
<p class="p1"><span class="s1">Pemimpin yang berani mengambil peran untuk menjadi inspirasi bagi orang di sekelilingnya, dan mengambil resiko untuk kesalahan yang mungkin dilakukan atas keputusan yang dibuatnya.</span></p>
<p class="p1">&nbsp;</p>
<p>&nbsp;</p>
```

Knight

```
<h2 style="text-align: center;"><span style="color: #ffcc00;">KNIGHT</span></h2>
<p>Para pemilik shio ini merupakan influencer dan pemimpin yang karismatik.</p>
<p>Mereka orang yang mampu memberi semangat selayaknya api yang mampu menerangi kegelapan dan mampu mempengaruhi orang lain dengan mudah.</p>
<p>Hal ini membuat Knight sangat handal dalam mengelola dan menciptakan manajemen SDM dan tim pelaksana yang baik.</p>
<table style="width: 100%; height: 71px; background-color: #f5f5f5;">
<tbody>
<tr>
<td style="width: 100px;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-PRIEST.png" width="100" height="100" /></td>
<td style="width: 497px;">Elemen Api melambangkan kemampuan untuk membakar apapun tanpa kecuali. Simbol dari sifat yang tegas dan lugas. Seorang yang cekatan dan tuntas dalam menyelesaikan persoalan, konsisten dan objektif.</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<p>Knight memiliki karakteristik warna kuning yang cerah atau ceria, yang dapat merangsang otak serta membuat lebih waspada dan tegas, dapat menarik perhatian tetapi tidak terlalu menonjol, juga terkadang mampu memberikan energi tetapi tidak menohok.</p>
<h2 style="text-align: center;"><br /><span style="color: #ffcc00;">KEKUATAN KNIGHT : HUBUNGAN</span><br /><br /></h2>
<table style="height: 78px; width: 100%; background: #f5f5f5; padding: 20; background-color: #ffcc00;">
<tbody>
<tr>
<td style="width: 30%; padding: 20;">
<p><span style="color: #000000;"><strong>Sisi Produktif</strong></span></p>
</td>
<td style="width: 70%;">
<p>Optimis, percaya diri, toleran, suka cita, antusiasme, kekeluargaan, persahabatan, keleluasaan, spontanitas, keramahan, kedermawanan.</p>
</td>
</tr>
<tr>
<td style="width: 174px;">
<p><span style="color: #000000;"><strong>Sisi Tantangan</strong></span></p>
</td>
<td style="width: 416px;">
<p>Berubah-ubah, kurang dapat diandalkan, irasionalitas, ketakutan, kerapuhan, emosional, depresi, kecemasan, menyalahkan diri sendiri</p>
</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<h2 class="p1" style="text-align: center;"><span style="color: #ffcc00;">SELF-LEADERSHIP</span></h2>
<p>Pemimpin yang mampu mendorong orang lain yang dipimpinnya untuk memiliki semangat yang sama besar.</p>
<p>Seorang pemimpin tipe ini juga memiliki prinsip mampu beradaptasi dimanapun ia berada.</p>
<p>Lingkungan dan orang-orang di sekitarnya, merupakan bahan bakar bagi pemimpin model ini, dimana ia perlu untuk bisa terkoneksi secara dalam dan benar-benar mengendalikan organisasi yang dipimpinnya, dan memastikan kepentingan yang menjadi tujuannya dapat tercapai.</p>
<p><br />Seorang pemimpin yang bertumpu pada fakta-fakta, dan mampu menunjukkan kepada orang lain tentang peluang yang ada serta pengambilan keputusan yang lebih efektif bagi semua pihak.</p>
<p class="p1">&nbsp;</p>
```

Arbiter

```
<h2 style="text-align: center;"><span style="color: #339966;">ARBITER</span></h2>
<p>Para pemilik shio ini merupakan penengah yang baik dan sangat menghindari konflik.</p>
<p>Mereka juga senang berinvestasi pada orang guna manfaat jangka panjang.</p>
<p>Seperti angin, mereka kadang tidak terlihat namun sangat terasa manfaatnya. Hal ini membuat Arbiter mampu menciptakan kemungkinan baru saat orang lain merasa tidak lagi ada jalan lain.</p>
<p><br /><br /></p>
<table style="width: 100%; height: 71px; background-color: #f5f5f5;">
<tbody>
<tr>
<td style="width: 100px;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-ARBITER.png" width="100" height="100" /></td>
<td style="width: 497px;">Elemen Angin melambangkan kemudahan untuk bergerak dan masuk ke segala kondisi. Seseorang yang teliti dan berhati-hati, terukur dalam bicara dan bertindak serta dalam pengambilan keputusan.</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<p>Arbiter memiliki karakteristik warna hijau terang yang terkait langsung dengan kedamaian dalam gerak, dengan kekuatan mediasi, penengah dalam perbedaan, penghindaran konflik, bersifat praktis dan membumi, hasrat bermain dengan optimisme dan keceriaan .</p>
<h2 style="text-align: center;"><br /><span style="color: #339966;">KEKUATAN ARBITER : KETELITIAN</span><br /><br /></h2>
<table style="height: 78px; width: 100%; background: #f5f5f5; padding: 20; background-color: #339966;">
<tbody>
<tr>
<td style="width: 30%; padding: 20;">
<p><span style="color: #ffffff;"><strong>Sisi Produktif</strong></span></p>
</td>
<td style="width: 70%;">
<p><span style="color: #ffffff;">Kompeten, kesehatan, selera bagus, efisien, kebahagiaan, universal, istirahat, kesadaran, ketabahan, semangat, santai, keingintahuan.</span></p>
</td>
</tr>
<tr>
<td style="width: 174px;">
<p><span style="color: #ffffff;"><strong>Sisi Tantangan</strong></span></p>
</td>
<td style="width: 416px;">
<p><span style="color: #ffffff;">Cemburu, kualitas rendah, tidak berharga, tertolak, kerapuhan, kecemasan, indesisif, mengorbankan diri, kekhawatiran.</span></p>
</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<h2 class="p1" style="text-align: center;"><span style="color: #339966;">SELF-LEADERSHIP</span></h2>
<p>Pemimpin yang senantiasa mampu bergerak cepat, fleksibel terhadap kemungkinan yang terjadi, dalam hubungannya dengan berbagai macam kepentingan.<br /><br />Pemimpin model ini tidak mudah lelah untuk menjadi jembatan atas sebuah hubungan dengan orang lain, dan untuk mendapatkan kesepakatan yang diperlukan demi kemajuan bersama. <br /><br />Seorang pemimpin yang bertumpu pada fakta-fakta, dan mampu menunjukkan kepada orang lain tentang peluang yang ada serta pengambilan keputusan yang lebih efektif bagi semua pihak.</p>
<p class="p1">&nbsp;</p>
```


Alchemist

```
<h2 style="text-align: center;"><span style="color: #993366;">ALCHEMIST</span></h2>
<p>Para pemilik shio ini merupakan penyempurna dan finisher terbaik.</p>
<p>Mereka juga cerdas dalam melakukan efisiensi dan efektivitas.</p>
<p>Kemampuan beradaptasi yang tinggi membuat mereka memiliki sifat rendah hati yang sangat natural. Di sisi lain mereka frontal dalam usaha mengejar setiap peluang yang ada.</p>
<p>&nbsp;</p>
<table style="width: 100%; height: 71px; background-color: #f5f5f5;">
<tbody>
<tr>
<td style="width: 100px;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-ALCHEMIST.png" width="100" height="100" /></td>
<td style="width: 497px;">Elemen Bulan melambangkan sumber cahaya di malam hari, penerang kehidupan dari kegelapan malam. Seorang yang bisa menjadi penuntun dan memberikan pencerahan kepada sekitarnya dalam kondisi gelap sekalipun.</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<p>Alchemist memiliki karakteristik warna ungu yang melambangkan pengharapan yang besar dan kepekaan, dipersepsikan terkait dengan imajinasi, spiritualitas, dan misterius, serta mencerminkan keagungan dan kebijaksanaan yang terkait dengan kesenangan dan kemewahan.</p>
<h2 style="text-align: center;"><br /><span style="color: #993366;">KEKUATAN ALCHEMIST : KETERATURAN</span><br /><br /></h2>
<table style="height: 78px; width: 100%; background: #f5f5f5; padding: 20; background-color: #993366;">
<tbody>
<tr>
<td style="width: 30%; padding: 20;">
<p><span style="color: #ffffff;"><strong>Sisi Produktif</strong></span></p>
</td>
<td style="width: 70%;">
<p><span style="color: #ffffff;">Magis, spiritualitas, misterius, imajinasi, sensitivitas, obsesif, ambisius, martabat, berkorban, kebijaksanaan, visioner, orisinalitas, kekayaan.</span></p>
</td>
</tr>
<tr>
<td style="width: 174px;">
<p><span style="color: #ffffff;"><strong>Sisi Tantangan</strong></span></p>
</td>
<td style="width: 416px;">
<p><span style="color: #ffffff;">Kurang teliti, kesendirian, bertele-tele, perfeksionis, suram, ketidak-sabaran, arogan, ktidak-berdayaan, apatis, terjebak masa lalu, frustasi.</span></p>
</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<h2 class="p1" style="text-align: center;"><span style="color: #993366;">SELF-LEADERSHIP</span></h2>
<p>Pemimpin yang mampu memberikan penerang bagi diri dan sekitarnya, dengan membangun sistem yang fair berbasis aturan dan prosedur untuk mencapai produktivitas optimal.</p>
<p>Pemimpin yang mampu merakit banyak hal yang belum terlihat orang lain, menjadi sebuah kesatuan yang bermanfaat.</p>
<p>Kekuatan terbesarnya adalah mencapai tujuan dengan membangun sistem yang teratur.</p>
<p><br />Pemimpin model ini tidak mudah lelah untuk menjadi jembatan atas sebuah hubungan dengan orang lain, dan untuk mendapatkan kesepakatan yang diperlukan demi kemajuan bersama. <br /><br />Seorang pemimpin yang bertumpu pada fakta-fakta, dan mampu menunjukkan kepada orang lain tentang peluang yang ada serta pengambilan keputusan yang lebih efektif bagi semua pihak.</p>
<p class="p1">&nbsp;</p>

```

MERCHANT

```
<h2 style="text-align: center;"><span style="color: #008000;">MERCHANT</span></h2>
<p>Para pemilik shio ini merupakan negosiator dan pedagang yang handal.</p>
<p>Mereka mampu mencari peluang karena selalu bekerja dalam parameter tertentu berdasarkan data.</p>
<p>Shio ini juga memiliki kemampuan menampung informasi apapun secara luas dan memanfaatkannya untuk mencari peluang dan memperoleh keberhasilan yang lebih besar</p>
<table style="width: 100%; height: 71px; background-color: #f5f5f5;">
<tbody>
<tr>
<td style="width: 100px;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-MERCHANT.png" width="100" height="100" /></td>
<td style="width: 497px;">Elemen Tanah melambangkan penyedia tempat dan dukungan bagi seluruh kehidupan. Seorang yang bisa mengarahkan perhatian untuk melihat yang bisa dilakukan dan untuk membawa kesejahteraan bagi sekitarnya.</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<p>Merchant memiliki karakteristik warna hijau pekat yang berkaitan dengan alam, yang berguna untuk membantu dalam situasi tertekan agar lebih mampu menyeimbangkan dan menenangkan emosi. Warna ini juga melambangkan penyembuhan serta mendorong perasaan empati.</p>
<h2 style="text-align: center;"><br /><span style="color: #008000;">KEKUATAN MERCHANT : PELUANG</span><br /><br /></h2>
<table style="height: 78px; width: 100%; background: #f5f5f5; padding: 20; background-color: #008000;">
<tbody>
<tr>
<td style="width: 30%; padding: 20;">
<p><span style="color: #ffffff;"><strong>Sisi Produktif</strong></span></p>
</td>
<td style="width: 70%;">
<p><span style="color: #ffffff;">Kehidupan, kedamaian, kemudahan, harmoni, empati, kesejukan, keberuntungan, pemulihan, jaminan, kesadaran, keamanan, ketabahan.</span></p>
</td>
</tr>
<tr>
<td style="width: 174px;">
<p><span style="color: #ffffff;"><strong>Sisi Tantangan</strong></span></p>
</td>
<td style="width: 416px;">
<p class="p1"><span class="s1" style="color: #ffffff;">Terperangkap, tersesat, kebosanan, stagnasi, superior, ambisi, keserakahan, ketertutupan, homeostasis.</span></p>
</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<h2 class="p1" style="text-align: center;"><span style="color: #008000;">SELF-LEADERSHIP</span></h2>
<p>&nbsp;</p>
<p>Pemimpin yang memiliki sifat keteguhan, yang bisa menjadi landasan pijakan dan memberi kehidupan bagi organisasi atau tim yang dipimpinnya.</p>
<p>Model kepemimpinan yang fleksibel ketika berhubungan ke arah manapun, karena ia sanggup menopang apapun, yang tidak mudah lelah untuk menciptakan peluang yang terbentuk dari berbagai macam sumber.</p>
<p>Pemimpin model ini bisa menjelma menjadi besar atau kecil, sesuai dengan kebutuhannya, serta tidak mudah terganggu atas penolakan, dimana kekuatan alaminya adalah bertumpu pada fakta yang ada dalam mengambil keputusan.</p>
```


PRIEST

```
<h2 style="text-align: center;"><span style="color: #33cccc;">PRIEST</span></h2>
<p>Para pemilik shio ini adalah penjaga yang baik, sabar serta dapat diandalkan.</p>
<p>Mereka selalu berhati-hati dalam melakukan hal apapun, terstruktur dan cerdas dalam mengelola risiko.</p>
<p>Mereka juga sangat tegas dalam menegakkan aturan sehingga ditakuti oleh para pelanggar aturan maupun para tim di bawahnya.</p>
<p>Namun di sisi lain mereka sangat safety-player sehingga seringkali kehilangan peluang yang ada.</p>
<table style="width: 100%; height: 71px; background-color: #f5f5f5;">
<tbody>
<tr>
<td style="width: 100px;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-KNIGHT.png" width="100" height="100" /></td>
<td style="width: 497px;">Elemen Samudra melambangkan keluasan dan kelapangan, sebagai muara yang menerima berbagai aliran air. Seorang yang berpikiran terbuka, bersedia menyerap berbagai hal baru serta mau menerima masukan dengan lapang dada.</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<p>Priest memiliki karakteristik warna tosca yang melambangkan konsentrasi, keseimbangan emosional, stabilitas diri, dan juga kesabaran, yang menghadirkan kejernihan pikiran dan kepercayaan diri, mengendalikan dan menyembuhkan untuk mencapai keamanan dan kestabilan.</p>
<h2 style="text-align: center;"><br /><span style="color: #33cccc;">KEKUATAN PRIEST : KEAMANAN</span><br /><br /></h2>
<table style="height: 78px; width: 100%; background: #f5f5f5; padding: 20; background-color: #33cccc;">
<tbody>
<tr>
<td style="width: 30%; padding: 20;">
<p><span style="color: #000000;"><strong>Sisi Produktif</strong></span></p>
</td>
<td style="width: 70%;">
<p><span style="color: #000000;">Keseimbangan, stabilitas, ketenangan, kesabaran, menghadirkan, keteraturan, mempengaruhi, terorganisir, kelembutan, ketepatan.</span></p>
</td>
</tr>
<tr>
<td style="width: 174px;">
<p><span style="color: #000000;"><strong>Sisi Tantangan</strong></span></p>
</td>
<td style="width: 416px;">
<p class="p1"><span class="s1" style="color: #000000;">Egois (self-centered), narsistik, mengabaikan, misterius, overaktif, ketidakseimbangan emosi, menyembunyikan, menahan, kebingungan.</span></p>
</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<h2 class="p1" style="text-align: center;"><span style="color: #33cccc;">SELF-LEADERSHIP</span></h2>
<p>&nbsp;</p>
<p>Pemimpin dengan keingintahuan dan wawasan yang luas, yang mampu mengatasi berbagai macam gejolak dalam dirinya dengan baik.</p>
<p>Tipe pemimpin yang mampu untuk menjadi penjaga atas keterlaksanaan sebuah hal atau aset, dimana hal ini berarti bahwa ia adalah pemimpin yang mampu menghasilkan keberhasilan tanpa harus melepas aset yang dimilikinya berupa sumber daya apapun (SDM, properti, kapital, dll).</p>
<p>Seorang pemimpin yang mempunyai kemampuan menampung berbagai masalah yang datang, dan mengubahnya menjadi sebuah kondisi yang menguntungkan.</p>
```


MARSHAL


```
<h2 style="text-align: center;"><span style="color: #3366ff;">MARSHAL</span></h2>
<p>Para pemilik shio ini merupakan penganalisa dan regulator yang baik.</p>
<p>Mereka sangat cocok berada dalam tim belakang layar karena sangat terencana dan pengatur yang<br />handal.</p>
<p>Kemampuan keras yang dimilikinya membuat mereka mampu memanfaatkan kekuatan waktu dan prioritas yang tersusun dengan baik untuk mengambil peluang yang dilewatinya.</p>
<table style="width: 100%; height: 71px; background-color: #f5f5f5;">
<tbody>
<tr>
<td style="width: 100px;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-MARSHAL.png" width="100" height="100" /></td>
<td style="width: 497px;">Elemen Air melambangkan kemampuan untuk berubah mengikuti wadahnya dan akan turun ke tempat yang lebh rendah. Seorang yang mampu untuk beradaptasi dengan baik, serta mampu untuk bekerja dengan baik dari balik layar.</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<p>Marshal memiliki karakteristik warna biru yang melambangkan kepercayaan dominan, cerdas, dan menampilkan keseriusan untuk hal-hal yang sifatnya tentang pencapaian, dengan menghadirkan citra keluasan, stabilitas, kesejukan, juga relaksasi.</p>
<h2 style="text-align: center;"><br /><span style="color: #3366ff;">KEKUATAN MARSHAL : PERENCANAAN</span><br /><br /></h2>
<table style="height: 78px; width: 100%; background: #f5f5f5; padding: 20; background-color: #3366ff;">
<tbody>
<tr>
<td style="width: 30%; padding: 20;">
<p><span style="color: #ffffff;"><strong>Sisi Produktif</strong></span></p>
</td>
<td style="width: 70%;">
<p><span style="color: #ffffff;">Kepercayaan, konsistensi, konsentrasi, ketenangan, keyakinan, keseriusan, professional, artistik, kekuatan, jernih, melankolis.</span></p>
</td>
</tr>
<tr>
<td style="width: 174px;">
<p><span style="color: #ffffff;"><strong>Sisi Tantangan</strong></span></p>
</td>
<td style="width: 416px;">
<p><span style="color: #ffffff;">Kaku, sulit akrab, keraguan, dingin, keras kepala, bangga diri, cuek, tidak ramah, tidak empatik, kesedihan, kesendirian, kesunyian.</span></p>
</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<h2 class="p1" style="text-align: center;"><span style="color: #3366ff;">SELF-LEADERSHIP</span></h2>
<p>&nbsp;</p>
<p>Pemimpin dengan kekuatan terbesarnya adalah strategi yang komprehensif, dalam proses mengusahakan keberhasilan bagi organisasi atau tim yang dipimpinnya.</p>
<p>Pemimpin yang bisa menjamin keamanan serta menunjukkan kewibawaan diri, dengan perhitungan yang matang.<br /><br />Model kepemimpinan ini juga mampu menyelesaikan tantangan yang ada di sekitar, dalam perjalanannya menuju target yang diinginkan, mampu untuk terus berjalan melalui berbagai penghalang dan tantangan yang ditemuinya.</p>
```


WIZARD
```
<h2 style="text-align: center;"><span style="color: #ff0000;">WIZARD</span></h2>
<p>Para pemilik shio ini merupakan konseptor dan creator yang baik.</p>
<p>Pemikirannya terhadap ide baru yang out-of-the-box dapat tercipta dengan spontan. Sayangnya hal tersebut membuat mereka sangat impulsif dan tidak teratur.</p>
<p>Seringkali orang sulit memahami apa yang ada dalam pikiran para Wizard.</p>
<table style="width: 100%; height: 71px; background-color: #f5f5f5;">
<tbody>
<tr>
<td style="width: 100px;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-WIZARD.png" width="100" height="100" /></td>
<td style="width: 497px;">Elemen Bintang melambangkan posisi yang tinggi di langit, penunjuk arah bagi kehidupan. Seorang yang selalu bisa diandalkan dan bisa menjadi contoh, serta menempati posisi yang terpandang.</td>
</tr>
</tbody>
</table>
<p>Wizard memiliki karakteristik warna merah yang inspiratif, menunjukkan kekuatan, energi, kehangatan, cinta, nafsu, dan agresi. Dapat memicu tingkat emosional, sekaligus warna yang mampu menarik perhatian dan memicu ketercapaian tujuan.</p>
<h2 style="text-align: center;"><br /><span style="color: #ff0000;">KEKUATAN WIZARD : KREATIFITAS</span><br /><br /></h2>
<table style="height: 78px; width: 100%; background: #f5f5f5; padding: 20; background-color: #ff0000;">
<tbody>
<tr>
<td style="width: 30%; padding: 20;">
<p><span style="color: #ffffff;"><strong>Sisi Produktif</strong></span></p>
</td>
<td style="width: 70%;">
<p><span style="color: #ffffff;">Bersemangat, enerjik, dinamis, komunikatif, mewah, cinta, kekuatan, percaya diri, dramatis, perjuangan, persisten, kreativitas, imajinatif</span></p>
</td>
</tr>
<tr>
<td style="width: 174px;">
<p><span style="color: #ffffff;"><strong>Sisi Tantangan</strong></span></p>
</td>
<td style="width: 416px;">
<p class="p1"><span class="s1" style="color: #ffffff;">Agresif, penuntut, kemarahan, nafsu, dominasi, teriakan, persaingan, kekerasan, penolakan, pertentangan.</span></p>
</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<h2 class="p1" style="text-align: center;"><span style="color: #ff0000;">SELF-LEADERSHIP</span></h2>
<p>Pemimpin yang mampu menciptakan banyak konsep baru dan orisinil, untuk bisa menghasilkan perubahan bersama dengan orang sekitarnya.</p>
<p>Pemimpin tipe ini perlu belajar bertindak tegas terhadap siapapun, apalagi jika terjadi penyimpangan terhadap kesepakatan dan bisa membawa kerugian.</p>
<p>Pemimpin yang mampu melihat banyak hal yang sudah ada, dengan cara yang baru, sehingga bisa menjadi inisiator perubahan dalam skala besar.</p>
```




```


UPDATE `shiokaya`.`contents` SET `content_text` ='<h2 style="text-align: center;"><span style="color: #ff6600;">BARD</span></h2>
<p>Para pemilik shio ini merupakan tenaga pemasar yang berbakat serta promotor terbaik.</p>
<p>Mereka juga merupakan pribadi yang kuat, bersinar serta mampu menginspirasi orang lain dengan penyampaian yang mudah dimengerti secara konsisten.</p>
<p>Kalangan Bard biasanya sangat butuh perhatian untuk tetap menjaga energi positif dalam dirinya.<br /><br /><br /></p>
<table style="width: 100%; height: 71px; background-color: #f5f5f5;">
<tbody>
<tr>
<td style="width: 100px;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-BARD.png" width="100" height="100" /></td>
<td style="width: 497px;">Elemen Matahari melambangkan sumber kehidupan bagi seluruh kehidupan di bumi. Seorang yang mampu memberikan inspirasi serta semangat kepada sekitarnya untuk menyelesaikan persoalan yang dihadapi.</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<p>Bard memiliki karakteristik warna orange yang kuat dan hangat, yang memberikan rasa nyaman, dan kerap digunakan untuk menciptakan hawa produktif dan menghasilkan repetisi di lingkungan kerja yang membutuhkan produktivitas. &nbsp;</p>
<h2 style="text-align: center;"><br /><span style="color: #ff6600;">KEKUATAN BARD : PENGARUH</span><br /><br /></h2>
<table style="height: 78px; width: 100%; background: #f5f5f5; padding: 20; background-color: #ff6600;">
<tbody>
<tr>
<td style="width: 30%; padding: 20;">
<p><span style="color: #ffffff;"><strong>Sisi Produktif</strong></span></p>
</td>
<td style="width: 70%;">
<p><span style="color: #ffffff;">Ceria, ambisi, energik, sensualitas, menyenangkan, kenyamanan, bersahabat, percaya diri, harapan, kreativitas, merangsang emosi.</span></p>
</td>
</tr>
<tr>
<td style="width: 174px;">
<p><span style="color: #ffffff;"><strong>Sisi Tantangan</strong></span></p>
</td>
<td style="width: 416px;"><span style="color: #ffffff;">&nbsp;Mudah dijangkau, gaduh, merangsang, hiperaktif, perampasan, frustrasi, kesembronoan, kurangnya intelektualisme, ketidak dewasaan.</span></td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<h2 class="p1" style="text-align: center;"><span style="color: #ff6600;">SELF-LEADERSHIP</span></h2>
<p class="p1"><span class="s1">Pemimpin yang memiliki kemampuan untuk menularkan semangat dan kekuatan yang penuh dinamika, serta menjadi sumber energi bagi dirinya dan orang lain. Mampu untuk sabar dalam menjalankan suatu hal. Tajam, terarah dan mampu melihat segala hal secara menyeluruh (big picture).</span></p>
<p class="p1"><span class="s1">Pemimpin yang berani mengambil peran untuk menjadi inspirasi bagi orang di sekelilingnya, dan mengambil resiko untuk kesalahan yang mungkin dilakukan atas keputusan yang dibuatnya.</span></p>
<p class="p1">&nbsp;</p>
<p>&nbsp;</p>' WHERE id=2;


UPDATE `shiokaya`.`contents` SET `content_text` ='<h2 style="text-align: center;"><span style="color: #ffcc00;">KNIGHT</span></h2>
<p>Para pemilik shio ini merupakan influencer dan pemimpin yang karismatik.</p>
<p>Mereka orang yang mampu memberi semangat selayaknya api yang mampu menerangi kegelapan dan mampu mempengaruhi orang lain dengan mudah.</p>
<p>Hal ini membuat Knight sangat handal dalam mengelola dan menciptakan manajemen SDM dan tim pelaksana yang baik.</p>
<table style="width: 100%; height: 71px; background-color: #f5f5f5;">
<tbody>
<tr>
<td style="width: 100px;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-PRIEST.png" width="100" height="100" /></td>
<td style="width: 497px;">Elemen Api melambangkan kemampuan untuk membakar apapun tanpa kecuali. Simbol dari sifat yang tegas dan lugas. Seorang yang cekatan dan tuntas dalam menyelesaikan persoalan, konsisten dan objektif.</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<p>Knight memiliki karakteristik warna kuning yang cerah atau ceria, yang dapat merangsang otak serta membuat lebih waspada dan tegas, dapat menarik perhatian tetapi tidak terlalu menonjol, juga terkadang mampu memberikan energi tetapi tidak menohok.</p>
<h2 style="text-align: center;"><br /><span style="color: #ffcc00;">KEKUATAN KNIGHT : HUBUNGAN</span><br /><br /></h2>
<table style="height: 78px; width: 100%; background: #f5f5f5; padding: 20; background-color: #ffcc00;">
<tbody>
<tr>
<td style="width: 30%; padding: 20;">
<p><span style="color: #000000;"><strong>Sisi Produktif</strong></span></p>
</td>
<td style="width: 70%;">
<p>Optimis, percaya diri, toleran, suka cita, antusiasme, kekeluargaan, persahabatan, keleluasaan, spontanitas, keramahan, kedermawanan.</p>
</td>
</tr>
<tr>
<td style="width: 174px;">
<p><span style="color: #000000;"><strong>Sisi Tantangan</strong></span></p>
</td>
<td style="width: 416px;">
<p>Berubah-ubah, kurang dapat diandalkan, irasionalitas, ketakutan, kerapuhan, emosional, depresi, kecemasan, menyalahkan diri sendiri</p>
</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<h2 class="p1" style="text-align: center;"><span style="color: #ffcc00;">SELF-LEADERSHIP</span></h2>
<p>Pemimpin yang mampu mendorong orang lain yang dipimpinnya untuk memiliki semangat yang sama besar.</p>
<p>Seorang pemimpin tipe ini juga memiliki prinsip mampu beradaptasi dimanapun ia berada.</p>
<p>Lingkungan dan orang-orang di sekitarnya, merupakan bahan bakar bagi pemimpin model ini, dimana ia perlu untuk bisa terkoneksi secara dalam dan benar-benar mengendalikan organisasi yang dipimpinnya, dan memastikan kepentingan yang menjadi tujuannya dapat tercapai.</p>
<p><br />Seorang pemimpin yang bertumpu pada fakta-fakta, dan mampu menunjukkan kepada orang lain tentang peluang yang ada serta pengambilan keputusan yang lebih efektif bagi semua pihak.</p>
<p class="p1">&nbsp;</p>' WHERE id=3;


UPDATE `shiokaya`.`contents` SET `content_text` ='<h2 style="text-align: center;"><span style="color: #339966;">ARBITER</span></h2>
<p>Para pemilik shio ini merupakan penengah yang baik dan sangat menghindari konflik.</p>
<p>Mereka juga senang berinvestasi pada orang guna manfaat jangka panjang.</p>
<p>Seperti angin, mereka kadang tidak terlihat namun sangat terasa manfaatnya. Hal ini membuat Arbiter mampu menciptakan kemungkinan baru saat orang lain merasa tidak lagi ada jalan lain.</p>
<p><br /><br /></p>
<table style="width: 100%; height: 71px; background-color: #f5f5f5;">
<tbody>
<tr>
<td style="width: 100px;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-ARBITER.png" width="100" height="100" /></td>
<td style="width: 497px;">Elemen Angin melambangkan kemudahan untuk bergerak dan masuk ke segala kondisi. Seseorang yang teliti dan berhati-hati, terukur dalam bicara dan bertindak serta dalam pengambilan keputusan.</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<p>Arbiter memiliki karakteristik warna hijau terang yang terkait langsung dengan kedamaian dalam gerak, dengan kekuatan mediasi, penengah dalam perbedaan, penghindaran konflik, bersifat praktis dan membumi, hasrat bermain dengan optimisme dan keceriaan .</p>
<h2 style="text-align: center;"><br /><span style="color: #339966;">KEKUATAN ARBITER : KETELITIAN</span><br /><br /></h2>
<table style="height: 78px; width: 100%; background: #f5f5f5; padding: 20; background-color: #339966;">
<tbody>
<tr>
<td style="width: 30%; padding: 20;">
<p><span style="color: #ffffff;"><strong>Sisi Produktif</strong></span></p>
</td>
<td style="width: 70%;">
<p><span style="color: #ffffff;">Kompeten, kesehatan, selera bagus, efisien, kebahagiaan, universal, istirahat, kesadaran, ketabahan, semangat, santai, keingintahuan.</span></p>
</td>
</tr>
<tr>
<td style="width: 174px;">
<p><span style="color: #ffffff;"><strong>Sisi Tantangan</strong></span></p>
</td>
<td style="width: 416px;">
<p><span style="color: #ffffff;">Cemburu, kualitas rendah, tidak berharga, tertolak, kerapuhan, kecemasan, indesisif, mengorbankan diri, kekhawatiran.</span></p>
</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<h2 class="p1" style="text-align: center;"><span style="color: #339966;">SELF-LEADERSHIP</span></h2>
<p>Pemimpin yang senantiasa mampu bergerak cepat, fleksibel terhadap kemungkinan yang terjadi, dalam hubungannya dengan berbagai macam kepentingan.<br /><br />Pemimpin model ini tidak mudah lelah untuk menjadi jembatan atas sebuah hubungan dengan orang lain, dan untuk mendapatkan kesepakatan yang diperlukan demi kemajuan bersama. <br /><br />Seorang pemimpin yang bertumpu pada fakta-fakta, dan mampu menunjukkan kepada orang lain tentang peluang yang ada serta pengambilan keputusan yang lebih efektif bagi semua pihak.</p>
<p class="p1">&nbsp;</p>' WHERE id=4;


UPDATE `shiokaya`.`contents` SET `content_text` ='<h2 style="text-align: center;"><span style="color: #008000;">MERCHANT</span></h2>
<p>Para pemilik shio ini merupakan negosiator dan pedagang yang handal.</p>
<p>Mereka mampu mencari peluang karena selalu bekerja dalam parameter tertentu berdasarkan data.</p>
<p>Shio ini juga memiliki kemampuan menampung informasi apapun secara luas dan memanfaatkannya untuk mencari peluang dan memperoleh keberhasilan yang lebih besar</p>
<table style="width: 100%; height: 71px; background-color: #f5f5f5;">
<tbody>
<tr>
<td style="width: 100px;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-MERCHANT.png" width="100" height="100" /></td>
<td style="width: 497px;">Elemen Tanah melambangkan penyedia tempat dan dukungan bagi seluruh kehidupan. Seorang yang bisa mengarahkan perhatian untuk melihat yang bisa dilakukan dan untuk membawa kesejahteraan bagi sekitarnya.</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<p>Merchant memiliki karakteristik warna hijau pekat yang berkaitan dengan alam, yang berguna untuk membantu dalam situasi tertekan agar lebih mampu menyeimbangkan dan menenangkan emosi. Warna ini juga melambangkan penyembuhan serta mendorong perasaan empati.</p>
<h2 style="text-align: center;"><br /><span style="color: #008000;">KEKUATAN MERCHANT : PELUANG</span><br /><br /></h2>
<table style="height: 78px; width: 100%; background: #f5f5f5; padding: 20; background-color: #008000;">
<tbody>
<tr>
<td style="width: 30%; padding: 20;">
<p><span style="color: #ffffff;"><strong>Sisi Produktif</strong></span></p>
</td>
<td style="width: 70%;">
<p><span style="color: #ffffff;">Kehidupan, kedamaian, kemudahan, harmoni, empati, kesejukan, keberuntungan, pemulihan, jaminan, kesadaran, keamanan, ketabahan.</span></p>
</td>
</tr>
<tr>
<td style="width: 174px;">
<p><span style="color: #ffffff;"><strong>Sisi Tantangan</strong></span></p>
</td>
<td style="width: 416px;">
<p class="p1"><span class="s1" style="color: #ffffff;">Terperangkap, tersesat, kebosanan, stagnasi, superior, ambisi, keserakahan, ketertutupan, homeostasis.</span></p>
</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<h2 class="p1" style="text-align: center;"><span style="color: #008000;">SELF-LEADERSHIP</span></h2>
<p>&nbsp;</p>
<p>Pemimpin yang memiliki sifat keteguhan, yang bisa menjadi landasan pijakan dan memberi kehidupan bagi organisasi atau tim yang dipimpinnya.</p>
<p>Model kepemimpinan yang fleksibel ketika berhubungan ke arah manapun, karena ia sanggup menopang apapun, yang tidak mudah lelah untuk menciptakan peluang yang terbentuk dari berbagai macam sumber.</p>
<p>Pemimpin model ini bisa menjelma menjadi besar atau kecil, sesuai dengan kebutuhannya, serta tidak mudah terganggu atas penolakan, dimana kekuatan alaminya adalah bertumpu pada fakta yang ada dalam mengambil keputusan.</p>' WHERE id=5;


UPDATE `shiokaya`.`contents` SET `content_text` ='<h2 style="text-align: center;"><span style="color: #33cccc;">PRIEST</span></h2>
<p>Para pemilik shio ini adalah penjaga yang baik, sabar serta dapat diandalkan.</p>
<p>Mereka selalu berhati-hati dalam melakukan hal apapun, terstruktur dan cerdas dalam mengelola risiko.</p>
<p>Mereka juga sangat tegas dalam menegakkan aturan sehingga ditakuti oleh para pelanggar aturan maupun para tim di bawahnya.</p>
<p>Namun di sisi lain mereka sangat safety-player sehingga seringkali kehilangan peluang yang ada.</p>
<table style="width: 100%; height: 71px; background-color: #f5f5f5;">
<tbody>
<tr>
<td style="width: 100px;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-KNIGHT.png" width="100" height="100" /></td>
<td style="width: 497px;">Elemen Samudra melambangkan keluasan dan kelapangan, sebagai muara yang menerima berbagai aliran air. Seorang yang berpikiran terbuka, bersedia menyerap berbagai hal baru serta mau menerima masukan dengan lapang dada.</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<p>Priest memiliki karakteristik warna tosca yang melambangkan konsentrasi, keseimbangan emosional, stabilitas diri, dan juga kesabaran, yang menghadirkan kejernihan pikiran dan kepercayaan diri, mengendalikan dan menyembuhkan untuk mencapai keamanan dan kestabilan.</p>
<h2 style="text-align: center;"><br /><span style="color: #33cccc;">KEKUATAN PRIEST : KEAMANAN</span><br /><br /></h2>
<table style="height: 78px; width: 100%; background: #f5f5f5; padding: 20; background-color: #33cccc;">
<tbody>
<tr>
<td style="width: 30%; padding: 20;">
<p><span style="color: #000000;"><strong>Sisi Produktif</strong></span></p>
</td>
<td style="width: 70%;">
<p><span style="color: #000000;">Keseimbangan, stabilitas, ketenangan, kesabaran, menghadirkan, keteraturan, mempengaruhi, terorganisir, kelembutan, ketepatan.</span></p>
</td>
</tr>
<tr>
<td style="width: 174px;">
<p><span style="color: #000000;"><strong>Sisi Tantangan</strong></span></p>
</td>
<td style="width: 416px;">
<p class="p1"><span class="s1" style="color: #000000;">Egois (self-centered), narsistik, mengabaikan, misterius, overaktif, ketidakseimbangan emosi, menyembunyikan, menahan, kebingungan.</span></p>
</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<h2 class="p1" style="text-align: center;"><span style="color: #33cccc;">SELF-LEADERSHIP</span></h2>
<p>&nbsp;</p>
<p>Pemimpin dengan keingintahuan dan wawasan yang luas, yang mampu mengatasi berbagai macam gejolak dalam dirinya dengan baik.</p>
<p>Tipe pemimpin yang mampu untuk menjadi penjaga atas keterlaksanaan sebuah hal atau aset, dimana hal ini berarti bahwa ia adalah pemimpin yang mampu menghasilkan keberhasilan tanpa harus melepas aset yang dimilikinya berupa sumber daya apapun (SDM, properti, kapital, dll).</p>
<p>Seorang pemimpin yang mempunyai kemampuan menampung berbagai masalah yang datang, dan mengubahnya menjadi sebuah kondisi yang menguntungkan.</p>' WHERE id=6;


UPDATE `shiokaya`.`contents` SET `content_text` ='<h2 style="text-align: center;"><span style="color: #3366ff;">MARSHAL</span></h2>
<p>Para pemilik shio ini merupakan penganalisa dan regulator yang baik.</p>
<p>Mereka sangat cocok berada dalam tim belakang layar karena sangat terencana dan pengatur yang<br />handal.</p>
<p>Kemampuan keras yang dimilikinya membuat mereka mampu memanfaatkan kekuatan waktu dan prioritas yang tersusun dengan baik untuk mengambil peluang yang dilewatinya.</p>
<table style="width: 100%; height: 71px; background-color: #f5f5f5;">
<tbody>
<tr>
<td style="width: 100px;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-MARSHAL.png" width="100" height="100" /></td>
<td style="width: 497px;">Elemen Air melambangkan kemampuan untuk berubah mengikuti wadahnya dan akan turun ke tempat yang lebh rendah. Seorang yang mampu untuk beradaptasi dengan baik, serta mampu untuk bekerja dengan baik dari balik layar.</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<p>Marshal memiliki karakteristik warna biru yang melambangkan kepercayaan dominan, cerdas, dan menampilkan keseriusan untuk hal-hal yang sifatnya tentang pencapaian, dengan menghadirkan citra keluasan, stabilitas, kesejukan, juga relaksasi.</p>
<h2 style="text-align: center;"><br /><span style="color: #3366ff;">KEKUATAN MARSHAL : PERENCANAAN</span><br /><br /></h2>
<table style="height: 78px; width: 100%; background: #f5f5f5; padding: 20; background-color: #3366ff;">
<tbody>
<tr>
<td style="width: 30%; padding: 20;">
<p><span style="color: #ffffff;"><strong>Sisi Produktif</strong></span></p>
</td>
<td style="width: 70%;">
<p><span style="color: #ffffff;">Kepercayaan, konsistensi, konsentrasi, ketenangan, keyakinan, keseriusan, professional, artistik, kekuatan, jernih, melankolis.</span></p>
</td>
</tr>
<tr>
<td style="width: 174px;">
<p><span style="color: #ffffff;"><strong>Sisi Tantangan</strong></span></p>
</td>
<td style="width: 416px;">
<p><span style="color: #ffffff;">Kaku, sulit akrab, keraguan, dingin, keras kepala, bangga diri, cuek, tidak ramah, tidak empatik, kesedihan, kesendirian, kesunyian.</span></p>
</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<h2 class="p1" style="text-align: center;"><span style="color: #3366ff;">SELF-LEADERSHIP</span></h2>
<p>&nbsp;</p>
<p>Pemimpin dengan kekuatan terbesarnya adalah strategi yang komprehensif, dalam proses mengusahakan keberhasilan bagi organisasi atau tim yang dipimpinnya.</p>
<p>Pemimpin yang bisa menjamin keamanan serta menunjukkan kewibawaan diri, dengan perhitungan yang matang.<br /><br />Model kepemimpinan ini juga mampu menyelesaikan tantangan yang ada di sekitar, dalam perjalanannya menuju target yang diinginkan, mampu untuk terus berjalan melalui berbagai penghalang dan tantangan yang ditemuinya.</p>' WHERE id=7;


UPDATE `shiokaya`.`contents` SET `content_text` ='<h2 style="text-align: center;"><span style="color: #993366;">ALCHEMIST</span></h2>
<p>Para pemilik shio ini merupakan penyempurna dan finisher terbaik.</p>
<p>Mereka juga cerdas dalam melakukan efisiensi dan efektivitas.</p>
<p>Kemampuan beradaptasi yang tinggi membuat mereka memiliki sifat rendah hati yang sangat natural. Di sisi lain mereka frontal dalam usaha mengejar setiap peluang yang ada.</p>
<p>&nbsp;</p>
<table style="width: 100%; height: 71px; background-color: #f5f5f5;">
<tbody>
<tr>
<td style="width: 100px;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-ALCHEMIST.png" width="100" height="100" /></td>
<td style="width: 497px;">Elemen Bulan melambangkan sumber cahaya di malam hari, penerang kehidupan dari kegelapan malam. Seorang yang bisa menjadi penuntun dan memberikan pencerahan kepada sekitarnya dalam kondisi gelap sekalipun.</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<p>Alchemist memiliki karakteristik warna ungu yang melambangkan pengharapan yang besar dan kepekaan, dipersepsikan terkait dengan imajinasi, spiritualitas, dan misterius, serta mencerminkan keagungan dan kebijaksanaan yang terkait dengan kesenangan dan kemewahan.</p>
<h2 style="text-align: center;"><br /><span style="color: #993366;">KEKUATAN ALCHEMIST : KETERATURAN</span><br /><br /></h2>
<table style="height: 78px; width: 100%; background: #f5f5f5; padding: 20; background-color: #993366;">
<tbody>
<tr>
<td style="width: 30%; padding: 20;">
<p><span style="color: #ffffff;"><strong>Sisi Produktif</strong></span></p>
</td>
<td style="width: 70%;">
<p><span style="color: #ffffff;">Magis, spiritualitas, misterius, imajinasi, sensitivitas, obsesif, ambisius, martabat, berkorban, kebijaksanaan, visioner, orisinalitas, kekayaan.</span></p>
</td>
</tr>
<tr>
<td style="width: 174px;">
<p><span style="color: #ffffff;"><strong>Sisi Tantangan</strong></span></p>
</td>
<td style="width: 416px;">
<p><span style="color: #ffffff;">Kurang teliti, kesendirian, bertele-tele, perfeksionis, suram, ketidak-sabaran, arogan, ktidak-berdayaan, apatis, terjebak masa lalu, frustasi.</span></p>
</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<h2 class="p1" style="text-align: center;"><span style="color: #993366;">SELF-LEADERSHIP</span></h2>
<p>Pemimpin yang mampu memberikan penerang bagi diri dan sekitarnya, dengan membangun sistem yang fair berbasis aturan dan prosedur untuk mencapai produktivitas optimal.</p>
<p>Pemimpin yang mampu merakit banyak hal yang belum terlihat orang lain, menjadi sebuah kesatuan yang bermanfaat.</p>
<p>Kekuatan terbesarnya adalah mencapai tujuan dengan membangun sistem yang teratur.</p>
<p><br />Pemimpin model ini tidak mudah lelah untuk menjadi jembatan atas sebuah hubungan dengan orang lain, dan untuk mendapatkan kesepakatan yang diperlukan demi kemajuan bersama. <br /><br />Seorang pemimpin yang bertumpu pada fakta-fakta, dan mampu menunjukkan kepada orang lain tentang peluang yang ada serta pengambilan keputusan yang lebih efektif bagi semua pihak.</p>
<p class="p1">&nbsp;</p>' WHERE id=8;
```