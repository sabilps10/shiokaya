CREATE TABLE `transactions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `transaction_id` varchar(100) DEFAULT NULL,
  `account_id` bigint(21) DEFAULT NULL,
  `transaction_status` tinyint(3) DEFAULT '0',
  `product_id` bigint(21) DEFAULT NULL,
  `product_type` varchar(30) DEFAULT 'subscription',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `transaction_id` (`account_id`,`transaction_id`) USING BTREE,
  UNIQUE KEY `product` (`account_id`,`product_id`,`product_type`) USING BTREE,

) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

CREATE TABLE `payment_logs` (
  `id` bigint(21) NOT NULL AUTO_INCREMENT,
  `transaction_id` varchar(255) DEFAULT NULL,
  `trx_id` varchar(255) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `status_code` varchar(5) DEFAULT NULL,
  `via` varchar(100) DEFAULT NULL,
  `channel` varchar(100) DEFAULT NULL,
  `sid` varchar(255) DEFAULT NULL,
  `total` varchar(255) DEFAULT NULL,
  `fee` varchar(255) DEFAULT NULL,
  `va` varchar(100) DEFAULT NULL,
  `uniqamount` varchar(100) DEFAULT NULL,
  `buyer_name` varchar(255) DEFAULT NULL,
  `buyer_email` varchar(255) DEFAULT NULL,
  `buyer_phone` varchar(100) DEFAULT NULL,
  `expired` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique` (`transaction_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;



CREATE TABLE `adcampaigns` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `campaign_id` varchar(100) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `landing_url` varchar(255) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `campaign_idx` (`campaign_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


CREATE TABLE `adcampaigns_assets` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `campaign_id` bigint DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx` (`campaign_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE `affiliates` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `company` varchar(100) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `phone` varchar(100) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `address` varchar(100) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `industry` varchar(100) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `instagram` varchar(100) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `twitter` varchar(100) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `facebook` varchar(100) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `affiliate_no` varchar(100) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `account_id` bigint(21) DEFAULT NULL,
  `email` varchar(100) COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `idx_affino` (`affiliate_no`) USING BTREE,
  UNIQUE KEY `uniq` (`account_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE `adlogs` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `logdate` date DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `campaign_id` bigint DEFAULT NULL,
  `affiliate_id` bigint DEFAULT NULL,
  `total` int DEFAULT '0',
  `logtype` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unik` (`logdate`,`campaign_id`,`affiliate_id`,`logtype`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


CREATE TABLE `affiliate_summary` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `affiliate_id` bigint DEFAULT NULL,
  `impressions` bigint DEFAULT '0',
  `clicks` bigint DEFAULT '0',
  `buys` bigint DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_aff` (`affiliate_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;




CREATE TABLE `vouchers` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `code` varchar(45) DEFAULT NULL,
  `expired_time` datetime DEFAULT NULL,
  `qty` int DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `discount` float(4,2) DEFAULT '0.00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;


CREATE TABLE `voucher_redeems` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `account_id` bigint DEFAULT NULL,
  `voucher_id` bigint DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `redeem` (`account_id`,`voucher_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

ALTER TABLE `shiokaya`.`vouchers` 
ADD COLUMN `discount` FLOAT(4,2) NULL DEFAULT 0.0 AFTER `updated_at`;



CREATE TABLE `shiokaya`.`price` (
  `id` BIGINT(21) NOT NULL AUTO_INCREMENT,
  `price` INT(10) NULL,
  `created_at` DATETIME NULL,
  `updated_at` DATETIME NULL,
  PRIMARY KEY (`id`));

  ALTER TABLE `shiokaya`.`transactions` 
ADD COLUMN `price_id` BIGINT(21) NULL DEFAULT 0 AFTER `updated_at`,
ADD COLUMN `affiliate_id` BIGINT(21) NULL DEFAULT 0 AFTER `price_id`,
ADD COLUMN `voucher_id` BIGINT(21) NULL DEFAULT 0 AFTER `affiliate_id`,
ADD COLUMN `transaction_amount` FLOAT(14,3) NULL DEFAULT 0 AFTER `voucher_id`;
