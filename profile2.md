<h2 style="text-align: center;"><span style="color: #993366;">ALCHEMIST</span></h2>
<p>Para pemilik shio ini merupakan penyempurna dan finisher terbaik.</p>
<p>Mereka juga cerdas dalam melakukan efisiensi dan efektivitas.</p>
<p>Kemampuan beradaptasi yang tinggi membuat mereka memiliki sifat rendah hati yang sangat natural. Di sisi lain mereka frontal dalam usaha mengejar setiap peluang yang ada.</p>
<p>&nbsp;</p>
<p><strong><span style="color: #993366;">Deskripsi Karakter ALCHEMIST :</span></strong></p>
<ul>
<li>Memajukan bisnis dengan cara membuat hal menjadi sederhana, namun memperbanyak peluang atau sumber penghasilan, kecil tapi dilakukan dengan volume yang besar (multiple source of income).</li>
<li>Mengembalikan energi ketika lelah, dengan melakukan hal-hal yang bersifat pribadi, hanya dengan sendiri atau dengan orang yang benar-benar dianggap dekat (self-recharging).</li>
<li>Mencari detail dalam berbagai hal terlebih dahulu, tertarik pada penjelasan yang sifatnya spesifik, akan cukup bermasalah untuk mau menerima gambaran umum terlebih dulu.</li>
<li>Terkait dengan fokus pada produk, tugas, dan sistem, dimana orang di sekitarnya diperlakukan sebagai objek, perasaan merupakan hal yang tidak profesional karena alchemist sangat berorientasi pada tugas.</li>
<li>Memilih untuk melihat kesempatan dan kemungkinan yang ada, daripada aturan yang harus diikuti, dengan fokus pada hasil, bukan pada caranya (rules is made to be broken).</li>
<li>Mengedepankan peluang yang bisa dicapai di masa depan dengan mempersiapkan berbagai kemungkinan, mementingkan gagasan yang tersirat, serta menghargai inspirasi dan imajinasi.</li>
<li>Pengambil resiko yang berani, mengedepankan reward yang mungkin didapat ketika maju dan mengambil tanggung jawab, menghadapi segala cercaan atau kritik yang dilontarkan.</li>
<li>Mengutamakan inovasi dari tiap proses yang dilakukan, dengan cara yang selalu di perbaiki, untuk menghasilkan kemajuan dan kesempurnaan, serta perubahan yang berkelanjutan.</li>
<li>Memiliki kebutuhan untuk mengeksplorasi hal-hal disekitar melalui kata-kata yang sifatnya logis di dalam pikiran, dengan seringkali tidak terstruktur dalam penyampaiannya.</li>
<li>Mengutamakan kreativitas dalam usaha mencapai prioritas target yang dituju, evaluatif terhadap langkah baru yang diambil, dan mampu menjadi pendorong untuk memastikan semuanya tercapai.</li>
<li>Fokus pada metode yang digunakan, dengan tujuan akhir adalah produk yang diinginkan, dimana sistem boleh diubah, dengan segala pertimbangan atas kualitas produk akhir yang diinginkan.</li>
<li>Berpikir dengan pertimbangan yang luas, namun tidak terlalu dalam, sehingga cepat dalam mengambil keputusan, walaupun terkadang ada beberapa konsekwensi yang luput dari pertimbangan.</li>
</ul>
<p>&nbsp;</p>
<table style="width: 100%; height: 71px; background-color: #f5f5f5;">
<tbody>
<tr>
<td style="width: 100px;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-ALCHEMIST.png" width="100" height="100" /></td>
<td style="width: 497px;">Elemen Bulan melambangkan sumber cahaya di malam hari, penerang kehidupan dari kegelapan malam. Seorang yang bisa menjadi penuntun dan memberikan pencerahan kepada sekitarnya dalam kondisi gelap sekalipun.</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<p>Alchemist memiliki karakteristik warna ungu yang melambangkan pengharapan yang besar dan kepekaan, dipersepsikan terkait dengan imajinasi, spiritualitas, dan misterius, serta mencerminkan keagungan dan kebijaksanaan yang terkait dengan kesenangan dan kemewahan.</p>
<h2 style="text-align: center;"><br /><span style="color: #993366;">KEKUATAN ALCHEMIST : KETERATURAN</span><br /><br /></h2>
<table style="height: 78px; width: 100%; background: #f5f5f5; padding: 20; background-color: #993366;">
<tbody>
<tr>
<td style="width: 30%; padding: 20;">
<p><span style="color: #ffffff;"><strong>Sisi Produktif</strong></span></p>
</td>
<td style="width: 70%;">
<p><span style="color: #ffffff;">Magis, spiritualitas, misterius, imajinasi, sensitivitas, obsesif, ambisius, martabat, berkorban, kebijaksanaan, visioner, orisinalitas, kekayaan.</span></p>
</td>
</tr>
<tr>
<td style="width: 174px;">
<p><span style="color: #ffffff;"><strong>Sisi Tantangan</strong></span></p>
</td>
<td style="width: 416px;">
<p><span style="color: #ffffff;">Kurang teliti, kesendirian, bertele-tele, perfeksionis, suram, ketidak-sabaran, arogan, ktidak-berdayaan, apatis, terjebak masa lalu, frustasi.</span></p>
</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<h2 class="p1" style="text-align: center;"><span style="color: #993366;">SELF-LEADERSHIP</span></h2>
<p>Pemimpin yang mampu memberikan penerang bagi diri dan sekitarnya, dengan membangun sistem yang fair berbasis aturan dan prosedur untuk mencapai produktivitas optimal.</p>
<p>Pemimpin yang mampu merakit banyak hal yang belum terlihat orang lain, menjadi sebuah kesatuan yang bermanfaat.</p>
<p>Kekuatan terbesarnya adalah mencapai tujuan dengan membangun sistem yang teratur.</p>
<p><br />Pemimpin model ini tidak mudah lelah untuk menjadi jembatan atas sebuah hubungan dengan orang lain, dan untuk mendapatkan kesepakatan yang diperlukan demi kemajuan bersama. <br /><br />Seorang pemimpin yang bertumpu pada fakta-fakta, dan mampu menunjukkan kepada orang lain tentang peluang yang ada serta pengambilan keputusan yang lebih efektif bagi semua pihak.</p>
<h2 class="p1" style="text-align: center; color: #993366;"><br /><strong>PROFIL: UTAMA &amp; BAYANGAN</strong>&nbsp;</h2>
<p>Profil Utama (main profile) Shio Kaya adalah bagian dominan dari diri Anda, yang sebenarnya mendorong dan menginspirasi diri Anda.</p>
<p>Profil utama (main profile) mencakup karakteristik alamiah Anda, yang dapat membantu Anda dalam membangun keberhasilan dengan lebih efektif dan efisien, menghemat waktu dan tenaga Anda.</p>
<p>Profil Bayangan (shadow profile) Shio Kaya merupakan bagian resesif (non-dominan) dari diri Anda, dengan hanya beberapa karakteristik yang muncul.</p>
<p>Profil bayangan (shadow profile) merupakan karakteristik yang bersifat seperti bayangan, bagian dari diri Anda, tak terpisahkan, tapi bukan mencerminkan diri Anda sepenuhnya.<br /><br />Shio Bayangan dari <strong>Alchemist</strong> adalah <strong><em>Marshal</em></strong> dan <strong><em>Wizard</em></strong>.</p>
<p>&nbsp;</p>
<hr />
<h2 style="text-align: center;"><strong><br />ALCHEMIST</strong>&nbsp;</h2>
<table style="height: 48px; background-color: #993366;" width="100%">
<tbody>
<tr style="height: 36.5312px;">
<td style="width: 288px; height: 36.5312px; text-align: center;"><span style="color: #ffffff;"><strong>KARAKTER ALAMI</strong></span></td>
</tr>
<tr style="height: 222px;">
<td style="width: 288px; height: 222px;"><span style="color: #ffffff;">&nbsp;</span>
<ul>
<li><span style="color: #ffffff;">Mampu mengendalikan dan mengatur orang lain secara tidak langsung</span></li>
<li><span style="color: #ffffff;">Seringkali sangat memperhatikan detail yang rinci</span></li>
<li><span style="color: #ffffff;">Sangat ahli dalam menyelesaikan dan menyempurnakan sesuatu</span></li>
<li><span style="color: #ffffff;">Cenderung perfeksionis, bahkan ketika akan memulai sesuatu</span></li>
<li><span style="color: #ffffff;">Mengelola organisasi dengan sistematis dan teratur</span></li>
<li><span style="color: #ffffff;">Cenderung untuk senang memegang kendali</span></li>
<li><span style="color: #ffffff;">Terus menerus belajar dan melakukan perubahan</span></li>
<li><span style="color: #ffffff;">Tidak terlalu memperhatikan penyampaian dan tampilan</span></li>
</ul>
<p><span style="color: #ffffff;">&nbsp;</span></p>
</td>
</tr>
</tbody>
</table>
<table style="height: 48px; background-color: #993366;" width="100%">
<tbody>
<tr style="height: 36.5312px;">
<td style="width: 288px; height: 36.5312px; text-align: center;"><span style="color: #ffffff;"><strong>FAKTOR SUKSES</strong></span></td>
</tr>
<tr style="height: 204px;">
<td style="width: 288px; height: 204px;">
<ul>
<li><span style="color: #ffffff;">Handal dalam membuat sistem dan prosedur</span></li>
<li><span style="color: #ffffff;">Hal yang dikerjakan terjamin aman dan terorganisir dengan baik</span></li>
<li><span style="color: #ffffff;">Selalu menghasilkan perbaikan yang berkelanjutan</span></li>
<li><span style="color: #ffffff;">Perencanaan sangat matang dan sudah mempertimbangkan banyak aspek</span></li>
<li><span style="color: #ffffff;">Organisasi terbangun secara berkelanjutan dan autopilot</span></li>
<li><span style="color: #ffffff;">Semua terkontrol baik dan tanggap bila ada kesalahan</span></li>
<li><span style="color: #ffffff;">Mampu memprediksi tren ke depan dan beradaptasi dengan cepat</span></li>
<li><span style="color: #ffffff;">Fokus pada menghasilkan fungsi yang efektif dan efisien</span></li>
</ul>
<p><span style="color: #ffffff;">&nbsp;</span></p>
</td>
</tr>
</tbody>
</table>
<table style="height: 48px; background-color: #993366;" width="100%">
<tbody>
<tr style="height: 10.5312px;">
<td style="width: 288px; height: 10.5312px;">
<p style="text-align: center;"><span style="color: #ffffff;"><strong>FAKTOR KEGAGALAN</strong></span></p>
</td>
</tr>
<tr style="height: 258px;">
<td style="width: 288px; height: 258px;"><span style="color: #ffffff;">&nbsp;</span>
<ul>
<li><span style="color: #ffffff;">Cenderung tidak memperhatikan faktor manusia saat membuat sistem</span></li>
<li><span style="color: #ffffff;">Terjebak pada mengerjakan hal-hal yang terlalu detail, melupakan gambaran besarnya</span></li>
<li><span style="color: #ffffff;">Tidak memperhatikan tim yang mengerjakan perlu untuk berhenti sesekali</span></li>
<li><span style="color: #ffffff;">Seringkali lama dalam memulai sesuatu akibat banyak pertimbangan</span></li>
<li><span style="color: #ffffff;">Bisa melupakan faktor perasaan manusia dalam menjalankan keteraturan organisasi</span></li>
<li><span style="color: #ffffff;">Bisa terjebak mengendalikan hingga detail, menciptakan rasa frustasi bagi tim</span></li>
<li><span style="color: #ffffff;">Seringkali sudah melakukan tindakan sia-sia, antisipasi sebelum waktunya</span></li>
<li><span style="color: #ffffff;">Bisa menimbulkan masalah saat customer meminta tampilan yang baik</span></li>
</ul>
<p><span style="color: #ffffff;">&nbsp;</span></p>
</td>
</tr>
</tbody>
</table>
<hr />
<h2 style="text-align: center;"><strong><br />MARSHAL</strong>&nbsp;</h2>
<table style="height: 48px; background-color: #3498db;" width="100%">
<tbody>
<tr style="height: 36.5312px;">
<td style="width: 288px; height: 36.5312px; text-align: center;"><span style="color: #ffffff;"><strong>KARAKTER ALAMI</strong></span></td>
</tr>
<tr style="height: 222px;">
<td style="width: 288px; height: 222px;"><span style="color: #ffffff;">&nbsp;</span>
<ul>
<li><span style="color: #ffffff;">Pengendali yang handal dari balik layar.</span></li>
<li><span style="color: #ffffff;">Mencintai detail, sampai hal terkecil sekalipun.</span></li>
<li><span style="color: #ffffff;">Mampu menggunakan aset tanpa harus memiliki aset tersebut.</span></li>
<li><span style="color: #ffffff;">Lebih mementingkan angka dari pada sebuah hubungan.</span></li>
<li><span style="color: #ffffff;">Menyukai kepastian dan tidak menyukai resiko yang tidak terukur.</span></li>
<li><span style="color: #ffffff;">Tidak mudah menyerah untuk menemukan celah.</span></li>
<li><span style="color: #ffffff;">Cenderung menyelesaikan apa yang sudah dimulai.</span></li>
<li><span style="color: #ffffff;">Tertib, terstruktur dan suka penghematan.</span></li>
</ul>
<p><span style="color: #ffffff;">&nbsp;</span></p>
</td>
</tr>
</tbody>
</table>
<table style="height: 48px; background-color: #3498db;" width="100%">
<tbody>
<tr style="height: 36.5312px;">
<td style="width: 288px; height: 36.5312px; text-align: center;"><span style="color: #ffffff;"><strong>FAKTOR SUKSES</strong></span></td>
</tr>
<tr style="height: 204px;">
<td style="width: 288px; height: 204px;">
<ul>
<li><span style="color: #ffffff;">Memberikan hasil atau proses yang nyaris sempurna.</span></li>
<li><span style="color: #ffffff;">Mampu menciptakan banyak peluang yang bisa diambil.</span></li>
<li><span style="color: #ffffff;">Fokus pada kesempurnaan proses maupun hasil yang harus dicapai</span></li>
<li><span style="color: #ffffff;">Hal yang dilakukan terjamin dan aman karena semua sudah diperhitungkan</span></li>
<li><span style="color: #ffffff;">Persisten dan teliti dalam proses untuk mencapai tujuan</span></li>
<li><span style="color: #ffffff;">Menggunakan kapital dan aset secara efektif dan efisien</span></li>
<li><span style="color: #ffffff;">Segala sesuatu dikerjakan sampai selesai, dan dapat di evaluasi untuk perbaikan.</span></li>
<li><span style="color: #ffffff;">Proyek atau program bisa berjalan tanpa penghalang.</span></li>
</ul>
<p><span style="color: #ffffff;">&nbsp;</span></p>
</td>
</tr>
</tbody>
</table>
<table style="height: 48px; background-color: #3498db;" width="100%">
<tbody>
<tr style="height: 10.5312px;">
<td style="width: 288px; height: 10.5312px;">
<p style="text-align: center;"><span style="color: #ffffff;"><strong>FAKTOR KEGAGALAN</strong></span></p>
</td>
</tr>
<tr style="height: 258px;">
<td style="width: 288px; height: 258px;"><span style="color: #ffffff;">&nbsp;</span>
<ul>
<li><span style="color: #ffffff;">Seringkali menjadi tidak terlihat dan terabaikan</span></li>
<li><span style="color: #ffffff;">Bisa terjebak pada micro-management dan terhenti pada satu hal terlalu lama</span></li>
<li><span style="color: #ffffff;">Tanpa pengelolaan yang baik, bisa menimbulkan konflik dengan banyak mitra</span></li>
<li><span style="color: #ffffff;">Cenderung tidak menganggap penting perasaan tim / mitra.</span></li>
<li><span style="color: #ffffff;">Bisa terlihat keras kepala dan mendominasi sebuah proses</span></li>
<li><span style="color: #ffffff;">Terkadang jadi terlihat pelit dan terlalu kaku akan banyak hal</span></li>
<li><span style="color: #ffffff;">Sering terjebak memikirkan hal-hal yang belum selesai terus menerus.</span></li>
<li><span style="color: #ffffff;">Menjadi lebih egois dan tidak percaya orang lain.</span></li>
</ul>
<p>&nbsp;</p>
</td>
</tr>
</tbody>
</table>
<hr />
<h2 style="text-align: center;"><strong><br />WIZARD</strong>&nbsp;</h2>
<table style="height: 48px; background-color: #cc0000;" width="100%">
<tbody>
<tr style="height: 36.5312px;">
<td style="width: 288px; height: 36.5312px; text-align: center;"><span style="color: #ffffff;"><strong>KARAKTER ALAMI</strong></span></td>
</tr>
<tr style="height: 222px;">
<td style="width: 288px; height: 222px;"><span style="color: #ffffff;">&nbsp;</span>
<ul>
<li><span style="color: #ffffff;">Pemikir dengan visi yang besar, Semua dipikirkan sendiri</span></li>
<li><span style="color: #ffffff;">Pemikiran yang sering melompat-lompat</span></li>
<li><span style="color: #ffffff;">Sulit menahan diri untuk tidak mencipta dan berinovasi, kreativitas diatas segalanya.</span></li>
<li><span style="color: #ffffff;">Tidak suka tertahan pada proses dan hal-hal yang sifatnya detail</span></li>
<li><span style="color: #ffffff;">Sulit dimengerti pikirannya karena terlalu cepat dan tidak terstruktur</span></li>
<li><span style="color: #ffffff;">Seringkali terlalu optimis dalam menghadapi sesuatu yang baru</span></li>
</ul>
<p><span style="color: #ffffff;">&nbsp;</span></p>
</td>
</tr>
</tbody>
</table>
<table style="height: 48px; background-color: #cc0000;" width="100%">
<tbody>
<tr style="height: 36.5312px;">
<td style="width: 288px; height: 36.5312px; text-align: center;"><span style="color: #ffffff;"><strong>FAKTOR SUKSES</strong></span></td>
</tr>
<tr style="height: 204px;">
<td style="width: 288px; height: 204px;">
<ul>
<li><span style="color: #ffffff;">Sanggup menciptakan produk dan perubahan yang otentik</span></li>
<li><span style="color: #ffffff;">Kesuksesan diperoleh dari mendelegasikan banyak hal</span></li>
<li><span style="color: #ffffff;">Selalu menghasilkan perubahan yang berkelanjutan</span></li>
<li><span style="color: #ffffff;">Menghasilkan pemikiran yang spontan dan out-of-the box</span></li>
<li><span style="color: #ffffff;">Produktif dan selalu menghasilkan inovasi yang baru</span></li>
<li><span style="color: #ffffff;">Mampu memukau sekitarnya dengan konsep baru dan pemikiran yang besar</span></li>
<li><span style="color: #ffffff;">Menghasilkan ritme yang cepat dan sulit untuk ditebak bagi pesaing</span></li>
<li><span style="color: #ffffff;">Mau mencoba hal-hal baru dan percaya diri bahwa bisa berhasil</span></li>
</ul>
<p><span style="color: #ffffff;">&nbsp;</span></p>
</td>
</tr>
</tbody>
</table>
<table style="height: 48px; background-color: #cc0000;" width="100%">
<tbody>
<tr style="height: 10.5312px;">
<td style="width: 288px; height: 10.5312px;">
<p style="text-align: center;"><span style="color: #ffffff;"><strong>FAKTOR KEGAGALAN</strong></span></p>
</td>
</tr>
<tr style="height: 258px;">
<td style="width: 288px; height: 258px;">
<ul>
<li><span style="color: #ffffff;">Terkadang melupakan atau tidak mau tahu tentang proses yang harus dilalui</span></li>
<li><span style="color: #ffffff;">Tertekan karena ingin mencapai semuanya sendiri</span></li>
<li><span style="color: #ffffff;">Seringkali berubah-ubah dan mudah bosan</span></li>
<li><span style="color: #ffffff;">Pemikiran yang tidak terstruktur dan seringkali membingungkan</span></li>
<li><span style="color: #ffffff;">Terlampau sering tidak menyelesaikan apa yang sudah dimulai</span></li>
<li><span style="color: #ffffff;">Seringkali bukan merupakan manajer yang handal bagi eksekusi ide dan konsepnya</span></li>
<li><span style="color: #ffffff;">Membuat tim menjadi tergopoh-gopoh dan sulit untuk dipahami</span></li>
<li><span style="color: #ffffff;">Kegagalan sering terjadi karena terlalu optimis tanpa perhitungan matang</span></li>
</ul>
</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<hr />
<p><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/diagram.png" alt="" width="100%" /></p>
<p>&nbsp;</p>
<table style="height: 392px; width: 100%;">
<tbody>
<tr>
<td style="width: 15.5194%;">
<p style="text-align: center;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-ALCHEMIST.png" width="100" height="100" /><br />ALCHEMIST</p>
</td>
<td style="width: 81.4806%;">
<p>Penyempurnaan yang baik, penyelesaian proses, menemukan cara yang lebih baik dab cerdas, meningkatkan efesiensi dan efektivitas produk/sitem yang ada.</p>
<p>Seseorang yang memilki kemampuan beradaptasi dengan menempatkan diri sesuai dengạn dimana ia berada. mampu untuk tetap merendah dan secara frontal berusaha untuk mengejar setiap peluang yang ada.</p>
</td>
</tr>
<tr>
<td style="width: 15.5194%; text-align: center;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-WIZARD.png" width="100" height="100" />WIZARD</td>
<td style="width: 81.4806%;">
<p>Konseptor yang &thorn;aik, penggagas hal baru, kreatif, inovatif, pencipta ide, pemikir out-of-the-box, spontan, tidak teratur, impulsif, baik untuk memulai sesuatu yang baru.</p>
<p>Seseorang yang pampu berinovasi secara berkelanjutan, seringkali kurang bisa dipahami oleh banyak orang disekitarnya, namun sanggup berpikir out-of-the-box, dalam mengbasilkan konsep-konsep baru.</p>
</td>
</tr>
<tr>
<td style="width: 15.5194%; text-align: center;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-BARD.png" width="100" height="100" />BARD</td>
<td style="width: 81.4806%;">
<p>Promotor yang baik, pemasar yang berbakat, merek pribadi yang kuat, &thorn;ergerak pat, butyh perhatian, bersinar, mampy menginspiraşi orang lain, individu yang kokoh.</p>
<p>Seseorang yang mampu memberi energi kepada orang-orang di sekelilingnya, mendukung dan menyebarkan semangat secara konsisten, sehingga mampu menyampaikan segala hal dengan penerimaan yang relatif mudah.</p>
</td>
</tr>
<tr>
<td style="width: 15.5194%; text-align: center;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-PRIEST.png" width="100" height="100" />KNIGHT</td>
<td style="width: 81.4806%;">
<p>Influencer yang baik, pemimpip yang karismatik, manajer pelaksana, penyelenggara, administrator, pendukung, baik dalam mencirtakan dan mengelola hubungan, baik dalam manajemen sumber daya manusia, fleksibel.</p>
<p>Seorang yang selalu mampu memberi semangat pada orang-orang disekitarnya, dimana api mampu menerangi kegelapan dan menyambar kemanapun, sebingga ia bisa mempengaruhi lebih banyak orang untuk sejalan seiring berjalannya waktu.</p>
</td>
</tr>
<tr>
<td style="width: 15.5194%; text-align: center;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-ARBITER.png" width="100" height="100" />ARBITER</td>
<td style="width: 81.4806%;">
<p>Penengah yang baik, pembawa damai, representatif bijak, melihat alternatif sebagai manfaat, penghindar konflik, berinyestasi pada orang untuk manfaat jangka panjang.</p>
<p>Seseorang yang tegas şerta mampu menciptakan kemungkinan baru, walaupun sebelumnya dirasakan tidak ada jalan lain, dimana angin biasanya memang tidak terlibat namun bisa dirasakan kehadirannya, mengisi apapun dan dimanapun.</p>
</td>
</tr>
<tr>
<td style="width: 15.5194%; text-align: center;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-MERCHANT.png" width="100" height="100" />MERCHANT</td>
<td style="width: 81.4806%;">
<p>Negosiator ulung, pedagang yang handal, pencari peluang, pengguna waktu yang efektif dan efisien, fleksibel, bekerja dalam pola/parameter tertentų, berdasarkan data.</p>
<p>Seseorang yang mampu memanfaatkan kekuatan dari wawasan yg luas, yang menampung apapun dari segala penjuru, mencapai keberhasilan dengan menerima berbagai hal, dan memanfaatkannva untuk keberhasilan yang lebih besar.&nbsp;</p>
</td>
</tr>
<tr>
<td style="width: 15.5194%; text-align: center;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-KNIGHT.png" width="100" height="100" />PRIEST</td>
<td style="width: 81.4806%;">
<p>Penjaga yang baik, dapat diandalkan, sabar, berhati-hati, berbasis fakta, safety- player, terstruktur, unggul dengan analisis informasi, cerdas dalam mengelola risiko.</p>
<p>Seseorang yang mampu menegakkan aturan secara tegas, disegani oleh orang- orang dibawahnya, dan ditakuti oleh siapapun yang melakukan pelanggaran aturan, dengan keteguhan tinggi sehingga mampu dijadikan sebagai tauladan (role-model).</p>
</td>
</tr>
<tr>
<td style="width: 15.5194%; text-align: center;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-MARSHAL.png" width="100" height="100" />MARSHAL</td>
<td style="width: 81.4806%;">
<p>Penganalisa yang baik, prediktif, lebih efektif di belakang layar, terencana, berkomitmen, pengatur yang handal, berkemauan keras, regulator yang baik.</p>
<p>Seseorang yang kokoh dan bisa diandalkan, terbuka untuk berbagai peluang yang lewat atau dilewatinya, dan mampu memanfaatkan apapun yang saat ini dimilikinya, termasuk memanfaatkan kekuatan waktu dan prioritas-prioritas yang tersusun dengan baik.</p>
</td>
</tr>
</tbody>
</table>



***MARSHAL***


<h2 style="text-align: center;"><span style="color: #3366ff;">MARSHAL</span></h2>
<p>Para pemilik shio ini merupakan penganalisa dan regulator yang baik.</p>
<p>Mereka sangat cocok berada dalam tim belakang layar karena sangat terencana dan pengatur yang<br />handal.</p>
<p>Kemampuan keras yang dimilikinya membuat mereka mampu memanfaatkan kekuatan waktu dan prioritas yang tersusun dengan baik untuk mengambil peluang yang dilewatinya.<br /><br /></p>
<p><strong>Deskripsi karakter MARSHAL :</strong></p>
<ul>
<li>Memajukan bisnis dengan cara memperbanyak peluang atau sumber penghasilan, tidak masalah kecil, tapi dengan volume yang besar (multiple source of income).</li>
<li>Mengembalikan energi ketika lelah, dengan melakukan hal-hal yang bersifat pribadi, hanya dengan sendiri atau dengan orang yang benar-benar dianggap dekat (self-recharging).</li>
<li>Mencari detail dalam berbagai hal terlebih dahulu, tertarik pada penjelasan yang sifatnya spesifik, bermasalah untuk mau menerima gambaran umum terlebih dulu.</li>
<li>Lebih bisa berkinerja tinggi ketika berada di dalam lingkup kerja yang terkait dengan benda (produk, alat kerja, dll) dan sistem (prosedur, framework, dll).</li>
<li>Menjalani instruksi dan prosedur secara baik, tidak terganggu oleh rutinitas, namun mampu untuk melihat celah dan kesempatan dalam meraih target/tujuan dengan lebih cepat.</li>
<li>Mendahulukan fakta dan data yang konkret untuk memilih pengerjaan sesuatu di saat ini dan jelas, namun bisa memanfaatkannya untuk melihat peluang yang ada di depan dan segala kemungkinannya.</li>
<li>
<p>Memastikan keamanan dengan mempertimbangkan segala hal yang masuk dalam logika, probabilitas resiko, dan memilih dengan berani hal yang sudah diperhitungkan tersebut.</p>
</li>
<li>
<p>Mementingkan keteraturan dan prioritas, namun tetap melakukan perbaikan terus menerus selama proses berjalan, yang semuanya perlu diatur dengan rambu-rambu dan indikator yang jelas.</p>
</li>
<li>
<p>Memiliki kebutuhan untuk memahami dunia, melalui suara dan logika di dalam pikiran sendiri, dengan analisa logis yang berperan penting dalam pengambilan keputusan.</p>
</li>
<li>
<p>Mengutamakan hal yang dianggap prioritas, cenderung terstruktur dalam perencanaan maupun implementasinya, menjadi mesin pendorong untuk sistem yang dibangun atau dijalankan.</p>
</li>
<li>
<p>Fokus pada metode dan proses yang digunakan untuk mencapai target yang diinginkan, dengan seperangkan aturan dan prosedur yang harus ditaati oleh siapapun.</p>
</li>
<li>
<p>Berpikir dengan pertimbangan luas, termasuk konsekwensi dari sebuah pengalaman atau tindakan, dengan kemampuan eksplorasi emosi, perasaan, dan pengetahuan dalam pengambilan keputusan.</p>
</li>
</ul>
<p>&nbsp;</p>
<table style="width: 100%; height: 71px; background-color: #f5f5f5;">
<tbody>
<tr>
<td style="width: 100px;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-MARSHAL.png" width="100" height="100" /></td>
<td style="width: 497px;">Elemen Air melambangkan kemampuan untuk berubah mengikuti wadahnya dan akan turun ke tempat yang lebh rendah. Seorang yang mampu untuk beradaptasi dengan baik, serta mampu untuk bekerja dengan baik dari balik layar.</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<p>Marshal memiliki karakteristik warna biru yang melambangkan kepercayaan dominan, cerdas, dan menampilkan keseriusan untuk hal-hal yang sifatnya tentang pencapaian, dengan menghadirkan citra keluasan, stabilitas, kesejukan, juga relaksasi.</p>
<h2 style="text-align: center;"><br /><span style="color: #3366ff;">KEKUATAN MARSHAL : PERENCANAAN</span><br /><br /></h2>
<table style="height: 78px; width: 100%; background: #f5f5f5; padding: 20; background-color: #3366ff;">
<tbody>
<tr>
<td style="width: 30%; padding: 20;">
<p><span style="color: #ffffff;"><strong>Sisi Produktif</strong></span></p>
</td>
<td style="width: 70%;">
<p><span style="color: #ffffff;">Kepercayaan, konsistensi, konsentrasi, ketenangan, keyakinan, keseriusan, professional, artistik, kekuatan, jernih, melankolis.</span></p>
</td>
</tr>
<tr>
<td style="width: 174px;">
<p><span style="color: #ffffff;"><strong>Sisi Tantangan</strong></span></p>
</td>
<td style="width: 416px;">
<p><span style="color: #ffffff;">Kaku, sulit akrab, keraguan, dingin, keras kepala, bangga diri, cuek, tidak ramah, tidak empatik, kesedihan, kesendirian, kesunyian.</span></p>
</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<h2 class="p1" style="text-align: center;"><span style="color: #3366ff;">SELF-LEADERSHIP</span></h2>
<p>&nbsp;</p>
<p>Pemimpin dengan kekuatan terbesarnya adalah strategi yang komprehensif, dalam proses mengusahakan keberhasilan bagi organisasi atau tim yang dipimpinnya.</p>
<p>Pemimpin yang bisa menjamin keamanan serta menunjukkan kewibawaan diri, dengan perhitungan yang matang.<br /><br />Model kepemimpinan ini juga mampu menyelesaikan tantangan yang ada di sekitar, dalam perjalanannya menuju target yang diinginkan, mampu untuk terus berjalan melalui berbagai penghalang dan tantangan yang ditemuinya.</p>
<h2 class="p1" style="text-align: center; color: #3366ff;"><br /><strong>PROFIL: UTAMA &amp; BAYANGAN</strong>&nbsp;</h2>
<p>Profil Utama (main profile) Shio Kaya adalah bagian dominan dari diri Anda, yang sebenarnya mendorong dan menginspirasi diri Anda.</p>
<p>Profil utama (main profile) mencakup karakteristik alamiah Anda, yang dapat membantu Anda dalam membangun keberhasilan dengan lebih efektif dan efisien, menghemat waktu dan tenaga Anda.</p>
<p>Profil Bayangan (shadow profile) Shio Kaya merupakan bagian resesif (non-dominan) dari diri Anda, dengan hanya beberapa karakteristik yang muncul.</p>
<p>Profil bayangan (shadow profile) merupakan karakteristik yang bersifat seperti bayangan, bagian dari diri Anda, tak terpisahkan, tapi bukan mencerminkan diri Anda sepenuhnya.<br /><br />Profil Bayangan dari <strong>Marshal</strong> adalah <strong>Alchemist</strong> dan <strong>Priest</strong></p>
<p>&nbsp;</p>
<hr />
<h2 style="text-align: center;"><strong><br />MARSHAL</strong>&nbsp;</h2>
<table style="height: 48px; background-color: #3366ff;" width="100%">
<tbody>
<tr style="height: 36.5312px;">
<td style="width: 288px; height: 36.5312px; text-align: center;"><span style="color: #ffffff;"><strong>KARAKTER ALAMI</strong></span></td>
</tr>
<tr style="height: 222px;">
<td style="width: 288px; height: 222px;"><span style="color: #ffffff;">&nbsp;</span>
<ul>
<li><span style="color: #ffffff;">Pengendali yang handal dari balik layar</span></li>
<li><span style="color: #ffffff;">Mencintai detail, sampai hal terkecil sekalipun</span></li>
<li><span style="color: #ffffff;">Mampu menggunakan aset tanpa harus memiliki aset tersebut </span></li>
<li><span style="color: #ffffff;">Lebih mementingkan angka dari pada sebuah hubungan</span></li>
<li><span style="color: #ffffff;">Menyukai kepastian dan tidak menyukai resiko yang tidak terukur</span></li>
<li><span style="color: #ffffff;">Tidak mudah menyerah untuk menemukan celah</span></li>
<li><span style="color: #ffffff;">Tertib dan terstruktur dan menyukai penghematan</span></li>
<li><span style="color: #ffffff;">Cenderung menyelesaikan apa yang sudah dimulai</span></li>
</ul>
<p><span style="color: #ffffff;">&nbsp;</span></p>
</td>
</tr>
</tbody>
</table>
<table style="height: 48px; background-color: #3366ff;" width="100%">
<tbody>
<tr style="height: 36.5312px;">
<td style="width: 288px; height: 36.5312px; text-align: center;"><span style="color: #ffffff;"><strong>FAKTOR SUKSES</strong></span></td>
</tr>
<tr style="height: 204px;">
<td style="width: 288px; height: 204px;">
<ul>
<li><span style="color: #ffffff;">Proyek atau program tetap berjalan tanpa ada penghalang </span></li>
<li><span style="color: #ffffff;">Memberikan hasil atau proses yang nyaris sempurna</span></li>
<li><span style="color: #ffffff;">Mampu menciptakan lebih banyak peluang yang bisa diambil</span></li>
<li><span style="color: #ffffff;">Fokus pada kesempurnaan proses maupun hasil yang harus dicapai</span></li>
<li><span style="color: #ffffff;">Hal yang dilakukan terjamin dan aman karena semua sudah diperhitungkan</span></li>
<li><span style="color: #ffffff;">Persisten dan teliti dalam proses untuk mencapai tujuan</span></li>
<li><span style="color: #ffffff;">Mampu menggunakan kapital dan aset secara efektif dan efisien</span></li>
<li><span style="color: #ffffff;">Segala sesuatu dikerjakan sampai selesai, dan bisa di evaluasi untuk perbaikan</span></li>
</ul>
<p><span style="color: #ffffff;">&nbsp;</span></p>
</td>
</tr>
</tbody>
</table>
<table style="height: 48px; background-color: #3366ff;" width="100%">
<tbody>
<tr style="height: 10.5312px;">
<td style="width: 288px; height: 10.5312px;">
<p style="text-align: center;"><span style="color: #ffffff;"><strong>FAKTOR KEGAGALAN</strong></span></p>
</td>
</tr>
<tr style="height: 258px;">
<td style="width: 288px; height: 258px;"><span style="color: #ffffff;">&nbsp;</span>
<ul>
<li><span style="color: #ffffff;">Seringkali menjadi tidak terlihat dan terabaikan</span></li>
<li><span style="color: #ffffff;">Bisa terjebak pada micro-management dan terhenti pada satu hal terlalu lama</span></li>
<li><span style="color: #ffffff;">Tanpa pengelolaan yang baik, bisa menimbulkan konflik dengan banyak mitra </span></li>
<li><span style="color: #ffffff;">Cenderung tidak terlalu menganggap penting perasaan tim atau mitra </span></li>
<li><span style="color: #ffffff;">Menjadi lebih egois dan tidak percaya pada orang lain</span></li>
<li><span style="color: #ffffff;">Bisa terlihat keras kepala dan mendominasi sebuah proses</span></li>
<li><span style="color: #ffffff;">Terkadang jadi terlihat pelit dan kaku terhadap banyak hal</span></li>
<li><span style="color: #ffffff;">Sering terjebak memikirkan hal-hal yang belum selesai terus menerus</span></li>
</ul>
<p><span style="color: #ffffff;">&nbsp;</span></p>
</td>
</tr>
</tbody>
</table>
<hr />
<h2 style="text-align: center;"><strong><br />ALCHEMIST</strong>&nbsp;</h2>
<table style="height: 48px; background-color: #993366;" width="100%">
<tbody>
<tr style="height: 36.5312px;">
<td style="width: 288px; height: 36.5312px; text-align: center;"><span style="color: #ffffff;"><strong>KARAKTER ALAMI</strong></span></td>
</tr>
<tr style="height: 222px;">
<td style="width: 288px; height: 222px;"><span style="color: #ffffff;">&nbsp;</span>
<ul>
<li><span style="color: #ffffff;">Mampu mengendalikan dan mengatur orang lain secara tidak langsung </span></li>
<li><span style="color: #ffffff;">Sangat memperhatikan detail yang rinci </span></li>
<li><span style="color: #ffffff;">Sangat ahli dalam menyelesaikan dan menyempurnakan sesuatu </span></li>
<li><span style="color: #ffffff;">Cenderung perfeksionis, bahkan ketika akan memulai sesuatu </span></li>
<li><span style="color: #ffffff;">Mengelola organisasi dengan sistematis dan teratur </span></li>
<li><span style="color: #ffffff;">Cenderung untuk senang memegang kendali </span></li>
<li><span style="color: #ffffff;">Terus menerus belajar dan melakukan perubahan </span></li>
<li><span style="color: #ffffff;">Tidak terlalu memperhatikan penyampaian dan tampilan</span></li>
</ul>
</td>
</tr>
</tbody>
</table>
<table style="height: 48px; background-color: #993366;" width="100%">
<tbody>
<tr style="height: 36.5312px;">
<td style="width: 288px; height: 36.5312px; text-align: center;"><span style="color: #ffffff;"><strong>FAKTOR SUKSES</strong></span></td>
</tr>
<tr style="height: 204px;">
<td style="width: 288px; height: 204px;">
<ul>
<li><span style="color: #ffffff;">Handal dalam membuat sistem dan prosedur </span></li>
<li><span style="color: #ffffff;">Hal yang dikerjakannya terjamin aman dan terorganisir dengan baik </span></li>
<li><span style="color: #ffffff;">Selalu menghasilkan perbaikan yang berkelanjutan </span></li>
<li><span style="color: #ffffff;">Perencanaan sangat matang dan sudah mempertimbangkan banyak aspek</span></li>
<li><span style="color: #ffffff;">Organisasi terbangun secara berkelanjutan dan autopilot </span></li>
<li><span style="color: #ffffff;">Semua terkontrol dengan baik dan tanggapan cepat apabila ada kesalahan </span></li>
<li><span style="color: #ffffff;">Mampu memprediksi tren ke depan dan beradaptasi dengan cepat </span></li>
<li><span style="color: #ffffff;">Fokus pada menghasilkan fungsi yang efektif dan efisien </span></li>
</ul>
</td>
</tr>
</tbody>
</table>
<table style="height: 48px; background-color: #993366;" width="100%">
<tbody>
<tr style="height: 10.5312px;">
<td style="width: 288px; height: 10.5312px;">
<p style="text-align: center;"><span style="color: #ffffff;"><strong>FAKTOR KEGAGALAN</strong></span></p>
</td>
</tr>
<tr style="height: 258px;">
<td style="width: 288px; height: 258px;"><span style="color: #ffffff;">&nbsp;</span>
<ul>
<li><span style="color: #ffffff;">Cenderung tidak memperhatikan faktor manusia dalam membuat sistem/prosedur</span></li>
<li><span style="color: #ffffff;">Terjebak pada mengerjakan hal-hal yang terlalu detail, melupakan gambaran besarnya </span></li>
<li><span style="color: #ffffff;">Tidak memperhatikan bahwa tim yang mengerjakan perlu untuk berhenti sesekali</span></li>
<li><span style="color: #ffffff;">Seringkali lama dalam memulai sesuatu karena terlalu banyak pertimbangan </span></li>
<li><span style="color: #ffffff;">Bisa melupakan faktor perasaan manusia dalam menjalankan keteraturan organisasi </span></li>
<li><span style="color: #ffffff;">Bisa terjebak mengendalikan sampai detail, menciptakan rasa frustasi bagi tim</span></li>
<li><span style="color: #ffffff;">Seringkali sudah melakukan tindakan sia-sia, antisipasi sebelum waktunya Bisa menimbulkan masalah ketika customer butuh tampilan yang baik</span> </li>
</ul>
</td>
</tr>
</tbody>
</table>
<hr />
<h2 style="text-align: center;"><strong><br />PRIEST</strong>&nbsp;</h2>
<table style="height: 48px; background-color: #1289a7;" width="100%">
<tbody>
<tr style="height: 36.5312px;">
<td style="width: 288px; height: 36.5312px; text-align: center;"><span style="color: #ffffff;"><strong>KARAKTER ALAMI</strong></span></td>
</tr>
<tr style="height: 222px;">
<td style="width: 288px; height: 222px;"><span style="color: #ffffff;">&nbsp;</span>
<ul>
<li><span style="color: #ffffff;">Paling mementingkan keamanan</span></li>
<li><span style="color: #ffffff;">Bertindak berdasarkan pemikiran logis dan jarang berdasarkan perasaan</span></li>
<li><span style="color: #ffffff;">Menunda dan menunggu untuk mencapai hasil maksimal Alami dalam mengubah arus uang (cashflow) menjadi aset</span></li>
<li><span style="color: #ffffff;">Kekuatan utama pada keputusan membeli sesuatu dan menahannya</span></li>
<li><span style="color: #ffffff;">Cenderung stabil dan dapat diandalkan Fokus utama adalah menyimpan (saving) Membutuhkan banyak data untuk membuat keputusan&nbsp;</span></li>
</ul>
<p><span style="color: #ffffff;">&nbsp;</span></p>
</td>
</tr>
</tbody>
</table>
<table style="height: 48px; background-color: #1289a7;" width="100%">
<tbody>
<tr style="height: 36.5312px;">
<td style="width: 288px; height: 36.5312px; text-align: center;"><span style="color: #ffffff;"><strong>FAKTOR SUKSES</strong></span></td>
</tr>
<tr style="height: 204px;">
<td style="width: 288px; height: 204px;">
<ul>
<li><span style="color: #ffffff;">Tindakan yang diambil sudah berdasarkan pertimbangan matang </span></li>
<li><span style="color: #ffffff;">Bersedia menunggu untuk data yang cukup supaya keputusan optimal </span></li>
<li><span style="color: #ffffff;">Sabar menunggu kondisi yang tepat untuk melakukan tindakan </span></li>
<li><span style="color: #ffffff;">Cenderung punya aset yang harganya naik sejalan dengan waktu </span></li>
<li><span style="color: #ffffff;">Mampu melihat dan menggunakan aset untuk keuntungan </span></li>
<li><span style="color: #ffffff;">Tidak diperlukan banyak pengawasan dan arahan dalam mengerjakan sesuatu</span></li>
<li><span style="color: #ffffff;">Punya kemampuan alami mengambil aset dan menyimpan untuk kondisi sulit Keputusan yang dibuat cenderung aman dari berbagai alternatif&nbsp;</span></li>
</ul>
</td>
</tr>
</tbody>
</table>
<table style="height: 48px; background-color: #1289a7;" width="100%">
<tbody>
<tr style="height: 10.5312px;">
<td style="width: 288px; height: 10.5312px;">
<p style="text-align: center;"><span style="color: #ffffff;"><strong>FAKTOR KEGAGALAN</strong></span></p>
</td>
</tr>
<tr style="height: 258px;">
<td style="width: 288px; height: 258px;">
<ul>
<li><span style="color: #ffffff;">Kadang terlalu membutuhkan banyak pertimbangan dan jaminan untuk bertindak</span></li>
<li><span style="color: #ffffff;">Terkadang tidak memperhatikan efek dari sebuah keputusan pada tim dan orang lain</span></li>
<li><span style="color: #ffffff;">Ritme yang terkesan lambat dan tidak memperhatikan perasaan mitra atau tim </span></li>
<li><span style="color: #ffffff;">Bisa terjebak dan tidak memegang uang cash untuk keperluan mendadak </span></li>
<li><span style="color: #ffffff;">Terjebak dengan melewatkan momen penting untuk melepas aset </span></li>
<li><span style="color: #ffffff;">Bisa menampilkan sisi tidak berrpikir terbuka atas masukan/feedback </span></li>
<li><span style="color: #ffffff;">Seringkali terkesan pelit dan tidak peduli pada kepentingan selain dirinya </span></li>
<li><span style="color: #ffffff;">Terkesan sering menunda-nunda dalam membuat keputusan</span></li>
</ul>
</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<hr />
<p><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/diagram.png" alt="" width="100%" /></p>
<p>&nbsp;</p>
<table style="height: 392px; width: 100%;">
<tbody>
<tr>
<td style="width: 15.5194%;">
<p style="text-align: center;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-ALCHEMIST.png" width="100" height="100" /><br />ALCHEMIST</p>
</td>
<td style="width: 81.4806%;">
<p>Penyempurnaan yang baik, penyelesaian proses, menemukan cara yang lebih baik dab cerdas, meningkatkan efesiensi dan efektivitas produk/sitem yang ada.</p>
<p>Seseorang yang memilki kemampuan beradaptasi dengan menempatkan diri sesuai dengạn dimana ia berada. mampu untuk tetap merendah dan secara frontal berusaha untuk mengejar setiap peluang yang ada.</p>
</td>
</tr>
<tr>
<td style="width: 15.5194%; text-align: center;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-WIZARD.png" width="100" height="100" />WIZARD</td>
<td style="width: 81.4806%;">
<p>Konseptor yang &thorn;aik, penggagas hal baru, kreatif, inovatif, pencipta ide, pemikir out-of-the-box, spontan, tidak teratur, impulsif, baik untuk memulai sesuatu yang baru.</p>
<p>Seseorang yang pampu berinovasi secara berkelanjutan, seringkali kurang bisa dipahami oleh banyak orang disekitarnya, namun sanggup berpikir out-of-the-box, dalam mengbasilkan konsep-konsep baru.</p>
</td>
</tr>
<tr>
<td style="width: 15.5194%; text-align: center;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-BARD.png" width="100" height="100" />BARD</td>
<td style="width: 81.4806%;">
<p>Promotor yang baik, pemasar yang berbakat, merek pribadi yang kuat, &thorn;ergerak pat, butyh perhatian, bersinar, mampy menginspiraşi orang lain, individu yang kokoh.</p>
<p>Seseorang yang mampu memberi energi kepada orang-orang di sekelilingnya, mendukung dan menyebarkan semangat secara konsisten, sehingga mampu menyampaikan segala hal dengan penerimaan yang relatif mudah.</p>
</td>
</tr>
<tr>
<td style="width: 15.5194%; text-align: center;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-PRIEST.png" width="100" height="100" />KNIGHT</td>
<td style="width: 81.4806%;">
<p>Influencer yang baik, pemimpip yang karismatik, manajer pelaksana, penyelenggara, administrator, pendukung, baik dalam mencirtakan dan mengelola hubungan, baik dalam manajemen sumber daya manusia, fleksibel.</p>
<p>Seorang yang selalu mampu memberi semangat pada orang-orang disekitarnya, dimana api mampu menerangi kegelapan dan menyambar kemanapun, sebingga ia bisa mempengaruhi lebih banyak orang untuk sejalan seiring berjalannya waktu.</p>
</td>
</tr>
<tr>
<td style="width: 15.5194%; text-align: center;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-ARBITER.png" width="100" height="100" />ARBITER</td>
<td style="width: 81.4806%;">
<p>Penengah yang baik, pembawa damai, representatif bijak, melihat alternatif sebagai manfaat, penghindar konflik, berinyestasi pada orang untuk manfaat jangka panjang.</p>
<p>Seseorang yang tegas şerta mampu menciptakan kemungkinan baru, walaupun sebelumnya dirasakan tidak ada jalan lain, dimana angin biasanya memang tidak terlibat namun bisa dirasakan kehadirannya, mengisi apapun dan dimanapun.</p>
</td>
</tr>
<tr>
<td style="width: 15.5194%; text-align: center;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-MERCHANT.png" width="100" height="100" />MERCHANT</td>
<td style="width: 81.4806%;">
<p>Negosiator ulung, pedagang yang handal, pencari peluang, pengguna waktu yang efektif dan efisien, fleksibel, bekerja dalam pola/parameter tertentų, berdasarkan data.</p>
<p>Seseorang yang mampu memanfaatkan kekuatan dari wawasan yg luas, yang menampung apapun dari segala penjuru, mencapai keberhasilan dengan menerima berbagai hal, dan memanfaatkannva untuk keberhasilan yang lebih besar.&nbsp;</p>
</td>
</tr>
<tr>
<td style="width: 15.5194%; text-align: center;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-KNIGHT.png" width="100" height="100" />PRIEST</td>
<td style="width: 81.4806%;">
<p>Penjaga yang baik, dapat diandalkan, sabar, berhati-hati, berbasis fakta, safety- player, terstruktur, unggul dengan analisis informasi, cerdas dalam mengelola risiko.</p>
<p>Seseorang yang mampu menegakkan aturan secara tegas, disegani oleh orang- orang dibawahnya, dan ditakuti oleh siapapun yang melakukan pelanggaran aturan, dengan keteguhan tinggi sehingga mampu dijadikan sebagai tauladan (role-model).</p>
</td>
</tr>
<tr>
<td style="width: 15.5194%; text-align: center;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-MARSHAL.png" width="100" height="100" />MARSHAL</td>
<td style="width: 81.4806%;">
<p>Penganalisa yang baik, prediktif, lebih efektif di belakang layar, terencana, berkomitmen, pengatur yang handal, berkemauan keras, regulator yang baik.</p>
<p>Seseorang yang kokoh dan bisa diandalkan, terbuka untuk berbagai peluang yang lewat atau dilewatinya, dan mampu memanfaatkan apapun yang saat ini dimilikinya, termasuk memanfaatkan kekuatan waktu dan prioritas-prioritas yang tersusun dengan baik.</p>
</td>
</tr>
</tbody>
</table>


###PRIEST ###

<h2 style="text-align: center;"><span style="color: #33cccc;">PRIEST</span></h2>
<p>Para pemilik shio ini adalah penjaga yang baik, sabar serta dapat diandalkan.</p>
<p>Mereka selalu berhati-hati dalam melakukan hal apapun, terstruktur dan cerdas dalam mengelola risiko.</p>
<p>Mereka juga sangat tegas dalam menegakkan aturan sehingga ditakuti oleh para pelanggar aturan maupun para tim di bawahnya.</p>
<p>Namun di sisi lain mereka sangat safety-player sehingga seringkali kehilangan peluang yang ada.<br /><br /><strong>KARAKTERISTIK PROFIL PRIEST<br /></strong><br />&bull; Memajukan bisnis dengan cara memperbanyak peluang atau sumber penghasilan, tidak masalah kecil, tapi dengan volume yang besar (multiple source of income)</p>
<p>&bull; Mengembalikan energi ketika lelah, dengan melakukan hal-hal yang bersifat pribadi, hanya dengan sendiri atau dengan orang yang benar-benar dianggap dekat (self-recharging).</p>
<p>&bull; Mencari detail dalam berbagai hal terlebih dahulu, tertarik pada penjelasan yang sifatnya spesifik, bermasalah untuk mau menerima gambaran umum terlebih dulu.</p>
<p>&bull; Lebih bisa berkinerja tinggi ketika berada di dalam lingkup kerja yang terkait dengan benda (produk, alat kerja, dll) dan sistem (prosedur, framework, dll).</p>
<p>&bull; Mengikuti instruksi dan prosedur yang ada untuk memastikan semua berjalan dengan baik, dengan kecenderungan untuk selalu menyelesaikan pekerjaan yang telah dimulai.</p>
<p>&bull; Mencari detail dalam berbagai hal terlebih dahulu, tertarik pada penjelasan yang sifatnya spesifik, bermasalah untuk mau menerima gambaran umum terlebih dulu.</p>
<p>&bull; mengambil posisi yang aman dengan cara memberi masukan atau tindakan yang dianggap masuk dalam logika berpikir, dengan segala probabilitas resiko yang bisa terjadi.</p>
<p>&bull; Mengutamakan ketepatan waktu, untuk mencapai target yang ditentukan, dengan keteraturan penyelesaian berdasarkan skala prioritas, sangat mengandalkan efektivitas dan efisiensi.</p>
<p>&bull; Memahami segala hal yang terjadi dengan merasakan ke dalam diri, mempertimbangkan suara dan logika yang muncul dalam pikiran, dan seringkali perlu mengalami langsung untuk percaya.</p>
<p>&bull; Mengutamakan keuletan untuk mencapai sebuah target, fleksibel namun tetap mengikuti sebuah struktur yang ada, supportif dan menjadi pendorong utama dalam menghasilkan/mencapai suatu hal.</p>
<p>&bull; Fokus pada komitmen yang sudah ada, dengan flaksibilitas yang akan disesuaikan terhadap kemungkinan terjadinya perubahan pada metode atau proses yang juga sudah ditentukan.</p>
<p>&bull; Berpikir secara logis, sekaligus mempertimbangkan berbagai aspek terkait dengan emosi dan perasaan, untuk bisa mencapai keputusan yang adil namun humanis.</p>
<table style="width: 100%; height: 71px; background-color: #f5f5f5;">
<tbody>
<tr>
<td style="width: 100px;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-KNIGHT.png" width="100" height="100" /></td>
<td style="width: 497px;">Elemen Samudra melambangkan keluasan dan kelapangan, sebagai muara yang menerima berbagai aliran air. Seorang yang berpikiran terbuka, bersedia menyerap berbagai hal baru serta mau menerima masukan dengan lapang dada.</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<p>Priest memiliki karakteristik warna tosca yang melambangkan konsentrasi, keseimbangan emosional, stabilitas diri, dan juga kesabaran, yang menghadirkan kejernihan pikiran dan kepercayaan diri, mengendalikan dan menyembuhkan untuk mencapai keamanan dan kestabilan.</p>
<h2 style="text-align: center;"><br /><span style="color: #33cccc;">KEKUATAN PRIEST : KEAMANAN</span><br /><br /></h2>
<table style="height: 78px; width: 100%; background: #f5f5f5; padding: 20; background-color: #33cccc;">
<tbody>
<tr>
<td style="width: 30%; padding: 20;">
<p><span style="color: #000000;"><strong>Sisi Produktif</strong></span></p>
</td>
<td style="width: 70%;">
<p><span style="color: #000000;">Keseimbangan, stabilitas, ketenangan, kesabaran, menghadirkan, keteraturan, mempengaruhi, terorganisir, kelembutan, ketepatan.</span></p>
</td>
</tr>
<tr>
<td style="width: 174px;">
<p><span style="color: #000000;"><strong>Sisi Tantangan</strong></span></p>
</td>
<td style="width: 416px;">
<p class="p1"><span class="s1" style="color: #000000;">Egois (self-centered), narsistik, mengabaikan, misterius, overaktif, ketidakseimbangan emosi, menyembunyikan, menahan, kebingungan.</span></p>
</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<h2 class="p1" style="text-align: center;"><span style="color: #33cccc;">SELF-LEADERSHIP</span></h2>
<p>&nbsp;</p>
<p>Pemimpin dengan keingintahuan dan wawasan yang luas, yang mampu mengatasi berbagai macam gejolak dalam dirinya dengan baik.</p>
<p>Tipe pemimpin yang mampu untuk menjadi penjaga atas keterlaksanaan sebuah hal atau aset, dimana hal ini berarti bahwa ia adalah pemimpin yang mampu menghasilkan keberhasilan tanpa harus melepas aset yang dimilikinya berupa sumber daya apapun (SDM, properti, kapital, dll).</p>
<p>Seorang pemimpin yang mempunyai kemampuan menampung berbagai masalah yang datang, dan mengubahnya menjadi sebuah kondisi yang menguntungkan.</p>
<h2 class="p1" style="text-align: center; color: #3366ff;"><br /><strong>PROFIL: UTAMA &amp; BAYANGAN</strong>&nbsp;</h2>
<p>Profil Utama (main profile) Shio Kaya adalah bagian dominan dari diri Anda, yang sebenarnya mendorong dan menginspirasi diri Anda.</p>
<p>Profil utama (main profile) mencakup karakteristik alamiah Anda, yang dapat membantu Anda dalam membangun keberhasilan dengan lebih efektif dan efisien, menghemat waktu dan tenaga Anda.</p>
<p>Profil Bayangan (shadow profile) Shio Kaya merupakan bagian resesif (non-dominan) dari diri Anda, dengan hanya beberapa karakteristik yang muncul.</p>
<p>Profil bayangan (shadow profile) merupakan karakteristik yang bersifat seperti bayangan, bagian dari diri Anda, tak terpisahkan, tapi bukan mencerminkan diri Anda sepenuhnya.<br /><br />Profil Bayangan dari <strong>PRIEST</strong>&nbsp;adalah <strong>MERCHANT</strong> dan <strong>MARSHAL</strong></p>
<p>&nbsp;</p>
<hr />
<h2 style="text-align: center;"><strong><br />PRIEST</strong>&nbsp;</h2>
<table style="height: 48px; background-color: #1289a7;" width="100%">
<tbody>
<tr style="height: 36.5312px;">
<td style="width: 288px; height: 36.5312px; text-align: center;"><span style="color: #ffffff;"><strong>KARAKTER ALAMI</strong></span></td>
</tr>
<tr style="height: 222px;">
<td style="width: 288px; height: 222px;"><span style="color: #ffffff;">&nbsp;</span>
<ul>
<li><span style="color: #ffffff;">Paling mementingkan keamanan</span></li>
<li><span style="color: #ffffff;">Bertindak berdasarkan pemikiran logis dan jarang berdasarkan perasaan</span></li>
<li><span style="color: #ffffff;">Menunda dan menunggu untuk mencapai hasil maksimal Alami dalam mengubah arus uang (cashflow) menjadi aset</span></li>
<li><span style="color: #ffffff;">Kekuatan utama pada keputusan membeli sesuatu dan menahannya</span></li>
<li><span style="color: #ffffff;">Cenderung stabil dan dapat diandalkan Fokus utama adalah menyimpan (saving) Membutuhkan banyak data untuk membuat keputusan&nbsp;</span></li>
</ul>
<p><span style="color: #ffffff;">&nbsp;</span></p>
</td>
</tr>
</tbody>
</table>
<table style="height: 48px; background-color: #1289a7;" width="100%">
<tbody>
<tr style="height: 36.5312px;">
<td style="width: 288px; height: 36.5312px; text-align: center;"><span style="color: #ffffff;"><strong>FAKTOR SUKSES</strong></span></td>
</tr>
<tr style="height: 204px;">
<td style="width: 288px; height: 204px;">
<ul>
<li><span style="color: #ffffff;">Tindakan yang diambil sudah berdasarkan pertimbangan matang </span></li>
<li><span style="color: #ffffff;">Bersedia menunggu untuk data yang cukup supaya keputusan optimal </span></li>
<li><span style="color: #ffffff;">Sabar menunggu kondisi yang tepat untuk melakukan tindakan </span></li>
<li><span style="color: #ffffff;">Cenderung punya aset yang harganya naik sejalan dengan waktu </span></li>
<li><span style="color: #ffffff;">Mampu melihat dan menggunakan aset untuk keuntungan </span></li>
<li><span style="color: #ffffff;">Tidak diperlukan banyak pengawasan dan arahan dalam mengerjakan sesuatu</span></li>
<li><span style="color: #ffffff;">Punya kemampuan alami mengambil aset dan menyimpan untuk kondisi sulit Keputusan yang dibuat cenderung aman dari berbagai alternatif&nbsp;</span></li>
</ul>
</td>
</tr>
</tbody>
</table>
<table style="height: 48px; background-color: #1289a7;" width="100%">
<tbody>
<tr style="height: 10.5312px;">
<td style="width: 288px; height: 10.5312px;">
<p style="text-align: center;"><span style="color: #ffffff;"><strong>FAKTOR KEGAGALAN</strong></span></p>
</td>
</tr>
<tr style="height: 258px;">
<td style="width: 288px; height: 258px;">
<ul>
<li><span style="color: #ffffff;">Kadang terlalu membutuhkan banyak pertimbangan dan jaminan untuk bertindak</span></li>
<li><span style="color: #ffffff;">Terkadang tidak memperhatikan efek dari sebuah keputusan pada tim dan orang lain</span></li>
<li><span style="color: #ffffff;">Ritme yang terkesan lambat dan tidak memperhatikan perasaan mitra atau tim </span></li>
<li><span style="color: #ffffff;">Bisa terjebak dan tidak memegang uang cash untuk keperluan mendadak </span></li>
<li><span style="color: #ffffff;">Terjebak dengan melewatkan momen penting untuk melepas aset </span></li>
<li><span style="color: #ffffff;">Bisa menampilkan sisi tidak berrpikir terbuka atas masukan/feedback </span></li>
<li><span style="color: #ffffff;">Seringkali terkesan pelit dan tidak peduli pada kepentingan selain dirinya </span></li>
<li><span style="color: #ffffff;">Terkesan sering menunda-nunda dalam membuat keputusan</span></li>
</ul>
</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<h2 style="text-align: center;"><strong><br />MARSHAL</strong>&nbsp;</h2>
<table style="height: 48px; background-color: #3366ff;" width="100%">
<tbody>
<tr style="height: 36.5312px;">
<td style="width: 288px; height: 36.5312px; text-align: center;"><span style="color: #ffffff;"><strong>KARAKTER ALAMI</strong></span></td>
</tr>
<tr style="height: 222px;">
<td style="width: 288px; height: 222px;"><span style="color: #ffffff;">&nbsp;</span>
<ul>
<li><span style="color: #ffffff;">Pengendali yang handal dari balik layar</span></li>
<li><span style="color: #ffffff;">Mencintai detail, sampai hal terkecil sekalipun</span></li>
<li><span style="color: #ffffff;">Mampu menggunakan aset tanpa harus memiliki aset tersebut </span></li>
<li><span style="color: #ffffff;">Lebih mementingkan angka dari pada sebuah hubungan</span></li>
<li><span style="color: #ffffff;">Menyukai kepastian dan tidak menyukai resiko yang tidak terukur</span></li>
<li><span style="color: #ffffff;">Tidak mudah menyerah untuk menemukan celah</span></li>
<li><span style="color: #ffffff;">Tertib dan terstruktur dan menyukai penghematan</span></li>
<li><span style="color: #ffffff;">Cenderung menyelesaikan apa yang sudah dimulai</span></li>
</ul>
<p><span style="color: #ffffff;">&nbsp;</span></p>
</td>
</tr>
</tbody>
</table>
<table style="height: 48px; background-color: #3366ff;" width="100%">
<tbody>
<tr style="height: 36.5312px;">
<td style="width: 288px; height: 36.5312px; text-align: center;"><span style="color: #ffffff;"><strong>FAKTOR SUKSES</strong></span></td>
</tr>
<tr style="height: 204px;">
<td style="width: 288px; height: 204px;">
<ul>
<li><span style="color: #ffffff;">Proyek atau program tetap berjalan tanpa ada penghalang </span></li>
<li><span style="color: #ffffff;">Memberikan hasil atau proses yang nyaris sempurna</span></li>
<li><span style="color: #ffffff;">Mampu menciptakan lebih banyak peluang yang bisa diambil</span></li>
<li><span style="color: #ffffff;">Fokus pada kesempurnaan proses maupun hasil yang harus dicapai</span></li>
<li><span style="color: #ffffff;">Hal yang dilakukan terjamin dan aman karena semua sudah diperhitungkan</span></li>
<li><span style="color: #ffffff;">Persisten dan teliti dalam proses untuk mencapai tujuan</span></li>
<li><span style="color: #ffffff;">Mampu menggunakan kapital dan aset secara efektif dan efisien</span></li>
<li><span style="color: #ffffff;">Segala sesuatu dikerjakan sampai selesai, dan bisa di evaluasi untuk perbaikan</span></li>
</ul>
<p><span style="color: #ffffff;">&nbsp;</span></p>
</td>
</tr>
</tbody>
</table>
<table style="height: 48px; background-color: #3366ff;" width="100%">
<tbody>
<tr style="height: 10.5312px;">
<td style="width: 288px; height: 10.5312px;">
<p style="text-align: center;"><span style="color: #ffffff;"><strong>FAKTOR KEGAGALAN</strong></span></p>
</td>
</tr>
<tr style="height: 258px;">
<td style="width: 288px; height: 258px;"><span style="color: #ffffff;">&nbsp;</span>
<ul>
<li><span style="color: #ffffff;">Seringkali menjadi tidak terlihat dan terabaikan</span></li>
<li><span style="color: #ffffff;">Bisa terjebak pada micro-management dan terhenti pada satu hal terlalu lama</span></li>
<li><span style="color: #ffffff;">Tanpa pengelolaan yang baik, bisa menimbulkan konflik dengan banyak mitra </span></li>
<li><span style="color: #ffffff;">Cenderung tidak terlalu menganggap penting perasaan tim atau mitra </span></li>
<li><span style="color: #ffffff;">Menjadi lebih egois dan tidak percaya pada orang lain</span></li>
<li><span style="color: #ffffff;">Bisa terlihat keras kepala dan mendominasi sebuah proses</span></li>
<li><span style="color: #ffffff;">Terkadang jadi terlihat pelit dan kaku terhadap banyak hal</span></li>
<li><span style="color: #ffffff;">Sering terjebak memikirkan hal-hal yang belum selesai terus menerus</span></li>
</ul>
<p><span style="color: #ffffff;">&nbsp;</span></p>
</td>
</tr>
</tbody>
</table>
<hr />
<h2 style="text-align: center;"><strong><br />MERCHANT</strong>&nbsp;</h2>
<table style="height: 48px; background-color: #993366;" width="100%">
<tbody>
<tr style="height: 36.5312px;">
<td style="width: 288px; height: 36.5312px; text-align: center;"><span style="color: #ffffff;"><strong>KARAKTER ALAMI</strong></span></td>
</tr>
<tr style="height: 222px;">
<td style="width: 288px; height: 222px;"><span style="color: #ffffff;">&nbsp;</span>
<ul>
<li><span style="color: #ffffff;">Sangat baik dalam pengaturan waktu Secara alami mampu melihat peluang menjual dan membeli </span></li>
<li><span style="color: #ffffff;">Mampu melepaskan diri dari ikatan produk atau kesempatan </span></li>
<li><span style="color: #ffffff;">Cenderung damai, mudah sepakat, dan menghindari konflik </span></li>
<li><span style="color: #ffffff;">Peluang merupakan nilai utama yang terpenting </span></li>
<li><span style="color: #ffffff;">Punya kesabaran yang baik dalam menunggu waktu yang tepat </span></li>
<li><span style="color: #ffffff;">Sanggup melihat dari sisi pandang penjual dan pembeli </span></li>
<li><span style="color: #ffffff;">Tidak mudah terjebak pada trend (arus) dan berani melawan arus</span></li>
</ul>
</td>
</tr>
</tbody>
</table>
<table style="height: 48px; background-color: #993366;" width="100%">
<tbody>
<tr style="height: 36.5312px;">
<td style="width: 288px; height: 36.5312px; text-align: center;"><span style="color: #ffffff;"><strong>FAKTOR SUKSES</strong></span></td>
</tr>
<tr style="height: 204px;">
<td style="width: 288px; height: 204px;">
<ul>
<li><span style="color: #ffffff;">Dihargai dan dipercaya karena menunjukkan ketepatan waktu </span></li>
<li><span style="color: #ffffff;">Sukses dengan sigap memanfaatkan peluang yang muncul dimanapun </span></li>
<li><span style="color: #ffffff;">Mampu untuk menjual produk apapun tanpa terikat selama ada kesempatan Mediator yang baik dan cepat dalam mencapai kesepakatan </span></li>
<li><span style="color: #ffffff;">Menghargai setiap peluang dan kecepatan untuk menangkapnya </span></li>
<li><span style="color: #ffffff;">Menempatkan hal-hal di posisi tepat untuk menghasilkan keuntungan terbaik</span></li>
<li><span style="color: #ffffff;">Mampu menguasai dari sisi pembeli dan penjual dan mengambil keuntungan Bisa bergerak bebas ke arah dimana keuntungan berada</span></li>
</ul>
</td>
</tr>
</tbody>
</table>
<table style="height: 48px; background-color: #993366;" width="100%">
<tbody>
<tr style="height: 10.5312px;">
<td style="width: 288px; height: 10.5312px;">
<p style="text-align: center;"><span style="color: #ffffff;"><strong>FAKTOR KEGAGALAN</strong></span></p>
</td>
</tr>
<tr style="height: 258px;">
<td style="width: 288px; height: 258px;"><span style="color: #ffffff;">&nbsp;</span>
<ul>
<li><span style="color: #ffffff;">Mudah terganggu ketika ada yang berjalan tidak dalam waktu yang semestinya</span></li>
<li><span style="color: #ffffff;">Seringkali terjebak dalam mengambil peluang tanpa analisa matang </span></li>
<li><span style="color: #ffffff;">Tidak loyal pada sebuah produk dan bisa berubah begitu ada peluang lain</span></li>
<li><span style="color: #ffffff;">Cenderung mau gampang dan menjadi penghindar masalah </span></li>
<li><span style="color: #ffffff;">Bisa memilih mengejar peluang baru di atas hubungan yang sudah ada </span></li>
<li><span style="color: #ffffff;">Gagal ketika menggunakan kesabaran untuk ketidakberanian mengambil resiko</span></li>
<li><span style="color: #ffffff;">Kegagalan terjadi karena tidak memahami atau mengendalikan kedua sisi </span></li>
<li><span style="color: #ffffff;">Gagal ketika lupa waktu dan terjebak pada sebuah tren (arus)</span></li>
</ul>
</td>
</tr>
</tbody>
</table>
<hr />
<p><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/diagram.png" alt="" width="100%" /></p>
<p>&nbsp;</p>
<table style="height: 392px; width: 100%;">
<tbody>
<tr>
<td style="width: 15.5194%;">
<p style="text-align: center;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-ALCHEMIST.png" width="100" height="100" /><br />ALCHEMIST</p>
</td>
<td style="width: 81.4806%;">
<p>Penyempurnaan yang baik, penyelesaian proses, menemukan cara yang lebih baik dab cerdas, meningkatkan efesiensi dan efektivitas produk/sitem yang ada.</p>
<p>Seseorang yang memilki kemampuan beradaptasi dengan menempatkan diri sesuai dengạn dimana ia berada. mampu untuk tetap merendah dan secara frontal berusaha untuk mengejar setiap peluang yang ada.</p>
</td>
</tr>
<tr>
<td style="width: 15.5194%; text-align: center;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-WIZARD.png" width="100" height="100" />WIZARD</td>
<td style="width: 81.4806%;">
<p>Konseptor yang &thorn;aik, penggagas hal baru, kreatif, inovatif, pencipta ide, pemikir out-of-the-box, spontan, tidak teratur, impulsif, baik untuk memulai sesuatu yang baru.</p>
<p>Seseorang yang pampu berinovasi secara berkelanjutan, seringkali kurang bisa dipahami oleh banyak orang disekitarnya, namun sanggup berpikir out-of-the-box, dalam mengbasilkan konsep-konsep baru.</p>
</td>
</tr>
<tr>
<td style="width: 15.5194%; text-align: center;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-BARD.png" width="100" height="100" />BARD</td>
<td style="width: 81.4806%;">
<p>Promotor yang baik, pemasar yang berbakat, merek pribadi yang kuat, &thorn;ergerak pat, butyh perhatian, bersinar, mampy menginspiraşi orang lain, individu yang kokoh.</p>
<p>Seseorang yang mampu memberi energi kepada orang-orang di sekelilingnya, mendukung dan menyebarkan semangat secara konsisten, sehingga mampu menyampaikan segala hal dengan penerimaan yang relatif mudah.</p>
</td>
</tr>
<tr>
<td style="width: 15.5194%; text-align: center;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-PRIEST.png" width="100" height="100" />KNIGHT</td>
<td style="width: 81.4806%;">
<p>Influencer yang baik, pemimpip yang karismatik, manajer pelaksana, penyelenggara, administrator, pendukung, baik dalam mencirtakan dan mengelola hubungan, baik dalam manajemen sumber daya manusia, fleksibel.</p>
<p>Seorang yang selalu mampu memberi semangat pada orang-orang disekitarnya, dimana api mampu menerangi kegelapan dan menyambar kemanapun, sebingga ia bisa mempengaruhi lebih banyak orang untuk sejalan seiring berjalannya waktu.</p>
</td>
</tr>
<tr>
<td style="width: 15.5194%; text-align: center;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-ARBITER.png" width="100" height="100" />ARBITER</td>
<td style="width: 81.4806%;">
<p>Penengah yang baik, pembawa damai, representatif bijak, melihat alternatif sebagai manfaat, penghindar konflik, berinyestasi pada orang untuk manfaat jangka panjang.</p>
<p>Seseorang yang tegas şerta mampu menciptakan kemungkinan baru, walaupun sebelumnya dirasakan tidak ada jalan lain, dimana angin biasanya memang tidak terlibat namun bisa dirasakan kehadirannya, mengisi apapun dan dimanapun.</p>
</td>
</tr>
<tr>
<td style="width: 15.5194%; text-align: center;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-MERCHANT.png" width="100" height="100" />MERCHANT</td>
<td style="width: 81.4806%;">
<p>Negosiator ulung, pedagang yang handal, pencari peluang, pengguna waktu yang efektif dan efisien, fleksibel, bekerja dalam pola/parameter tertentų, berdasarkan data.</p>
<p>Seseorang yang mampu memanfaatkan kekuatan dari wawasan yg luas, yang menampung apapun dari segala penjuru, mencapai keberhasilan dengan menerima berbagai hal, dan memanfaatkannva untuk keberhasilan yang lebih besar.&nbsp;</p>
</td>
</tr>
<tr>
<td style="width: 15.5194%; text-align: center;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-KNIGHT.png" width="100" height="100" />PRIEST</td>
<td style="width: 81.4806%;">
<p>Penjaga yang baik, dapat diandalkan, sabar, berhati-hati, berbasis fakta, safety- player, terstruktur, unggul dengan analisis informasi, cerdas dalam mengelola risiko.</p>
<p>Seseorang yang mampu menegakkan aturan secara tegas, disegani oleh orang- orang dibawahnya, dan ditakuti oleh siapapun yang melakukan pelanggaran aturan, dengan keteguhan tinggi sehingga mampu dijadikan sebagai tauladan (role-model).</p>
</td>
</tr>
<tr>
<td style="width: 15.5194%; text-align: center;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-MARSHAL.png" width="100" height="100" />MARSHAL</td>
<td style="width: 81.4806%;">
<p>Penganalisa yang baik, prediktif, lebih efektif di belakang layar, terencana, berkomitmen, pengatur yang handal, berkemauan keras, regulator yang baik.</p>
<p>Seseorang yang kokoh dan bisa diandalkan, terbuka untuk berbagai peluang yang lewat atau dilewatinya, dan mampu memanfaatkan apapun yang saat ini dimilikinya, termasuk memanfaatkan kekuatan waktu dan prioritas-prioritas yang tersusun dengan baik.</p>
</td>
</tr>
</tbody>
</table>

###MERCHANT###

<h2 style="text-align: center;"><span style="color: #008000;">MERCHANT</span></h2>
<p>Para pemilik shio ini merupakan negosiator dan pedagang yang handal.</p>
<p>Mereka mampu mencari peluang karena selalu bekerja dalam parameter tertentu berdasarkan data.</p>
<p>Shio ini juga memiliki kemampuan menampung informasi apapun secara luas dan memanfaatkannya untuk mencari peluang dan memperoleh keberhasilan yang lebih besar.<br /><br /><strong>KARAKTERISTIK PROFIL MERCHANT</strong></p>
<p>&bull; Menciptakan daya saing dengan berkolaborasi, memungkinkan untuk fokus terhadap sebuah produk/proyek namun tetap mendapat hasil multiplikasi dari hasil kolaborasi tersebut.</p>
<p>&bull; Kekuatan fleksibilitas yang mampu untuk merasa nyaman dengan kondisi yang penuh dengan interaksi sosial, juga bisa menikmati kondisi sendirian atau yang jauh dari keramaian, untuk memperoleh semangat kembali.</p>
<p>&bull; Mampu secara cepat beralih dari berpikir secara umum ke detail atau sebaliknya, tergantung pada kebutuhan yang sedang dihadapi, dan bisa sama efektif di kedua skala berpikir tersebut.</p>
<p>&bull; Terkait dengan kemampuan untuk sama efektifnya dalam memfokuskan diri pada produk, tugas, sistem, maupun perasaan dan pikiran orang-orang di lingkungan profesional, tergantung pada situasi yang terjadi.</p>
<p>&bull; Mengikuti instruksi dan prosedur yang ada untuk memastikan semua berjalan dengan baik, dengan kecenderungan untuk selalu menyelesaikan pekerjaan yang telah dimulai.</p>
<p>&bull; Menyukai hal yang konkret dan cenderung linier, mengerjakan sesuatu dengan hasil yang jelas, mengerjakan dan mengkomunikasikan hal-hal yang ada secara bertahap.</p>
<p>&bull; Mengambil posisi yang aman dengan cara memberi masukan atau tindakan yang dianggap masuk dalam logika berpikir, dengan segala probabilitas resiko yang bisa terjadi.</p>
<p>&bull; Mengutamakan ketepatan waktu, untuk mencapai target yang ditentukan, dengan keteraturan penyelesaian berdasarkan skala prioritas, sangat mengandalkan efektivitas dan efisiensi.</p>
<p>&bull; Perlu waktu lebih untuk terhubung dengan suatu hal, lebih peka terhadap perasaan yang terlibat, belajar dengan cara praktek, bergerak, atau menyentuh, bertendensi untuk bisa merasakan energi orang lain.</p>
<p>&bull; Mengutamakan keuletan dalam mencapai tujuan yang diinginkan, fleksibel ketika berusaha selama prosesnya, suportif terhadap kepentingan di sekitarnya yang selaras dengan tujuannya.</p>
<p>&bull; Fokus pada komitmen yang dibuat dalam proses pencapaian target, apapun konsekwensi yang ada, penting untuk jalan sesuai dengan struktur yang sudah disepakai, berapapun harganya.</p>
<p>&bull; Berpikir dengan menganalisis dan memahami data, dengan pola yang terstruktur, haus atas wawasan dan pengetahuan untuk menyaring informasi, dalam membuat keputusan yang kuat.</p>
<table style="width: 100%; height: 71px; background-color: #f5f5f5;">
<tbody>
<tr>
<td style="width: 100px;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-MERCHANT.png" width="100" height="100" /></td>
<td style="width: 497px;">Elemen Tanah melambangkan penyedia tempat dan dukungan bagi seluruh kehidupan. Seorang yang bisa mengarahkan perhatian untuk melihat yang bisa dilakukan dan untuk membawa kesejahteraan bagi sekitarnya.</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<p>Merchant memiliki karakteristik warna hijau pekat yang berkaitan dengan alam, yang berguna untuk membantu dalam situasi tertekan agar lebih mampu menyeimbangkan dan menenangkan emosi. Warna ini juga melambangkan penyembuhan serta mendorong perasaan empati.</p>
<h2 style="text-align: center;"><br /><span style="color: #008000;">KEKUATAN MERCHANT : PELUANG</span><br /><br /></h2>
<table style="height: 78px; width: 100%; background: #f5f5f5; padding: 20; background-color: #008000;">
<tbody>
<tr>
<td style="width: 30%; padding: 20;">
<p><span style="color: #ffffff;"><strong>Sisi Produktif</strong></span></p>
</td>
<td style="width: 70%;">
<p><span style="color: #ffffff;">Kehidupan, kedamaian, kemudahan, harmoni, empati, kesejukan, keberuntungan, pemulihan, jaminan, kesadaran, keamanan, ketabahan.</span></p>
</td>
</tr>
<tr>
<td style="width: 174px;">
<p><span style="color: #ffffff;"><strong>Sisi Tantangan</strong></span></p>
</td>
<td style="width: 416px;">
<p class="p1"><span class="s1" style="color: #ffffff;">Terperangkap, tersesat, kebosanan, stagnasi, superior, ambisi, keserakahan, ketertutupan, homeostasis.</span></p>
</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<h2 class="p1" style="text-align: center;"><span style="color: #008000;">SELF-LEADERSHIP</span></h2>
<p>&nbsp;</p>
<p>Pemimpin yang memiliki sifat keteguhan, yang bisa menjadi landasan pijakan dan memberi kehidupan bagi organisasi atau tim yang dipimpinnya.</p>
<p>Model kepemimpinan yang fleksibel ketika berhubungan ke arah manapun, karena ia sanggup menopang apapun, yang tidak mudah lelah untuk menciptakan peluang yang terbentuk dari berbagai macam sumber.</p>
<p>Pemimpin model ini bisa menjelma menjadi besar atau kecil, sesuai dengan kebutuhannya, serta tidak mudah terganggu atas penolakan, dimana kekuatan alaminya adalah bertumpu pada fakta yang ada dalam mengambil keputusan.</p>
<h2 class="p1" style="text-align: center; color: #008000;"><br /><strong>PROFIL: UTAMA &amp; BAYANGAN</strong>&nbsp;</h2>
<p>Profil Utama (main profile) Shio Kaya adalah bagian dominan dari diri Anda, yang sebenarnya mendorong dan menginspirasi diri Anda.</p>
<p>Profil utama (main profile) mencakup karakteristik alamiah Anda, yang dapat membantu Anda dalam membangun keberhasilan dengan lebih efektif dan efisien, menghemat waktu dan tenaga Anda.</p>
<p>Profil Bayangan (shadow profile) Shio Kaya merupakan bagian resesif (non-dominan) dari diri Anda, dengan hanya beberapa karakteristik yang muncul.</p>
<p>Profil bayangan (shadow profile) merupakan karakteristik yang bersifat seperti bayangan, bagian dari diri Anda, tak terpisahkan, tapi bukan mencerminkan diri Anda sepenuhnya.<br /><br />Profil Bayangan dari <strong>MERCHANT</strong>&nbsp;adalah <strong>PRIEST</strong> dan <strong>ARBITER</strong></p>
<p>&nbsp;</p>
<h2 style="text-align: center;"><strong><br />MERCHANT</strong>&nbsp;</h2>
<table style="height: 48px; background-color: #008000;" width="100%">
<tbody>
<tr style="height: 36.5312px;">
<td style="width: 288px; height: 36.5312px; text-align: center;"><span style="color: #ffffff;"><strong>KARAKTER ALAMI</strong></span></td>
</tr>
<tr style="height: 222px;">
<td style="width: 288px; height: 222px;"><span style="color: #ffffff;">&nbsp;</span>
<ul>
<li><span style="color: #ffffff;">Sangat baik dalam pengaturan waktu </span></li>
<li><span style="color: #ffffff;">Secara alami mampu melihat peluang menjual dan membeli </span></li>
<li><span style="color: #ffffff;">Mampu melepaskan diri dari ikatan produk atau kesempatan </span></li>
<li><span style="color: #ffffff;">Cenderung damai, mudah sepakat, dan menghindari konflik </span></li>
<li><span style="color: #ffffff;">Peluang merupakan nilai utama yang terpenting </span></li>
<li><span style="color: #ffffff;">Punya kesabaran yang baik dalam menunggu waktu yang tepat </span></li>
<li><span style="color: #ffffff;">Sanggup melihat dari sisi pandang penjual dan pembeli </span></li>
<li><span style="color: #ffffff;">Tidak mudah terjebak pada trend (arus) dan berani melawan arus</span></li>
</ul>
</td>
</tr>
</tbody>
</table>
<table style="height: 48px; background-color: #008000;" width="100%">
<tbody>
<tr style="height: 36.5312px;">
<td style="width: 288px; height: 36.5312px; text-align: center;"><span style="color: #ffffff;"><strong>FAKTOR SUKSES</strong></span></td>
</tr>
<tr style="height: 204px;">
<td style="width: 288px; height: 204px;">
<ul>
<li><span style="color: #ffffff;">Dihargai dan dipercaya karena menunjukkan ketepatan waktu </span></li>
<li><span style="color: #ffffff;">Sukses dengan sigap memanfaatkan peluang yang muncul dimanapun </span></li>
<li><span style="color: #ffffff;">Mampu untuk menjual produk apapun tanpa terikat selama ada kesempatan Mediator yang baik dan cepat dalam mencapai kesepakatan </span></li>
<li><span style="color: #ffffff;">Menghargai setiap peluang dan kecepatan untuk menangkapnya </span></li>
<li><span style="color: #ffffff;">Menempatkan hal-hal di posisi tepat untuk menghasilkan keuntungan terbaik</span></li>
<li><span style="color: #ffffff;">Mampu menguasai dari sisi pembeli dan penjual dan mengambil keuntungan Bisa bergerak bebas ke arah dimana keuntungan berada</span></li>
</ul>
</td>
</tr>
</tbody>
</table>
<table style="height: 48px; background-color: #008000;" width="100%">
<tbody>
<tr style="height: 10.5312px;">
<td style="width: 288px; height: 10.5312px;">
<p style="text-align: center;"><span style="color: #ffffff;"><strong>FAKTOR KEGAGALAN</strong></span></p>
</td>
</tr>
<tr style="height: 258px;">
<td style="width: 288px; height: 258px;"><span style="color: #ffffff;">&nbsp;</span>
<ul>
<li><span style="color: #ffffff;">Mudah terganggu ketika ada yang berjalan tidak dalam waktu yang semestinya</span></li>
<li><span style="color: #ffffff;">Seringkali terjebak dalam mengambil peluang tanpa analisa matang </span></li>
<li><span style="color: #ffffff;">Tidak loyal pada sebuah produk dan bisa berubah begitu ada peluang lain</span></li>
<li><span style="color: #ffffff;">Cenderung mau gampang dan menjadi penghindar masalah </span></li>
<li><span style="color: #ffffff;">Bisa memilih mengejar peluang baru di atas hubungan yang sudah ada </span></li>
<li><span style="color: #ffffff;">Gagal ketika menggunakan kesabaran untuk ketidakberanian mengambil resiko</span></li>
<li><span style="color: #ffffff;">Kegagalan terjadi karena tidak memahami atau mengendalikan kedua sisi </span></li>
<li><span style="color: #ffffff;">Gagal ketika lupa waktu dan terjebak pada sebuah tren (arus)</span></li>
</ul>
</td>
</tr>
</tbody>
</table>
<hr />
<h2 style="text-align: center;"><strong><br />PRIEST</strong>&nbsp;</h2>
<table style="height: 48px; background-color: #1289a7;" width="100%">
<tbody>
<tr style="height: 36.5312px;">
<td style="width: 288px; height: 36.5312px; text-align: center;"><span style="color: #ffffff;"><strong>KARAKTER ALAMI</strong></span></td>
</tr>
<tr style="height: 222px;">
<td style="width: 288px; height: 222px;"><span style="color: #ffffff;">&nbsp;</span>
<ul>
<li><span style="color: #ffffff;">Paling mementingkan keamanan</span></li>
<li><span style="color: #ffffff;">Bertindak berdasarkan pemikiran logis dan jarang berdasarkan perasaan</span></li>
<li><span style="color: #ffffff;">Menunda dan menunggu untuk mencapai hasil maksimal Alami dalam mengubah arus uang (cashflow) menjadi aset</span></li>
<li><span style="color: #ffffff;">Kekuatan utama pada keputusan membeli sesuatu dan menahannya</span></li>
<li><span style="color: #ffffff;">Cenderung stabil dan dapat diandalkan Fokus utama adalah menyimpan (saving) Membutuhkan banyak data untuk membuat keputusan&nbsp;</span></li>
</ul>
<p><span style="color: #ffffff;">&nbsp;</span></p>
</td>
</tr>
</tbody>
</table>
<table style="height: 48px; background-color: #1289a7;" width="100%">
<tbody>
<tr style="height: 36.5312px;">
<td style="width: 288px; height: 36.5312px; text-align: center;"><span style="color: #ffffff;"><strong>FAKTOR SUKSES</strong></span></td>
</tr>
<tr style="height: 204px;">
<td style="width: 288px; height: 204px;">
<ul>
<li><span style="color: #ffffff;">Tindakan yang diambil sudah berdasarkan pertimbangan matang </span></li>
<li><span style="color: #ffffff;">Bersedia menunggu untuk data yang cukup supaya keputusan optimal </span></li>
<li><span style="color: #ffffff;">Sabar menunggu kondisi yang tepat untuk melakukan tindakan </span></li>
<li><span style="color: #ffffff;">Cenderung punya aset yang harganya naik sejalan dengan waktu </span></li>
<li><span style="color: #ffffff;">Mampu melihat dan menggunakan aset untuk keuntungan </span></li>
<li><span style="color: #ffffff;">Tidak diperlukan banyak pengawasan dan arahan dalam mengerjakan sesuatu</span></li>
<li><span style="color: #ffffff;">Punya kemampuan alami mengambil aset dan menyimpan untuk kondisi sulit Keputusan yang dibuat cenderung aman dari berbagai alternatif&nbsp;</span></li>
</ul>
</td>
</tr>
</tbody>
</table>
<table style="height: 48px; background-color: #1289a7;" width="100%">
<tbody>
<tr style="height: 10.5312px;">
<td style="width: 288px; height: 10.5312px;">
<p style="text-align: center;"><span style="color: #ffffff;"><strong>FAKTOR KEGAGALAN</strong></span></p>
</td>
</tr>
<tr style="height: 258px;">
<td style="width: 288px; height: 258px;">
<ul>
<li><span style="color: #ffffff;">Kadang terlalu membutuhkan banyak pertimbangan dan jaminan untuk bertindak</span></li>
<li><span style="color: #ffffff;">Terkadang tidak memperhatikan efek dari sebuah keputusan pada tim dan orang lain</span></li>
<li><span style="color: #ffffff;">Ritme yang terkesan lambat dan tidak memperhatikan perasaan mitra atau tim </span></li>
<li><span style="color: #ffffff;">Bisa terjebak dan tidak memegang uang cash untuk keperluan mendadak </span></li>
<li><span style="color: #ffffff;">Terjebak dengan melewatkan momen penting untuk melepas aset </span></li>
<li><span style="color: #ffffff;">Bisa menampilkan sisi tidak berrpikir terbuka atas masukan/feedback </span></li>
<li><span style="color: #ffffff;">Seringkali terkesan pelit dan tidak peduli pada kepentingan selain dirinya </span></li>
<li><span style="color: #ffffff;">Terkesan sering menunda-nunda dalam membuat keputusan</span></li>
</ul>
</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<h2 style="text-align: center;"><strong><br />ARBITER</strong>&nbsp;</h2>
<table style="height: 48px; background-color: #81ecec;" width="100%">
<tbody>
<tr style="height: 36.5312px;">
<td style="width: 288px; height: 36.5312px; text-align: center;"><span style="color: #000000;"><strong>KARAKTER ALAMI</strong></span></td>
</tr>
<tr style="height: 222px;">
<td style="width: 288px; height: 222px;"><span style="color: #000000;">&nbsp;</span>
<ul>
<li>Mengandalkan pada koneksi dan hubungan yang ada di sekelilingnya</li>
<li>Fokus pada kemampuan negosiasi yang luar biasa serta pendengar yang baik</li>
<li>Mengembangkan diri dengan memperbesar lingkar pengaruh</li>
<li>Menciptakan sukses dengan memanfaatkan koneksi dan menciptakan kesepakatan</li>
<li>Kemampuan untuk menggunakan waktu sebagai kekuatan (saat ini, present)</li>
<li>Pengamat yang baik dalam banyak kesempatan</li>
<li>Kesuksesan berdasar pada kemudahan orang lain terkoneksi/mengakses dirinya Punya kemampuan adaptasi yang baik</li>
</ul>
</td>
</tr>
</tbody>
</table>
<table style="height: 48px; background-color: #81ecec;" width="100%">
<tbody>
<tr style="height: 36.5312px;">
<td style="width: 288px; height: 36.5312px; text-align: center;"><span style="color: #000000;"><strong>FAKTOR SUKSES</strong></span></td>
</tr>
<tr style="height: 204px;">
<td style="width: 288px; height: 204px;">
<ul>
<li>Menjaga hubungan jangka panjang dan memberikan nilai tambah</li>
<li>Mampu membuat kesepakatan yang menguntungkan semua pihak yang terlibat</li>
<li>Peluang yang semakin besar untuk melakukan kesepakatan yang menguntungkan</li>
<li>Membangun hubungan jangka panjang, untuk kestabilan income (sustainability)</li>
<li>Mampu cepat mengambil keputusan atas peluang yang ada dan muncul di saat ini</li>
<li>Cenderung bisa memanfaatkan aspek yang jarang dilihat dalam kesepakatan</li>
<li>Semakin sukses, semakin mudah dalam menciptakan peluang Bisa menjadi bunglon dalam menghadapi orang di depannya</li>
</ul>
<p><span style="color: #000000;">&nbsp;</span></p>
</td>
</tr>
</tbody>
</table>
<table style="height: 48px; background-color: #81ecec;" width="100%">
<tbody>
<tr style="height: 10.5312px;">
<td style="width: 288px; height: 10.5312px;">
<p style="text-align: center;"><span style="color: #000000;"><strong>FAKTOR KEGAGALAN</strong></span></p>
</td>
</tr>
<tr style="height: 258px;">
<td style="width: 288px; height: 258px;"><span style="color: #000000;">&nbsp;</span>
<ul>
<li>Kadang lupa untuk memperluas koneksi dan membangun hubungan baru</li>
<li>Terjebak memenangkan negosiasi tanpa mengindahkan hubungan jangka panjang</li>
<li>Bisa terperangkap melakukan detail operasional yang bukan kekuatannya</li>
<li>Jatuh apabila kesepakatan dibuat tidak menguntungkan mitra atau juga diri sendiri</li>
<li>Bisa terjebak dengan apa yang didepan mata tanpa menimbang pengalaman</li>
<li>Seringkali terjebak pada detail sehingga lupa pada nilai tambah</li>
<li>Masalah pada prioritas ketika mencoba memanfaatkan peluang yang semakin banyak</li>
<li>Bisa terjebak pada sebuah koneksi yang kurang produktif</li>
</ul>
<p><span style="color: #ffffff;">&nbsp;</span></p>
</td>
</tr>
</tbody>
</table>
<hr /><hr />
<p><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/diagram.png" alt="" width="100%" /></p>
<p>&nbsp;</p>
<table style="height: 392px; width: 100%;">
<tbody>
<tr>
<td style="width: 15.5194%;">
<p style="text-align: center;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-ALCHEMIST.png" width="100" height="100" /><br />ALCHEMIST</p>
</td>
<td style="width: 81.4806%;">
<p>Penyempurnaan yang baik, penyelesaian proses, menemukan cara yang lebih baik dab cerdas, meningkatkan efesiensi dan efektivitas produk/sitem yang ada.</p>
<p>Seseorang yang memilki kemampuan beradaptasi dengan menempatkan diri sesuai dengạn dimana ia berada. mampu untuk tetap merendah dan secara frontal berusaha untuk mengejar setiap peluang yang ada.</p>
</td>
</tr>
<tr>
<td style="width: 15.5194%; text-align: center;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-WIZARD.png" width="100" height="100" />WIZARD</td>
<td style="width: 81.4806%;">
<p>Konseptor yang &thorn;aik, penggagas hal baru, kreatif, inovatif, pencipta ide, pemikir out-of-the-box, spontan, tidak teratur, impulsif, baik untuk memulai sesuatu yang baru.</p>
<p>Seseorang yang pampu berinovasi secara berkelanjutan, seringkali kurang bisa dipahami oleh banyak orang disekitarnya, namun sanggup berpikir out-of-the-box, dalam mengbasilkan konsep-konsep baru.</p>
</td>
</tr>
<tr>
<td style="width: 15.5194%; text-align: center;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-BARD.png" width="100" height="100" />BARD</td>
<td style="width: 81.4806%;">
<p>Promotor yang baik, pemasar yang berbakat, merek pribadi yang kuat, &thorn;ergerak pat, butyh perhatian, bersinar, mampy menginspiraşi orang lain, individu yang kokoh.</p>
<p>Seseorang yang mampu memberi energi kepada orang-orang di sekelilingnya, mendukung dan menyebarkan semangat secara konsisten, sehingga mampu menyampaikan segala hal dengan penerimaan yang relatif mudah.</p>
</td>
</tr>
<tr>
<td style="width: 15.5194%; text-align: center;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-PRIEST.png" width="100" height="100" />KNIGHT</td>
<td style="width: 81.4806%;">
<p>Influencer yang baik, pemimpip yang karismatik, manajer pelaksana, penyelenggara, administrator, pendukung, baik dalam mencirtakan dan mengelola hubungan, baik dalam manajemen sumber daya manusia, fleksibel.</p>
<p>Seorang yang selalu mampu memberi semangat pada orang-orang disekitarnya, dimana api mampu menerangi kegelapan dan menyambar kemanapun, sebingga ia bisa mempengaruhi lebih banyak orang untuk sejalan seiring berjalannya waktu.</p>
</td>
</tr>
<tr>
<td style="width: 15.5194%; text-align: center;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-ARBITER.png" width="100" height="100" />ARBITER</td>
<td style="width: 81.4806%;">
<p>Penengah yang baik, pembawa damai, representatif bijak, melihat alternatif sebagai manfaat, penghindar konflik, berinyestasi pada orang untuk manfaat jangka panjang.</p>
<p>Seseorang yang tegas şerta mampu menciptakan kemungkinan baru, walaupun sebelumnya dirasakan tidak ada jalan lain, dimana angin biasanya memang tidak terlibat namun bisa dirasakan kehadirannya, mengisi apapun dan dimanapun.</p>
</td>
</tr>
<tr>
<td style="width: 15.5194%; text-align: center;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-MERCHANT.png" width="100" height="100" />MERCHANT</td>
<td style="width: 81.4806%;">
<p>Negosiator ulung, pedagang yang handal, pencari peluang, pengguna waktu yang efektif dan efisien, fleksibel, bekerja dalam pola/parameter tertentų, berdasarkan data.</p>
<p>Seseorang yang mampu memanfaatkan kekuatan dari wawasan yg luas, yang menampung apapun dari segala penjuru, mencapai keberhasilan dengan menerima berbagai hal, dan memanfaatkannva untuk keberhasilan yang lebih besar.&nbsp;</p>
</td>
</tr>
<tr>
<td style="width: 15.5194%; text-align: center;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-KNIGHT.png" width="100" height="100" />PRIEST</td>
<td style="width: 81.4806%;">
<p>Penjaga yang baik, dapat diandalkan, sabar, berhati-hati, berbasis fakta, safety- player, terstruktur, unggul dengan analisis informasi, cerdas dalam mengelola risiko.</p>
<p>Seseorang yang mampu menegakkan aturan secara tegas, disegani oleh orang- orang dibawahnya, dan ditakuti oleh siapapun yang melakukan pelanggaran aturan, dengan keteguhan tinggi sehingga mampu dijadikan sebagai tauladan (role-model).</p>
</td>
</tr>
<tr>
<td style="width: 15.5194%; text-align: center;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-MARSHAL.png" width="100" height="100" />MARSHAL</td>
<td style="width: 81.4806%;">
<p>Penganalisa yang baik, prediktif, lebih efektif di belakang layar, terencana, berkomitmen, pengatur yang handal, berkemauan keras, regulator yang baik.</p>
<p>Seseorang yang kokoh dan bisa diandalkan, terbuka untuk berbagai peluang yang lewat atau dilewatinya, dan mampu memanfaatkan apapun yang saat ini dimilikinya, termasuk memanfaatkan kekuatan waktu dan prioritas-prioritas yang tersusun dengan baik.</p>
</td>
</tr>
</tbody>
</table>



##ARBITER##
<h2 style="text-align: center;"><span style="color: #339966;">ARBITER</span></h2>
<p>Para pemilik shio ini merupakan penengah yang baik dan sangat menghindari konflik.</p>
<p>Mereka juga senang berinvestasi pada orang guna manfaat jangka panjang.</p>
<p>Seperti angin, mereka kadang tidak terlihat namun sangat terasa manfaatnya. Hal ini membuat Arbiter mampu menciptakan kemungkinan baru saat orang lain merasa tidak lagi ada jalan lain.</p>
<p><strong>KARAKTERISTIK ARBITER<br /></strong><br />&bull;Menghasilkan keuntungan dengan membuat sebuah hal yang sempurna dan kompleks, lalu memperbesar produk/proyek yang ada, sehingga menghasilkan income yang juga besar.</p>
<p>&bull; Memperoleh energi dari bertemu orang baru untuk mengembangkan berbagai hal dan ide baru, antusias dan sangat membutuhkan interaksi, bisa merasa kesepian saat sendirian.</p>
<p>&bull; Masuk dengan gambaran umum terlebih dahulu, sebelum tertarik pada detail, berpikir pada tingkat abstraksi yang tinggi dan umum, tidak suka pekerjaan yang sifatnya rutin dan detail.</p>
<p>&bull; Terkait perasaan dan pikiran orang-orang dalam lingkungan profesional, emosi menjadi penting sehingga fokus yang diberikan adalah cenderung pada orang-orang dan perasaan, mudah membangun hubungan.</p>
<p>&bull; Mengikuti instruksi dan prosedur yang ada untuk memastikan semua berjalan dengan baik, dengan kecenderungan untuk selalu menyelesaikan pekerjaan yang telah dimulai.</p>
<p>&bull; Menyukai hal yang konkret dan cenderung linier, mengerjakan sesuatu dengan hasil yang jelas, mengerjakan dan mengkomunikasikan hal-hal yang ada secara bertahap.</p>
<p>&bull; Mengambil posisi yang aman dengan cara memberi masukan atau tindakan yang dianggap masuk dalam logika berpikir, dengan segala probabilitas resiko yang bisa terjadi.</p>
<p>&bull; Mengutamakan ketepatan waktu, untuk mencapai target yang ditentukan, dengan keteraturan penyelesaian berdasarkan skala prioritas, sangat mengandalkan efektivitas dan efisiensi.</p>
<p>&bull; Responsif dalam komunikasi, mampu mengingat menggunakan kata-kata, baik dari orang lain maupun diri sendiri, dengan mengkaitkan pada emosi atau perasaan yang terjadi.</p>
<p>&bull; Mengutamakan kekuatan sosialisasi untuk mencapai tujuan yang diinginkan, tegas dalam mengutarakan sesuatu, dengan tetap selaras terhadap lingkungan dalam mencapai tujuan.</p>
<p>&bull; Fokus pada semua orang yang terlibat dalam proses pencapaian hasil, dengan tetap mengikuti kesepakatan yang sudah dibuat dengan segala konsekwensi atas kesepakatan tersebut.</p>
<p>&bull; Berpikir secara logis dan analitis, kuat dan lugas dalam memutuskan, untuk menjaga segala hal berada dalam tatanan dan struktur, cenderung memilih keputusan yang tidak terlalu ekstrim.<br /><br /></p>
<table style="width: 100%; height: 71px; background-color: #f5f5f5;">
<tbody>
<tr>
<td style="width: 100px;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-ARBITER.png" width="100" height="100" /></td>
<td style="width: 497px;">Elemen Angin melambangkan kemudahan untuk bergerak dan masuk ke segala kondisi. Seseorang yang teliti dan berhati-hati, terukur dalam bicara dan bertindak serta dalam pengambilan keputusan.</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<p>Arbiter memiliki karakteristik warna hijau terang yang terkait langsung dengan kedamaian dalam gerak, dengan kekuatan mediasi, penengah dalam perbedaan, penghindaran konflik, bersifat praktis dan membumi, hasrat bermain dengan optimisme dan keceriaan .</p>
<h2 style="text-align: center;"><br /><span style="color: #339966;">KEKUATAN ARBITER : KETELITIAN</span><br /><br /></h2>
<table style="height: 78px; width: 100%; background: #f5f5f5; padding: 20; background-color: #339966;">
<tbody>
<tr>
<td style="width: 30%; padding: 20;">
<p><span style="color: #ffffff;"><strong>Sisi Produktif</strong></span></p>
</td>
<td style="width: 70%;">
<p><span style="color: #ffffff;">Kompeten, kesehatan, selera bagus, efisien, kebahagiaan, universal, istirahat, kesadaran, ketabahan, semangat, santai, keingintahuan.</span></p>
</td>
</tr>
<tr>
<td style="width: 174px;">
<p><span style="color: #ffffff;"><strong>Sisi Tantangan</strong></span></p>
</td>
<td style="width: 416px;">
<p><span style="color: #ffffff;">Cemburu, kualitas rendah, tidak berharga, tertolak, kerapuhan, kecemasan, indesisif, mengorbankan diri, kekhawatiran.</span></p>
</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<h2 class="p1" style="text-align: center;"><span style="color: #339966;">SELF-LEADERSHIP</span></h2>
<p>Pemimpin yang senantiasa mampu bergerak cepat, fleksibel terhadap kemungkinan yang terjadi, dalam hubungannya dengan berbagai macam kepentingan.<br /><br />Pemimpin model ini tidak mudah lelah untuk menjadi jembatan atas sebuah hubungan dengan orang lain, dan untuk mendapatkan kesepakatan yang diperlukan demi kemajuan bersama. <br /><br />Seorang pemimpin yang bertumpu pada fakta-fakta, dan mampu menunjukkan kepada orang lain tentang peluang yang ada serta pengambilan keputusan yang lebih efektif bagi semua pihak.</p>
<p class="p1">&nbsp;</p>
<!--ADDITIONAL--->
<h2 class="p1" style="text-align: center; color: #008000;"><br /><strong>PROFIL: UTAMA &amp; BAYANGAN</strong>&nbsp;</h2>
<p>Profil Utama (main profile) Shio Kaya adalah bagian dominan dari diri Anda, yang sebenarnya mendorong dan menginspirasi diri Anda.</p>
<p>Profil utama (main profile) mencakup karakteristik alamiah Anda, yang dapat membantu Anda dalam membangun keberhasilan dengan lebih efektif dan efisien, menghemat waktu dan tenaga Anda.</p>
<p>Profil Bayangan (shadow profile) Shio Kaya merupakan bagian resesif (non-dominan) dari diri Anda, dengan hanya beberapa karakteristik yang muncul.</p>
<p>Profil bayangan (shadow profile) merupakan karakteristik yang bersifat seperti bayangan, bagian dari diri Anda, tak terpisahkan, tapi bukan mencerminkan diri Anda sepenuhnya.<br /><br />Profil Bayangan dari <strong>ARBITER</strong>&nbsp;adalah <strong>KNIGHT</strong> dan <strong>MERCHANT</strong></p>
<p>&nbsp;</p>
<h2 style="text-align: center;"><strong><br />ARBITER</strong>&nbsp;</h2>
<table style="height: 48px; background-color: #81ecec;" width="100%">
<tbody>
<tr style="height: 36.5312px;">
<td style="width: 288px; height: 36.5312px; text-align: center;"><span style="color: #000000;"><strong>KARAKTER ALAMI</strong></span></td>
</tr>
<tr style="height: 222px;">
<td style="width: 288px; height: 222px;"><span style="color: #000000;">&nbsp;</span>
<ul>
<li>Mengandalkan pada koneksi dan hubungan yang ada di sekelilingnya</li>
<li>Fokus pada kemampuan negosiasi yang luar biasa serta pendengar yang baik</li>
<li>Mengembangkan diri dengan memperbesar lingkar pengaruh</li>
<li>Menciptakan sukses dengan memanfaatkan koneksi dan menciptakan kesepakatan</li>
<li>Kemampuan untuk menggunakan waktu sebagai kekuatan (saat ini, present)</li>
<li>Pengamat yang baik dalam banyak kesempatan</li>
<li>Kesuksesan berdasar pada kemudahan orang lain terkoneksi/mengakses dirinya Punya kemampuan adaptasi yang baik</li>
</ul>
</td>
</tr>
</tbody>
</table>
<table style="height: 48px; background-color: #81ecec;" width="100%">
<tbody>
<tr style="height: 36.5312px;">
<td style="width: 288px; height: 36.5312px; text-align: center;"><span style="color: #000000;"><strong>FAKTOR SUKSES</strong></span></td>
</tr>
<tr style="height: 204px;">
<td style="width: 288px; height: 204px;">
<ul>
<li>Menjaga hubungan jangka panjang dan memberikan nilai tambah</li>
<li>Mampu membuat kesepakatan yang menguntungkan semua pihak yang terlibat</li>
<li>Peluang yang semakin besar untuk melakukan kesepakatan yang menguntungkan</li>
<li>Membangun hubungan jangka panjang, untuk kestabilan income (sustainability)</li>
<li>Mampu cepat mengambil keputusan atas peluang yang ada dan muncul di saat ini</li>
<li>Cenderung bisa memanfaatkan aspek yang jarang dilihat dalam kesepakatan</li>
<li>Semakin sukses, semakin mudah dalam menciptakan peluang Bisa menjadi bunglon dalam menghadapi orang di depannya</li>
</ul>
<p><span style="color: #000000;">&nbsp;</span></p>
</td>
</tr>
</tbody>
</table>
<table style="height: 48px; background-color: #81ecec;" width="100%">
<tbody>
<tr style="height: 10.5312px;">
<td style="width: 288px; height: 10.5312px;">
<p style="text-align: center;"><span style="color: #000000;"><strong>FAKTOR KEGAGALAN</strong></span></p>
</td>
</tr>
<tr style="height: 258px;">
<td style="width: 288px; height: 258px;"><span style="color: #000000;">&nbsp;</span>
<ul>
<li>Kadang lupa untuk memperluas koneksi dan membangun hubungan baru</li>
<li>Terjebak memenangkan negosiasi tanpa mengindahkan hubungan jangka panjang</li>
<li>Bisa terperangkap melakukan detail operasional yang bukan kekuatannya</li>
<li>Jatuh apabila kesepakatan dibuat tidak menguntungkan mitra atau juga diri sendiri</li>
<li>Bisa terjebak dengan apa yang didepan mata tanpa menimbang pengalaman</li>
<li>Seringkali terjebak pada detail sehingga lupa pada nilai tambah</li>
<li>Masalah pada prioritas ketika mencoba memanfaatkan peluang yang semakin banyak</li>
<li>Bisa terjebak pada sebuah koneksi yang kurang produktif</li>
</ul>
<p><span style="color: #ffffff;">&nbsp;</span></p>
</td>
</tr>
</tbody>
</table>
<hr />
<h2 style="text-align: center;"><strong><br />MERCHANT</strong>&nbsp;</h2>
<table style="height: 48px; background-color: #008000;" width="100%">
<tbody>
<tr style="height: 36.5312px;">
<td style="width: 288px; height: 36.5312px; text-align: center;"><span style="color: #ffffff;"><strong>KARAKTER ALAMI</strong></span></td>
</tr>
<tr style="height: 222px;">
<td style="width: 288px; height: 222px;"><span style="color: #ffffff;">&nbsp;</span>
<ul>
<li><span style="color: #ffffff;">Sangat baik dalam pengaturan waktu </span></li>
<li><span style="color: #ffffff;">Secara alami mampu melihat peluang menjual dan membeli </span></li>
<li><span style="color: #ffffff;">Mampu melepaskan diri dari ikatan produk atau kesempatan </span></li>
<li><span style="color: #ffffff;">Cenderung damai, mudah sepakat, dan menghindari konflik </span></li>
<li><span style="color: #ffffff;">Peluang merupakan nilai utama yang terpenting </span></li>
<li><span style="color: #ffffff;">Punya kesabaran yang baik dalam menunggu waktu yang tepat </span></li>
<li><span style="color: #ffffff;">Sanggup melihat dari sisi pandang penjual dan pembeli </span></li>
<li><span style="color: #ffffff;">Tidak mudah terjebak pada trend (arus) dan berani melawan arus</span></li>
</ul>
</td>
</tr>
</tbody>
</table>
<table style="height: 48px; background-color: #008000;" width="100%">
<tbody>
<tr style="height: 36.5312px;">
<td style="width: 288px; height: 36.5312px; text-align: center;"><span style="color: #ffffff;"><strong>FAKTOR SUKSES</strong></span></td>
</tr>
<tr style="height: 204px;">
<td style="width: 288px; height: 204px;">
<ul>
<li><span style="color: #ffffff;">Dihargai dan dipercaya karena menunjukkan ketepatan waktu </span></li>
<li><span style="color: #ffffff;">Sukses dengan sigap memanfaatkan peluang yang muncul dimanapun </span></li>
<li><span style="color: #ffffff;">Mampu untuk menjual produk apapun tanpa terikat selama ada kesempatan Mediator yang baik dan cepat dalam mencapai kesepakatan </span></li>
<li><span style="color: #ffffff;">Menghargai setiap peluang dan kecepatan untuk menangkapnya </span></li>
<li><span style="color: #ffffff;">Menempatkan hal-hal di posisi tepat untuk menghasilkan keuntungan terbaik</span></li>
<li><span style="color: #ffffff;">Mampu menguasai dari sisi pembeli dan penjual dan mengambil keuntungan Bisa bergerak bebas ke arah dimana keuntungan berada</span></li>
</ul>
</td>
</tr>
</tbody>
</table>
<table style="height: 48px; background-color: #008000;" width="100%">
<tbody>
<tr style="height: 10.5312px;">
<td style="width: 288px; height: 10.5312px;">
<p style="text-align: center;"><span style="color: #ffffff;"><strong>FAKTOR KEGAGALAN</strong></span></p>
</td>
</tr>
<tr style="height: 258px;">
<td style="width: 288px; height: 258px;"><span style="color: #ffffff;">&nbsp;</span>
<ul>
<li><span style="color: #ffffff;">Mudah terganggu ketika ada yang berjalan tidak dalam waktu yang semestinya</span></li>
<li><span style="color: #ffffff;">Seringkali terjebak dalam mengambil peluang tanpa analisa matang </span></li>
<li><span style="color: #ffffff;">Tidak loyal pada sebuah produk dan bisa berubah begitu ada peluang lain</span></li>
<li><span style="color: #ffffff;">Cenderung mau gampang dan menjadi penghindar masalah </span></li>
<li><span style="color: #ffffff;">Bisa memilih mengejar peluang baru di atas hubungan yang sudah ada </span></li>
<li><span style="color: #ffffff;">Gagal ketika menggunakan kesabaran untuk ketidakberanian mengambil resiko</span></li>
<li><span style="color: #ffffff;">Kegagalan terjadi karena tidak memahami atau mengendalikan kedua sisi </span></li>
<li><span style="color: #ffffff;">Gagal ketika lupa waktu dan terjebak pada sebuah tren (arus)</span></li>
</ul>
</td>
</tr>
</tbody>
</table>
<hr />
<h2 style="text-align: center;"><strong><br />KNIGHT</strong>&nbsp;</h2>
<table style="height: 48px; background-color: #ff9f43;" width="100%">
<tbody>
<tr style="height: 36.5312px;">
<td style="width: 288px; height: 36.5312px; text-align: center;"><span style="color: #000000;"><strong>KARAKTER ALAMI</strong></span></td>
</tr>
<tr style="height: 222px;">
<td style="width: 288px; height: 222px;"><span style="color: #ffffff;">&nbsp;</span>
<ul>
<li>Punya keterampilan pengelolaan emosi yang baik</li>
<li>Cenderung punya sifat kepemimpinan yang baik secara alami</li>
<li>Kemampuan yang baik dalam memotivasi tim</li>
<li>Memiliki kemampuan komunikasi yang baik dengan orang lain</li>
<li>Punya kemampuan untuk menempatkan diri dalam berbagai situasi</li>
<li>Mudah berkembang dengan kepercayaan penuh untuk memimpin</li>
<li>Nyaman dalam membangun interaksi dengan banyak orang baru Hubungan termasuk merupakan aset yang penting</li>
</ul>
<p><span style="color: #ffffff;">&nbsp;</span></p>
</td>
</tr>
</tbody>
</table>
<table style="height: 48px; background-color: #ff9f43;" width="100%">
<tbody>
<tr style="height: 36.5312px;">
<td style="width: 288px; height: 36.5312px; text-align: center;"><span style="color: #000000;"><strong>FAKTOR SUKSES</strong></span></td>
</tr>
<tr style="height: 204px;">
<td style="width: 288px; height: 204px;">
<ul>
<li>Dapat diandalkan dan menjadi favorit banyak orang</li>
<li>Mampu memimpin dengan efektif, terhadap tim, mitra, dan customer</li>
<li>Team-player yang baik dan mendorong tim untuk mencapai target</li>
<li>Mampu untuk mengerti orang lain dan membangun hubungan yang kondusif</li>
<li>Bisa bermain peran sesuai dengan kebutuhan saat itu</li>
<li>Cenderung menjadi pemimpin yang kuat dan kompeten</li>
<li>Menjadi pembangun jaringan yang baik, dengan lobby-lobby yang efektfi</li>
<li>Bagus dalam membangun hubungan baru dan mempertahankan hubungan lama</li>
</ul>
</td>
</tr>
</tbody>
</table>
<table style="height: 48px; background-color: #ff9f43;" width="100%">
<tbody>
<tr style="height: 10.5312px;">
<td style="width: 288px; height: 10.5312px;">
<p style="text-align: center;"><span style="color: #000000;"><strong>FAKTOR KEGAGALAN</strong></span></p>
</td>
</tr>
<tr style="height: 258px;">
<td style="width: 288px; height: 258px;">
<ul>
<li>Mungkin terjebak dengan merasa lebih daripada orang lain</li>
<li>Cenderung menjadi orang yang terlalu menghakimi atau menilai orang lain Kehilangan kepercayaan karena kata-kata yang tidak didukung dengan tindakan nyata</li>
<li>Sering menggampangkan sesuatu karena merasa bisa diselesaikan dengan komunikasi</li>
<li>Bisa menjadi manipulatif dalam menjalani sebuah hubungan</li>
<li>Bisa terjebak menjadi over-confident dalam membuat berbagai keputusan Seringkali menjadi kurang asertif terhadap orang lain</li>
<li>Bermasalah dengan skala prioritas dan ketepatan waktu</li>
</ul>
</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<hr />
<p><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/diagram.png" alt="" width="100%" /></p>
<p>&nbsp;</p>
<table style="height: 392px; width: 100%;">
<tbody>
<tr>
<td style="width: 15.5194%;">
<p style="text-align: center;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-ALCHEMIST.png" width="100" height="100" /><br />ALCHEMIST</p>
</td>
<td style="width: 81.4806%;">
<p>Penyempurnaan yang baik, penyelesaian proses, menemukan cara yang lebih baik dab cerdas, meningkatkan efesiensi dan efektivitas produk/sitem yang ada.</p>
<p>Seseorang yang memilki kemampuan beradaptasi dengan menempatkan diri sesuai dengạn dimana ia berada. mampu untuk tetap merendah dan secara frontal berusaha untuk mengejar setiap peluang yang ada.</p>
</td>
</tr>
<tr>
<td style="width: 15.5194%; text-align: center;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-WIZARD.png" width="100" height="100" />WIZARD</td>
<td style="width: 81.4806%;">
<p>Konseptor yang &thorn;aik, penggagas hal baru, kreatif, inovatif, pencipta ide, pemikir out-of-the-box, spontan, tidak teratur, impulsif, baik untuk memulai sesuatu yang baru.</p>
<p>Seseorang yang pampu berinovasi secara berkelanjutan, seringkali kurang bisa dipahami oleh banyak orang disekitarnya, namun sanggup berpikir out-of-the-box, dalam mengbasilkan konsep-konsep baru.</p>
</td>
</tr>
<tr>
<td style="width: 15.5194%; text-align: center;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-BARD.png" width="100" height="100" />BARD</td>
<td style="width: 81.4806%;">
<p>Promotor yang baik, pemasar yang berbakat, merek pribadi yang kuat, &thorn;ergerak pat, butyh perhatian, bersinar, mampy menginspiraşi orang lain, individu yang kokoh.</p>
<p>Seseorang yang mampu memberi energi kepada orang-orang di sekelilingnya, mendukung dan menyebarkan semangat secara konsisten, sehingga mampu menyampaikan segala hal dengan penerimaan yang relatif mudah.</p>
</td>
</tr>
<tr>
<td style="width: 15.5194%; text-align: center;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-PRIEST.png" width="100" height="100" />KNIGHT</td>
<td style="width: 81.4806%;">
<p>Influencer yang baik, pemimpip yang karismatik, manajer pelaksana, penyelenggara, administrator, pendukung, baik dalam mencirtakan dan mengelola hubungan, baik dalam manajemen sumber daya manusia, fleksibel.</p>
<p>Seorang yang selalu mampu memberi semangat pada orang-orang disekitarnya, dimana api mampu menerangi kegelapan dan menyambar kemanapun, sebingga ia bisa mempengaruhi lebih banyak orang untuk sejalan seiring berjalannya waktu.</p>
</td>
</tr>
<tr>
<td style="width: 15.5194%; text-align: center;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-ARBITER.png" width="100" height="100" />ARBITER</td>
<td style="width: 81.4806%;">
<p>Penengah yang baik, pembawa damai, representatif bijak, melihat alternatif sebagai manfaat, penghindar konflik, berinyestasi pada orang untuk manfaat jangka panjang.</p>
<p>Seseorang yang tegas şerta mampu menciptakan kemungkinan baru, walaupun sebelumnya dirasakan tidak ada jalan lain, dimana angin biasanya memang tidak terlibat namun bisa dirasakan kehadirannya, mengisi apapun dan dimanapun.</p>
</td>
</tr>
<tr>
<td style="width: 15.5194%; text-align: center;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-MERCHANT.png" width="100" height="100" />MERCHANT</td>
<td style="width: 81.4806%;">
<p>Negosiator ulung, pedagang yang handal, pencari peluang, pengguna waktu yang efektif dan efisien, fleksibel, bekerja dalam pola/parameter tertentų, berdasarkan data.</p>
<p>Seseorang yang mampu memanfaatkan kekuatan dari wawasan yg luas, yang menampung apapun dari segala penjuru, mencapai keberhasilan dengan menerima berbagai hal, dan memanfaatkannva untuk keberhasilan yang lebih besar.&nbsp;</p>
</td>
</tr>
<tr>
<td style="width: 15.5194%; text-align: center;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-KNIGHT.png" width="100" height="100" />PRIEST</td>
<td style="width: 81.4806%;">
<p>Penjaga yang baik, dapat diandalkan, sabar, berhati-hati, berbasis fakta, safety- player, terstruktur, unggul dengan analisis informasi, cerdas dalam mengelola risiko.</p>
<p>Seseorang yang mampu menegakkan aturan secara tegas, disegani oleh orang- orang dibawahnya, dan ditakuti oleh siapapun yang melakukan pelanggaran aturan, dengan keteguhan tinggi sehingga mampu dijadikan sebagai tauladan (role-model).</p>
</td>
</tr>
<tr>
<td style="width: 15.5194%; text-align: center;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-MARSHAL.png" width="100" height="100" />MARSHAL</td>
<td style="width: 81.4806%;">
<p>Penganalisa yang baik, prediktif, lebih efektif di belakang layar, terencana, berkomitmen, pengatur yang handal, berkemauan keras, regulator yang baik.</p>
<p>Seseorang yang kokoh dan bisa diandalkan, terbuka untuk berbagai peluang yang lewat atau dilewatinya, dan mampu memanfaatkan apapun yang saat ini dimilikinya, termasuk memanfaatkan kekuatan waktu dan prioritas-prioritas yang tersusun dengan baik.</p>
</td>
</tr>
</tbody>
</table>



###KNIGHT###


<h2 style="text-align: center;"><span style="color: #ffcc00;">KNIGHT</span></h2>
<p>Para pemilik shio ini merupakan influencer dan pemimpin yang karismatik.</p>
<p>Mereka orang yang mampu memberi semangat selayaknya api yang mampu menerangi kegelapan dan mampu mempengaruhi orang lain dengan mudah.</p>
<p>Hal ini membuat Knight sangat handal dalam mengelola dan menciptakan manajemen SDM dan tim pelaksana yang baik.<br /><br />KARAKTERISTIK PROFIL KNIGHT<br /><br />&bull; Menghasilkan keuntungan dengan membuat sebuah hal yang sempurna dan kompleks, lalu memperbesar produk yang ada, sehingga menghasilkan income yang juga besar.</p>
<p>&bull; Memperoleh energi dari bertemu orang baru untuk mengembangkan berbagai hal dan ide baru, antusias dan membutuhkan aktivitas/interaksi, bisa merasa kesepian saat sendirian.</p>
<p>&bull; Masuk dengan gambaran umum terlebih dahulu, sebelum tertarik pada detail, berpikir pada tingkat abstraksi yang tinggi dan umum, tidak suka pekerjaan yang sifatnya rutin dan detail.</p>
<p>&bull; Terkait perasaan dan pikiran orang-orang dalam lingkungan profesional, emosi menjadi penting sehingga fokus yang diberikan adalah cenderung pada orang-orang dan perasaan, mudah membangun hubungan.</p>
<p>&bull; Menjalani instruksi dan prosedur secara baik, tidak terganggu oleh rutinitas, namun mampu untuk melihat celah dan kesempatan dalam meraih target/tujuan dengan lebih cepat.</p>
<p>&bull; Mendahulukan fakta dan data yang konkret untuk memilih pengerjaan sesuatu di saat ini dan jelas, namun bisa memanfaatkannya untuk melihat peluang yang ada di depan dan segala kemungkinannya.</p>
<p>&bull; Memastikan keamanan dengan mempertimbangkan segala hal yang masuk dalam logika, probabilitas resiko, dan memilih dengan berani hal yang sudah diperhitungkan tersebut.</p>
<p>&bull; Mementingkan keteraturan dan prioritas, namun tetap melakukan perbaikan terus menerus selama proses, yang semuanya perlu diatur dengan rambu-rambu dan indikator yang jelas.</p>
<p>&bull; Responsif terhadap perubahan suara dalam komunikasi, mampu mengingat instruksi yang disampaikan dengan lebih mudah, lebih suka berkomunikasi melalui lisan daripada tertulis.</p>
<p>&bull; Mengutamakan kekuatan dalam bersosialisasi, mencapai hal-hal yang memberikan dampak bagi diri sendiri dan sekitarnya, mampu untuk tegas dalam mengutarakan sesuatu.</p>
<p>&bull; Fokus pada orang-orang yang terlibat dalam proses pencapaian hasil, bagaimana efek terhadap citra, cenderung mengutamakan faktor orang-orang yang mendukung pencapaian hasil daripada yang lainnya.</p>
<p>&bull; Berpikir diposisi yang sifatnya mendukung dan cenderung netral, lebih mengedepankan kestabilan dan kedamaian, cenderung mendukung keputusan yang tidak menimbulkan konflik.</p>
<p>&nbsp;</p>
<table style="width: 100%; height: 71px; background-color: #f5f5f5;">
<tbody>
<tr>
<td style="width: 100px;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-PRIEST.png" width="100" height="100" /></td>
<td style="width: 497px;">Elemen Api melambangkan kemampuan untuk membakar apapun tanpa kecuali. Simbol dari sifat yang tegas dan lugas. Seorang yang cekatan dan tuntas dalam menyelesaikan persoalan, konsisten dan objektif.</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<p>Knight memiliki karakteristik warna kuning yang cerah atau ceria, yang dapat merangsang otak serta membuat lebih waspada dan tegas, dapat menarik perhatian tetapi tidak terlalu menonjol, juga terkadang mampu memberikan energi tetapi tidak menohok.</p>
<h2 style="text-align: center;"><br /><span style="color: #ffcc00;">KEKUATAN KNIGHT : HUBUNGAN</span><br /><br /></h2>
<table style="height: 78px; width: 100%; background: #f5f5f5; padding: 20; background-color: #ffcc00;">
<tbody>
<tr>
<td style="width: 30%; padding: 20;">
<p><span style="color: #000000;"><strong>Sisi Produktif</strong></span></p>
</td>
<td style="width: 70%;">
<p>Optimis, percaya diri, toleran, suka cita, antusiasme, kekeluargaan, persahabatan, keleluasaan, spontanitas, keramahan, kedermawanan.</p>
</td>
</tr>
<tr>
<td style="width: 174px;">
<p><span style="color: #000000;"><strong>Sisi Tantangan</strong></span></p>
</td>
<td style="width: 416px;">
<p>Berubah-ubah, kurang dapat diandalkan, irasionalitas, ketakutan, kerapuhan, emosional, depresi, kecemasan, menyalahkan diri sendiri</p>
</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<h2 class="p1" style="text-align: center;"><span style="color: #ffcc00;">SELF-LEADERSHIP</span></h2>
<p>Pemimpin yang mampu mendorong orang lain yang dipimpinnya untuk memiliki semangat yang sama besar.</p>
<p>Seorang pemimpin tipe ini juga memiliki prinsip mampu beradaptasi dimanapun ia berada.</p>
<p>Lingkungan dan orang-orang di sekitarnya, merupakan bahan bakar bagi pemimpin model ini, dimana ia perlu untuk bisa terkoneksi secara dalam dan benar-benar mengendalikan organisasi yang dipimpinnya, dan memastikan kepentingan yang menjadi tujuannya dapat tercapai.</p>
<p><br />Seorang pemimpin yang bertumpu pada fakta-fakta, dan mampu menunjukkan kepada orang lain tentang peluang yang ada serta pengambilan keputusan yang lebih efektif bagi semua pihak.</p>
<p class="p1">&nbsp;</p>
<h2 class="p1" style="text-align: center; color: #ffcc00;"><br /><strong>PROFIL: UTAMA &amp; BAYANGAN</strong>&nbsp;</h2>
<p>Profil Utama (main profile) Shio Kaya adalah bagian dominan dari diri Anda, yang sebenarnya mendorong dan menginspirasi diri Anda.</p>
<p>Profil utama (main profile) mencakup karakteristik alamiah Anda, yang dapat membantu Anda dalam membangun keberhasilan dengan lebih efektif dan efisien, menghemat waktu dan tenaga Anda.</p>
<p>Profil Bayangan (shadow profile) Shio Kaya merupakan bagian resesif (non-dominan) dari diri Anda, dengan hanya beberapa karakteristik yang muncul.</p>
<p>Profil bayangan (shadow profile) merupakan karakteristik yang bersifat seperti bayangan, bagian dari diri Anda, tak terpisahkan, tapi bukan mencerminkan diri Anda sepenuhnya.<br /><br />Profil Bayangan dari <strong>KNIGHT</strong>&nbsp;adalah <strong>BARD</strong>&nbsp;dan <strong>ARBITER</strong></p>
<p>&nbsp;</p>
<h2 style="text-align: center;"><strong><br />KNIGHT</strong>&nbsp;</h2>
<table style="height: 48px; background-color: #feca57;" width="100%">
<tbody>
<tr style="height: 36.5312px;">
<td style="width: 288px; height: 36.5312px; text-align: center;"><span style="color: #000000;"><strong>KARAKTER ALAMI</strong></span></td>
</tr>
<tr style="height: 222px;">
<td style="width: 288px; height: 222px;"><span style="color: #ffffff;">&nbsp;</span>
<ul>
<li>Punya keterampilan pengelolaan emosi yang baik</li>
<li>Cenderung punya sifat kepemimpinan yang baik secara alami</li>
<li>Kemampuan yang baik dalam memotivasi tim</li>
<li>Memiliki kemampuan komunikasi yang baik dengan orang lain</li>
<li>Punya kemampuan untuk menempatkan diri dalam berbagai situasi</li>
<li>Mudah berkembang dengan kepercayaan penuh untuk memimpin</li>
<li>Nyaman dalam membangun interaksi dengan banyak orang baru Hubungan termasuk merupakan aset yang penting</li>
</ul>
<p><span style="color: #ffffff;">&nbsp;</span></p>
</td>
</tr>
</tbody>
</table>
<table style="height: 48px; background-color: #feca57;" width="100%">
<tbody>
<tr style="height: 36.5312px;">
<td style="width: 288px; height: 36.5312px; text-align: center;"><span style="color: #000000;"><strong>FAKTOR SUKSES</strong></span></td>
</tr>
<tr style="height: 204px;">
<td style="width: 288px; height: 204px;">
<ul>
<li>Dapat diandalkan dan menjadi favorit banyak orang</li>
<li>Mampu memimpin dengan efektif, terhadap tim, mitra, dan customer</li>
<li>Team-player yang baik dan mendorong tim untuk mencapai target</li>
<li>Mampu untuk mengerti orang lain dan membangun hubungan yang kondusif</li>
<li>Bisa bermain peran sesuai dengan kebutuhan saat itu</li>
<li>Cenderung menjadi pemimpin yang kuat dan kompeten</li>
<li>Menjadi pembangun jaringan yang baik, dengan lobby-lobby yang efektfi</li>
<li>Bagus dalam membangun hubungan baru dan mempertahankan hubungan lama</li>
</ul>
</td>
</tr>
</tbody>
</table>
<table style="height: 48px; background-color: #feca57;" width="100%">
<tbody>
<tr style="height: 10.5312px;">
<td style="width: 288px; height: 10.5312px;">
<p style="text-align: center;"><span style="color: #000000;"><strong>FAKTOR KEGAGALAN</strong></span></p>
</td>
</tr>
<tr style="height: 258px;">
<td style="width: 288px; height: 258px;">
<ul>
<li>Mungkin terjebak dengan merasa lebih daripada orang lain</li>
<li>Cenderung menjadi orang yang terlalu menghakimi atau menilai orang lain Kehilangan kepercayaan karena kata-kata yang tidak didukung dengan tindakan nyata</li>
<li>Sering menggampangkan sesuatu karena merasa bisa diselesaikan dengan komunikasi</li>
<li>Bisa menjadi manipulatif dalam menjalani sebuah hubungan</li>
<li>Bisa terjebak menjadi over-confident dalam membuat berbagai keputusan Seringkali menjadi kurang asertif terhadap orang lain</li>
<li>Bermasalah dengan skala prioritas dan ketepatan waktu</li>
</ul>
</td>
</tr>
</tbody>
</table>
<h2 style="text-align: center;"><strong><br />ARBITER</strong>&nbsp;</h2>
<table style="height: 48px; background-color: #81ecec;" width="100%">
<tbody>
<tr style="height: 36.5312px;">
<td style="width: 288px; height: 36.5312px; text-align: center;"><span style="color: #000000;"><strong>KARAKTER ALAMI</strong></span></td>
</tr>
<tr style="height: 222px;">
<td style="width: 288px; height: 222px;"><span style="color: #000000;">&nbsp;</span>
<ul>
<li>Mengandalkan pada koneksi dan hubungan yang ada di sekelilingnya</li>
<li>Fokus pada kemampuan negosiasi yang luar biasa serta pendengar yang baik</li>
<li>Mengembangkan diri dengan memperbesar lingkar pengaruh</li>
<li>Menciptakan sukses dengan memanfaatkan koneksi dan menciptakan kesepakatan</li>
<li>Kemampuan untuk menggunakan waktu sebagai kekuatan (saat ini, present)</li>
<li>Pengamat yang baik dalam banyak kesempatan</li>
<li>Kesuksesan berdasar pada kemudahan orang lain terkoneksi/mengakses dirinya Punya kemampuan adaptasi yang baik</li>
</ul>
</td>
</tr>
</tbody>
</table>
<table style="height: 48px; background-color: #81ecec;" width="100%">
<tbody>
<tr style="height: 36.5312px;">
<td style="width: 288px; height: 36.5312px; text-align: center;"><span style="color: #000000;"><strong>FAKTOR SUKSES</strong></span></td>
</tr>
<tr style="height: 204px;">
<td style="width: 288px; height: 204px;">
<ul>
<li>Menjaga hubungan jangka panjang dan memberikan nilai tambah</li>
<li>Mampu membuat kesepakatan yang menguntungkan semua pihak yang terlibat</li>
<li>Peluang yang semakin besar untuk melakukan kesepakatan yang menguntungkan</li>
<li>Membangun hubungan jangka panjang, untuk kestabilan income (sustainability)</li>
<li>Mampu cepat mengambil keputusan atas peluang yang ada dan muncul di saat ini</li>
<li>Cenderung bisa memanfaatkan aspek yang jarang dilihat dalam kesepakatan</li>
<li>Semakin sukses, semakin mudah dalam menciptakan peluang Bisa menjadi bunglon dalam menghadapi orang di depannya</li>
</ul>
<p><span style="color: #000000;">&nbsp;</span></p>
</td>
</tr>
</tbody>
</table>
<table style="height: 48px; background-color: #81ecec;" width="100%">
<tbody>
<tr style="height: 10.5312px;">
<td style="width: 288px; height: 10.5312px;">
<p style="text-align: center;"><span style="color: #000000;"><strong>FAKTOR KEGAGALAN</strong></span></p>
</td>
</tr>
<tr style="height: 258px;">
<td style="width: 288px; height: 258px;"><span style="color: #000000;">&nbsp;</span>
<ul>
<li>Kadang lupa untuk memperluas koneksi dan membangun hubungan baru</li>
<li>Terjebak memenangkan negosiasi tanpa mengindahkan hubungan jangka panjang</li>
<li>Bisa terperangkap melakukan detail operasional yang bukan kekuatannya</li>
<li>Jatuh apabila kesepakatan dibuat tidak menguntungkan mitra atau juga diri sendiri</li>
<li>Bisa terjebak dengan apa yang didepan mata tanpa menimbang pengalaman</li>
<li>Seringkali terjebak pada detail sehingga lupa pada nilai tambah</li>
<li>Masalah pada prioritas ketika mencoba memanfaatkan peluang yang semakin banyak</li>
<li>Bisa terjebak pada sebuah koneksi yang kurang produktif</li>
</ul>
<p><span style="color: #ffffff;">&nbsp;</span></p>
</td>
</tr>
</tbody>
</table>
<hr />
<h2 style="text-align: center;"><strong><br />BARD</strong>&nbsp;</h2>
<table style="height: 48px; background-color: #ff9f43;" width="100%">
<tbody>
<tr style="height: 36.5312px;">
<td style="width: 288px; height: 36.5312px; text-align: center;"><span style="color: #000000;"><strong>KARAKTER ALAMI</strong></span></td>
</tr>
<tr style="height: 222px;">
<td style="width: 288px; height: 222px;"><span style="color: #ffffff;">&nbsp;</span>
<ul>
<li>Mengenal potensi diri mereka dengan baik</li>
<li>Percaya diri, sanggup mempertahankan pendapat dan mampu berkomunikasi dengan meyakinkan</li>
<li>Menjadi perhatian utama dalam setiap pertunjukan</li>
<li>Berani cepat dalam mengambil keputusan dan menciptakan sesuatu</li>
<li>Seringkali mengerjakan semua sendiri secara one-manshow</li>
<li>Kualitas diri merupakan hal yang sangat penting</li>
<li>Mampu impulsif, spontan, dan cepat</li>
</ul>
</td>
</tr>
</tbody>
</table>
<table style="height: 48px; background-color: #ff9f43;" width="100%">
<tbody>
<tr style="height: 36.5312px;">
<td style="width: 288px; height: 36.5312px; text-align: center;"><span style="color: #000000;"><strong>FAKTOR SUKSES</strong></span></td>
</tr>
<tr style="height: 204px;">
<td style="width: 288px; height: 204px;">
<ul>
<li>Mampu menjual potensi dan image diri dengan efektif</li>
<li>Sanggup untuk mempengaruhi banyak orang</li>
<li>Mampu menggunakan karisma pribadi untuk menjual apapun</li>
<li>Menjadi sosok yang disegani karena seringkali dianggap kompeten</li>
<li>Cepat dalam berpendapat dan memutuskan hal-hal yang bersifat urgent</li>
<li>Mencapai keberhasilan yang luar biasa ketika bermitra</li>
<li>Berpikiran terbuka, berani berinvestasi pada banyak hal</li>
<li>Cepat dalam mengambil risiko dan peluang</li>
</ul>
</td>
</tr>
</tbody>
</table>
<table style="height: 48px; background-color: #ff9f43;" width="100%">
<tbody>
<tr style="height: 10.5312px;">
<td style="width: 288px; height: 10.5312px;">
<p style="text-align: center;"><span style="color: #000000;"><strong>FAKTOR KEGAGALAN</strong></span></p>
</td>
</tr>
<tr style="height: 258px;">
<td style="width: 288px; height: 258px;"><span style="color: #ffffff;">&nbsp;</span>
<ul>
<li>Seringkali terlalu percaya diri dan terkesan arogan</li>
<li>Pendapat bisa tidak didukung fakta dan kompetensi yang tepat</li>
<li>Sanggup melakukan hal-hal yang akhirnya kontraproduktif untuk eksis</li>
<li>Sering menciptakan alasan-alasan atas kegagalan diri</li>
<li>Pendapat dan keputusan yang diambil seringkali tanpa pemikiran yang matang</li>
<li>Termasuk sulit untuk bisa mempercayai kualitas kerja orang lain</li>
<li>Seringkali boros dan tidak menghitung dengan tepat banyak hal</li>
<li>Berulangkali terpaksa menghabiskan waktu karena masalah yang tidak perlu</li>
</ul>
</td>
</tr>
</tbody>
</table>
<hr />
<p>&nbsp;</p>
<hr />
<p><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/diagram.png" alt="" width="100%" /></p>
<p>&nbsp;</p>
<table style="height: 392px; width: 100%;">
<tbody>
<tr>
<td style="width: 15.5194%;">
<p style="text-align: center;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-ALCHEMIST.png" width="100" height="100" /><br />ALCHEMIST</p>
</td>
<td style="width: 81.4806%;">
<p>Penyempurnaan yang baik, penyelesaian proses, menemukan cara yang lebih baik dab cerdas, meningkatkan efesiensi dan efektivitas produk/sitem yang ada.</p>
<p>Seseorang yang memilki kemampuan beradaptasi dengan menempatkan diri sesuai dengạn dimana ia berada. mampu untuk tetap merendah dan secara frontal berusaha untuk mengejar setiap peluang yang ada.</p>
</td>
</tr>
<tr>
<td style="width: 15.5194%; text-align: center;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-WIZARD.png" width="100" height="100" />WIZARD</td>
<td style="width: 81.4806%;">
<p>Konseptor yang &thorn;aik, penggagas hal baru, kreatif, inovatif, pencipta ide, pemikir out-of-the-box, spontan, tidak teratur, impulsif, baik untuk memulai sesuatu yang baru.</p>
<p>Seseorang yang pampu berinovasi secara berkelanjutan, seringkali kurang bisa dipahami oleh banyak orang disekitarnya, namun sanggup berpikir out-of-the-box, dalam mengbasilkan konsep-konsep baru.</p>
</td>
</tr>
<tr>
<td style="width: 15.5194%; text-align: center;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-BARD.png" width="100" height="100" />BARD</td>
<td style="width: 81.4806%;">
<p>Promotor yang baik, pemasar yang berbakat, merek pribadi yang kuat, &thorn;ergerak pat, butyh perhatian, bersinar, mampy menginspiraşi orang lain, individu yang kokoh.</p>
<p>Seseorang yang mampu memberi energi kepada orang-orang di sekelilingnya, mendukung dan menyebarkan semangat secara konsisten, sehingga mampu menyampaikan segala hal dengan penerimaan yang relatif mudah.</p>
</td>
</tr>
<tr>
<td style="width: 15.5194%; text-align: center;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-PRIEST.png" width="100" height="100" />KNIGHT</td>
<td style="width: 81.4806%;">
<p>Influencer yang baik, pemimpip yang karismatik, manajer pelaksana, penyelenggara, administrator, pendukung, baik dalam mencirtakan dan mengelola hubungan, baik dalam manajemen sumber daya manusia, fleksibel.</p>
<p>Seorang yang selalu mampu memberi semangat pada orang-orang disekitarnya, dimana api mampu menerangi kegelapan dan menyambar kemanapun, sebingga ia bisa mempengaruhi lebih banyak orang untuk sejalan seiring berjalannya waktu.</p>
</td>
</tr>
<tr>
<td style="width: 15.5194%; text-align: center;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-ARBITER.png" width="100" height="100" />ARBITER</td>
<td style="width: 81.4806%;">
<p>Penengah yang baik, pembawa damai, representatif bijak, melihat alternatif sebagai manfaat, penghindar konflik, berinyestasi pada orang untuk manfaat jangka panjang.</p>
<p>Seseorang yang tegas şerta mampu menciptakan kemungkinan baru, walaupun sebelumnya dirasakan tidak ada jalan lain, dimana angin biasanya memang tidak terlibat namun bisa dirasakan kehadirannya, mengisi apapun dan dimanapun.</p>
</td>
</tr>
<tr>
<td style="width: 15.5194%; text-align: center;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-MERCHANT.png" width="100" height="100" />MERCHANT</td>
<td style="width: 81.4806%;">
<p>Negosiator ulung, pedagang yang handal, pencari peluang, pengguna waktu yang efektif dan efisien, fleksibel, bekerja dalam pola/parameter tertentų, berdasarkan data.</p>
<p>Seseorang yang mampu memanfaatkan kekuatan dari wawasan yg luas, yang menampung apapun dari segala penjuru, mencapai keberhasilan dengan menerima berbagai hal, dan memanfaatkannva untuk keberhasilan yang lebih besar.&nbsp;</p>
</td>
</tr>
<tr>
<td style="width: 15.5194%; text-align: center;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-KNIGHT.png" width="100" height="100" />PRIEST</td>
<td style="width: 81.4806%;">
<p>Penjaga yang baik, dapat diandalkan, sabar, berhati-hati, berbasis fakta, safety- player, terstruktur, unggul dengan analisis informasi, cerdas dalam mengelola risiko.</p>
<p>Seseorang yang mampu menegakkan aturan secara tegas, disegani oleh orang- orang dibawahnya, dan ditakuti oleh siapapun yang melakukan pelanggaran aturan, dengan keteguhan tinggi sehingga mampu dijadikan sebagai tauladan (role-model).</p>
</td>
</tr>
<tr>
<td style="width: 15.5194%; text-align: center;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-MARSHAL.png" width="100" height="100" />MARSHAL</td>
<td style="width: 81.4806%;">
<p>Penganalisa yang baik, prediktif, lebih efektif di belakang layar, terencana, berkomitmen, pengatur yang handal, berkemauan keras, regulator yang baik.</p>
<p>Seseorang yang kokoh dan bisa diandalkan, terbuka untuk berbagai peluang yang lewat atau dilewatinya, dan mampu memanfaatkan apapun yang saat ini dimilikinya, termasuk memanfaatkan kekuatan waktu dan prioritas-prioritas yang tersusun dengan baik.</p>
</td>
</tr>
</tbody>
</table>




<h2 style="text-align: center;"><span style="color: #ff6600;">BARD</span></h2>
<p>Para pemilik shio ini merupakan tenaga pemasar yang berbakat serta promotor terbaik.</p>
<p>Mereka juga merupakan pribadi yang kuat, bersinar serta mampu menginspirasi orang lain dengan penyampaian yang mudah dimengerti secara konsisten.</p>
<p>Kalangan Bard biasanya sangat butuh perhatian untuk tetap menjaga energi positif dalam dirinya.<br /><br /><strong>KARAKTERISTIK PROFIL BARD</strong><br /><br />&bull; Menghasilkan keuntungan dengan membuat sebuah hal yang sempurna dan kompleks, lalu memperbesar produk yang ada, sehingga menghasilkan income yang juga besar.</p>
<p>&bull; Memperoleh energi dari bertemu orang baru untuk mengembangkan berbagai hal dan ide baru, antusias dan membutuhkan banyak interaksi, bisa merasa kesepian saat sendirian.</p>
<p>&bull; Masuk dengan gambaran umum dahulu, sebelum tertarik pada detail, berpikir pada tingkat abstraksi yang tinggi dan umum, tidak suka pekerjaan yang bersifat rutin dan detail.</p>
<p>&bull; Terkait perasaan dan pikiran orang-orang dalam lingkungan profesional, emosi menjadi penting sehingga fokus yang diberikan adalah cenderung pada orang-orang dan perasaan, mudah membangun hubungan.</p>
<p>&bull; Memilih untuk melihat kesempatan dan kemungkinan yang ada, daripada aturan yang harus diikuti, dengan fokus pada hasil, bukan pada caranya (rules is made to be broken).</p>
<p>&bull; Mengedepankan peluang yang bisa dicapai di masa depan dengan mempersiapkan berbagai kemungkinan, mementingkan pola dan gagasan yang tersirat, serta menghargai inspirasi dan imajinasi.</p>
<p>&bull; Pengambil resiko yang berani, mengedepankan reward yang mungkin didapat ketika maju dan mengambil tanggung jawab, menghadapi segala cercaan atau kritik yang dilontarkan.</p>
<p>&bull; Mengutamakan inovasi dari tiap proses yang dilakukan, dengan cara yang selalu di perbaiki, untuk menghasilkan kemajuan dan kesempurnaan, serta perubahan yang berkelanjutan.</p>
<p>&bull; Mengutamakan apa yang bisa dibayangkan dan/atau dilihat, yang disusun dari kata-kata orang yang memberi masukan, atau dari kata-kata di pikiran sendiri.</p>
<p>&bull; Mengutamakan imajinasi untuk menghasilkan sesuatu yang baru, yang menghasilkan dampak yang lebih luas, evaluatif dalam persiapan untuk mengutarakan suatu hal secara lugas.</p>
<p>&bull; Fokus pada produk akhir, dengan kesadaran bahwa kualitas yang dihasilkan akan sangat bergantung pada orang-orang yang terlibat dalam proses pencapaian hasil tersebut.</p>
<p>&bull; Berpikir cepat dengan tetap menggunakan dasar kestabilan dan kedamaian, seringkali perlu melakukan revisi terhadap keputusan yang sudah diambil, demi untuk mendukung keseimbangan yang dibutuhkan.<br /><br /></p>
<table style="width: 100%; height: 71px; background-color: #f5f5f5;">
<tbody>
<tr>
<td style="width: 100px;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-BARD.png" width="100" height="100" /></td>
<td style="width: 497px;">Elemen Matahari melambangkan sumber kehidupan bagi seluruh kehidupan di bumi. Seorang yang mampu memberikan inspirasi serta semangat kepada sekitarnya untuk menyelesaikan persoalan yang dihadapi.</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<p>Bard memiliki karakteristik warna orange yang kuat dan hangat, yang memberikan rasa nyaman, dan kerap digunakan untuk menciptakan hawa produktif dan menghasilkan repetisi di lingkungan kerja yang membutuhkan produktivitas. &nbsp;</p>
<h2 style="text-align: center;"><br /><span style="color: #ff6600;">KEKUATAN BARD : PENGARUH</span><br /><br /></h2>
<table style="height: 78px; width: 100%; background: #f5f5f5; padding: 20; background-color: #ff6600;">
<tbody>
<tr>
<td style="width: 30%; padding: 20;">
<p><span style="color: #ffffff;"><strong>Sisi Produktif</strong></span></p>
</td>
<td style="width: 70%;">
<p><span style="color: #ffffff;">Ceria, ambisi, energik, sensualitas, menyenangkan, kenyamanan, bersahabat, percaya diri, harapan, kreativitas, merangsang emosi.</span></p>
</td>
</tr>
<tr>
<td style="width: 174px;">
<p><span style="color: #ffffff;"><strong>Sisi Tantangan</strong></span></p>
</td>
<td style="width: 416px;"><span style="color: #ffffff;">&nbsp;Mudah dijangkau, gaduh, merangsang, hiperaktif, perampasan, frustrasi, kesembronoan, kurangnya intelektualisme, ketidak dewasaan.</span></td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<h2 class="p1" style="text-align: center;"><span style="color: #ff6600;">SELF-LEADERSHIP</span></h2>
<p class="p1"><span class="s1">Pemimpin yang memiliki kemampuan untuk menularkan semangat dan kekuatan yang penuh dinamika, serta menjadi sumber energi bagi dirinya dan orang lain. Mampu untuk sabar dalam menjalankan suatu hal. Tajam, terarah dan mampu melihat segala hal secara menyeluruh (big picture).</span></p>
<p class="p1"><span class="s1">Pemimpin yang berani mengambil peran untuk menjadi inspirasi bagi orang di sekelilingnya, dan mengambil resiko untuk kesalahan yang mungkin dilakukan atas keputusan yang dibuatnya.</span></p>
<p class="p1">&nbsp;</p>
<p>&nbsp;</p>
<!--ADDITIONAL-->
<h2 class="p1" style="text-align: center; color: #ffcc00;"><br /><strong>PROFIL: UTAMA &amp; BAYANGAN</strong>&nbsp;</h2>
<p>Profil Utama (main profile) Shio Kaya adalah bagian dominan dari diri Anda, yang sebenarnya mendorong dan menginspirasi diri Anda.</p>
<p>Profil utama (main profile) mencakup karakteristik alamiah Anda, yang dapat membantu Anda dalam membangun keberhasilan dengan lebih efektif dan efisien, menghemat waktu dan tenaga Anda.</p>
<p>Profil Bayangan (shadow profile) Shio Kaya merupakan bagian resesif (non-dominan) dari diri Anda, dengan hanya beberapa karakteristik yang muncul.</p>
<p>Profil bayangan (shadow profile) merupakan karakteristik yang bersifat seperti bayangan, bagian dari diri Anda, tak terpisahkan, tapi bukan mencerminkan diri Anda sepenuhnya.<br /><br />Profil Bayangan dari <strong>BARD</strong>&nbsp;adalah <strong>WIZARD</strong>&nbsp;dan <strong>KNIGHT</strong></p>
<p>&nbsp;</p>
<h2 style="text-align: center;"><strong><br />BARD</strong>&nbsp;</h2>
<table style="height: 48px; background-color: #ff9f43;" width="100%">
<tbody>
<tr style="height: 36.5312px;">
<td style="width: 288px; height: 36.5312px; text-align: center;"><span style="color: #000000;"><strong>KARAKTER ALAMI</strong></span></td>
</tr>
<tr style="height: 222px;">
<td style="width: 288px; height: 222px;"><span style="color: #ffffff;">&nbsp;</span>
<ul>
<li>Mengenal potensi diri mereka dengan baik</li>
<li>Percaya diri, sanggup mempertahankan pendapat dan mampu berkomunikasi dengan meyakinkan</li>
<li>Menjadi perhatian utama dalam setiap pertunjukan</li>
<li>Berani cepat dalam mengambil keputusan dan menciptakan sesuatu</li>
<li>Seringkali mengerjakan semua sendiri secara one-manshow</li>
<li>Kualitas diri merupakan hal yang sangat penting</li>
<li>Mampu impulsif, spontan, dan cepat</li>
</ul>
</td>
</tr>
</tbody>
</table>
<table style="height: 48px; background-color: #ff9f43;" width="100%">
<tbody>
<tr style="height: 36.5312px;">
<td style="width: 288px; height: 36.5312px; text-align: center;"><span style="color: #000000;"><strong>FAKTOR SUKSES</strong></span></td>
</tr>
<tr style="height: 204px;">
<td style="width: 288px; height: 204px;">
<ul>
<li>Mampu menjual potensi dan image diri dengan efektif</li>
<li>Sanggup untuk mempengaruhi banyak orang</li>
<li>Mampu menggunakan karisma pribadi untuk menjual apapun</li>
<li>Menjadi sosok yang disegani karena seringkali dianggap kompeten</li>
<li>Cepat dalam berpendapat dan memutuskan hal-hal yang bersifat urgent</li>
<li>Mencapai keberhasilan yang luar biasa ketika bermitra</li>
<li>Berpikiran terbuka, berani berinvestasi pada banyak hal</li>
<li>Cepat dalam mengambil risiko dan peluang</li>
</ul>
</td>
</tr>
</tbody>
</table>
<table style="height: 48px; background-color: #ff9f43;" width="100%">
<tbody>
<tr style="height: 10.5312px;">
<td style="width: 288px; height: 10.5312px;">
<p style="text-align: center;"><span style="color: #000000;"><strong>FAKTOR KEGAGALAN</strong></span></p>
</td>
</tr>
<tr style="height: 258px;">
<td style="width: 288px; height: 258px;"><span style="color: #ffffff;">&nbsp;</span>
<ul>
<li>Seringkali terlalu percaya diri dan terkesan arogan</li>
<li>Pendapat bisa tidak didukung fakta dan kompetensi yang tepat</li>
<li>Sanggup melakukan hal-hal yang akhirnya kontraproduktif untuk eksis</li>
<li>Sering menciptakan alasan-alasan atas kegagalan diri</li>
<li>Pendapat dan keputusan yang diambil seringkali tanpa pemikiran yang matang</li>
<li>Termasuk sulit untuk bisa mempercayai kualitas kerja orang lain</li>
<li>Seringkali boros dan tidak menghitung dengan tepat banyak hal</li>
<li>Berulangkali terpaksa menghabiskan waktu karena masalah yang tidak perlu</li>
</ul>
</td>
</tr>
</tbody>
</table>
<hr />
<h2 style="text-align: center;"><strong><br />WIZARD</strong>&nbsp;</h2>
<table style="height: 48px; background-color: #cc0000;" width="100%">
<tbody>
<tr style="height: 36.5312px;">
<td style="width: 288px; height: 36.5312px; text-align: center;"><span style="color: #ffffff;"><strong>KARAKTER ALAMI</strong></span></td>
</tr>
<tr style="height: 222px;">
<td style="width: 288px; height: 222px;"><span style="color: #ffffff;">&nbsp;</span>
<ul>
<li><span style="color: #ffffff;">Pemikir dengan visi yang besar, Semua dipikirkan sendiri</span></li>
<li><span style="color: #ffffff;">Pemikiran yang sering melompat-lompat</span></li>
<li><span style="color: #ffffff;">Sulit menahan diri untuk tidak mencipta dan berinovasi, kreativitas diatas segalanya.</span></li>
<li><span style="color: #ffffff;">Tidak suka tertahan pada proses dan hal-hal yang sifatnya detail</span></li>
<li><span style="color: #ffffff;">Sulit dimengerti pikirannya karena terlalu cepat dan tidak terstruktur</span></li>
<li><span style="color: #ffffff;">Seringkali terlalu optimis dalam menghadapi sesuatu yang baru</span></li>
</ul>
<p><span style="color: #ffffff;">&nbsp;</span></p>
</td>
</tr>
</tbody>
</table>
<table style="height: 48px; background-color: #cc0000;" width="100%">
<tbody>
<tr style="height: 36.5312px;">
<td style="width: 288px; height: 36.5312px; text-align: center;"><span style="color: #ffffff;"><strong>FAKTOR SUKSES</strong></span></td>
</tr>
<tr style="height: 204px;">
<td style="width: 288px; height: 204px;">
<ul>
<li><span style="color: #ffffff;">Sanggup menciptakan produk dan perubahan yang otentik</span></li>
<li><span style="color: #ffffff;">Kesuksesan diperoleh dari mendelegasikan banyak hal</span></li>
<li><span style="color: #ffffff;">Selalu menghasilkan perubahan yang berkelanjutan</span></li>
<li><span style="color: #ffffff;">Menghasilkan pemikiran yang spontan dan out-of-the box</span></li>
<li><span style="color: #ffffff;">Produktif dan selalu menghasilkan inovasi yang baru</span></li>
<li><span style="color: #ffffff;">Mampu memukau sekitarnya dengan konsep baru dan pemikiran yang besar</span></li>
<li><span style="color: #ffffff;">Menghasilkan ritme yang cepat dan sulit untuk ditebak bagi pesaing</span></li>
<li><span style="color: #ffffff;">Mau mencoba hal-hal baru dan percaya diri bahwa bisa berhasil</span></li>
</ul>
<p><span style="color: #ffffff;">&nbsp;</span></p>
</td>
</tr>
</tbody>
</table>
<table style="height: 48px; background-color: #cc0000;" width="100%">
<tbody>
<tr style="height: 10.5312px;">
<td style="width: 288px; height: 10.5312px;">
<p style="text-align: center;"><span style="color: #ffffff;"><strong>FAKTOR KEGAGALAN</strong></span></p>
</td>
</tr>
<tr style="height: 258px;">
<td style="width: 288px; height: 258px;">
<ul>
<li><span style="color: #ffffff;">Terkadang melupakan atau tidak mau tahu tentang proses yang harus dilalui</span></li>
<li><span style="color: #ffffff;">Tertekan karena ingin mencapai semuanya sendiri</span></li>
<li><span style="color: #ffffff;">Seringkali berubah-ubah dan mudah bosan</span></li>
<li><span style="color: #ffffff;">Pemikiran yang tidak terstruktur dan seringkali membingungkan</span></li>
<li><span style="color: #ffffff;">Terlampau sering tidak menyelesaikan apa yang sudah dimulai</span></li>
<li><span style="color: #ffffff;">Seringkali bukan merupakan manajer yang handal bagi eksekusi ide dan konsepnya</span></li>
<li><span style="color: #ffffff;">Membuat tim menjadi tergopoh-gopoh dan sulit untuk dipahami</span></li>
<li><span style="color: #ffffff;">Kegagalan sering terjadi karena terlalu optimis tanpa perhitungan matang</span></li>
</ul>
</td>
</tr>
</tbody>
</table>
<hr />
<h2 style="text-align: center;"><strong><br />KNIGHT</strong>&nbsp;</h2>
<table style="height: 48px; background-color: #feca57;" width="100%">
<tbody>
<tr style="height: 36.5312px;">
<td style="width: 288px; height: 36.5312px; text-align: center;"><span style="color: #000000;"><strong>KARAKTER ALAMI</strong></span></td>
</tr>
<tr style="height: 222px;">
<td style="width: 288px; height: 222px;"><span style="color: #ffffff;">&nbsp;</span>
<ul>
<li>Punya keterampilan pengelolaan emosi yang baik</li>
<li>Cenderung punya sifat kepemimpinan yang baik secara alami</li>
<li>Kemampuan yang baik dalam memotivasi tim</li>
<li>Memiliki kemampuan komunikasi yang baik dengan orang lain</li>
<li>Punya kemampuan untuk menempatkan diri dalam berbagai situasi</li>
<li>Mudah berkembang dengan kepercayaan penuh untuk memimpin</li>
<li>Nyaman dalam membangun interaksi dengan banyak orang baru Hubungan termasuk merupakan aset yang penting</li>
</ul>
<p><span style="color: #ffffff;">&nbsp;</span></p>
</td>
</tr>
</tbody>
</table>
<table style="height: 48px; background-color: #feca57;" width="100%">
<tbody>
<tr style="height: 36.5312px;">
<td style="width: 288px; height: 36.5312px; text-align: center;"><span style="color: #000000;"><strong>FAKTOR SUKSES</strong></span></td>
</tr>
<tr style="height: 204px;">
<td style="width: 288px; height: 204px;">
<ul>
<li>Dapat diandalkan dan menjadi favorit banyak orang</li>
<li>Mampu memimpin dengan efektif, terhadap tim, mitra, dan customer</li>
<li>Team-player yang baik dan mendorong tim untuk mencapai target</li>
<li>Mampu untuk mengerti orang lain dan membangun hubungan yang kondusif</li>
<li>Bisa bermain peran sesuai dengan kebutuhan saat itu</li>
<li>Cenderung menjadi pemimpin yang kuat dan kompeten</li>
<li>Menjadi pembangun jaringan yang baik, dengan lobby-lobby yang efektfi</li>
<li>Bagus dalam membangun hubungan baru dan mempertahankan hubungan lama</li>
</ul>
</td>
</tr>
</tbody>
</table>
<table style="height: 48px; background-color: #feca57;" width="100%">
<tbody>
<tr style="height: 10.5312px;">
<td style="width: 288px; height: 10.5312px;">
<p style="text-align: center;"><span style="color: #000000;"><strong>FAKTOR KEGAGALAN</strong></span></p>
</td>
</tr>
<tr style="height: 258px;">
<td style="width: 288px; height: 258px;">
<ul>
<li>Mungkin terjebak dengan merasa lebih daripada orang lain</li>
<li>Cenderung menjadi orang yang terlalu menghakimi atau menilai orang lain Kehilangan kepercayaan karena kata-kata yang tidak didukung dengan tindakan nyata</li>
<li>Sering menggampangkan sesuatu karena merasa bisa diselesaikan dengan komunikasi</li>
<li>Bisa menjadi manipulatif dalam menjalani sebuah hubungan</li>
<li>Bisa terjebak menjadi over-confident dalam membuat berbagai keputusan Seringkali menjadi kurang asertif terhadap orang lain</li>
<li>Bermasalah dengan skala prioritas dan ketepatan waktu</li>
</ul>
</td>
</tr>
</tbody>
</table>
<hr />
<p>&nbsp;</p>
<hr />
<p><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/diagram.png" alt="" width="100%" /></p>
<p>&nbsp;</p>
<table style="height: 392px; width: 100%;">
<tbody>
<tr>
<td style="width: 15.5194%;">
<p style="text-align: center;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-ALCHEMIST.png" width="100" height="100" /><br />ALCHEMIST</p>
</td>
<td style="width: 81.4806%;">
<p>Penyempurnaan yang baik, penyelesaian proses, menemukan cara yang lebih baik dab cerdas, meningkatkan efesiensi dan efektivitas produk/sitem yang ada.</p>
<p>Seseorang yang memilki kemampuan beradaptasi dengan menempatkan diri sesuai dengạn dimana ia berada. mampu untuk tetap merendah dan secara frontal berusaha untuk mengejar setiap peluang yang ada.</p>
</td>
</tr>
<tr>
<td style="width: 15.5194%; text-align: center;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-WIZARD.png" width="100" height="100" />WIZARD</td>
<td style="width: 81.4806%;">
<p>Konseptor yang &thorn;aik, penggagas hal baru, kreatif, inovatif, pencipta ide, pemikir out-of-the-box, spontan, tidak teratur, impulsif, baik untuk memulai sesuatu yang baru.</p>
<p>Seseorang yang pampu berinovasi secara berkelanjutan, seringkali kurang bisa dipahami oleh banyak orang disekitarnya, namun sanggup berpikir out-of-the-box, dalam mengbasilkan konsep-konsep baru.</p>
</td>
</tr>
<tr>
<td style="width: 15.5194%; text-align: center;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-BARD.png" width="100" height="100" />BARD</td>
<td style="width: 81.4806%;">
<p>Promotor yang baik, pemasar yang berbakat, merek pribadi yang kuat, &thorn;ergerak pat, butyh perhatian, bersinar, mampy menginspiraşi orang lain, individu yang kokoh.</p>
<p>Seseorang yang mampu memberi energi kepada orang-orang di sekelilingnya, mendukung dan menyebarkan semangat secara konsisten, sehingga mampu menyampaikan segala hal dengan penerimaan yang relatif mudah.</p>
</td>
</tr>
<tr>
<td style="width: 15.5194%; text-align: center;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-PRIEST.png" width="100" height="100" />KNIGHT</td>
<td style="width: 81.4806%;">
<p>Influencer yang baik, pemimpip yang karismatik, manajer pelaksana, penyelenggara, administrator, pendukung, baik dalam mencirtakan dan mengelola hubungan, baik dalam manajemen sumber daya manusia, fleksibel.</p>
<p>Seorang yang selalu mampu memberi semangat pada orang-orang disekitarnya, dimana api mampu menerangi kegelapan dan menyambar kemanapun, sebingga ia bisa mempengaruhi lebih banyak orang untuk sejalan seiring berjalannya waktu.</p>
</td>
</tr>
<tr>
<td style="width: 15.5194%; text-align: center;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-ARBITER.png" width="100" height="100" />ARBITER</td>
<td style="width: 81.4806%;">
<p>Penengah yang baik, pembawa damai, representatif bijak, melihat alternatif sebagai manfaat, penghindar konflik, berinyestasi pada orang untuk manfaat jangka panjang.</p>
<p>Seseorang yang tegas şerta mampu menciptakan kemungkinan baru, walaupun sebelumnya dirasakan tidak ada jalan lain, dimana angin biasanya memang tidak terlibat namun bisa dirasakan kehadirannya, mengisi apapun dan dimanapun.</p>
</td>
</tr>
<tr>
<td style="width: 15.5194%; text-align: center;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-MERCHANT.png" width="100" height="100" />MERCHANT</td>
<td style="width: 81.4806%;">
<p>Negosiator ulung, pedagang yang handal, pencari peluang, pengguna waktu yang efektif dan efisien, fleksibel, bekerja dalam pola/parameter tertentų, berdasarkan data.</p>
<p>Seseorang yang mampu memanfaatkan kekuatan dari wawasan yg luas, yang menampung apapun dari segala penjuru, mencapai keberhasilan dengan menerima berbagai hal, dan memanfaatkannva untuk keberhasilan yang lebih besar.&nbsp;</p>
</td>
</tr>
<tr>
<td style="width: 15.5194%; text-align: center;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-KNIGHT.png" width="100" height="100" />PRIEST</td>
<td style="width: 81.4806%;">
<p>Penjaga yang baik, dapat diandalkan, sabar, berhati-hati, berbasis fakta, safety- player, terstruktur, unggul dengan analisis informasi, cerdas dalam mengelola risiko.</p>
<p>Seseorang yang mampu menegakkan aturan secara tegas, disegani oleh orang- orang dibawahnya, dan ditakuti oleh siapapun yang melakukan pelanggaran aturan, dengan keteguhan tinggi sehingga mampu dijadikan sebagai tauladan (role-model).</p>
</td>
</tr>
<tr>
<td style="width: 15.5194%; text-align: center;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-MARSHAL.png" width="100" height="100" />MARSHAL</td>
<td style="width: 81.4806%;">
<p>Penganalisa yang baik, prediktif, lebih efektif di belakang layar, terencana, berkomitmen, pengatur yang handal, berkemauan keras, regulator yang baik.</p>
<p>Seseorang yang kokoh dan bisa diandalkan, terbuka untuk berbagai peluang yang lewat atau dilewatinya, dan mampu memanfaatkan apapun yang saat ini dimilikinya, termasuk memanfaatkan kekuatan waktu dan prioritas-prioritas yang tersusun dengan baik.</p>
</td>
</tr>
</tbody>
</table>





###WIZARD##

<h2 style="text-align: center;"><span style="color: #ff0000;">WIZARD</span></h2>
<p>Para pemilik shio ini merupakan konseptor dan creator yang baik.</p>
<p>Pemikirannya terhadap ide baru yang out-of-the-box dapat tercipta dengan spontan. Sayangnya hal tersebut membuat mereka sangat impulsif dan tidak teratur.</p>
<p>Seringkali orang sulit memahami apa yang ada dalam pikiran para Wizard.<br /><br /><strong>KARAKTERISTIK PROFIL WIZARD</strong><br /><br />&bull; Menciptakan daya saing dengan berkolaborasi, fokus terhadap sebuah produk/proyek, namun tetap mendapat hasil multiplikasi dari produk/proyek tersebut dari kolaborasi tersebut</p>
<p>&bull; Kekuatan fleksibilitas yang mampu untuk merasa nyaman dengan kondisi yang penuh dengan interaksi sosial, juga bisa menikmati kondisi sendirian atau yang jauh dari keramaian, untuk memperoleh semangat kembali.</p>
<p>&bull; Mampu untuk secara cepat beralih dari berpikir secara umum ke detail atau sebaliknya, tergantung pada kebutuhan yang sedang dihadapi, dan bisa sama efektifnya di kedua skala berpikir tersebut.</p>
<p>&bull; Terkait dengan kemampuan untuk sama efektifnya dalam memfokuskan diri pada produk, tugas, sistem, maupun perasaan dan pikiran orang-orang di lingkungan profesional, tergantung pada situasi yang terjadi.</p>
<p>&bull; Memilih untuk melihat kesempatan dan kemungkinan yang ada, daripada sebuah aturan yang harus diikuti, dengan fokus pada hasil, bukan pada caranya (rules is made to be broken).</p>
<p>&bull; mengedepankan peluang yang bisa dicapai di masa depan dengan mempersiapkan berbagai kemungkinan, mementingkan pola dan gagasan tersirat, serta menghargai inspirasi dan imajinasi.</p>
<p>&bull; Pengambil resiko yang berani, mengedepankan reward yang mungkin didapat ketika maju dan mengambil tanggung jawab, menghadapi segala cercaan atau kritik yang dilontarkan.</p>
<p>&bull; Mengutamakan inovasi dari tiap proses yang dilakukan, dengan cara yang selalu di perbaiki, untuk menghasilkan kemajuan dan kesempurnaan, serta perubahan yang berkelanjutan.</p>
<p>&bull; Mengutamakan apa yang terlihat, memprioritaskan visualisasi untuk pengambilan keputusan, seringkali berbicara cepat dan lompat-lompat ketika menyampaikan suatu hal karena banyaknya ide di pikiran.</p>
<p>&bull; Mengutamakan kemampuan imajinasi untuk menghasilkan sesuatu, keterampilan yang sifatnya investigatif dalam menggali, dan evaluatif dalam usaha mencapai sebuah keputusan.</p>
<p>&bull; Fokus pada produk akhir yang dihasilkan, berorientasi pada tujuan, seringkali mampu mengambil jalan pintas, kurang peka terhadap faktor orang-orang dan cara yang digunakan.</p>
<p>&bull; Berpikir dan bertindak cepat didasari insting, dengan pertimbangan yang cenderung cepat, sehingga seringkali menghasilkan konsekuensi yang tidak sesuai keinginan dan mengganggu tujuan jangka panjang.</p>
<p>&nbsp;</p>
<table style="width: 100%; height: 71px; background-color: #f5f5f5;">
<tbody>
<tr>
<td style="width: 100px;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-WIZARD.png" width="100" height="100" /></td>
<td style="width: 497px;">Elemen Bintang melambangkan posisi yang tinggi di langit, penunjuk arah bagi kehidupan. Seorang yang selalu bisa diandalkan dan bisa menjadi contoh, serta menempati posisi yang terpandang.</td>
</tr>
</tbody>
</table>
<p>Wizard memiliki karakteristik warna merah yang inspiratif, menunjukkan kekuatan, energi, kehangatan, cinta, nafsu, dan agresi. Dapat memicu tingkat emosional, sekaligus warna yang mampu menarik perhatian dan memicu ketercapaian tujuan.</p>
<h2 style="text-align: center;"><br /><span style="color: #ff0000;">KEKUATAN WIZARD : KREATIFITAS</span><br /><br /></h2>
<table style="height: 78px; width: 100%; background: #f5f5f5; padding: 20; background-color: #ff0000;">
<tbody>
<tr>
<td style="width: 30%; padding: 20;">
<p><span style="color: #ffffff;"><strong>Sisi Produktif</strong></span></p>
</td>
<td style="width: 70%;">
<p><span style="color: #ffffff;">Bersemangat, enerjik, dinamis, komunikatif, mewah, cinta, kekuatan, percaya diri, dramatis, perjuangan, persisten, kreativitas, imajinatif</span></p>
</td>
</tr>
<tr>
<td style="width: 174px;">
<p><span style="color: #ffffff;"><strong>Sisi Tantangan</strong></span></p>
</td>
<td style="width: 416px;">
<p class="p1"><span class="s1" style="color: #ffffff;">Agresif, penuntut, kemarahan, nafsu, dominasi, teriakan, persaingan, kekerasan, penolakan, pertentangan.</span></p>
</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<h2 class="p1" style="text-align: center;"><span style="color: #ff0000;">SELF-LEADERSHIP</span></h2>
<p>Pemimpin yang mampu menciptakan banyak konsep baru dan orisinil, untuk bisa menghasilkan perubahan bersama dengan orang sekitarnya.</p>
<p>Pemimpin tipe ini perlu belajar bertindak tegas terhadap siapapun, apalagi jika terjadi penyimpangan terhadap kesepakatan dan bisa membawa kerugian.</p>
<p>Pemimpin yang mampu melihat banyak hal yang sudah ada, dengan cara yang baru, sehingga bisa menjadi inisiator perubahan dalam skala besar.</p>
<!--ADDITIONAL-->
<h2 class="p1" style="text-align: center; color: #ff0000;"><br /><strong>PROFIL: UTAMA &amp; BAYANGAN</strong>&nbsp;</h2>
<p>Profil Utama (main profile) Shio Kaya adalah bagian dominan dari diri Anda, yang sebenarnya mendorong dan menginspirasi diri Anda.</p>
<p>Profil utama (main profile) mencakup karakteristik alamiah Anda, yang dapat membantu Anda dalam membangun keberhasilan dengan lebih efektif dan efisien, menghemat waktu dan tenaga Anda.</p>
<p>Profil Bayangan (shadow profile) Shio Kaya merupakan bagian resesif (non-dominan) dari diri Anda, dengan hanya beberapa karakteristik yang muncul.</p>
<p>Profil bayangan (shadow profile) merupakan karakteristik yang bersifat seperti bayangan, bagian dari diri Anda, tak terpisahkan, tapi bukan mencerminkan diri Anda sepenuhnya.<br /><br />Profil Bayangan dari <strong>WIZARD</strong>&nbsp;adalah <strong>ALCHEMIST</strong>&nbsp;dan <strong>BARD</strong></p>
<p>&nbsp;</p>
<h2 style="text-align: center;"><strong><br />WIZARD</strong>&nbsp;</h2>
<table style="height: 48px; background-color: #cc0000;" width="100%">
<tbody>
<tr style="height: 36.5312px;">
<td style="width: 288px; height: 36.5312px; text-align: center;"><span style="color: #ffffff;"><strong>KARAKTER ALAMI</strong></span></td>
</tr>
<tr style="height: 222px;">
<td style="width: 288px; height: 222px;"><span style="color: #ffffff;">&nbsp;</span>
<ul>
<li><span style="color: #ffffff;">Pemikir dengan visi yang besar, Semua dipikirkan sendiri</span></li>
<li><span style="color: #ffffff;">Pemikiran yang sering melompat-lompat</span></li>
<li><span style="color: #ffffff;">Sulit menahan diri untuk tidak mencipta dan berinovasi, kreativitas diatas segalanya.</span></li>
<li><span style="color: #ffffff;">Tidak suka tertahan pada proses dan hal-hal yang sifatnya detail</span></li>
<li><span style="color: #ffffff;">Sulit dimengerti pikirannya karena terlalu cepat dan tidak terstruktur</span></li>
<li><span style="color: #ffffff;">Seringkali terlalu optimis dalam menghadapi sesuatu yang baru</span></li>
</ul>
<p><span style="color: #ffffff;">&nbsp;</span></p>
</td>
</tr>
</tbody>
</table>
<table style="height: 48px; background-color: #cc0000;" width="100%">
<tbody>
<tr style="height: 36.5312px;">
<td style="width: 288px; height: 36.5312px; text-align: center;"><span style="color: #ffffff;"><strong>FAKTOR SUKSES</strong></span></td>
</tr>
<tr style="height: 204px;">
<td style="width: 288px; height: 204px;">
<ul>
<li><span style="color: #ffffff;">Sanggup menciptakan produk dan perubahan yang otentik</span></li>
<li><span style="color: #ffffff;">Kesuksesan diperoleh dari mendelegasikan banyak hal</span></li>
<li><span style="color: #ffffff;">Selalu menghasilkan perubahan yang berkelanjutan</span></li>
<li><span style="color: #ffffff;">Menghasilkan pemikiran yang spontan dan out-of-the box</span></li>
<li><span style="color: #ffffff;">Produktif dan selalu menghasilkan inovasi yang baru</span></li>
<li><span style="color: #ffffff;">Mampu memukau sekitarnya dengan konsep baru dan pemikiran yang besar</span></li>
<li><span style="color: #ffffff;">Menghasilkan ritme yang cepat dan sulit untuk ditebak bagi pesaing</span></li>
<li><span style="color: #ffffff;">Mau mencoba hal-hal baru dan percaya diri bahwa bisa berhasil</span></li>
</ul>
<p><span style="color: #ffffff;">&nbsp;</span></p>
</td>
</tr>
</tbody>
</table>
<table style="height: 48px; background-color: #cc0000;" width="100%">
<tbody>
<tr style="height: 10.5312px;">
<td style="width: 288px; height: 10.5312px;">
<p style="text-align: center;"><span style="color: #ffffff;"><strong>FAKTOR KEGAGALAN</strong></span></p>
</td>
</tr>
<tr style="height: 258px;">
<td style="width: 288px; height: 258px;">
<ul>
<li><span style="color: #ffffff;">Terkadang melupakan atau tidak mau tahu tentang proses yang harus dilalui</span></li>
<li><span style="color: #ffffff;">Tertekan karena ingin mencapai semuanya sendiri</span></li>
<li><span style="color: #ffffff;">Seringkali berubah-ubah dan mudah bosan</span></li>
<li><span style="color: #ffffff;">Pemikiran yang tidak terstruktur dan seringkali membingungkan</span></li>
<li><span style="color: #ffffff;">Terlampau sering tidak menyelesaikan apa yang sudah dimulai</span></li>
<li><span style="color: #ffffff;">Seringkali bukan merupakan manajer yang handal bagi eksekusi ide dan konsepnya</span></li>
<li><span style="color: #ffffff;">Membuat tim menjadi tergopoh-gopoh dan sulit untuk dipahami</span></li>
<li><span style="color: #ffffff;">Kegagalan sering terjadi karena terlalu optimis tanpa perhitungan matang</span></li>
</ul>
</td>
</tr>
</tbody>
</table>
<hr />
<h2 style="text-align: center;"><strong><br />ALCHEMIST</strong>&nbsp;</h2>
<table style="height: 48px; background-color: #993366;" width="100%">
<tbody>
<tr style="height: 36.5312px;">
<td style="width: 288px; height: 36.5312px; text-align: center;"><span style="color: #ffffff;"><strong>KARAKTER ALAMI</strong></span></td>
</tr>
<tr style="height: 222px;">
<td style="width: 288px; height: 222px;"><span style="color: #ffffff;">&nbsp;</span>
<ul>
<li><span style="color: #ffffff;">Mampu mengendalikan dan mengatur orang lain secara tidak langsung </span></li>
<li><span style="color: #ffffff;">Sangat memperhatikan detail yang rinci </span></li>
<li><span style="color: #ffffff;">Sangat ahli dalam menyelesaikan dan menyempurnakan sesuatu </span></li>
<li><span style="color: #ffffff;">Cenderung perfeksionis, bahkan ketika akan memulai sesuatu </span></li>
<li><span style="color: #ffffff;">Mengelola organisasi dengan sistematis dan teratur </span></li>
<li><span style="color: #ffffff;">Cenderung untuk senang memegang kendali </span></li>
<li><span style="color: #ffffff;">Terus menerus belajar dan melakukan perubahan </span></li>
<li><span style="color: #ffffff;">Tidak terlalu memperhatikan penyampaian dan tampilan</span></li>
</ul>
</td>
</tr>
</tbody>
</table>
<table style="height: 48px; background-color: #993366;" width="100%">
<tbody>
<tr style="height: 36.5312px;">
<td style="width: 288px; height: 36.5312px; text-align: center;"><span style="color: #ffffff;"><strong>FAKTOR SUKSES</strong></span></td>
</tr>
<tr style="height: 204px;">
<td style="width: 288px; height: 204px;">
<ul>
<li><span style="color: #ffffff;">Handal dalam membuat sistem dan prosedur </span></li>
<li><span style="color: #ffffff;">Hal yang dikerjakannya terjamin aman dan terorganisir dengan baik </span></li>
<li><span style="color: #ffffff;">Selalu menghasilkan perbaikan yang berkelanjutan </span></li>
<li><span style="color: #ffffff;">Perencanaan sangat matang dan sudah mempertimbangkan banyak aspek</span></li>
<li><span style="color: #ffffff;">Organisasi terbangun secara berkelanjutan dan autopilot </span></li>
<li><span style="color: #ffffff;">Semua terkontrol dengan baik dan tanggapan cepat apabila ada kesalahan </span></li>
<li><span style="color: #ffffff;">Mampu memprediksi tren ke depan dan beradaptasi dengan cepat </span></li>
<li><span style="color: #ffffff;">Fokus pada menghasilkan fungsi yang efektif dan efisien </span></li>
</ul>
</td>
</tr>
</tbody>
</table>
<table style="height: 48px; background-color: #993366;" width="100%">
<tbody>
<tr style="height: 10.5312px;">
<td style="width: 288px; height: 10.5312px;">
<p style="text-align: center;"><span style="color: #ffffff;"><strong>FAKTOR KEGAGALAN</strong></span></p>
</td>
</tr>
<tr style="height: 258px;">
<td style="width: 288px; height: 258px;"><span style="color: #ffffff;">&nbsp;</span>
<ul>
<li><span style="color: #ffffff;">Cenderung tidak memperhatikan faktor manusia dalam membuat sistem/prosedur</span></li>
<li><span style="color: #ffffff;">Terjebak pada mengerjakan hal-hal yang terlalu detail, melupakan gambaran besarnya </span></li>
<li><span style="color: #ffffff;">Tidak memperhatikan bahwa tim yang mengerjakan perlu untuk berhenti sesekali</span></li>
<li><span style="color: #ffffff;">Seringkali lama dalam memulai sesuatu karena terlalu banyak pertimbangan </span></li>
<li><span style="color: #ffffff;">Bisa melupakan faktor perasaan manusia dalam menjalankan keteraturan organisasi </span></li>
<li><span style="color: #ffffff;">Bisa terjebak mengendalikan sampai detail, menciptakan rasa frustasi bagi tim</span></li>
<li><span style="color: #ffffff;">Seringkali sudah melakukan tindakan sia-sia, antisipasi sebelum waktunya Bisa menimbulkan masalah ketika customer butuh tampilan yang baik</span></li>
</ul>
</td>
</tr>
</tbody>
</table>
<hr />
<h2 style="text-align: center;"><strong><br />BARD</strong>&nbsp;</h2>
<table style="height: 48px; background-color: #ff9f43;" width="100%">
<tbody>
<tr style="height: 36.5312px;">
<td style="width: 288px; height: 36.5312px; text-align: center;"><span style="color: #000000;"><strong>KARAKTER ALAMI</strong></span></td>
</tr>
<tr style="height: 222px;">
<td style="width: 288px; height: 222px;"><span style="color: #ffffff;">&nbsp;</span>
<ul>
<li>Mengenal potensi diri mereka dengan baik</li>
<li>Percaya diri, sanggup mempertahankan pendapat dan mampu berkomunikasi dengan meyakinkan</li>
<li>Menjadi perhatian utama dalam setiap pertunjukan</li>
<li>Berani cepat dalam mengambil keputusan dan menciptakan sesuatu</li>
<li>Seringkali mengerjakan semua sendiri secara one-manshow</li>
<li>Kualitas diri merupakan hal yang sangat penting</li>
<li>Mampu impulsif, spontan, dan cepat</li>
</ul>
</td>
</tr>
</tbody>
</table>
<table style="height: 48px; background-color: #ff9f43;" width="100%">
<tbody>
<tr style="height: 36.5312px;">
<td style="width: 288px; height: 36.5312px; text-align: center;"><span style="color: #000000;"><strong>FAKTOR SUKSES</strong></span></td>
</tr>
<tr style="height: 204px;">
<td style="width: 288px; height: 204px;">
<ul>
<li>Mampu menjual potensi dan image diri dengan efektif</li>
<li>Sanggup untuk mempengaruhi banyak orang</li>
<li>Mampu menggunakan karisma pribadi untuk menjual apapun</li>
<li>Menjadi sosok yang disegani karena seringkali dianggap kompeten</li>
<li>Cepat dalam berpendapat dan memutuskan hal-hal yang bersifat urgent</li>
<li>Mencapai keberhasilan yang luar biasa ketika bermitra</li>
<li>Berpikiran terbuka, berani berinvestasi pada banyak hal</li>
<li>Cepat dalam mengambil risiko dan peluang</li>
</ul>
</td>
</tr>
</tbody>
</table>
<table style="height: 48px; background-color: #ff9f43;" width="100%">
<tbody>
<tr style="height: 10.5312px;">
<td style="width: 288px; height: 10.5312px;">
<p style="text-align: center;"><span style="color: #000000;"><strong>FAKTOR KEGAGALAN</strong></span></p>
</td>
</tr>
<tr style="height: 258px;">
<td style="width: 288px; height: 258px;"><span style="color: #ffffff;">&nbsp;</span>
<ul>
<li>Seringkali terlalu percaya diri dan terkesan arogan</li>
<li>Pendapat bisa tidak didukung fakta dan kompetensi yang tepat</li>
<li>Sanggup melakukan hal-hal yang akhirnya kontraproduktif untuk eksis</li>
<li>Sering menciptakan alasan-alasan atas kegagalan diri</li>
<li>Pendapat dan keputusan yang diambil seringkali tanpa pemikiran yang matang</li>
<li>Termasuk sulit untuk bisa mempercayai kualitas kerja orang lain</li>
<li>Seringkali boros dan tidak menghitung dengan tepat banyak hal</li>
<li>Berulangkali terpaksa menghabiskan waktu karena masalah yang tidak perlu</li>
</ul>
</td>
</tr>
</tbody>
</table>
<hr /><hr />
<p>&nbsp;</p>
<hr />
<p><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/diagram.png" alt="" width="100%" /></p>
<p>&nbsp;</p>
<table style="height: 392px; width: 100%;">
<tbody>
<tr>
<td style="width: 15.5194%;">
<p style="text-align: center;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-ALCHEMIST.png" width="100" height="100" /><br />ALCHEMIST</p>
</td>
<td style="width: 81.4806%;">
<p>Penyempurnaan yang baik, penyelesaian proses, menemukan cara yang lebih baik dab cerdas, meningkatkan efesiensi dan efektivitas produk/sitem yang ada.</p>
<p>Seseorang yang memilki kemampuan beradaptasi dengan menempatkan diri sesuai dengạn dimana ia berada. mampu untuk tetap merendah dan secara frontal berusaha untuk mengejar setiap peluang yang ada.</p>
</td>
</tr>
<tr>
<td style="width: 15.5194%; text-align: center;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-WIZARD.png" width="100" height="100" />WIZARD</td>
<td style="width: 81.4806%;">
<p>Konseptor yang &thorn;aik, penggagas hal baru, kreatif, inovatif, pencipta ide, pemikir out-of-the-box, spontan, tidak teratur, impulsif, baik untuk memulai sesuatu yang baru.</p>
<p>Seseorang yang pampu berinovasi secara berkelanjutan, seringkali kurang bisa dipahami oleh banyak orang disekitarnya, namun sanggup berpikir out-of-the-box, dalam mengbasilkan konsep-konsep baru.</p>
</td>
</tr>
<tr>
<td style="width: 15.5194%; text-align: center;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-BARD.png" width="100" height="100" />BARD</td>
<td style="width: 81.4806%;">
<p>Promotor yang baik, pemasar yang berbakat, merek pribadi yang kuat, &thorn;ergerak pat, butyh perhatian, bersinar, mampy menginspiraşi orang lain, individu yang kokoh.</p>
<p>Seseorang yang mampu memberi energi kepada orang-orang di sekelilingnya, mendukung dan menyebarkan semangat secara konsisten, sehingga mampu menyampaikan segala hal dengan penerimaan yang relatif mudah.</p>
</td>
</tr>
<tr>
<td style="width: 15.5194%; text-align: center;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-PRIEST.png" width="100" height="100" />KNIGHT</td>
<td style="width: 81.4806%;">
<p>Influencer yang baik, pemimpip yang karismatik, manajer pelaksana, penyelenggara, administrator, pendukung, baik dalam mencirtakan dan mengelola hubungan, baik dalam manajemen sumber daya manusia, fleksibel.</p>
<p>Seorang yang selalu mampu memberi semangat pada orang-orang disekitarnya, dimana api mampu menerangi kegelapan dan menyambar kemanapun, sebingga ia bisa mempengaruhi lebih banyak orang untuk sejalan seiring berjalannya waktu.</p>
</td>
</tr>
<tr>
<td style="width: 15.5194%; text-align: center;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-ARBITER.png" width="100" height="100" />ARBITER</td>
<td style="width: 81.4806%;">
<p>Penengah yang baik, pembawa damai, representatif bijak, melihat alternatif sebagai manfaat, penghindar konflik, berinyestasi pada orang untuk manfaat jangka panjang.</p>
<p>Seseorang yang tegas şerta mampu menciptakan kemungkinan baru, walaupun sebelumnya dirasakan tidak ada jalan lain, dimana angin biasanya memang tidak terlibat namun bisa dirasakan kehadirannya, mengisi apapun dan dimanapun.</p>
</td>
</tr>
<tr>
<td style="width: 15.5194%; text-align: center;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-MERCHANT.png" width="100" height="100" />MERCHANT</td>
<td style="width: 81.4806%;">
<p>Negosiator ulung, pedagang yang handal, pencari peluang, pengguna waktu yang efektif dan efisien, fleksibel, bekerja dalam pola/parameter tertentų, berdasarkan data.</p>
<p>Seseorang yang mampu memanfaatkan kekuatan dari wawasan yg luas, yang menampung apapun dari segala penjuru, mencapai keberhasilan dengan menerima berbagai hal, dan memanfaatkannva untuk keberhasilan yang lebih besar.&nbsp;</p>
</td>
</tr>
<tr>
<td style="width: 15.5194%; text-align: center;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-KNIGHT.png" width="100" height="100" />PRIEST</td>
<td style="width: 81.4806%;">
<p>Penjaga yang baik, dapat diandalkan, sabar, berhati-hati, berbasis fakta, safety- player, terstruktur, unggul dengan analisis informasi, cerdas dalam mengelola risiko.</p>
<p>Seseorang yang mampu menegakkan aturan secara tegas, disegani oleh orang- orang dibawahnya, dan ditakuti oleh siapapun yang melakukan pelanggaran aturan, dengan keteguhan tinggi sehingga mampu dijadikan sebagai tauladan (role-model).</p>
</td>
</tr>
<tr>
<td style="width: 15.5194%; text-align: center;"><img src="https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/S-MARSHAL.png" width="100" height="100" />MARSHAL</td>
<td style="width: 81.4806%;">
<p>Penganalisa yang baik, prediktif, lebih efektif di belakang layar, terencana, berkomitmen, pengatur yang handal, berkemauan keras, regulator yang baik.</p>
<p>Seseorang yang kokoh dan bisa diandalkan, terbuka untuk berbagai peluang yang lewat atau dilewatinya, dan mampu memanfaatkan apapun yang saat ini dimilikinya, termasuk memanfaatkan kekuatan waktu dan prioritas-prioritas yang tersusun dengan baik.</p>
</td>
</tr>
</tbody>
</table>
