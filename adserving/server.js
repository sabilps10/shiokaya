import models from "./libs";
import CrudActions from "./libs/crud";
const crudAction = new CrudActions({});
const express = require("express");
const config = require("config");
const app = express();
const port = config.get("port");
const jwt = require("jsonwebtoken");
const moment = require("moment");
const async = require("async");
const path = require("path");
const bodyParser = require("body-parser");
const _ = require("underscore");

const rateLimit = require("express-rate-limit");
var mysql2 = require("mysql2/promise");

const multer = require("multer");
const sha1 = require("sha1");

const crudable = require("./crudable");
const crudOptions = require("./crudOptions");

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, config.get("upload_path"));
  },
  filename: function (req, file, cb) {
    let chunks = file.originalname.split(".");
    let ext = chunks[chunks.length - 1];
    cb(null, sha1(file.originalname) + "." + ext);
  },
});
const upload = multer({ storage: storage });

const limiter = rateLimit({
  windowMs: config.get("ratelimit.windowMs"), // 15 minutes
  max: config.get("ratelimit.max"), // limit each IP to 100 requests per windowMs
});
app.use(limiter);

//modules
const cruds = require("./routes/cruds");
const login = require("./libs/login");
const guest = require("./libs/guest");

const token = require("./token");
const register = require("./libs/register");
const confirm = require("./libs/confirmation");
const activation = require("./libs/activation");

const userHasAccess = (access, req) => {
  console.log('meta',req.meta);
  let roles = req.meta.roles;
  if (!roles) return;
  if (roles.length === 0) return;
  for (let k in access) {
    if (roles.includes(access[k])) return true;
  }
  return false;
};

app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);

app.use(bodyParser.json());

app.use(function (req, res, next) {
  res.setHeader("Access-Control-Allow-Origin", "*");

  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, OPTIONS, PUT, PATCH, DELETE"
  );
  res.setHeader(
    "Access-Control-Allow-Headers",
    "X-Requested-With,content-type,Authorization,authorization,Content-Type"
  );
  res.setHeader("Access-Control-Allow-Credentials", false);
  //intercepts OPTIONS method
  if ("OPTIONS" === req.method) {
    //respond with 200
    res.send(200);
    return;
  }
  //inject pool
  req.pool = models.getPool();
  req.crud = {
    fields: ["*"],
    joins: [],
    filters: [],
  };

  next();
});

app.use(express.static("public"));
app.set("view engine", "ejs");

function isAuthorized(req, res, next) {
  if (typeof req.headers.authorization === "undefined") {
    console.log("need login");
    throw new Error("need_login");
  }

  let chunks = req.headers.authorization.split(" ");
  let token = chunks[1];
  var payload = jwt.verify(token, config.get("JWT.secret"));

  const { sub, session_id, roles } = payload;
  req.meta = { sub, session_id, roles };
  //if (!_.includes(roles, "user")) throw new Error("access_denied");

  next();
}
function isAuthor(req, res, next) {
  const { roles } = req.meta;
  if (typeof roles === "undefined") throw new Error("access_denied");
  if (roles.includes("admin") || roles.includes("cms")) next();
  else throw new Error("access_denied");
}
function isAdmin(req, res, next) {
  const { roles } = req.meta;

  if (typeof roles === "undefined") throw new Error("access_denied");
  if (roles[0] !== "admin") throw new Error("access_denied");
  next();
}
function isPremium(req, res, next) {
  const { roles } = req.meta;

  if (typeof roles === "undefined") throw new Error("access_denied");
  if (roles[0] !== "premium") throw new Error("access_denied");
  next();
}
const allowCrud = (req, res, next) => {
  if (crudable.includes(req.params.schema)) return next();
  return next(new Error("the end point is not available"));
};
function errorHandler(err, req, res, next) {
  console.log("error", err.message);
  if (res.headersSent) {
    return next(err);
  }

  if (err.message === "need_login") {
    res.status(401);
    return res.send({ status: 401, message: "Please login" });
  }

  if (err.message === "jwt expired") {
    res.status(403);
    return res.send({ status: 403, message: "Session Expired, Please Login" });
  }

  res.status(400);
  res.send({
    status: 0,
    error: err.message,
  });
}

app.use("/file", express.static("uploads"));

//upload media
app.post("/media/upload", [upload.single("file")], (req, res) => {
  try {
    res.send({
      file_url: config.get("download_url") + "/" + req.file.filename,
    });
  } catch (err) {
    console.log(err.message);
    res.send(400);
  }''/////
});
app.get("/campaign/:campaignId/:bannerId/banner",models.ads.getBanner);
app.get("/campaign/:campaignId/:bannerId/click",models.ads.click);

//misc
app.get("/", (req, res) => res.send({ status: 1 }));

app.use(errorHandler);

app.listen(port, () => console.log(`Ambu app listening on port ${port}!`));

function cleanup() {
  models.getPool().end();
  process.exit();
}

process.on("SIGINT", cleanup);
process.on("SIGTERM", cleanup);
