const crudOptions = {
  access:
    //those who falls into these arrays, means that requires role checking
    {
      blocks: ["admin", "cms"],
      pages: ["admin","cms"],
      settings: ["admin","cms"],
      pretests: ["admin"],
      questions: ["admin"],
      'question-groups': ["admin"],
      texts: ["admin"],
      profesi: ["admin"],
      industri: ["admin"],
      accounts: ["admin"],
      reset_token: ["admin"],
      'payment-confirmations': ["admin"],
      
    },
  fields: {
    usr_asn: ["id", "username", "nama_lengkap", "tanggal_akhir"],
    accounts: ["id", "name", "email", "session_id", "phone"],
  },
  filters: {
    iga_skala: ["iga_indikator_id"],
    iga_klaster_daerah: ["iga_klaster_id"],
    laporan: ["polsek_id"],
    dpo: ["polsek_id"],
    posts: ["category_id"],
    products: ["category_id"],
  },

  searchFilters: {
    laporan: [
      "tkp",
      "pelapor_nama",
      "nomor_mesin",
      "nomor_rangka",
      "pelapor_ktp",
      "pelapor_nama",
      "pelapor_telp",
      "jenis_motor",
    ],
    dpo: ["nama_lengkap", "nomor_laporan"],
    posts: ["posts.title"],
    products: ["products.name"],
    accounts: ["name", "email", "phone"],
  },
  relationships: {
    questions: [
      {
        targetSchema: "question_groups",
        pk: "id",
        fk: "group_id",
        type: "belongsTo",
        fields: ["id", "no", "answer_type"],
      },
    ],
    payment_confirmations: [
      {
        targetSchema: "accounts",
        pk: "id",
        fk: "session_id",
        type: "belongsTo",
        fields: ["id", "name", "email"],
      },
      {
        targetSchema: "results",
        pk: "id",
        fk: "session_id",
        type: "belongsTo",
        fields: [
          "creator",
          "mechanic",
          "star",
          "supporter",
          "dealmaker",
          "trader",
          "accumulator",
          "lord",
        ],
      },
    ],
    iga_skala: [
      {
        targetSchema: "iga_indikator",
        pk: "id",
        fk: "iga_indikator_id",
        type: "belongsTo",
        fields: ["id", "indikator"],
      },
    ],
    accounts: [
      {
        targetSchema: "roles",
        assocSchema: "account_roles",
        pk: "id", //account.id
        targetPk: "id", //roles.id
        targetFk: "role_id", //account_roles.role_id
        fk: "account_id", //account_roles.account_id
        type: "ManyToMany",
        fields: ["roles.id", "roles.name"], //roles.id/name
      },
    ],
    laporan: [
      {
        targetSchema: "polsek",
        pk: "id",
        fk: "polsek_id",
        type: "belongsTo",
        fields: ["id", "nama"],
      },
      {
        targetSchema: "saksi",
        pk: "id",
        fk: "laporan_id",
        type: "hasMany",
        fields: [
          "id",
          "nama_lengkap",
          "alamat",
          "telp",
          "tgl_lahir",
          "pekerjaan",
          "jenis_kelamin",
          "ktp",
        ],
      },
    ],
    dpo: [
      {
        targetSchema: "polsek",
        pk: "id",
        fk: "polsek_id",
        type: "belongsTo",
        fields: ["id", "nama"],
      },
    ],
  },
  joins: {
    laporan: [
      {
        targetSchema: "accounts",
        pk: "id",
        fk: "author_id",
        type: "inner",
        fields: ["id", "username", "name"],
      },
    ],
    posts: [
      {
        targetSchema: "post_categories",
        pk: "id",
        fk: "category_id",
        type: "inner",
        fields: ["id", "name"],
      },
    ],
    products: [
      {
        targetSchema: "categories",
        pk: "id",
        fk: "category_id",
        type: "inner",
        fields: ["id", "name"],
      },
    ],
    shades: [
      {
        targetSchema: "colors",
        pk: "id",
        fk: "color_id",
        type: "inner",
        fields: ["id", "name", "no"],
      },
      {
        targetSchema: "levels",
        pk: "id",
        fk: "level_id",
        type: "inner",
        fields: ["id", "name", "no"],
      },
    ],
  },
};

module.exports = crudOptions;
