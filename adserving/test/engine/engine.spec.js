import Engine from '../../libs/engine';

const engine = new Engine;

const question_groups = [
    {
      id: 1,
      questions: [
        {
          question: "Question 1.1",
          answer_type: "left-right",
          answers: ["Tidak Peduli", "Keduanya", "Peduli"],
          slots: ["C1", "C2", "C3"]
        },
        {
          question: "Question 1.2",
          answer_type: "left-right",
          answers: ["Tidak Peduli", "keduanya", "Peduli"],
          slots: ["C3", "C2", "C1"]
        },
        {
          question: "Question 1.3",
          answer_type: "left-right",
          answers: ["Tidak Peduli", "Biasa saja", "Peduli"],
          slots: ["C1", "C2", "C3"]
        }
      ]
    },
    {
      id: 2,
      questions: [
        {
          question: "Question 2.1",
          answer_type: "top-down",
          answers: ["Tidak Peduli", "Peduli"],
          slots: ["R1", "R2", "R3"]
        },
        {
          question: "Question 2.2",
          answer_type: "top-down",
          answers: ["Tidak Peduli", "Peduli"],
          slots: ["R3", "R2", "R1"]
        },
        {
          question: "Question 2.3",
          answer_type: "top-down",
          answers: ["Tidak Peduli", "Peduli"],
          slots: ["R1", "R2", "R3"]
        }
      ]
    },
    {
      id: 3,
      questions: [
        {
          question: "Question 3.1",
          answer_type: "ana",
          answers: ["Tidak Peduli", "Peduli"],
          slots: ["An", "A"]
        },
        {
          question: "Question 3.2",
          answer_type: "ana",
          answers: ["Tidak Peduli", "Peduli"],
          slots: ["A", "An"]
        },
        {
          question: "Question 3.3",
          answer_type: "ana",
          answers: ["Tidak Peduli", "Peduli"],
          slots: ["An", "A"]
        }
      ]
    },
    {
      id: 4,
      questions: [
        {
          question: "Question 1.1",
          answer_type: "vk",
          answers: ["Tidak Peduli", "Peduli"],
          slots: ["V", "K"]
        },
        {
          question: "Question 1.2",
          answer_type: "vk",
          answers: ["Tidak Peduli", "Peduli"],
          slots: ["K", "V"]
        },
        {
          question: "Question 1.3",
          answer_type: "vk",
          answers: ["Tidak Peduli", "Peduli"],
          slots: ["V", "K"]
        }
      ]
    }
  ];
  
var assert = require('assert');
describe('Engine', function() {
  describe('#shuffle()', function() {
    it('success', function() {
      let arr = engine.shuffle(question_groups);
      assert.equal(arr.length, question_groups.length);
    });
  });
  describe('#results()', function() {
    it('leftrightOK', function() {
      let results = engine.shuffle(questions, answers);
      assert.equal(results, question_groups.length);
    });
  });
});