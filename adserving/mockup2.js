/**
 * matrix
 */

const _ = require("lodash");

const matrix1 = ["A1", "A2", "A3", "B1", "B2", "B3", "C1", "C2", "C3"];

const matrix2 = [null, "V", null, "An", null, "A", null, "K", null];

const slots = {
  C1: [0, 3, 6],
  C2: [1, 7],
  C3: [2, 5, 8],
  R1: [0, 1, 2],
  R2: [3, 5],
  R3: [6, 7, 8],
  An: [3],
  A: [5],
  V: [1],
  K: [7]
};

const question_groups = [
  {
    id: 1,
    questions: [
      {
        question: "Question 1.1",
        answer_type: "left-right",
        answers: ["Tidak Peduli", "Keduanya", "Peduli"],
        slots: ["C1", "C2", "C3"]
      },
      {
        question: "Question 1.2",
        answer_type: "left-right",
        answers: ["Tidak Peduli", "keduanya", "Peduli"],
        slots: ["C3", "C2", "C1"]
      },
      {
        question: "Question 1.3",
        answer_type: "left-right",
        answers: ["Tidak Peduli", "Biasa saja", "Peduli"],
        slots: ["C1", "C2", "C3"]
      }
    ]
  },
  {
    id: 2,
    questions: [
      {
        question: "Question 2.1",
        answer_type: "top-down",
        answers: ["Tidak Peduli", "Peduli"],
        slots: ["R1", "R2", "R3"]
      },
      {
        question: "Question 2.2",
        answer_type: "top-down",
        answers: ["Tidak Peduli", "Peduli"],
        slots: ["R3", "R2", "R1"]
      },
      {
        question: "Question 2.3",
        answer_type: "top-down",
        answers: ["Tidak Peduli", "Peduli"],
        slots: ["R1", "R2", "R3"]
      }
    ]
  },
  {
    id: 3,
    questions: [
      {
        question: "Question 3.1",
        answer_type: "ana",
        answers: ["Tidak Peduli", "Peduli"],
        slots: ["An", "A"]
      },
      {
        question: "Question 3.2",
        answer_type: "ana",
        answers: ["Tidak Peduli", "Peduli"],
        slots: ["A", "An"]
      },
      {
        question: "Question 3.3",
        answer_type: "ana",
        answers: ["Tidak Peduli", "Peduli"],
        slots: ["An", "A"]
      }
    ]
  },
  {
    id: 4,
    questions: [
      {
        question: "Question 1.1",
        answer_type: "vk",
        answers: ["Tidak Peduli", "Peduli"],
        slots: ["V", "K"]
      },
      {
        question: "Question 1.2",
        answer_type: "vk",
        answers: ["Tidak Peduli", "Peduli"],
        slots: ["K", "V"]
      },
      {
        question: "Question 1.3",
        answer_type: "vk",
        answers: ["Tidak Peduli", "Peduli"],
        slots: ["V", "K"]
      }
    ]
  }
];

const question = data => {
  let q = _.shuffle(data)[0];
  return q;
};

const shuffled = [];
const displayQuestions = () => {
  let n = 1;
  question_groups.map(item => {
    let q = question(item.questions);
    shuffled.push(q);
    console.log(n + "." + q.question);
    console.log(q.answers[0] + " * * * * * * " + q.answers[1]);
    console.log(q.slots);
    n++;
  });
};

const handleLeftRightScenario = (question, answer) => {
  const { slots } = question;
  const [left, middle, right] = slots;
  let [leftScore, middleScore, rightScore] = [0, 0, 0];
  switch (answer) {
    case 1:
      leftScore = 4;
      middleScore = 2;
      rightScore = 0;
      break;
    case 2:
      leftScore = 2;
      middleScore = 4;
      rightScore = 0;
      break;
    case 3:
      leftScore = 0;
      middleScore = 8;
      rightScore = 0;
      break;
    case 4:
      leftScore = 0;
      middleScore = 4;
      rightScore = 2;
      break;
    case 5:
      leftScore = 0;
      middleScore = 2;
      rightScore = 4;
      break;
    case 6:
      rightScore = 8;
      break;
    default:
      leftScore = 8;
      break;
  }
  let results = [0, 0, 0, 0, 0, 0, 0, 0, 0];
  results = mapScore(results, left, leftScore);
  results = mapScore(results, middle, middleScore);
  results = mapScore(results, right, rightScore);
  return results;
};

const handleTopDownScenario = (question, answer) => {
  const { slots } = question;
  const [top, middle, bottom] = slots;
  let [topScore, middleScore, bottomScore] = [0, 0, 0];
  switch (answer) {
    case 1:
      topScore = 4;
      middleScore = 2;
      bottomScore = 0;
      break;
    case 2:
      topScore = 2;
      middleScore = 4;
      bottomScore = 0;

      break;
    case 3:
      topScore = 0;
      middleScore = 8;
      bottomScore = 0;
      break;
    case 4:
      topScore = 0;
      middleScore = 4;
      bottomScore = 2;
      break;
    case 5:
      topScore = 0;
      middleScore = 2;
      bottomScore = 4;
      break;
    case 6:
      bottomScore = 8;
      break;
    default:
      topScore = 8;
      break;
  }
  let results = [0, 0, 0, 0, 0, 0, 0, 0, 0];
  results = mapScore(results, top, topScore);
  results = mapScore(results, middle, middleScore);
  results = mapScore(results, bottom, bottomScore);
  return results;
};

const handleAnA = (question, answer) => {
  const { slots } = question;
  const [left, right] = slots;
  let [leftScore, rightScore] = [0, 0, 0];
  switch (answer) {
    case 1:
      leftScore = 4;

      leftScore = 0;
      break;
    case 2:
      leftScore = 2;

      leftScore = 0;

      break;
    case 3:
      leftScore = 1;
      leftScore = 1;
      break;
    case 4:
      leftScore = 0;
      leftScore = 2;
      break;
    case 5:
      leftScore = 0;
      leftScore = 4;
      break;
    case 6:
      leftScore = 8;
      break;
    default:
      leftScore = 8;
      break;
  }
  let results = [0, 0, 0, 0, 0, 0, 0, 0, 0];
  results = mapScore(results, left, leftScore);
  results = mapScore(results, right, rightScore);
  return results;
};
const handleVK = (question, answer) => {
  const { slots } = question;
  const [top, bottom] = slots;
  let [topScore, bottomScore] = [0, 0, 0];
  switch (answer) {
    case 1:
      topScore = 4;
      bottomScore = 0;
      break;
    case 2:
      topScore = 2;
      bottomScore = 0;

      break;
    case 3:
      topScore = 1;
      bottomScore = 1;
      break;
    case 4:
      topScore = 0;
      bottomScore = 2;
      break;
    case 5:
      topScore = 0;
      bottomScore = 4;
      break;
    case 6:
      bottomScore = 8;
      break;
    default:
      topScore = 8;
      break;
  }
  let results = [0, 0, 0, 0, 0, 0, 0, 0, 0];
  results = mapScore(results, top, topScore);
  results = mapScore(results, bottom, bottomScore);
  return results;
};

const mapScore = (results, slot, points) => {
  let items = slots[slot];
  items.map(item => {
    results[item] = points;
  });
  return results;
};

const simulate = () => {
  let n = 1;
  let results = [0, 0, 0, 0, 0, 0, 0, 0, 0];

  shuffled.map(q => {
    let roll = Math.floor(Math.random() * 7);
    try {
      
      if (q.answer_type === "left-right") {
        let r = handleLeftRightScenario(q, roll);
        console.log(q, roll, "left-right", r);
        for (let i = 0; i < results.length; i++) {
          results[i] += r[i];
        }
      }
      if (q.answer_type === "top-down") {
        let r = handleTopDownScenario(q, roll);
        console.log(q, roll, "top-down", r);
        for (let i = 0; i < results.length; i++) {
          results[i] += r[i];
        }
      }
      
      if (q.answer_type === "ana") {
        let r = handleAnA(q, roll);
        console.log(q, roll, "An-A", r);
        for (let i = 0; i < results.length; i++) {
          results[i] += r[i];
        }
      }
      if (q.answer_type === "vk") {
        let r = handleVK(q, roll);
        console.log(q, roll, "V-K", r);
        for (let i = 0; i < results.length; i++) {
          results[i] += r[i];
        }
      }
    } catch (e) {
      console.log(roll, e.message);
    }

    //console.log("shuffled:", q, "roll:", roll);
  });
  //console.log(results);
  console.log(results[0] + "|" + results[1] + "|" + results[2]);
  console.log(results[3] + "|" + results[4] + "|" + results[5]);
  console.log(results[6] + "|" + results[7] + "|" + results[8]);
};

displayQuestions();

console.log("Simulation Answers: ");
simulate();
