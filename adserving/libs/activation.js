const config = require("config");
const moment = require("moment-timezone");
const sha1 = require("sha1");
const sha256 = require("sha256");

const mysql = require("mysql2/promise");
const nodemailer = require("nodemailer");
const fs = require("fs");

let transport = nodemailer.createTransport({
  host: config.get("nodemailer.host"),
  port: config.get("nodemailer.port"),
  auth: {
    user: config.get("nodemailer.user"),
    pass: config.get("nodemailer.pass")
  }
});
//TODO - check jwt, system only
module.exports = {
  async activate(pool, req, res, next) {
    try {
      let id = req.params.id;
      let [
        rows,
        fields
      ] = await pool.execute(
        "SELECT * FROM payment_confirmations WHERE id = ? LIMIT 1",
        [id]
      );
     
      let email = rows[0].email;
      let session_id = rows[0].session_id;
      let name = rows[0].nama_rekening;
      let plain_pass = Math.ceil(Math.random() * 9999999);
      let secret = sha1(moment().unix());
      //let session_id = sha256("--" + moment().unix() + Math.floor(Math.random() * 999));
      let password = sha1(plain_pass + secret);
     
      let checkEmail = await pool.query(`select count(*) as jml from accounts \ 
                                                 where email like "%${email}%" limit 1`);

      if (checkEmail[0][0].jml === 0) {
        next(
          new Error("Mohon Maaf, E-Mail ini belum terdaftar sebelumnya.")
        );
        return;
      }
      /*
      let insertAccount = await pool.query(
        `
                                    insert into accounts \ 
                                    (name,email,password,created_at,secret,session_id) \
                                    values(?,?,?,NOW(),?,?)`,
        [name, email, password, secret, session_id]
      );
        */
       
      //update user account
      let updateAccount = await pool.query("UPDATE accounts SET password = ?,secret=? WHERE session_id=?", [password, secret, session_id]);
      let updateConfirm = await pool.query("UPDATE payment_confirmations SET confirm_status = 1 WHERE id = ? ",
        [id]
      );
      const PREMIUM = 3;

      let accounts = await pool.query("SELECT id FROM accounts WHERE session_id=? LIMIT 1", [session_id]);
      let account_id = accounts[0][0].id;
      let role = await pool.query("INSERT INTO account_roles(account_id,role_id) VALUES(?,?) ",
        [account_id, PREMIUM]
      );
        
      let template = fs.readFileSync(
        "./templates/email/activated.html",
        "utf8"
      );
      template = template.replace(
        "{{date}}",
        moment()
          .tz("UTC")
          .format("DD MMMM, YYYY")
      );
      template = template.replace("{{name}}", name);
      template = template.replace("{{token}}", plain_pass);
      template = template.replace("{{email}}", email);

      let mailOptions = {
        from: config.get("email"),
        to: email,
        subject: "Selamat!, Akun Premium anda sudah aktif !",
        html: template
      };

      let send = await transport.sendMail(mailOptions);
      
      res.send({ status: 1 });
    } catch (error) {
      next(error);
    }
  },
  async manual_activation(pool, req, res, next) {
    try {
      let id = req.params.id;
      let token = req.query.token;
      let [
        rows,
        fields
      ] = await pool.execute(
        "SELECT * FROM payment_confirmations WHERE id = ? LIMIT 1",
        [id]
      );
     
      let email = rows[0].email.trim();
      let session_id = rows[0].session_id;
      let name = rows[0].nama_rekening;
      let plain_pass = token;
      let secret = sha1(moment().unix());
      //let session_id = sha256("--" + moment().unix() + Math.floor(Math.random() * 999));
      let password = sha1(plain_pass + secret);
     
      let checkEmail = await pool.query(`select count(*) as jml from accounts \ 
                                                 where email like "%${email}%" limit 1`);

      if (checkEmail[0][0].jml === 0) {
        next(
          new Error("Mohon Maaf, E-Mail ini belum terdaftar sebelumnya.")
        );
        return;
      }
      /*
      let insertAccount = await pool.query(
        `
                                    insert into accounts \ 
                                    (name,email,password,created_at,secret,session_id) \
                                    values(?,?,?,NOW(),?,?)`,
        [name, email, password, secret, session_id]
      );
        */
       
      //update user account
      let updateAccount = await pool.query("UPDATE accounts SET password = ?,secret=? WHERE session_id=?", [password, secret, session_id]);
      let updateConfirm = await pool.query("UPDATE payment_confirmations SET confirm_status = 1 WHERE id = ? ",
        [id]
      );
      const PREMIUM = 3;

      let accounts = await pool.query("SELECT id FROM accounts WHERE session_id=? AND email is not null LIMIT 1", [session_id]);
      let account_id = accounts[0][0].id;
      let role = await pool.query("INSERT IGNORE INTO account_roles(account_id,role_id) VALUES(?,?) ",
        [account_id, PREMIUM]
      );
        
      let template = fs.readFileSync(
        "./templates/email/activated.html",
        "utf8"
      );
      template = template.replace(
        "{{date}}",
        moment()
          .tz("UTC")
          .format("DD MMMM, YYYY")
      );
      template = template.replace("{{name}}", name);
      template = template.replace("{{token}}", plain_pass);
      template = template.replace("{{email}}", email);

      let mailOptions = {
        from: config.get("email"),
        to: email,
        subject: "Berikut adalah token baru anda : " + plain_pass,
        html: template
      };

      let send = await transport.sendMail(mailOptions);
      
      res.send({ status: 1 });
    } catch (error) {
      next(error);
    }
  }
};
