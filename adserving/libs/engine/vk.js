import mapScore from "./mapScore";

const handleVK = (question, answer) => {
  const { slots } = question;
  const [top, bottom] = slots;
  let [topScore, bottomScore] = [0, 0, 0];
  switch (answer) {
    case 1:
      topScore = 4;
      bottomScore = 0;
      break;
    case 2:
      topScore = 2;
      bottomScore = 0;

      break;
    case 3:
      topScore = 1;
      bottomScore = 1;
      break;
    case 4:
      topScore = 0;
      bottomScore = 2;
      break;
    case 5:
      topScore = 0;
      bottomScore = 4;
      break;
    case 6:
      bottomScore = 8;
      break;
    default:
      topScore = 8;
      break;
  }
  let results = [0, 0, 0, 0, 0, 0, 0, 0, 0];
  results = mapScore(results, top, topScore);
  results = mapScore(results, bottom, bottomScore);
  return results;
};
export default handleVK;
