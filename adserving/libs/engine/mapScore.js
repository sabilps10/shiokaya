import slots from "./slots";

const mapScore = (results, slot, points) => {
    let items = slots[slot];
    items.map(item => {
      results[item] = points;
    });
    return results;
  };
  
  export default mapScore;