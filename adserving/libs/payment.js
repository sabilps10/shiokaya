import Actions from "./actions";
const axios = require("axios");
const config = require("config");
const moment = require("moment");
const sha1 = require("sha1");
axios.defaults.headers.post["Content-Type"] = "application/json";

const payment_endpoint = config.get("payment.endpoint");

const nodemailer = require("nodemailer");
const fs = require("fs");

let transport = nodemailer.createTransport({
  host: config.get("nodemailer.host"),
  port: config.get("nodemailer.port"),
  auth: {
    user: config.get("nodemailer.user"),
    pass: config.get("nodemailer.pass"),
  },
});

class Payment extends Actions {
  constructor(props) {
    super({
      ...props,
      tableName: "transactions",
    });
    this.get_url = this.get_url.bind(this);
    this.notify = this.notify.bind(this);
    this.activate = this.activate.bind(this);
  }
  async get_url(req, res, next) {
    try {
      let session_id = req.body.session_id;
      let account = await this.select("SELECT * FROM accounts where session_id=? LIMIT 1",[session_id])
      let account_id = account[0].id;
      let transaction_id = account_id + "-" + moment().unix();
      console.log(transaction_id);
      let payload = {
        transaction_id,
        product: "Shiokaya Premium",
        quantity: 1,
        price: 200000,
        comment: "one-time subscription",
      };
      let insertId = await this.insert(
        {
          account_id,
          transaction_id,
          transaction_status: 0,
        },
        [],
        "created_at"
      );
      
      if (insertId > 0) {
          
        let response = await axios.post(payment_endpoint, {
          ...payload,
        });
        return res.send({
            status: 1,
            trx_id: insertId,
            ...response.data
          });
        
      }else{
          return res.send({status:0, message:"Maaf, Kami gagal menyiapkan transaksi untuk anda. Silahkan coba kembali !"})
      }

      
    } catch (error) {
      return next(error);
    }
  }
  async notify(req, res, next) {
    try {
        console.log('notify:',req.body);
        const {transaction_id} = req.body;
     
      console.log("CHECK logs : ", transaction_id);

      let transaction = await this.select(
        "SELECT status_code FROM payment_logs WHERE transaction_id = ? LIMIT 1",
        [transaction_id]
      );
      console.log(transaction_id, transaction);
      if(transaction[0].status_code === '1'){
          console.log(transaction_id);
          console.log(moment().format("YYYY-MM-DD HH:mm:ss")," - ", transaction_id  ," - COMPLETED");
          let trans = await this.select("SELECT * FROM transactions where transaction_id = ? ",[transaction_id]);
          const {id,account_id} = trans[0];
          let update = await this.queryWithValues("UPDATE transactions SET transaction_status = 1, updated_at = NOW() WHERE id = ?", [id]);
          let activate = await this.activate(account_id);
          if(activate){
             console.log("activated");
          }
          
      }else{
        console.log(moment().format("YYYY-MM-DD HH:mm:ss")," - ",transaction_id  ," - FAILED -" + transaction[0].status_code);
      }
      return res.send({
        status: 1,
      });
    } catch (error) {
      next(error);
    }
  }
  async activate(account_id) {
    let plain_pass = Math.ceil(Math.random() * 9999999);
    let secret = sha1(moment().unix());
    let password = sha1(plain_pass + secret);
    let updateAccount = await this.queryWithValues(
      "UPDATE accounts SET password = ?,secret=? WHERE id=?",
      [password, secret, account_id]
    );
    let account = await this.select("SELECT * FROM accounts WHERE id = ? LIMIT 1", [account_id]);
    const {
        name,
        email
    } = account[0];

    const PREMIUM = 3;
    let role = await this.queryWithValues(
      "INSERT INTO account_roles(account_id,role_id) VALUES(?,?) ",
      [account_id, PREMIUM]
    );
    let template = fs.readFileSync("./templates/email/activated.html", "utf8");
    template = template.replace(
      "{{date}}",
      moment().tz("UTC").format("DD MMMM, YYYY")
    );
    template = template.replace("{{name}}", name);
    template = template.replace("{{token}}", plain_pass);
    template = template.replace("{{email}}", email);

    let mailOptions = {
      from: config.get("email"),
      to: email,
      subject: "Selamat!, Akun Premium anda sudah aktif !",
      html: template,
    };

    let send = await transport.sendMail(mailOptions);
    return send;
  }
}

export default Payment;
