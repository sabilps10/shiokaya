import CRUD from "./db/crud";
export default class Actions extends CRUD {
  /**
   * props:{
   *   tableName: tableName,
   *   pool: connection_pool
   * }
   */
  constructor(props) {
    super(props);
   
    this.fields = [];
    this.props = props;
    this.getAll = this.getAll.bind(this);
    this.get = this.get.bind(this);
    this.add = this.add.bind(this);
    this.update = this.update.bind(this);
    this.delete = this.delete.bind(this);
  }
  async getAll(req, res, next) {
    let {page, per_page, search} = req.query
    page = page === null ? 0 : parseInt(page)
    per_page = per_page === null ? 0 : parseInt(per_page)
    try {
      let result = await this.paginate({
        page:  page ,
        limit: per_page,
        conditions:[]
      });
      return res.send({
        status: 1,
        data: result.rows,
        page: page,
        total: result.totalResults
      });
    } catch (error) {
      next(error);
    }
  }
  async get(req, res, next) {
    const id = req.params.id;
    try {
      let data = await this.findById(id);
      return res.send({
        status: 1,
        data
      });
    } catch (error) {
      next(error);
    }
  }
  async add(req, res, next) {
    try {
      let rs = await this.insert(req.body);
      return res.send({
        status: 1,
        id: rs[0].insertId,
        ...req.body,
        message: "Pertanyaan telah ditambah !"
      });
    } catch (error) {
      next(error);
    }
  }
  async update(req, res, next) {
   
    try {
      const id = req.params.id;
    
      let rs = await this.save(id, req.body);
      return res.send({
        status: 1,
        ...req.body,
        message: "Pertanyaan telah diupdate !"
      });
    } catch (error) {
      console.log(error);
      next(error);
    }
  }
  async delete(req, res, next) {
    const id = req.params.id;
    try {
      let rs = await this.remove(id);
      return res.send({
        status: 1,
        message: "Pertanyaan telah dihapus !"
      });
    } catch (error) {
      next(error);
    }
  }
}
