import Actions from "./actions";
import Account from "./account";
const qs = require('qs');
const axios = require("axios");
const config = require("config");
const moment = require("moment");
const sha1 = require("sha1");
axios.defaults.headers.post["Content-Type"] = "application/json";
const payment_endpoint = config.get("payment.endpoint");

const nodemailer = require("nodemailer");
const fs = require("fs");

let transport = nodemailer.createTransport({
  host: config.get("nodemailer.host"),
  port: config.get("nodemailer.port"),
  auth: {
    user: config.get("nodemailer.user"),
    pass: config.get("nodemailer.pass"),
  },
});

class Ads extends Actions {
  constructor(props) {
    super({
      ...props,
      tableName: "adcampaigns",
    });

    this.getBanner = this.getBanner.bind(this);
    this.click = this.click.bind(this);
    this.addStats = this.addStats.bind(this);
  }
  async addStats(props){
    
    try {
      const {
        campaignId,
        affiliate_id,
        logtype
      } = props;
      let logdate = moment().format("YYYY-MM-DD");
      let rs = await this.queryWithValues(`INSERT INTO 
      adlogs(logdate,campaign_id,affiliate_id,logtype, total)
      VALUES(?,?,?,?, 1) ON DUPLICATE KEY UPDATE total = total + 1`,
      [logdate, campaignId,affiliate_id,logtype]);
      return rs;
      //return res.sendFile('/home/duf/projects/shiokaya/api/uploads/1f218b0c24ed41529ec701c000ff9a003cb1ca8e.png');
    } catch (error) {
      return next("Affiliate:", error);
    }
  }
  //http://localhost:3095/campaign/1589622029/1/banner?affiliate_id=12
  async getBanner(req, res, next) {
    try {
      const { campaignId, bannerId } = req.params;
      const {affiliate_id} = req.query;

      let rs = await this.selectOne(`SELECT * FROM adcampaigns WHERE campaign_id = ? LIMIT 1`, [campaignId]);
      
      let asset = await this.selectOne(`SELECT * FROM adcampaigns_assets WHERE campaign_id = ? AND id = ?`,[rs.id, bannerId]);
      
      let stat = await this.addStats({
        campaignId: asset.campaign_id,
        affiliate_id,
        logtype:"IMP"
      })
      return res.redirect(asset.file);
      //return res.sendFile('/home/duf/projects/shiokaya/api/uploads/1f218b0c24ed41529ec701c000ff9a003cb1ca8e.png');
    } catch (error) {
      return next("Affiliate:", error);
    }
  }
  async click(req, res, next) {
    try {
      const { campaignId, bannerId } = req.params;
      const {affiliate_id} = req.query;

      let rs = await this.selectOne(`SELECT * FROM adcampaigns WHERE campaign_id = ? LIMIT 1`, [campaignId]);
      
      let asset = await this.selectOne(`SELECT * FROM adcampaigns_assets WHERE campaign_id = ? AND id = ?`,[rs.id, bannerId]);
      
      let stat = await this.addStats({
        campaignId: asset.campaign_id,
        affiliate_id,
        logtype:"CLICK"
      })

      let c = rs.landing_url.split("?");
      let p = c.length > 1 ? qs.parse(c[1]) : {}
      p.affiliate_id = affiliate_id;
      res.redirect(c[0] + "?" + qs.stringify(p));
    } catch (error) {
      return next(error);
    }
  }

}

export default Ads;
