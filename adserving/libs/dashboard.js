import Actions from "./actions";
import Account from "./account";

const axios = require("axios");
const config = require("config");
const moment = require("moment");
const sha1 = require("sha1");
axios.defaults.headers.post["Content-Type"] = "application/json";
const payment_endpoint = config.get("payment.endpoint");

const nodemailer = require("nodemailer");
const fs = require("fs");

let transport = nodemailer.createTransport({
  host: config.get("nodemailer.host"),
  port: config.get("nodemailer.port"),
  auth: {
    user: config.get("nodemailer.user"),
    pass: config.get("nodemailer.pass"),
  },
});

class Dashboard extends Actions {
  constructor(props) {
    super({
      ...props,
      tableName: "affiliates",
    });
    this.getSummary = this.getSummary.bind(this);
  }
  async getSummary(req, res, next) {
    try {
      let account_id = req.meta.sub;
      let affiliate = await this.selectOne("SELECT * FROM affiliates WHERE account_id=? LIMIT 1", [account_id]);
      
      return res.send({
          status:1,
          affiliate_no: affiliate.affiliate_no
      })
     
    } catch (error) {
      return next(error);
    }
  }
  async sendEmail(props){
    const {name, email, affiliate_no} = props;

    let template = fs.readFileSync(
      "./templates/email/affiliate_register.html",
      "utf8"
    );
    
    template = template.replace(
      "{{date}}",
      moment()
        .tz("UTC")
        .format("DD MMMM, YYYY")
    );
    template = template.replace("{{name}}", name);
    template = template.replace("{{affiliate_no}}", affiliate_no);
    template = template.replace("{{email}}", email);

    let mailOptions = {
      from: config.get("email"),
      to: email,
      subject: "Welcome to Shiokaya Affiliation Programme",
      html: template
    };

    let send = await transport.sendMail(mailOptions);
    return send;
  }
}

export default Dashboard;
