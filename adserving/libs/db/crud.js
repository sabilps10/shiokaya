//
// reference https://github.com/sidorares/node-mysql2/blob/master/documentation/Promise-Wrapper.md
//
const _ = require("lodash");
const moment = require("moment-timezone");
export default class CRUD {
  /**
   * props:{
   *   tableName: tableName,
   *   pool: connection_pool
   * }
   */
  constructor(props) {
    this.fields = [];
    this.props = props;
    this.formatJoins = this.formatJoins.bind(this);
    this.getJoinType = this.getJoinType.bind(this);
  }
  /**
   * compile the joins array and transformed it into an SQL
   */
  formatJoins(schema, joins) {
    let str = "";
    let selects = [];
    if (typeof joins === "undefined") return ["", []];
    let allJoins = joins.map((item, index) => {
      const { type, targetSchema, pk, fk, fields } = item;

      const alias = "a" + index;
      str +=
        this.getJoinType(type) +
        " " +
        targetSchema +
        " " +
        alias +
        " ON " +
        schema +
        "." +
        fk +
        "=" +
        alias +
        "." +
        pk +
        "\n";

      let the_fields = [];
      for (var t in fields) {
        the_fields.push(
          alias + "." + fields[t] + " as " + targetSchema + "_" + fields[t]
        );
      }
      return the_fields;
    });

    for (let i in allJoins) {
      selects.push(...allJoins[i]);
    }

    return [str, selects];
  }
  getJoinType(type) {
    let str = "INNER JOIN";
    switch (type) {
      case "left":
        str = "LEFT JOIN";
        break;
      case "right":
        str = "RIGHT JOIN";
        break;
      case "left_outer":
        str = "LEFT OUTER JOIN";
        break;
      case "right_outer":
        str = "RIGHT OUTER JOIN";
        break;
      case "inner":
        str = "INNER JOIN";
        break;
      default:
        str = "INNER JOIN";
        break;
    }
    return str;
  }
  async findById(id) {
    const { tableName, pool } = this.props;
    const [rows, fields] = await pool.execute(
      "SELECT * FROM " + tableName + " WHERE id = ? LIMIT 1",
      [id]
    );
    this.fields = fields;
    return rows;
  }

  async findAll(opts) {
    const { tableName, pool } = this.props;
    let { start, limit, where, select, joins, searchConditions, order } =
      typeof opts === "undefined" ? { start: 0, limit: 100 } : opts;
    if (typeof start === "undefined") start = 0;
    if (typeof limit === "undefined") limit = 100;
    if (typeof where === "undefined") where = {};
  
    //console.log(searchConditions);
    let conditions = [];
    let conditions2 = [];
    let values = [];

    for (let k in where) {
      conditions.push(k + "=?");
      values.push(where[k]);
    }
    for (let k in searchConditions) {
      conditions2.push(k + " LIKE '%" + searchConditions[k] + "%'");
      //values.push(where[k]);
    }

    const [joinQuery, joinSelects] = this.formatJoins(tableName, joins);
    if (joinSelects.length > 0) {
      if (typeof select === "undefined") select = [tableName + ".*"];
      if (select === null) select = [tableName + ".*"];
      joinSelects.map(select_item => {
        select.push(select_item);
      });
    }

    if (typeof order === "undefined")
      order = {
        id: "DESC"
      };
    let orderBy = [];
    for (let k in order) {
      orderBy.push(k + " " + order[k]);
    }

    if (typeof select === 'undefined') select = null;
    //default order
    if (typeof orderBy === "undefined") orderBy = [];
    if (orderBy === null) orderBy = [];
    if (orderBy.length === 0) orderBy.push("id DESC");

    //console.log(select,orderBy,conditions,conditions2);
    let sql =
      "SELECT " +
      (select !== null ? select.join(",") : "*") +
      " FROM " +
      tableName +
      " " +
      joinQuery +
      " " +
      (conditions.length > 0 ? "WHERE " + conditions.join(" AND ") : "") +
      (conditions.length > 0 && conditions2.length > 0
        ? " AND (" + conditions2.join(" OR ") + ")"
        : "") +
      (conditions.length === 0 && conditions2.length > 0
        ? "WHERE (" + conditions2.join(" OR ") + ")"
        : "") +
      " ORDER BY " +
      orderBy.join(",") +
      " LIMIT " +
      start +
      "," +
      limit;
       // console.log(sql)
    const [rows, fields] = await pool.execute(sql, values);
    this.fields = fields;
    this.sql = sql;
    return rows;
  }
  async count(opts) {
    const { tableName, pool } = this.props;
    const { where } = opts;

    let conditions = [];
    let values = [];

    for (let k in where) {
      conditions.push(k + "=?");
      values.push(where[k]);
    }

    const [rows, fields] = await pool.execute(
      "SELECT COUNT(*) as total FROM " +
        tableName +
        " " +
        (conditions.length > 0 ? "WHERE " + conditions.join(" AND ") : ""),
      values
    );
    this.fields = fields;

    return rows[0].total;
  }

  async paginate(opts) {
    let {
      conditions,
      page,
      limit,
      joins,
      relationships,
      searchConditions,
      order,
      select
    } = opts;

    if (page === null) page = 0;
    page = parseInt(page);
    limit = parseInt(limit);
    let start = parseInt(page) * parseInt(limit);
   
    let queryOpts = {
      where: conditions,
      searchConditions,
      start,
      limit,
      joins,
      relationships,
      order,
      select
    };

    let rows = await this.findAll(queryOpts);

    let data = [];
    rows.map(async (row, index) => {
      if (typeof relationships !== "undefined") {
        let relations = relationships.map(async relation => {
          try {
            let q = null;

            if (relation.type === "hasMany") {
              q = await this.select(
                "SELECT * FROM " +
                  relation.targetSchema +
                  " WHERE " +
                  relation.fk +
                  "=? LIMIT 1",
                [row[relation.pk]]
              );
              /*  console.log(
                "SELECT * FROM " +
                  relation.targetSchema +
                  " WHERE " +
                  relation.fk +
                  "=? LIMIT 1",
                row[relation.pk]
              );*/
              return {
                name: relation.targetSchema,
                row: q ? q : []
              };
            } else if (relation.type === "ManyToMany") {
              let sql =
                "SELECT " +
                relation.fields.join(",") +
                " FROM " +
                relation.assocSchema +
                " a INNER JOIN " +
                relation.targetSchema +
                " ON a." +
                relation.targetFk +
                " = " +
                relation.targetSchema +
                "." +
                relation.targetPk +
                " \
                WHERE a." +
                relation.fk +
                " = ?;";

              q = await this.select(sql, [row[relation.pk]]);

              return {
                name: relation.targetSchema,
                row: q ? q : []
              };
            } else {
              q = await this.select(
                "SELECT * FROM " +
                  relation.targetSchema +
                  " WHERE " +
                  relation.pk +
                  "=? LIMIT 1",
                [row[relation.fk]]
              );
             
              return {
                name: relation.targetSchema,
                row: q ? q[0] : null
              };
            }
          } catch (err) {
            console.log("relationError:", err.message);
            return {
              name: relation.targetSchema,
              row: null
            };
          }
        });

        let p = await Promise.all(relations);
        p.map(r => {
          // console.log("relation", r);
          row[r.name] = r.row;
        });
      }
      data.push(row);
    });

    let totalResults = await this.count(queryOpts);
    let totalPage = Math.floor(totalResults / limit);
    return {
      rows: data,
      totalResults,
      page: parseInt(page),
      limit,
      totalPage
    };
  }

  async insert(input, excludes, timestamp) {
    const { tableName, pool } = this.props;
    let fields = [];
    let values = [];
    let vars = [];
    for (let k in input) {
      if (!_.includes(excludes, k) && input[k] !== null) {
        fields.push(k);
        values.push(input[k]);
        vars.push("?");
      }
    }
    if (timestamp) {
      fields.push(timestamp);
      values.push(moment().format("YYYY-MM-DD HH:mm:ss"));
      vars.push("?");
    }

    let sql =
      "INSERT INTO " +
      tableName +
      "(" +
      fields.join(",") +
      ") VALUES( " +
      vars.join(",") +
      ")";
    try{
      let rs = await pool.query(sql, values);
      return rs[0].insertId;
    }catch(error){
      console.log("INSERT ERROR ",error.message);
      throw new Error("unable to insert data into ", tableName);
    }
   
  }
  async insertOrUpdate(input, excludes, timestamp) {
    const { tableName, pool } = this.props;
    let fields = [];
    let values = [];
    let vars = [];
    let updates = [];
    for (let k in input) {
      if (!_.includes(excludes, k) && input[k] !== null) {
        fields.push(k);
        values.push(input[k]);
        vars.push("?");
        updates.push(k + " = VALUES(" + k + ")");
      }
    }
    if (timestamp) {
      fields.push(timestamp);
      values.push(moment().format("YYYY-MM-DD HH:mm:ss"));
      vars.push("?");
    }

    let sql =
      "INSERT INTO " +
      tableName +
      "(" +
      fields.join(",") +
      ") VALUES( " +
      vars.join(",") +
      ") ON DUPLICATE KEY UPDATE " +
      updates.join(",");

    let rs = await pool.query(sql, values);
    return rs;
  }
  async save(id, input, timestamp) {
    const pool = this.props.pool;
    const { tableName } = this.props;
    let fields = [];
    let values = [];
    for (let k in input) {
      fields.push(k + "=?");
      if (k === "created_at" || k === "updated_at") {
        values.push(moment(input[k]).format("YYYY-MM-DD HH:mm:ss"));
      } else {
        values.push(input[k]);
      }
    }

    if (timestamp) {
      fields.push(timestamp + "=NOW()");
    }

    values.push(parseInt(id));

    let sql =
      "UPDATE " + tableName + " SET " + fields.join(",") + " WHERE id = ?";
    //console.log(sql, values);
    let rs = await pool.query(sql, values);
    return rs;
  }

  async remove(id) {
    const pool = this.props.pool;
    const { tableName } = this.props;
    let sql = "DELETE FROM " + tableName + " WHERE id = ? ";
    let rs = await pool.query(sql, [id]);
  }

  async query(sql) {
    const pool = this.props.pool;
    let rs = await pool.query(sql);
    return rs;
  }
  async queryWithValues(sql, values) {
    const pool = this.props.pool;
    
    console.log(sql, values);
    let rs = await pool.query(sql, values);
    
    return rs;
  }

  async select(sql, value) {
    const pool = this.props.pool;

    let [rows, fields] = await pool.execute(sql, value);

    return rows;
  }
  async selectOne(sql, value) {
    const pool = this.props.pool;
    
    let [rows, fields] = await pool.execute(sql, value);
   
    if(rows.length === 0) return null;
    return rows[0];
  }
}
