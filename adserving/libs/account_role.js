import Actions from "./actions";


class AccountRole extends Actions {
  constructor(props) {
    super({
      ...props,
      tableName: "account_roles",
    });
  }
 
}

export default AccountRole;
