import Actions from "./actions";
import Engine from "./engine";
import moment from "moment";
const _ = require("lodash");

class Content extends Actions {
  constructor(props) {
    super({
      ...props,
      tableName: "contents",
    });
    this.getContent = this.getContent.bind(this);
    this.getPremium = this.getPremium.bind(this);
    this.getPremiumBySlug = this.getPremiumBySlug.bind(this);
    this.getPremiumContent = this.getPremiumContent.bind(this);
  }
  async getContent(req, res, next) {
    try {
      let slug = req.params.slug;
      let content = await this.select(
        "SELECT id,title,summary,photo_url,logo_url,simbol,warna,nama,tagline,karakter,fungsi,color FROM contents WHERE slug = ? LIMIT 1",
        [slug]
      );

      res.send({
        status: 1,
        data: content[0],
      });
    } catch (error) {
      return next(error);
    }
  }
  async getContentCalculated(result) {
    //calculate the result
    let arr = [
      {
        name: "mechanic",
        score: result.mechanic,
        value: 0,
      },
      {
        name: "creator",
        score: result.creator,
        value: 0,
      },
      {
        name: "star",
        score: result.star,
        value: 0,
      },
      {
        name: "supporter",
        score: result.supporter,
        value: 0,
      },
      {
        name: "dealmaker",
        score: result.dealmaker,
        value: 0,
      },
      {
        name: "trader",
        score: result.trader,
        value: 0,
      },
      {
        name: "accumulator",
        score: result.accumulator,
        value: 0,
      },
      {
        name: "lord",
        score: result.lord,
        value: 0,
      },
    ];

    let p = 0;
    let n = 0;
    for (let i = 0; i < arr.length; i++) {
      p = i - 1;
      n = i + 1;
      if (p < 0) p = 7;
      if (n > 7) n = 0;
      //console.log(p,i,n,arr[p].score,arr[i].score,arr[n].score);
      if (arr[i].score === arr[p].score && arr[i].score === arr[n].score) {
        arr[i].value =
          (parseInt(arr[p].score) +
            parseInt(arr[i].score) +
            parseInt(arr[n].score)) /
          3;
      } else if (arr[i].score === arr[p].score) {
        arr[i].value =
          (parseInt(arr[p].score) +
            parseInt(arr[i].score) +
            parseInt(arr[n].score)) /
          3;
      } else if (arr[i].score === arr[n].score) {
        arr[i].value =
          (parseInt(arr[p].score) +
            parseInt(arr[i].score) +
            parseInt(arr[n].score)) /
          3;
      } else {
        arr[i].value = arr[i].score;
      }

      //arr[i].value = (parseInt(arr[p].score) + parseInt(arr[i].score) + parseInt(arr[n].score)) / 3;
    }
    //calculate for the same score between 2
    let rs = _.orderBy(arr, ["value"], ["desc"]);

    try {
      let content = await this.select(
        "SELECT * FROM contents WHERE attribute = ?",
        [rs[0].name]
      );
      let profil = content[0].slug;
      let nilai_slug = "nilai-" + profil;
      let nilai = await this.select(
        "SELECT * FROM texts WHERE slug=? LIMIT 1",
        [nilai_slug]
      );
      let [kekuatan, naluri] = nilai[0].content.replace("<p>","").replace("</p>","").split("|");
      let profesi = await this.select("SELECT id,slug,name,profesi FROM profesi ORDER BY slug ASC,name ASC");
      let industri = await this.select("SELECT id,slug,jenis,profesi FROM industri ORDER BY jenis ASC,slug ASC");
      let copytext = await this.select("SELECT * FROM texts", []);
      let bayangan = content[0].profil_bayangan.split('|');
      let bayangan1 = await this.select("SELECT title,color,deskripsi_singkat,deskripsi_karakter,karakter_alami,faktor_kesuksesan,faktor_kegagalan,dukungan FROM contents WHERE slug=?", [bayangan[0]]);
      let bayangan2 = await this.select("SELECT title,color,deskripsi_singkat,deskripsi_karakter,karakter_alami,faktor_kesuksesan,faktor_kegagalan,dukungan FROM contents WHERE slug=?", [bayangan[1]]);
      let dukungan_primer_slug = content[0].dukungan_primer.split("|");
      let dukungan_sekunder_slug = content[0].dukungan_sekunder.split("|");
      let konflik_sekunder_slug = content[0].konflik_sekunder.split("|");
      let konflik_tersier_slug = content[0].konflik_tersier.split("|");
      let shio = await this.select("SELECT title,slug,dukungan,color,kekuatan FROM contents", []);
      let dukungan_primer = [];
      let dukungan_sekunder = [];
      let konflik_primer = [];
      let konflik_sekunder = [];
      let konflik_tersier = [];

      let shio_profesi = {
            alchemist:[],
            wizard:[],
            bard:[],
            knight:[],
            arbiter:[],
            merchant:[],
            priest:[],
            marshal:[],
              };

      let shio_industri = {
        'Property':[],
        'Education':[],
        'Finance':[],
        'Manufacture':[]
      }

      profesi.map((item)=>{
        if(typeof shio_profesi[item.slug] === 'undefined') shio_profesi[item.slug] = [];
        shio_profesi[item.slug].push(item);
      })

      industri.map((item)=>{
        if(typeof shio_industri[item.jenis] === 'undefined') shio_industri[item.jenis] = [];
        shio_industri[item.jenis].push(item);
      })

      shio.map(item=>{
        dukungan_primer_slug.map(slug=>{
          if(item.slug === slug) dukungan_primer.push(item);
        });
      })
      
      shio.map(item=>{
        dukungan_sekunder_slug.map(slug=>{
          if(item.slug === slug) dukungan_sekunder.push(item);
        });
      })

      shio.map(item=>{
        if(item.slug === content[0].konflik_primer) konflik_primer.push(item);
      })
      shio.map(item=>{
        konflik_sekunder_slug.map(slug=>{
          if(item.slug === slug) konflik_sekunder.push(item);
        });
      })
      shio.map(item=>{
        konflik_tersier_slug.map(slug=>{
          if(item.slug === slug) konflik_tersier.push(item);
        });
      })

      return {
        content: content[0],
        nilai: nilai[0],
        nilai_kekuatan: kekuatan.split(","),
        nilai_naluri: naluri.split(","),
        kekuatan: this.getKekuatan(copytext, kekuatan),
        naluri: this.getNaluri(copytext, naluri),
        primer: this.getCopy(copytext, 'primer'),
        sekunder: this.getCopy(copytext, 'sekunder'),
        tersier: this.getCopy(copytext, 'tersier'),
        profil_utama_bayangan: this.getCopy(copytext, 'profil_utama_bayangan'),
        mitra: this.getCopy(copytext, 'mitra'),
        konflik: this.getCopy(copytext, 'konflik'),
        nilai: this.getCopy(copytext, 'nilai'),
        naluri_copy: this.getCopy(copytext, 'naluri'),
        alchemist_ub: this.getCopy(copytext, 'alchemist_ub'),
        wizard_ub: this.getCopy(copytext, 'wizard_ub'),
        bard_ub: this.getCopy(copytext, 'bard_ub'),
        knight_ub: this.getCopy(copytext, 'knight_ub'),
        arbiter_ub: this.getCopy(copytext, 'arbiter_ub'),
        merchant_ub: this.getCopy(copytext, 'merchant_ub'),
        priest_ub: this.getCopy(copytext, 'priest_ub'),
        marshal_ub: this.getCopy(copytext, 'marshal_ub'),
        shio,
        bayangan1:bayangan1[0],
        bayangan2:bayangan2[0],
        dukungan_primer,
        dukungan_sekunder,
        konflik_primer,
        konflik_sekunder,
        konflik_tersier,
        shio_profesi,
        shio_industri
      };
    } catch (error) {
      console.log(error);
      return null;
    }
  }
  getCopy(arr, slug){
    let content = "";
    arr.map(item=>{
      if(item.slug === slug) content = item.content
    })
    return content;
  }
  getKekuatan(arr, level) {
    let inovasi = "";
    let komunikasi = "";
    let waktu = "";
    let kesempurnaan = "";
    let levels = level.split(",");
   
    arr.map((item) => {
      
      
      if (item.slug === "inovasi" + levels[0].trim()) inovasi = item.content;
      if (item.slug === "komunikasi" + levels[1].trim()) komunikasi = item.content;
      if (item.slug === "waktu" + levels[2].trim()) waktu = item.content;
      if (item.slug === "kesempurnaan" + levels[3].trim()) kesempurnaan = item.content;
    });
   

    return {
      inovasi,
      komunikasi,
      waktu,
      kesempurnaan
    }
  }
  getNaluri(arr, level) {
    let influencing = "";
    let arranging = "";
    let accumulating = "";
    let integrating = "";
    let levels = level.split(",");
    arr.map((item) => {
      if (item.slug === "influencing" + levels[0]) influencing = item.content;
      if (item.slug === "arranging" + levels[1]) arranging = item.content;
      if (item.slug === "accumulating" + levels[2]) accumulating = item.content;
      if (item.slug === "integrating" + levels[3]) integrating = item.content;
    });

    return {
      influencing,
      arranging,
      accumulating,
      integrating
    }
  }
  /**
   * this will get obseleted soon
   * @param {*} req
   * @param {*} res
   * @param {*} next
   */
  async getPremium(req, res, next) {
    try {
      let session_id = req.meta.session_id;
     
      let exist = await this.select(
        "SELECT * FROM results WHERE session_id = ? LIMIT 1",
        [session_id]
      );
     
      let content = await this.getContentCalculated(exist[0]);
     
      res.send({
        status: 1,
        result: exist[0],
        content: content.content,
      });
    } catch (error) {
      return next(error);
    }
  }
  async getPremiumContent(req, res, next) {
    try {
      let session_id = req.meta.session_id;

      let exist = await this.select(
        "SELECT * FROM results WHERE session_id = ? LIMIT 1",
        [session_id]
      );

      let content = await this.getContentCalculated(exist[0]);

      res.send({
        status: 1,
        result: exist[0],
        content,
      });
    } catch (error) {
      return next(error);
    }
  }
  async getPremiumBySlug(req, res, next) {
    try {
      let session_id = req.meta.session_id;
      let slug = req.params.slug;
     
      let exist = await this.select(
        "SELECT * FROM results WHERE session_id = ? LIMIT 1",
        [session_id]
      );
      //let content = await this.getContent(exist[0]);
      let content = await this.select(
        "SELECT * FROM contents WHERE slug = ? LIMIT 1",
        [slug]
      );
      res.send({
        status: 1,
        result: exist[0],
        content: content[0],
      });
    } catch (error) {
      return next(error);
    }
  }
}

export default Content;
