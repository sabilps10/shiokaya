const config = require("config");
const jwt = require("jsonwebtoken");
const moment = require("moment");
const async = require("async");
const sha256 = require("sha256");
const _ = require("underscore");

module.exports = {
  async guest(pool,req,res,next){
    try{
        let session_id = sha256("--" + moment().unix() + Math.floor(Math.random() * 999));
        var token = jwt.sign(
            {
              sub: 0,
              roles: ['guest'],
              session_id,
              exp: moment()
                .add(24*30, "hour")
                .unix()
            },
            config.get("JWT.secret")
          );
      
         return res.send({
            status: 1,
            user: {
                name: 'guest',
                session_id
            },
            access_token: token
          });
    }catch(error){
        return next(error)
    }
    
  }
  
};