import CRUD from "./db/crud";
export default class Actions extends CRUD {
  /**
   * props:{
   *   tableName: tableName,
   *   pool: connection_pool
   * }
   */
  constructor(props) {
    super(props);
    
    this.fields = [];
    this.props = props;
    this.getAll = this.getAll.bind(this);
    this.get = this.get.bind(this);
    this.add = this.add.bind(this);
    this.update = this.update.bind(this);
    this.delete = this.delete.bind(this);
  }
  async getAll(req, res, next) {
    let {page, per_page, search, orderBy, orderDirection} = typeof req.query !== 'undefined' ? req.query : {
      page:0,
      per_page:5,
      search:''
    }
    const order = {};

    if(typeof page === 'undefined') page = 0;
    if(typeof per_page === 'undefined') per_page = 5;
    if(typeof search === 'undefined') search = null;

    if(typeof orderBy!=='undefined'){
      order[orderBy.field] = orderDirection;
    }
    page = parseInt(page);
    
    let conditions = {...this.formatFilters(req)};
    let searchConditions = {...this.formatLookupFilters(search, req)}
    console.log('searchConditions:',searchConditions, req.crud.searchFilters)
    try {
      let result = await this.paginate({
        page:  page ,
        limit: per_page,
        conditions,
        searchConditions,
        order,
        select: req.crud.select ? req.crud.select : null,
        relationships: typeof req.crud.relationships !== 'undefined' ? req.crud.relationships : [],
        joins: typeof req.crud.joins !== 'undefined' ? req.crud.joins : []
      });
      
      return res.send({
        status: 1,
        data: result.rows,
        page: page,
        total: result.totalResults
      });

    } catch (error) {
      console.log("actions.getAll - ERROR -", error.message)
      next(error);
    }
  }
  formatFilters(req){
    let filters = req.crud.filters;
    let items = {};
    for(let k in filters){
      if(typeof req.query[filters[k]] !== 'undefined')
        items[filters[k]] = req.query[filters[k]]
    }
    return items;
  }
  formatLookupFilters(search, req){
    let filters = req.crud.searchFilters;
    let items = {};
    for(let k in filters){
        items[filters[k]] = search.trim();
    }
    return items;
  }
  async get(req, res, next) {
    const id = req.params.id;
    try {
      let data = await this.findById(id);
      return res.send({
        status: 1,
        data
      });
    } catch (error) {
      next(error);
    }
  }
  async add(req, res, next) {
    try {
      let rs = await this.insert(req.body);
      return res.send({
        status: 1,
        id: rs[0].insertId,
        ...req.body,
        message: "Pertanyaan telah ditambah !"
      });
    } catch (error) {
      next(error);
    }
  }
  async update(req, res, next) {
   
    try {
      const id = req.params.id;
     // console.log('update:',id,req.body);
      let rs = await this.save(id, req.body);
      return res.send({
        status: 1,
        ...req.body,
        message: "Update Sukses"
      });
      
    } catch (error) {
   //   console.log(error);
      next(error);
    }
  }
  async delete(req, res, next) {
    const id = req.params.id;
    try {
      let rs = await this.remove(id);
      return res.send({
        status: 1,
        message: "Data telah berhasil dihapus"
      });
    } catch (error) {
      next(error);
    }
  }
}
