import Actions from "./actions";
import Engine from "./engine";
import moment from "moment";
const _ = require("lodash");
class Result extends Actions {
  constructor(props) {
    super({
      ...props,
      tableName: "results"
    });
    this.engine = new Engine();
    this.submit = this.submit.bind(this);
    this.retrieve = this.retrieve.bind(this);
    this.getContent = this.getContent.bind(this);
    this.validate = this.validate.bind(this);
  }
  async validate(req,res,next){
    try{
      let session_id = req.body.session_id;
      let rs = await this.select("SELECT id FROM accounts where session_id=?",[session_id]);
      let status = 0;
      if(rs.length === 0) return res.send({status:0})
      if(typeof rs[0].id !== 'undefined') status = 1;
      res.send({
        status
      })
    }catch(error){
      return next(error)
    }
  }
  async getContent(result) {
    //calculate the result
    let arr = [
      {
        name: "mechanic",
        score: result.mechanic,
        value: 0
      },
      {
        name: "creator",
        score: result.creator,
        value: 0
      },
      {
        name: "star",
        score: result.star,
        value: 0
      },
      {
        name: "supporter",
        score: result.supporter,
        value: 0
      },
      {
        name: "dealmaker",
        score: result.dealmaker,
        value: 0
      },
      {
        name: "trader",
        score: result.trader,
        value: 0
      },
      {
        name: "accumulator",
        score: result.accumulator,
        value: 0
      },
      {
        name: "lord",
        score: result.lord,
        value: 0
      },
    ];

    let p = 0;
    let n = 0;
    for (let i = 0; i < arr.length; i++) {
      p = i - 1;
      n = i + 1;
      if (p < 0) p = 7;
      if (n > 7) n = 0;
      if(arr[i].score === arr[p].score && arr[i].score === arr[n].score){
        arr[i].value = (parseInt(arr[p].score) + parseInt(arr[i].score) + parseInt(arr[n].score)) / 3;
      }else if(arr[i].score === arr[p].score){
        arr[i].value = (parseInt(arr[p].score) + parseInt(arr[i].score) + parseInt(arr[n].score)) / 3;
      }else if(arr[i].score === arr[n].score){
        arr[i].value = (parseInt(arr[p].score) + parseInt(arr[i].score) + parseInt(arr[n].score)) / 3;
      }else{
        arr[i].value = arr[i].score;
      }
      //arr[i].value = (arr[p].score + arr[i].score + arr[n].score) / 3;
    }
    let rs = _.orderBy(arr, ['value'], ['desc']);
   

    try{
        let content = await this.select("SELECT id,title,summary,photo_url,logo_url,simbol,warna,nama,tagline,karakter,fungsi,color FROM contents WHERE attribute = ?",
                    [rs[0].name])

        return content[0];
    }catch(error){
        console.log(error);
        return null;
    }
    
  }
  async submit(req, res, next) {
    let { questions, progresses } = req.body;

    //let account_id = req.meta.sub;
    let session_id = req.meta.session_id;

    try {
      let exist = await this.select(
        "SELECT * FROM results WHERE session_id = ? LIMIT 1",
        [session_id]
      );

      if (exist.length === 0) {
        let answers = [];
        for (let i = 1; i < progresses.length; i++) {
          answers.push(progresses[i] / 10 - 1);
        }

        let scores = this.engine.results({
          questions,
          answers
        });
        console.log(scores);
        let account = await this.select("SELECT id FROM accounts WHERE session_id = ? LIMIT 1", [session_id])
        let params = {
          account_id: account[0].id,
          session_id,
          mechanic: scores[0],
          creator: scores[1],
          star: scores[2],
          lord: scores[3],
          supporter: scores[5],
          accumulator: scores[6],
          trader: scores[7],
          dealmaker: scores[8],
          created_at: moment().format("YYYY-MM-DD HH:mm:ss")
        };

        console.log(params);
        
        let rs = await this.insert(params);
        exist = await this.select(
          "SELECT * FROM results WHERE session_id = ? LIMIT 1",
          [session_id]
        );
        
      }
      let content = await this.getContent(exist[0]);
        res.send({
          status: 1,
          result: exist[0],
          content,
        });
    } catch (error) {
      console.log('submit-error', error.message);
      next(error);
    }
  }

  async retrieve(req,res,next){
    //let account_id = req.meta.sub;
    let session_id = req.meta.session_id;
    try {
      let exist = await this.select(
        "SELECT * FROM results WHERE session_id = ? LIMIT 1",
        [session_id]
      );
      console.log(session_id,exist.length);
      if(exist.length === 0) return res.send({status:0});

      let content = await this.getContent(exist[0]);
        res.send({
          status: 1,
          result: exist[0],
          content
        });
    } catch (error) {
      next(error);
    }
  }
}

export default Result;
