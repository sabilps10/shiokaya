const config = require('config')
const moment = require('moment-timezone')
const sha1 = require('sha1')
const mysql = require("mysql2/promise");
const nodemailer = require('nodemailer');
const fs = require('fs');
const jwt = require("jsonwebtoken");
let transport = nodemailer.createTransport({
    host: config.get("nodemailer.host"),
    port: config.get("nodemailer.port"),
    auth: {
        user: config.get("nodemailer.user"),
        pass: config.get("nodemailer.pass"),
    }
});

module.exports = {
    async register(pool, req, res, next) {
        try {
          
            let email = req.body.email;
            let name = req.body.name;
            let phone = req.body.phone;
            let session_id = req.body.session_id;
            let secret = sha1(moment().unix());
            var token = jwt.sign(
                {
                  email,
                  name,
                  phone,
                  session_id,
                  exp: moment()
                    .add(1, "days")
                    .unix()
                },
                config.get("JWT.secret")
              );

            const confirm_url = config.get("landing_url") + "/confirm?token=" + token; 

          
            let checkEmail = await pool.query(`select count(*) as jml from accounts \ 
                                                 where email like "%${email}%" limit 1`);
           
            if (checkEmail[0][0].jml === 0) {
                let ins = await pool.query("INSERT INTO accounts(name,email,phone,session_id,secret,created_at) VALUES(?,?,?,?,?,NOW());",
                [
                    name,
                    email,
                    phone,
                    session_id,
                    secret
                ])
            }

            
            
            let template = fs.readFileSync("./templates/email/register_success.html", "utf8");
            template = template.replace("{{date}}", moment().tz("UTC").format("DD MMMM, YYYY"));
            template = template.replace("{{name}}", name);
            template = template.replace("{{confirm_url}}",confirm_url);


            let mailOptions = {
                from: config.get("email"),
                to: email,
                subject: "Silahkan bayar Akses Premium Profil Shiokaya Anda.",
                html: template
            }
            

            /*
           let template = fs.readFileSync("./templates/email/payment_closed.html", "utf8");
            template = template.replace("{{date}}", moment().tz("UTC").format("DD MMMM, YYYY"));
            template = template.replace("{{name}}", name);
            template = template.replace("{{confirm_url}}",confirm_url);


            let mailOptions = {
                from: config.get("email"),
                to: email,
                subject: "Akses Shiokaya Premium",
                html: template
            }
            */

        //kalau sudah konek ke payment gateway, ga perlu kirim email bayar
           //let send = await transport.sendMail(mailOptions);
            res.send({ status: 1 });
        } catch (error) {
            next(error);
        }
    },
    async submit(pool, req, res, next) {
        try {
          
            let email = req.body.email;
            let name = req.body.name;
            let phone = req.body.phone;
            let session_id = req.body.session_id;

            let checkEmail = await pool.query(`select count(*) as jml from accounts \ 
                                                 where email like "%${email}%" limit 1`);

            let secret = sha1(moment().unix());
          
            //if (checkEmail[0][0].jml === 0) {
               let ins = await pool.query("INSERT INTO accounts(name,email,phone,session_id,secret,created_at) VALUES(?,?,?,?,?,NOW()) ON DUPLICATE KEY UPDATE session_id = VALUES(session_id);",
                [
                    name,
                    email,
                    phone,
                    session_id,
                    secret
                ])
           // }

            res.send({ status: 1 });

        } catch (error) {
            next(error);
        }
    }
};