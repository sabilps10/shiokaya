import Actions from "./actions";
import { timesSeries } from "async";
import AccountRole from "./account_role";
const axios = require("axios");
const config = require("config");
const moment = require("moment");
const sha1 = require("sha1");

class Account extends Actions {
  constructor(props) {
    super({
      ...props,
      tableName: "accounts",
    });
    this.accountRole = new AccountRole(props);
    this.createAccount = this.createAccount.bind(this);
    this.lookup = this.lookup.bind(this);
    this.addAccount = this.addAccount.bind(this);
    this.updateAccount = this.updateAccount.bind(this);
    //this.hapusAccount = this.hapusAccount.bind(this);
  }
  async lookup(req, res, next) {
    console.log('lookup');
    try {
      let {
        filters, orderDirection, page, pageSize, search, totalCount, per_page
      } = req.query;
      let where = [];
      let whereValues = [];
      let whereOrs = [];
      
      let start = page * per_page;

      if(search.length> 0){
        whereOrs = [
          "a.name LIKE '%"+search+"%'",
          "a.email LIKE '%"+search+"%'",
          "a.phone LIKE '%"+search+"%'",
         
          
        ];
        //whereValues.push("%" +search +"%");
      }
      let whereOrsString = "";
      if(whereOrs.length > 0){
        whereOrsString = (where.length > 0 ? " AND " : "WHERE ") + whereOrs.join(" OR ");
      }
      
      let sql = "SELECT a.name,a.email,a.phone,b.role_id, c.name as role_name FROM accounts a INNER JOIN account_roles b \
      ON a.id = b.account_id INNER JOIN roles c ON c.id = b.role_id \
      " + (where.length > 0 ? (" WHERE " +
      where.join(" AND ")) : "") + whereOrsString 
      
        " LIMIT " + parseInt(start) + "," + parseInt(per_page) + ";";

      let sql2 = "SELECT COUNT(a.id) as total FROM accounts a INNER JOIN account_roles b \
      ON a.id = b.account_id INNER JOIN roles c ON c.id = b.role_id \
      " + (where.length > 0 ? (" WHERE " +
      where.join(" AND ")) : "") + whereOrsString + ";";
       
    
      let rows = await this.select(
        sql,
        whereValues
      );

      let total = await this.select(sql2, whereValues);

      let results = rows.map( async (item)=>{
        let role = await this.select("SELECT id,name FROM roles WHERE id = ? LIMIT 1", [
          item.role_id
        ])
        item.role = role[0];  
        return item;
      })
      
      let data = await Promise.all(results);
      
      return res.send({
        status: 1,
        data,
        page: parseInt(page),
        total: total[0].total
      });
    } catch (error) {
      console.log('account.lookup:', error.message);
      return next(error);
    }
  }
  async addAccount(req,res,next){
    try{
      let {name,email,phone,password,role} = req.body;
      let rs = await this.createAccount({
        name,email,phone,password
      }, role);
      return res.send({
        status:1
      })
    }catch(error){
      return next(error);
    }
  }
  async updateAccount(req,res,next){
    try{
      let id = req.params.id;
      let {name,email,phone,password,role} = req.body;
      let rs = await this.createAccount({
        name,email,phone,password
      }, role);
      return res.send({
        status:1
      })
    }catch(error){
      return next(error);
    }
  }
  
  async createAccount(input, role) {
    try {
      let { name, email, password, phone, session_id } = input;
      console.log(input, role)
      if (name === null) throw new Error("invalid request");
      if (email === null) throw new Error("invalid request");
      if (password === null) throw new Error("invalid request");
      if (phone === null) throw new Error("invalid request");
      if (role === null) throw new Error("invalid request");
      if (email.length < 5) throw new Error("invalid request");
      if (password.length < 5) throw new Error("invalid request");

      let secret = sha1(moment().unix());
      let hash = sha1(`${password}${secret}`);
      let _role = await this.selectOne("SELECT id FROM roles WHERE name = ?", [
        role,
      ]);
      console.log(_role);
      let role_id = _role.id;
      //check if the email is already exist
      let check = await this.selectOne(
        "SELECT email FROM accounts WHERE email=? LIMIT 1",
        [email]
      );
      console.log(check);
      if (check !== null)
        throw new Error("Sorry the email is already registered!");

      let account_id = await this.insert(
        {
          name,
          email,
          password: hash,
          phone,
          session_id,
          secret,
        },
        [],
        "created_at"
      );
      console.log(account_id);
      if(account_id > 0){
        //add role
        let insRole = this.accountRole.insert({
            account_id,
            role_id
        })  
        
        return account_id;
      }
      return 0;
    } catch (error) {
        console.log(moment() + " - account.createAccount: ", error.message);
      return 0;
    }
  }
}

export default Account;
