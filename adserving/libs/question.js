import Actions from "./actions";

class Question extends Actions {
  constructor(props) {
    super({
      ...props,
      tableName: "questions"
    });
   
    this.getAll = this.getAll.bind(this);
    this.getAllQuestions = this.getAllQuestions.bind(this);
  }
  async getAll(req, res, next) {
    let { group_id } = req.query;
    if(typeof group_id === 'undefined') return this.getAllQuestions(req,res,next);
    group_id = parseInt(group_id);
    try {
      let data = await this.findAll({
        where: {
          group_id: group_id
        }
      });
      return res.send({
        status: 1,
        data
      });
      
    } catch (error) {
      next(error);
    }
  }
  async getAllQuestions(req,res,next){
    let {page, per_page, search} = req.query
    page = page !== null ? parseInt(page) : 0
    per_page = per_page !== null ? parseInt(per_page) : 0
    search = search !== null ? search : ""
    try {
      let result = await this.paginate({
        page :page,
        limit: per_page,
        conditions: {} //todo
      });
      let p = await result.rows.map(async (rs)=>{
      
        let groups = await this.select("SELECT * from question_groups where id = ? LIMIT 1", [rs.group_id]);
        rs.group = groups[0];
       
        return rs;
      })
    
      let rows = await Promise.all(p);
     
      result.rows = rows;
      return res.send({
        status: 1,
        data: result.rows,
        page: page,
        total: result.totalResults
      });
      
    } catch (error) {
      next(error);
    }
  }
 
}


export default Question;
