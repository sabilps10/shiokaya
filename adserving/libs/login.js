const config = require("config");
const jwt = require("jsonwebtoken");
const moment = require("moment");
const async = require("async");
const sha1 = require("sha1");
const _ = require("underscore");

module.exports = {
  
  async login(pool, req, res, next) {
   
      try {
          let [rows,fields] = await pool.execute("SELECT * FROM accounts WHERE email=? LIMIT 1",
                                                            [req.body.email])
          if(rows==null){
            next(new Error("account not found"));
                    return;
          }
          let user = rows[0];
          let hash = sha1(req.body.password + user.secret);

          if (hash.localeCompare(user.password) !== 0){
           
            return next(new Error("wrong email and/or password !"));
          }
          let [rows2,fields2] = await pool.execute( "SELECT b.name FROM account_roles a \
                                INNER JOIN roles b ON a.role_id = b.id \
                                WHERE account_id = ?",
                                [user.id])
          user.roles = [];
          if(rows2.length === 0) {
              user.roles=['user']
          }else{
              
              rows2.map(item=>{
                user.roles.push(
                    item.name
                );
            });
              
            
          }

          var token = jwt.sign(
            {
              sub: user.id,
              roles: user.roles,
              session_id: user.session_id,
              exp: moment()
                .add(1, "days")
                .unix()
            },
            config.get("JWT.secret")
          );

         

          res.send({
            status: 1,
            user: {
                name: user.name,
                session_id: user.session_id,
            },
            roles: user.roles,
            access_token: token
          });
          
       
      } catch (err) {
        console.log("error", err.message);
      
        next(err);
      }
   
  }
};