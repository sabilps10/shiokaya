import Actions from "./actions";

class Pretests extends Actions {
  constructor(props) {
    super({
      ...props,
      tableName: "pretests"
    });
    this.getData = this.getData.bind(this);
    this.getContent = this.getContent.bind(this);
  }
  async getData(req,res,next){
    try{
      let data = await this.findAll();
     
      return res.send({
        status:1,
        data
      })
    }catch(error){
      return next(error);
    }
  }
  async getContent(req,res,next){
    try{
      let {ids} = req.body;
      for(let k in ids){
        ids[k] += 1;
      }
      let data = await this.select("SELECT id,slug,title,brief1,brief2,brief3,photo_url,simbol,nama,logo_url FROM contents WHERE id IN ("+ids.join(",")+")",[]);
      
      return res.send({
        status:1,
        data
      })
    }catch(error){
      return next(error);
    }
  }
}


export default Pretests;
