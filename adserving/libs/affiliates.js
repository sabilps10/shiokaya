import Actions from "./actions";
import Account from "./account";

const axios = require("axios");
const config = require("config");
const moment = require("moment");
const sha1 = require("sha1");
axios.defaults.headers.post["Content-Type"] = "application/json";
const payment_endpoint = config.get("payment.endpoint");

const nodemailer = require("nodemailer");
const fs = require("fs");

let transport = nodemailer.createTransport({
  host: config.get("nodemailer.host"),
  port: config.get("nodemailer.port"),
  auth: {
    user: config.get("nodemailer.user"),
    pass: config.get("nodemailer.pass"),
  },
});

class Affiliates extends Actions {
  constructor(props) {
    super({
      ...props,
      tableName: "affiliates",
    });
    this.account = new Account(props);
    this.register = this.register.bind(this);
    this.sendEmail = this.sendEmail.bind(this);
  }
  async register(req, res, next) {
    try {
      let {
        name,
        email,
        password,
        phone,
        address,
        company,
        industry,
      } = req.body;

      //create account
      let account_id = await this.account.createAccount(
        {
          name,
          email,
          password,
          phone,
        },
        "affiliate"
      );

      if (account_id === 0)
        throw new Error("cannot create account, please try again later !");

      let affiliate_no = `A-${account_id}-${moment().format("YYYYMMDD")}`;

      //insert affiliates profile
     
      let insertId = await this.insert(
        {
          name,
          email,
          phone,
          company,
          address,
          industry,
          affiliate_no,
          account_id,
        },
        [],
        "created_at"
      );

      if (insertId > 0) {
        //send Email
        let sent = await this.sendEmail({
          name,
          email,
          affiliate_no
        })
        return res.send({ status: 1 });
      }
      else return res.send({ status: 0 });
    } catch (error) {
      return next("Affiliate:", error);
    }
  }
  async sendEmail(props){
    const {name, email, affiliate_no} = props;

    let template = fs.readFileSync(
      "./templates/email/affiliate_register.html",
      "utf8"
    );
    
    template = template.replace(
      "{{date}}",
      moment()
        .tz("UTC")
        .format("DD MMMM, YYYY")
    );
    template = template.replace("{{name}}", name);
    template = template.replace("{{affiliate_no}}", affiliate_no);
    template = template.replace("{{email}}", email);

    let mailOptions = {
      from: config.get("email"),
      to: email,
      subject: "Welcome to Shiokaya Affiliation Programme",
      html: template
    };

    let send = await transport.sendMail(mailOptions);
    return send;
  }
  
}

export default Affiliates;
