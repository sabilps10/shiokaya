/**
 * matrix
 */
const _ = require('lodash');
const matrix = ['A1','A2','A3',
				'B1','B2','B3',
				'C1','C2','C3'];

const question_groups = [
	{
		id:1,
		questions:[
			{
				question:"Question 1.1",
				answer_type: 'leftright',
				answers:[
					'Tidak Peduli',
					'Peduli'
				],
				slots:['A1','A2','A3']
			},
			{
				question:"Question 1.2",
				answer_type: 'leftright',
				answers:[
					'Tidak Peduli',
					'Peduli'
				],
				slots:['A3','B3','C3']
			},
			{
				question:"Question 1.3",
				answer_type: 'leftright',
				answers:[
					'Tidak Peduli',
					'Peduli'
				],
				slots:['A1','B1','C1']
			}
			
		],
		
	},
	{
		id:2,
		questions:[
			{
				question:"Question 2.1",
				answer_type: 'leftright',
				answers:[
					'Tidak Peduli',
					'Peduli'
				],
				slots:['A1','A2','A3']
			},
			{
				question:"Question 2.2",
				answer_type: 'leftright',
				answers:[
					'Tidak Peduli',
					'Peduli'
				],
				slots:['C1','C2','C3']
			},
			{
				question:"Question 2.3",
				answer_type: 'leftright',
				answers:[
					'Tidak Peduli',
					'Peduli'
				],
				slots:['B1','B2','B3']
			}
			
		],
		
	},
	{
		id:3,
		questions:[
			{
				question:"Question 3.1",
				answer_type: 'leftright',
				answers:[
					'Tidak Peduli',
					'Peduli'
				],
				slots:['A1','A2','A3']
			},
			{
				question:"Question 3.2",
				answer_type: 'leftright',
				answers:[
					'Tidak Peduli',
					'Peduli'
				],
				slots:['A1','B2','A3']
			},
			{
				question:"Question 3.3",
				answer_type: 'leftright',
				answers:[
					'Tidak Peduli',
					'Peduli'
				],
				slots:['A1','B1','C1']
			}
			
		],
		
	},
	{
		id:1,
		questions:[
			{
				question:"Question 1.1",
				answer_type: 'leftright',
				answers:[
					'Tidak Peduli',
					'Peduli'
				],
				slots:['A1','A2','A3']
			},
			{
				question:"Question 1.2",
				answer_type: 'leftright',
				answers:[
					'Tidak Peduli',
					'Peduli'
				],
				slots:['A3','B3','C3']
			},
			{
				question:"Question 1.3",
				answer_type: 'leftright',
				answers:[
					'Tidak Peduli',
					'Peduli'
				],
				slots:['A1','B1','C1']
			}
			
		],
		
	},
	{
		id:2,
		questions:[
			{
				question:"Question 2.1",
				answer_type: 'leftright',
				answers:[
					'Tidak Peduli',
					'Peduli'
				],
				slots:['A1','A2','A3']
			},
			{
				question:"Question 2.2",
				answer_type: 'leftright',
				answers:[
					'Tidak Peduli',
					'Peduli'
				],
				slots:['C1','C2','C3']
			},
			{
				question:"Question 2.3",
				answer_type: 'leftright',
				answers:[
					'Tidak Peduli',
					'Peduli'
				],
				slots:['B1','B2','B3']
			}
			
		],
		
	},
	{
		id:3,
		questions:[
			{
				question:"Question 3.1",
				answer_type: 'leftright',
				answers:[
					'Tidak Peduli',
					'Peduli'
				],
				slots:['A1','A2','A3']
			},
			{
				question:"Question 3.2",
				answer_type: 'leftright',
				answers:[
					'Tidak Peduli',
					'Peduli'
				],
				slots:['A1','B2','A3']
			},
			{
				question:"Question 3.3",
				answer_type: 'leftright',
				answers:[
					'Tidak Peduli',
					'Peduli'
				],
				slots:['A1','B1','C1']
			}
			
		],
		
	},
	
];

const question = (data) => {
	let q = _.shuffle(data)[0];
	return q;
}

const shuffled = [];
const displayQuestions = () => {
	let n = 1;
	question_groups.map((item)=>{
		let q = question(item.questions);
		shuffled.push(q);
		console.log(n + "." + q.question);
		console.log(q.answers[0]+" * * * * * * "+q.answers[1]);
		console.log(q.slots);
		n++;
	})
}
const findSlot = (slot)=>{
	for(let i=0; i < matrix.length; i++ ){
		if(matrix[i] === slot) return i;
	}
}
const simulate = () => {
	let n = 1;
	let results = [0,0,0,0,0,0,0,0,0];
	
	shuffled.map((q)=>{
		
		let point = 0;
		let roll = Math.floor( Math.random() * 7 );
		
		try{
			
			let rewarded_item = 0;
			
			if(roll < 2) reward_item = 0;
			if(roll >= 2 && roll < 5){
				reward_item = 1;
			}
			if(roll >= 5){
				reward_item = 2;
			}
			console.log('reward_item:',reward_item);
			switch(roll){
				case 0:
					point = 10;
				break;
				case 1:
					point = 5;
				break;
				case 2:
					point = 5;
				break;
				case 3:
					point = 10;
				break;
				case 4:
					point = 5;
				break;
				case 5:
					point = 5;
				break;
				case 6:
					point = 10;
				break;
			}
			let tt = q.slots[reward_item]
			results[findSlot(tt)] += point;
			console.log(n+". Nilai:",roll, " ---> item: "+tt, 'Points:'+point);
			n++;
		}catch(err){
			console.log("ERROR: roll: " + roll, q);
		}
		
	})
	//console.log(results);
	console.log(results[0]+'|'+results[1]+'|'+ results[2]);
	console.log(results[3]+'|'+results[4]+'|'+ results[5]);
	console.log(results[6]+'|'+results[7]+'|'+ results[8]);
}
console.log("Questions: ")
displayQuestions();

console.log("Simulation Answers: ");
simulate();
