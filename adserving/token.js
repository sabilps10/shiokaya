const config = require('config')
const jwt = require('jsonwebtoken')
const moment = require('moment')
const async = require('async')
const sha1 = require('sha1')
const _ = require('underscore')

module.exports = {
    verify(req, res, next){
       let token = req.query.token
       var decoded = jwt.verify(token, config.get("JWT.secret"));
       if(decoded && typeof decoded.sub !== 'undefined'){
            res.send({status:1,payload:decoded});
       }else{
            res.send({status:0});
       }


    }
};
