CREATE TABLE `shiokaya`.`daily_sales` (
  `id` BIGINT(21) NOT NULL AUTO_INCREMENT,
  `tanggal` DATETIME NULL,
  `affiliate_id` BIGINT(21) NULL,
  `total` BIGINT(21) NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UNIK` (`tanggal` ASC));

  CREATE TABLE `shiokaya`.`daily_pending_transactions` (
  `id` BIGINT(21) NOT NULL AUTO_INCREMENT,
  `tanggal` DATETIME NULL,
  `affiliate_id` BIGINT(21) NULL,
  `total` BIGINT(21) NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UNIK` (`tanggal` ASC));

ALTER TABLE `shiokaya`.`daily_pending_transactions` 
DROP INDEX `UNIK` ,
ADD UNIQUE INDEX `UNIK` (`tanggal` ASC, `affiliate_id` ASC);

ALTER TABLE `shiokaya`.`daily_sales` 
DROP INDEX `UNIK` ,
ADD UNIQUE INDEX `UNIK` (`tanggal` ASC, `affiliate_id` ASC);


-- 2021-01-13 16:56:18.3280
ALTER TABLE `shiokaya`.`affiliate_summary` ADD COLUMN `revenue` decimal(14,3) NULL DEFAULT '0' COMMENT '';
ALTER TABLE `shiokaya`.`affiliate_summary` ADD COLUMN `withdrawn` decimal(14,3) NULL DEFAULT '0' COMMENT '';
ALTER TABLE `shiokaya`.`affiliate_summary` ADD COLUMN `saldo` decimal(14,3) NULL DEFAULT '0' COMMENT '';

CREATE TABLE `affiliate_withdrawals` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `affiliate_id` bigint(21) DEFAULT NULL,
  `amount` decimal(14,3) DEFAULT '0.000',
  `withdraw_status` int(3) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


CREATE TABLE `affiliate_revenues` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `affiliate_id` bigint(21) DEFAULT '0',
  `transaction_id` bigint(21) DEFAULT '0',
  `amount` decimal(14,3) DEFAULT '0.000',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `unik` (`affiliate_id`,`transaction_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


CREATE TABLE `affiliates_stats` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `affiliate_id` bigint(21) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `clicks` int(11) DEFAULT '0',
  `buys` int(11) DEFAULT '0',
  `impressions` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `uniq` (`affiliate_id`,`tanggal`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

ALTER TABLE `shiokaya`.`affiliates` ADD COLUMN `commisions` decimal(5,2) NULL DEFAULT '0.4' COMMENT '';
ALTER TABLE `shiokaya`.`affiliates` CHANGE `commisions` `commisions` decimal(5,2) NULL DEFAULT '0.3' COMMENT '';
ALTER TABLE `shiokaya`.`affiliates` ADD COLUMN `deskripsi` longtext NULL COMMENT '';
ALTER TABLE `shiokaya`.`affiliates` ADD COLUMN `photo_url` varchar(140) NULL COMMENT '';
CREATE TABLE `shiokaya`.`affiliate_photos` (
  `id` BIGINT(21) NOT NULL AUTO_INCREMENT,
  `affiliate_id` BIGINT(21) NULL,
  `photo_url` VARCHAR(255) NULL,
  `hp_url` VARCHAR(255) NULL,
  `thumb_url` VARCHAR(255) NULL,
  `created_at` DATETIME NULL,
  `updated_at` DATETIME NULL,
  PRIMARY KEY (`id`));


ALTER TABLE `shiokaya`.`affiliates` ADD COLUMN `link` varchar(140) NULL COMMENT '';
CREATE INDEX `affiliate_id` ON `shiokaya`.`transactions` (`affiliate_id`) USING BTREE;
ALTER TABLE `shiokaya`.`affiliate_revenues` CHANGE `transaction_id` `transaction_id` varchar(200) NULL DEFAULT '0' COMMENT '';