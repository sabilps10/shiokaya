**DASHBOARD ENGINE**


***Modules Example***

```
const modules = [
  {
    name: "Dashboard",
    slug: "dashboard",
    crud: false,
    component: DashboardContainer
  },
  {
    name: "Questions",
    slug: "questions",
    singularName: "question",
    crud: true,
    endpoint: "/question",
    updateLabel: 'question',
    fields: [
      {
        label: "Pertanyaan Nomor:",
        name:"group_id",
        type: "lookup",
        resource: "question-groups",
        key:"id",
        resource_label:"no"
      },
      {
        label: "Question",
        name: "question",
        type: "text"
      },
      {
        label:"Answer 1",
        name: "answer_1",
        type: "text"
      },
      {
        label:"Answer 2",
        name: "answer_2",
        type: "text"
      },
      {
        label:"Answer 3",
        name: "answer_3",
        type: "text"
      },
      {
        label:"Slot 1",
        name: "slot_1",
        type: "lookup",
        values: ["C1", "C2", "C3", "R1", "R2", "R3", "An", "A", "V", "K"]
      },
      {
        label:"Slot 2",
        name: "slot_2",
        type: "lookup",
        values: ["C1", "C2", "C3", "R1", "R2", "R3", "An", "A", "V", "K"]
      },
      {
        label:"Slot 3",
        name: "slot_3",
        type: "lookup",
        values: ["C1", "C2", "C3", "R1", "R2", "R3", "An", "A", "V", "K"]
      }
    ]
  },
  {
    name: "Question Groups",
    slug: "question-groups",
    singularName: "group",
    crud: true,
    endpoint: "/question-groups",
    updateLabel: 'no',
    fields: [
      {
        label:"Answer Type",
        name: "answer_type",
        type: "lookup",
        values: ['left-right','top-down','AnA','VK']
      },
      {
        label:"Question No.",
        name: "no",
        type: "number"
      },
      
    ]
  },
  {
    
    name: "Contents",
    slug: "contents",
    singularName: "content",
    crud: true,
    endpoint: "/contents",
    updateLabel: 'title',
    fields: [
      {
        label:"Slug",
        name: "slug",
        type: "text",
       
      },
      {
        label:"Title",
        name: "title",
        type: "text"
      },
      {
        label:"Text",
        name: "content_text",
        type: "richtext"
      },
     
      {
        label:"Photo",
        name: "photo_url",
        type: "image"
      },
      {
        label:"Attribute",
        name: "attribute",
        type: "text"
      },
      
    ]
  },
  {
    name: "Settings",
    slug: "settings",
    singularName: "setting",
    crud: false,
    component: SettingsContainer
  },
  {
    name: "Logout",
    slug: "logout",
    crud: false,
    component: DashboardContainer
  }
];
```