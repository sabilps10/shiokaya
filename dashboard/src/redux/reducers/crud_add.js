
let INITIAL_STATE = {
    data:null,
    payload:null,
    fetching:false,
    error:false,
    message:"",
}
export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case "CRUD_ADD":
      return {
        data: action.data,
        fetching:true,
    };
    case "CRUD_ADD_SUCCESS":
      return {
        payload: action.payload,
        fetching:false
    };
    case "CRUD_ADD_ERROR":
      return {
        data:null,
        error: true,
        fetching:false,
        payload:null,
        message: action.message
    };
    default:
      return state;
  }
};
