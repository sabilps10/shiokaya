import Api from "../../Services/Api";


const api = new Api();

export default (service, data) => dispatch => {
    
    dispatch({
        type: 'CRUD',
        data: data,
        payload:null
    })
    
    api.crud({
        endpoint: service.endpoint,
        actionType:"LIST",
        data
    }).then((response)=>{
        
        if(typeof response.data !== 'undefined'){
            dispatch({
                type:"CRUD_SUCCESS",
                payload: response
            })
        }else{
            dispatch({
                type:"CRUD_ERROR",
                payload:null,
            })
        }
    })
}