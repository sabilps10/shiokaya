import Api from "../../Services/Api";


const api = new Api();

export default (opts) => dispatch => {
    
    dispatch({
        type: 'CRUD_GET',
        data: opts.data,
        payload:null
    })
    
    api.crud(opts).then((response)=>{
        
        if(typeof response.data !== 'undefined'){
            dispatch({
                type:"CRUD_GET_SUCCESS",
                payload: response
            })
        }else{
            dispatch({
                type:"CRUD_GET_ERROR",
                payload:null,
            })
        }
    })
}