import React, { useState } from "react";
import { Grid } from "@material-ui/core";
import {
  Radar,
  RadarChart,
  PolarAngleAxis,
  PolarGrid,
  PolarRadiusAxis,
  Legend
} from "recharts";

export default function QuizResult(props) {
  const {data, content} = props
  if(data === null) return (<div>Loading...</div>)
  return (
    <div style={{ marginTop: 100 }}>
      <h1
        style={{
          textAlign: "center"
        }}
      >
        RESULTS
      </h1>
      <Grid container>
        <Grid item xs={12} sm={4}>
          <Grid item xs={12}>
            <div style={{ margin: "0 auto", textAlign: "center" }}>
              {data.length > 0 ? (<RadarChart
                key={Math.random()*9999}
                outerRadius={90}
                width={300}
                height={250}
                data={data}
                style={{ margin: "0 auto", textAlign: "center" }}
              >
                <PolarGrid />
                <PolarAngleAxis dataKey="subject" />
                <PolarRadiusAxis angle={30} domain={[0, props.max]} />
                <Radar
                  name="Profile"
                  dataKey="score"
                  stroke="#8884d8"
                  fill="#8884d8"
                  fillOpacity={0.6}
                />
              </RadarChart>) : (null)}
              
            </div>
          </Grid>
          {content !== null ? (<Grid item xs={12}>
            <h4 style={{ textAlign: "center" }}>
              Anda adalah seorang `{content.title}`
            </h4>
            <div style={{ textAlign: "center" }}>
              <img
                src={content.photo_url}
                style={{
                  maxHeight: 350
                }}
              />
            </div>
          </Grid>) : (null)}
          
        </Grid>
        {content !== null ? (<Grid item xs={12} sm={8}>
          <div
            style={{
              background: "#fff",
              padding: 15
            }}
          >
            <h3>{content.title}</h3>
            {content.content_text}
          </div>
        </Grid>) : (null)}
        
      </Grid>
    </div>
  );
}
