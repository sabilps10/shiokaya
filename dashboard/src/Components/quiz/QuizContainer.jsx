import React from "react";
import { Grid } from "@material-ui/core";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import QuizContainerStyles from "./QuizContainerStyles";
import Slider from "@material-ui/core/Slider";
import Button from "@material-ui/core/Button";
import QuizResult from "./QuizResult";
import QuestionAction from "../../redux/actions/QuestionAction";
import ResultAction from "../../redux/actions/ResultAction";
const _ = require("lodash");

const defaults = [
  {
    subject: "Wizard",
    score: 0,
    fullMark: 150,
    name: "creator"
  },
  {
    subject: "Bard",
    score: 0,
    fullMark: 150,
    name: "star"
  },
  {
    subject: "Priest",
    score: 0,
    fullMark: 150,
    name: "supporter"
  },
  {
    subject: "Knight",
    score: 0,
    fullMark: 150,
    name: "dealmaker"
  },
  {
    subject: "Merchant",
    score: 0,
    fullMark: 150,
    name: "trader"
  },
  {
    subject: "Oracle",
    score: 0,
    fullMark: 150,
    name: "accumulator"
  },
  {
    subject: "Warlock",
    score: 0,
    fullMark: 150,
    name: "lord"
  },
  {
    subject: "Alchemist",
    score: 0,
    fullMark: 150,
    name: "mechanic"
  }
];
class QuizContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      page: 0,
      clusters: [],
      finished: false,
      questions: [],
      progresses: [],
      result_data: [],
      chartkey: Math.random() * 99999,
      max: 0,
    };

    this.createCluster = this.createCluster.bind(this);
    this.handleNext = this.handleNext.bind(this);
    this.saveProgresses = this.saveProgresses.bind(this);
    this.loadQuestions = this.loadQuestions.bind(this);
    this.initClusters = this.initClusters.bind(this);
    this.populateResult = this.populateResult.bind(this);
  }

  componentDidMount() {
    const { questionAction } = this.props;

    //current page
    let current_page = localStorage.getItem("page");
    if (current_page === null) localStorage.setItem("page", 0);

    //current questions
    let questions = localStorage.getItem("questions");
    if (questions === null) questionAction();
    //load questions
    else{
      this.loadQuestions(JSON.parse(questions));
      this.setState({
        page: current_page,
        questions: JSON.parse(questions)
      });
    }
      

    //current progress
    //when the user accidentally refresh the browser, we can still reload the progress somehow.
    let progress = localStorage.getItem("progress");
    if (progress === null) localStorage.setItem("progress", JSON.stringify([]));
    else
      this.setState({
        progresses: JSON.parse(progress)
      });
  }
  componentDidUpdate(_props, _states) {
    const { questions, resultAction, result } = this.props;
    const { finished, page, clusters } = this.state;
    if (_props.questions.payload !== questions.payload) {
      if (typeof questions.payload !== "undefined") {
        if (questions.payload !== null)
          this.loadQuestions(questions.payload.data);
      }
    }
    if (_props.result !== result) {
      if (typeof result !== "undefined") {
        if (typeof result.payload !== "undefined" && result.payload !== null) {
          let rs = result.payload.result;
          this.populateResult(rs);
        }
      }
    }
    if (this.state.questions !== _states.questions) {
      if (this.state.questions !== null) this.initClusters();
    }

    if (
      page !== _states.page &&
      _states.clusters.length > 0 &&
      _states.finished === false
    ) {
      if (parseInt(page) >= clusters.length) {
        this.setState({
          finished: true
        });
      }
    }

    if (finished !== _states.finished && finished === true) {
      resultAction({
        questions: this.state.questions,
        progresses: this.state.progresses
      });
    }
  }
  populateResult(data) {
    let fullscore = 0;
    for (var k in data) {
      if (k !== "created_at" && k !== "account_id")
        fullscore = data[k] > fullscore ? data[k] : fullscore;
    }
    let d = [];
    for (let i = 0; i < defaults.length; i++) {
      d[i] = defaults[i];
      d[i].score = data[defaults[i].name];
      d[i].fullMark = fullscore;
      console.log(d[i].score, d[i].fullMark);
    }

    this.setState({
      result_data: d,
      max: fullscore,

    });
  }
  initClusters() {
    const { questions, page } = this.state;
    let clusters = this.createCluster();
    let progresses = [0];
    for (let i = 0; i < questions.length; i++) {
      progresses.push(10);
    }
    let progress = localStorage.getItem("progress");
    if (progress !== null) progresses = JSON.parse(progress);
    let finished = false;
    if (parseInt(page) >= clusters.length && clusters.length > 0) {
      finished = true;
    }
    this.setState({
      clusters,
      progresses,
      finished
    });
  }
  loadQuestions(questions) {
    localStorage.setItem("questions", JSON.stringify(questions));
    let progress = localStorage.getItem("progress");
    if(progress === null) progress = [0];
    else progress = JSON.parse(progress);
    for(let i=1; i<=questions.length; i++){
      if(typeof progress[i] === 'undefined')  progress[i] = 10;
     
    }
    console.log(progress);
    localStorage.setItem("progress",JSON.stringify(progress));
    this.setState({
      questions,
      progresses:progress
    });
  }
  createCluster() {
    const { questions } = this.state;
    let clusters = [];
    let n_clusters = Math.ceil(questions.length / 4);
    let n = 0;
    for (let i = 0; i < n_clusters; i++) {
      let items = [];
      for (let j = 0; j < 4; j++) {
        let idx = n * 4 + j;
        if (typeof questions[idx] !== "undefined") {
          questions[idx].no = idx + 1;
          items.push(questions[idx]);
        }
      }
      clusters.push(items);
      n++;
    }
    return clusters;
  }
  handleNext() {
    const { resultAction } = this.props;
    const { page, clusters } = this.state;
    //save progresses
    this.saveProgresses();

    //proceed next page
    let nextPage = parseInt(page) + 1;

    if (nextPage >= clusters.length) {
      resultAction();
      this.setState({ finished: true });
    } else {
      this.setState({
        page: nextPage
      });
    }
    localStorage.setItem("page", nextPage);
  }
  saveProgresses() {
    const { progresses,questions } = this.state;
    for(let i=1; i<questions.length; i++){
      if(progresses[i]===null) progresses[i] = 10;
    }
    localStorage.setItem("progress", JSON.stringify(progresses));
  }
  render() {
    const { classes, result, resultAction } = this.props;
    const {
      page,
      clusters,
      finished,
      progresses,
      result_data,
      chartkey,
      max
    } = this.state;

    if (clusters.length === 0) return <div>Loading...</div>;
    if (finished) {
      return (
        <div>
          <header
            style={{
              padding: 10,
              background: "#ffa502",
              borderBottom: "1px solid #ccc",
              position: "fixed",
              width: "100%",
              zIndex: 3,
              top: 0
            }}
          >
            <h1
              style={{
                margin: 0,
                textAlign: "center",
                fontSize: "110%"
              }}
            >
              Shio Kaya
            </h1>
          </header>
          <QuizResult data={result_data} key={chartkey} max={max} content={ (typeof result.payload !== 'undefined') ? (result.payload !== null ? result.payload.content : null) : null} />
        </div>
      );
    }

    if (page >= clusters.length) {
      return (
        <div>
          <header
            style={{
              padding: 10,
              background: "#ffa502",
              borderBottom: "1px solid #ccc",
              position: "fixed",
              width: "100%",
              zIndex: 3,
              top: 0
            }}
          >
            <h1
              style={{
                margin: 0,
                textAlign: "center",
                fontSize: "110%"
              }}
            >
              Shio Kaya
            </h1>
          </header>
          <QuizResult data={result_data} key={chartkey} max={max} content={ (typeof result.payload !== 'undefined') ? (result.payload !== null ? result.payload.content : null) : null} />
        </div>
      );
    }

    return (
      <div>
        <header
          style={{
            padding: 10,
            background: "#ffa502",
            borderBottom: "1px solid #ccc",
            position: "fixed",
            width: "100%",
            zIndex: 3,
            top: 0
          }}
        >
          <h1
            style={{
              margin: 0,
              textAlign: "center",
              fontSize: "110%"
            }}
          >
            Shio Kaya
          </h1>
        </header>

        <div
          className={classes.root}
          style={{
            padding: 10,
            paddingTop: 34
          }}
        >
          <Grid container>
            <Grid item xs={12}>
              {parseInt(page) === 0 ? (
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. In in
                  blandit lorem, id ultrices tellus. Sed iaculis ex eget
                  lobortis mattis. Proin imperdiet erat in nulla fermentum, in
                  fermentum sapien faucibus.{" "}
                </p>
              ) : (
                <div style={{ marginBottom: 30 }}></div>
              )}

              {clusters[page].map((q, index) => {
                return (
                  <div
                    key={q.id}
                    style={{
                      padding: "8px 10px 20px 10px",
                      backgroundColor: "#f1f2f6",
                      borderRadius: 4,
                      marginBottom: 10
                    }}
                  >
                    <div>
                      <span style={{ fontWeight: "bold" }}>{q.no}.</span>{" "}
                      {q.question}
                    </div>
                    <div>
                      <div
                        style={{
                          width: "100%",
                          display: "inline-block"
                        }}
                      >
                        <Slider
                          defaultValue={10}
                          aria-labelledby="discrete-slider"
                          step={10}
                          marks
                          min={10}
                          max={70}
                          onChangeCommitted={(evt, value) => {
                            let p = progresses;
                            p[q.no] = value;
                            this.setState({
                              progresses: p
                            });
                          }}
                        />
                      </div>
                      <div>
                        <div
                          style={{
                            display: "inline-block",
                            width: "25%",
                            textAlign: "left",
                            verticalAlign: "top",
                            fontSize: "80%"
                          }}
                        >
                          <div
                            className={classes.bullet}
                            style={{
                              marginLeft: 0
                            }}
                          ></div>
                          {q.answer_1}
                        </div>
                        <div
                          style={{
                            width: "50%",
                            display: "inline-block",
                            textAlign: "center",
                            verticalAlign: "top",
                            fontSize: "80%"
                          }}
                        >
                          <div className={classes.bullet}></div>
                          {q.slots.length === 3 ? q.answer_2 : ""}
                        </div>

                        <div
                          style={{
                            display: "inline-block",
                            width: "25%",
                            textAlign: "right",
                            verticalAlign: "top",
                            fontSize: "80%"
                          }}
                        >
                          <div
                            className={classes.bullet}
                            style={{
                              marginRight: 0
                            }}
                          ></div>
                          {q.slots.length === 3 ? q.answer_3 : q.answer_2}
                        </div>
                      </div>
                    </div>
                  </div>
                );
              })}

              <div
                style={{
                  textAlign: "center"
                }}
              >
                <Button
                  variant="contained"
                  color="primary"
                  onClick={() => {
                    this.handleNext();
                  }}
                >
                  SELANJUTNYA
                </Button>
              </div>
            </Grid>
          </Grid>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  const { questions, result } = state;
  return {
    questions,
    result
  };
};

const mapDispatchToProps = dispatch => ({
  questionAction: data => dispatch(QuestionAction(data)),
  resultAction: data => dispatch(ResultAction(data))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(QuizContainerStyles)(QuizContainer));
