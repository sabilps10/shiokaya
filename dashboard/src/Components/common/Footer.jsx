import React from "react";
import {
    Container,
    Grid,
    Button,
    TextField,
    CircularProgress,
    Snackbar
  } from "@material-ui/core";

  
export default function Footer(props){
    return (
        <div
        style={{
        
          marginTop:60,
          bottom: 0,
          color:"#fff",
          backgroundColor: "#1e3799",
          width: "100%"
        }}
      >
        <Grid Container space={2} className="footer">
          <Grid item xs={12}>
            Version 1.0
          </Grid>
        </Grid>
      </div>
    )
}