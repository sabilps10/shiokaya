**DASHBOARD ENGINE**

**CRUD COMPONENT**

CRUD COMPONENT is the opinionated Component designed to handle the common CRUD Operation.

how to setup: 

1. simply define the module inside ```store.js```
2. prepare the endpoints (see how to setup endpoints)

how does these work ? 

1. the components will be wrapped with a `ModuleWrapper`
2. ModuleWrapper then injected the loaded component with a a `moduleProps` and `modules`


***Modules Example***

```
const modules = [
  {
    name: "Dashboard",
    slug: "dashboard",
    crud: false,
    component: DashboardContainer
  },
  {
    name: "Questions",
    slug: "questions",
    singularName: "question",
    crud: true,
    endpoint: "/question",
    updateLabel: 'question',
    fields: [
      {
        label: "Pertanyaan Nomor:",
        name:"group_id",
        type: "lookup",
        resource: "question-groups",
        key:"id",
        resource_label:"no"
      },
      {
        label: "Question",
        name: "question",
        type: "text"
      },
      {
        label:"Answer 1",
        name: "answer_1",
        type: "text"
      },
      {
        label:"Answer 2",
        name: "answer_2",
        type: "text"
      },
      {
        label:"Answer 3",
        name: "answer_3",
        type: "text"
      },
      {
        label:"Slot 1",
        name: "slot_1",
        type: "lookup",
        values: ["C1", "C2", "C3", "R1", "R2", "R3", "An", "A", "V", "K"]
      },
      {
        label:"Slot 2",
        name: "slot_2",
        type: "lookup",
        values: ["C1", "C2", "C3", "R1", "R2", "R3", "An", "A", "V", "K"]
      },
      {
        label:"Slot 3",
        name: "slot_3",
        type: "lookup",
        values: ["C1", "C2", "C3", "R1", "R2", "R3", "An", "A", "V", "K"]
      }
    ]
  },
  {
    name: "Question Groups",
    slug: "question-groups",
    singularName: "group",
    crud: true,
    endpoint: "/question-groups",
    updateLabel: 'no',
    fields: [
      {
        label:"Answer Type",
        name: "answer_type",
        type: "lookup",
        values: ['left-right','top-down','AnA','VK']
      },
      {
        label:"Question No.",
        name: "no",
        type: "number"
      },
      
    ]
  },
  {
    
    name: "Contents",
    slug: "contents",
    singularName: "content",
    crud: true,
    endpoint: "/contents",
    updateLabel: 'title',
    fields: [
      {
        label:"Slug",
        name: "slug",
        type: "text",
       
      },
      {
        label:"Title",
        name: "title",
        type: "text"
      },
      {
        label:"Text",
        name: "content_text",
        type: "richtext"
      },
     
      {
        label:"Photo",
        name: "photo_url",
        type: "image"
      },
      {
        label:"Attribute",
        name: "attribute",
        type: "text"
      },
      
    ]
  },
  {
    name: "Settings",
    slug: "settings",
    singularName: "setting",
    crud: false,
    component: SettingsContainer
  },
  {
    name: "Logout",
    slug: "logout",
    crud: false,
    component: DashboardContainer
  }
];
```


**CreateStore**

```
CreateStore(storeName, endpoint , scenario)
```

parameter | type | description
--- | --- | ---
storeName | string | store name, or the action name
endpoint  | string / callback function | the end point URI String. see example if we want to custom the endpoint using the callback function.
scenario | string | scenario type (see below)


***Callback function example***

```
(data)=>{
  return "/foo/" + data.id +"/bar"
}
```

***Scenarios***

scenario | description
--- | ---
ADD | Create
UPDATE | Update
DELETE | delete
GET | any GET requests
POST | any POST requests
LOOKUP | lookup requests

Example: 

```
const [login, loginAction] = CreateStore("LOGIN","/login","post");

const [register, registerAction] = CreateStore("REGISTER","/register","post");

const [content, getContentAction] = CreateStore("CONTENT","/content","get");

const [buy, buyAction] = CreateStore("BUY","/buy","post",true);

const [sell, sellAction] = CreateStore("SELL","/sell","post", true);

const [upcoming_courses, upcomingCoursesAction] = CreateStore("UPCOMING_COURSE","/course/available","pagination",true);

const [course_request, courseRequestAction] = CreateStore("COURSE_REQUEST","/course/request","pagination",true);

const [taken_courses, takenCoursesAction] = CreateStore("TAKEN_COURSE","/course/ongoing","pagination",true);

const [history, historyAction] = CreateStore("COURSE_HISTORY","/course/history","pagination",true);

const [summary, summaryAction] = CreateStore("SUMMARY","/profile/summary","get",true);

const [take_request, takeRequestAction] = CreateStore("TAKE_REQUEST",(data)=>{
  return "/course/request/" + data.id + "/take"
},"post",true);

const [take_course, takeCourseAction] = CreateStore("TAKE_COURSE_REQUEST",(data)=>{
  return "/course/" + data.id + "/take"
},"post",true);

const [transfer_course, transferCourseAction] = CreateStore("TRANSFER_REQUEST",(data)=>{
  return "/course/" + data.id + "/transfer"
},"post",true);

```

**CrudStore**

CrudStore is the opinionated  redux store generator for CRUD Operation.

Usage: 
```
CrudStore(storeName, endpoint);
```


parameter | type | description
--- | --- | ---
storeName | string | store name, or the action name
endpoint  | string | the end point URI String

example :

```
const profile = CrudStore("profile","/profile");
```
