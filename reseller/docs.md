```
import withAuth from "./withAuth";
import LoginContainer from "./Components/LoginContainer";
import DashboardContainer from "./Components/DashboardContainer";
import ModuleContainer from "./Components/ModuleContainer";
import SettingsContainer from "./Components/SettingsContainer";

import ModuleWrapper from "./ModuleWrapper";

const tipe_indikator = (input) => {


  const indikator = [
    {
      text: "Satuan Pemerintah Daerah",
      value: "P"
    },
    {
      text: "Satuan Inovasi",
      value: "I"
    },
  ]

  if(typeof input !== 'undefined') {
    for(let k in indikator){
      if(indikator[k].value === input) return indikator[k];
    }
  }
  return indikator;
};


const jenis_indikator = (input) => {
 

  const jenis = [
    {
      text: "Kuantitatif",
      value: "T"
    },
    {
      text: "Kualitatif",
      value: "L"
    },
    {
      text: "Pilihan",
      value: "H"
    },
    {
      text: "Progress",
      value: "G"
    },
    {
      text: "Upload Video dan Kolom Url",
      value: "V"
    },
    {
      text: "Upload Foto",
      value: "F"
    }
  ]
  
  if(typeof input !== 'undefined') {
    for(let k in jenis){
      if(jenis[k].value === input) return jenis[k];
    }
  }
  return jenis;
};


const modules = [
  {
    name: "Dashboard",
    slug: "dashboard",
    crud: false,
    component: DashboardContainer
  },
  {
    name: "Questions",
    slug: "questions",
    singularName: "question",
    crud: true,
    endpoint: "/question",
    updateLabel: "question",
    fields: [
      {
        label: "Pertanyaan Nomor:",
        name: "group_id",
        type: "lookup",
        resource: "question-groups",
        key: "id",
        resource_label: "no"
      },
      {
        label: "Question",
        name: "question",
        type: "text"
      },
      {
        label: "Answer 1",
        name: "answer_1",
        type: "text"
      },
      {
        label: "Answer 2",
        name: "answer_2",
        type: "text"
      },
      {
        label: "Answer 3",
        name: "answer_3",
        type: "text"
      },
      {
        label: "Slot 1",
        name: "slot_1",
        type: "lookup",
        values: ["C1", "C2", "C3", "R1", "R2", "R3", "An", "A", "V", "K"]
      },
      {
        label: "Slot 2",
        name: "slot_2",
        type: "lookup",
        values: ["C1", "C2", "C3", "R1", "R2", "R3", "An", "A", "V", "K"]
      },
      {
        label: "Slot 3",
        name: "slot_3",
        type: "lookup",
        values: ["C1", "C2", "C3", "R1", "R2", "R3", "An", "A", "V", "K"]
      }
    ]
  },
  {
    name: "Master Data - Tim Penilai",
    slug: "penilai",
    singularName: "penilai",
    crud: true,
    endpoint: "/crud/iga_penilai",
    updateLabel: "nama",
    meta:{
      delete_flag: true
    },
    fields: [
      {
        label: "USER ASN",
        name: "usr_asn_id",
        type: "lookup",
        resource: "crud/usr_asn",
        key: "id",
        resource_label: (data) => {
          return data.nama_lengkap + " (" + data.username + ")"
        }
      },
      {
        label: "Nama",
        name: "nama",
        type: "text"
      },
      {
        label: "Instansi",
        name: "instansi",
        type: "text"
      },
      {
        label: "Dibuat Oleh",
        name: "create_by",
        type: "auto",
        update: false, //cannot be updated.
        value: localStorage.getItem("name") 
      },
      {
        label: "Last Update Oleh",
        name: "last_update_by",
        update: true,
        type: "auto",
        value: localStorage.getItem("name") 
      },
    ]
  },
  {
    name: "Indikator",
    slug: "indikator",
    singularName: "indikator",
    crud: true,
    endpoint: "/crud/iga_indikator",
    updateLabel: "nama",
    meta:{
      delete_flag: true
    },
    fields: [
     
      {
        label: "Tipe",
        name: "tipe",
        type: "lookup",
        resource_label: (data) =>{
          return tipe_indikator(data.tipe).text
        },
        values: tipe_indikator()
      },
      {
        label: "Indikator",
        name: "indikator",
        type: "text"
      },
      {
        label: "Keterangan",
        name: "keterangan",
        type: "text"
      },
      {
        label: "Bobot",
        name: "bobot",
        type: "text"
      },
      {
        label: "Jenis",
        name: "jenis",
        type: "lookup",
        resource_label: (data) =>{
          return jenis_indikator(data.jenis).text
        },
        values: jenis_indikator()
      },
      
    ]
  },
  {
    name: "Articles",
    slug: "articles",
    singularName: "article",
    crud: true,
    endpoint: "/question",
    updateLabel: "question",
    fields: [
      {
        label: "Pertanyaan Nomor:",
        name: "group_id",
        type: "lookup",
        resource: "question-groups",
        key: "id",
        resource_label: "no"
      },
      {
        label: "Question",
        name: "question",
        type: "text"
      },
      {
        label: "Answer 1",
        name: "answer_1",
        type: "text"
      },
      {
        label: "Answer 2",
        name: "answer_2",
        type: "text"
      },
      {
        label: "Answer 3",
        name: "answer_3",
        type: "text"
      },
      {
        label: "Slot 1",
        name: "slot_1",
        type: "lookup",
        values: ["C1", "C2", "C3", "R1", "R2", "R3", "An", "A", "V", "K"]
      },
      {
        label: "Slot 2",
        name: "slot_2",
        type: "lookup",
        values: ["C1", "C2", "C3", "R1", "R2", "R3", "An", "A", "V", "K"]
      },
      {
        label: "Slot 3",
        name: "slot_3",
        type: "lookup",
        values: ["C1", "C2", "C3", "R1", "R2", "R3", "An", "A", "V", "K"]
      }
    ]
  },
  {
    name: "Question Groups",
    slug: "question-groups",
    singularName: "group",
    crud: true,
    endpoint: "/question-groups",
    updateLabel: "no",
    fields: [
      {
        label: "Answer Type",
        name: "answer_type",
        type: "lookup",
        values: ["left-right", "top-down", "AnA", "VK"]
      },
      {
        label: "Question No.",
        name: "no",
        type: "number"
      }
    ]
  },
  {
    name: "Contents",
    slug: "contents",
    singularName: "content",
    crud: true,
    endpoint: "/contents",
    updateLabel: "title",
    fields: [
      {
        label: "Slug",
        name: "slug",
        type: "text"
      },
      {
        label: "Title",
        name: "title",
        type: "text"
      },
      {
        label: "Text",
        name: "content_text",
        type: "richtext",
        visible: false //visible in listing ?
      },

      {
        label: "Photo",
        name: "photo_url",
        type: "image"
      },
      {
        label: "Attribute",
        name: "attribute",
        type: "text"
      }
    ]
  },
  {
    name: "Payment Confirmation",
    slug: "payment-confirmations",
    singularName: "Payment Confirmation",
    crud: true,
    endpoint:'/confirmation/pending',
    updateLabel:'email',
    actions:[{
      icon:"settings",
      label:"Activate",
      uri:'/confirmation/activate',
    }],
    edit:false,
    delete:false,
    add:false,
    fields: [
      {
        label: "Email",
        name: "email",
        type: "text"
      },
      {
        label: "Nomor Rekening",
        name: "nomor_rekening",
        type: "text"
      },
      {
        label: "Nama Rekening",
        name: "nama_rekening",
        type: "text",
       
      },

      {
        label: "Tanggal Transfer",
        name: "tanggal_transfer",
        type: "datetime",
       
      },
      {
        label: "Jumlah",
        name: "jumlah",
        type: "number"
      },
      {
        label: "Submit Date",
        name: "created_at",
        type: "datetime",
        readonly: true
      },
      {
        label: "Status",
        name: "confirm_status",
        type: "enum",
        values:{
          0: 'Pending',
          1: 'Confirmed'
        }
      }
    ]
  },
  {
    name: "Settings",
    slug: "settings",
    singularName: "setting",
    crud: false,
    component: SettingsContainer
  },
  {
    name: "Logout",
    slug: "logout",
    crud: false,
    component: DashboardContainer
  }
];
const getModules = () => {
  let routes = [];
  return modules.map((item, index) => {
    return {
      path: "/" + item.slug,
      exact: true,
      component: item.crud
        ? withAuth(ModuleWrapper(ModuleContainer, item, modules))
        : withAuth(ModuleWrapper(item.component, item, modules))
    };
  });
};
const routes = [
  {
    path: "/",
    exact: true,
    component: LoginContainer
  },
  {
    path: "/login",
    exact: true,
    component: LoginContainer
  },

  ...getModules()
];

export default routes;

```