import React from "react";
import { Redirect } from "react-router-dom";
import {
  Container,
  Grid,
  Button,
  TextField,
  CircularProgress,
  Snackbar,
} from "@material-ui/core";

import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import RegisterAction from "../redux/actions/RegisterAction";
import CallAction from "../redux/actions/CallAction";
import "video-react/dist/video-react.css"; // import css

import Footer from "./common/Footer";
import ModuleForm from "./ModuleForm";
import { showMessage, hasValue } from "../libs/helper";

const styles = (theme) => ({
  root: {
    display: "flex",
    flexWrap: "wrap",
    flexDirection: "column",
  },
  formControl: {
    margin: theme.spacing(1),
    width: "100%",
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  action: {
    textAlign: "right",
  },
  topMargin: {
    marginTop: "30px",
  },
  filterButton: {
    marginTop: "20px",
  },
  logo: {
    width: "150px",
    marginLeft: "auto",
    marginRight: "auto",
    marginTop: "120px",
  },
  loginForm: {
    marginTop: 30,
    maxWidth: 400,
  },
  button: {
    margin: theme.spacing(1),
    marginTop: theme.spacing(5),
  },
  action: {
    textAlign: "center",
  },
});

class RegistrationContainer extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      name: "",
      email: "",
      fetching: false,
      done: false,
      error: false,
      message: "",
      snackbar: false,
      snackbarText: "",
      success: false,
    };

    this.renderRegistrationDone = this.renderRegistrationDone.bind(this);
  }
  componentDidMount() {
    localStorage.clear();
  }
  componentDidUpdate(_props, _states) {
    const { call_action } = this.props;
    if (call_action.register !== _props.call_action.register) {
      this.handleRegister(call_action.register);
    }
  }
  handleRegister(payload) {

    if(!hasValue(payload)) return
   

    if (payload.status === 1) {

      this.setState({
        fetching: false,
        success: true,
        
      });

    } else {
      this.setState({
        fetching: false,
        
        snackbar:true,
        snackbarText: "Sorry, unable to register your account.  please try again later !"
      });
    }
  }
  renderRegistrationDone() {
    const { classes, history } = this.props;
    window.scroll({top: 0, left: 0, behavior: 'smooth' })
    return (
      <div className={classes.root}>
        <div
          style={{
            zIndex: 10,
            backgroundColor: "white",
            height: 50,
            padding: "4px 10px",
          }}
        >
          <Grid container>
            <Grid item xs={6} style={{ textAlign: "left" }}>
              <img src={'https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/logo-shiokaya.png'} height={50} />
            </Grid>
            <Grid item xs={6}>
              {/*}
              <div style={{ textAlign: "right", marginTop: 10 }}>
                <a href="/login" style={{ fontWeight: 700 }}>
                  Login
                </a>
        </div>{*/}
            </Grid>
          </Grid>
        </div>

        <div
          style={{
            paddingTop: 10,
            paddingBottom: 10,
            paddingLeft: 15,
            paddingRight: 15,
            backgroundColor: "#000000",
            color: "#ffffff",
          }}
        >
          <Grid container spacing={2}>
            <Grid item md={12}>
              <div style={{ textAlign: "center" }}>
                <h1 style={{ margin: 0 }}>Registrasi</h1>
             
              </div>
            </Grid>
          </Grid>
        </div>
        <div
          style={{
            paddingTop: 30,
            paddingBottom: 30,
            paddingLeft: 15,
            paddingRight: 15,
            minHeight: window.innerHeight * 0.7,
          }}
        >
          <Grid container spacing={2}>
            <Grid item md={3}></Grid>
            <Grid item md={6}>
              <h3>Terima Kasih!</h3>
              <p>
                Terima kasih sudah bergabung bersama kami di program afiliasi ShioKaya.
              </p>
              <p>
                Kami akan segera menghubungi anda!
              </p>
              <div>
                <Button
                  variant="contained"
                  className={classes.button}
                  color="primary"
                  onClick={() => {
                    history.push("/login");
                  }}
                >
                  KEMBALI
                </Button>
              </div>
            </Grid>
            <Grid item md={3}></Grid>
          </Grid>
        </div>
        <Footer />
      </div>
    );
  }
  render() {
    const { classes, callAction, call_action } = this.props;
    const {
      email,
      password,
      fetching,
      success,
      session_id,
      snackbar,
      snackbarText,
    } = this.state;
    if (success) return this.renderRegistrationDone();
    return (
      <div className={classes.root}>
        <div
          style={{
            zIndex: 10,
            backgroundColor: "white",
            height: 50,
            padding: "4px 10px",
          }}
        >
          <Grid container>
            <Grid item xs={6} style={{ textAlign: "left" }}>
              <img src={'https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/logo-shiokaya.png'} height={50} />
            </Grid>
            <Grid item xs={6}>
              
            </Grid>
          </Grid>
        </div>

        <div
          style={{
            paddingTop: 10,
            paddingBottom: 10,
            paddingLeft: 15,
            paddingRight: 15,
           
          }}
        >
          <Grid container spacing={2}>
            <Grid item md={12}>
              <div style={{ textAlign: "center" }}>
                <h1 style={{ margin: 0 }}>Registrasi</h1>
                <p>Mohon isi formulir dibawah ini dengan lengkap!</p>
              </div>
            </Grid>
          </Grid>
        </div>
        <div
          style={{
            paddingTop: 30,
            paddingBottom: 30,
            paddingLeft: 15,
            paddingRight: 15,
            minHeight: window.innerHeight * 0.7,
          }}
        >
          <Grid container spacing={2}>
           
            <Grid item md={12}>
              <div style={{
                margin:'0 auto',
                maxWidth:600
              }}>
              <ModuleForm
                {...this.props}
                submitText={"Daftar"}
                progress={fetching}
                onSubmit={(values) => {
                  if (
                    values.password.localeCompare(values.confirm_password) !== 0
                  ) {
                    showMessage(this,"Password anda tidak sama!")
                  } else {

                    callAction(call_action,"register",{
                      endpoint:"/affiliates/register",
                      scenario:'post',
                      data: values
                    });

                    this.setState({
                      fetching: true,
                    });

                  }
                }}
                onError={(invalids) => {
                  console.log("invalids:", invalids);
                }}
              />
              </div>
             
            </Grid>
           
          </Grid>
        </div>
        <Footer />
        <Snackbar
          open={snackbar}
          message={snackbarText}
          autoHideDuration={6000}
        ></Snackbar>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  const { call_action } = state;
  return {
    call_action,
  };
};

const mapDispatchToProps = (dispatch) => ({
  callAction: (state, name, opts) => dispatch(CallAction(state, name, opts)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(RegistrationContainer));

//export default withStyles(styles)(LoginContainer);
