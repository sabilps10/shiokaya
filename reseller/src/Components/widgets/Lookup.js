import React from 'react'
import TextField from '@material-ui/core/TextField';

export default function LabelAndTextInput(props){
    const {label,onChange} = props
    return (
        <div style={{
            marginBottom:"10px",
            ...props.containerStyle
        }}>
            <div style={{
                fontWeight:"bold",
                ...props.labelStyle
            }}>
              {label}
            </div>
            <div>
            <TextField
                id="outlined-basic"
                style={{
                    width:'100%',
                    
                   
                }}
                margin="normal"
                variant="outlined"
                inputProps={
                    {
                        style:{
                            paddingTop:8,
                            paddingBottom:8 ,
                            ...props.textFieldStyle
                        }
                    }
                }
                onChange={onChange}
                />
            </div>
        </div>
    )
}