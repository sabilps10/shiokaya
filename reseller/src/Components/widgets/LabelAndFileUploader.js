/**
 * reference: https://www.npmjs.com/package/reactjs-file-uploader
 */
import React, { useState } from "react";
import TextField from "@material-ui/core/TextField";
import CircularProgress from "@material-ui/core/CircularProgress";
import ImageUploader from "react-images-upload";
import Api from "../../Services/Api";
const api = new Api();

export default function LabelAndFileUploader(props) {
  const { label, onChange, value, action } = props;
  const [progress, setProgress] = useState(false);
  const [fileurl, setFileUrl] = useState("");
  const [files, setFiles] = useState(null);

  const [uploadedFiles, setUploadedFiles] = useState(null);
  return (
    <div>
      <input
        type="file"
        accept="application/msword, application/vnd.ms-excel, application/vnd.ms-powerpoint,
text/plain, application/pdf, image/*"
        onChange={event =>
          setFiles(files.concat(Array.from(event.target.files)))
        }
      />
      <FileManager files={files}>{uploadedFiles}</FileManager>
    </div>
  );
}
