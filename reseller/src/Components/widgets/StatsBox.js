import React from "react";
import styled from "styled-components";


const Stats = styled.div`
    h3{
        font-size:120%;
        margin:0;
        font-weight:700;
    }
    h1{
        font-size:150%;
        margin:0;
        font-weight:700;
    }
`
export default function (props) {
  const { text, value } = props;
  return (
    <Stats style={{ padding: 20, borderRadius: 10, border: "1px solid #cccccc" }}>
      <h3>{text}</h3>
      <h1>{value}</h1>
    </Stats>
  );
}
