
import React from 'react'
import TextField from '@material-ui/core/TextField';

export default function LabelAndPassword(props){
    const {label,onChange, value} = props
    return (
        <div style={{
            marginBottom:"10px",
            ...props.containerStyle
        }}>
            <div style={{
                fontWeight:"bold",
                ...props.labelStyle
            }}>
              {label}
            </div>
            <div>
            <TextField
                id="outlined-basic"
                type="password"
                style={{
                    width:'100%',
                    
                   
                }}
                defaultValue={value}
                margin="normal"
                variant="outlined"
                inputProps={
                    {
                        style:{
                            paddingTop:8,
                            paddingBottom:8 ,
                            ...props.textFieldStyle
                        }
                    }
                }
                onBlur={onChange}
                />
            </div>
        </div>
    )
}