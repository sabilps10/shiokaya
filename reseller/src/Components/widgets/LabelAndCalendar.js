import React, { useState } from "react";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
export default function LabelAndCalendar(props) {
  const { label, value, onChange, onBlur } = props;
  const [date, setDate ] = useState(new Date());

  return (
    <div
      style={{
        marginBottom: "10px"
      }}
    >
      <div
        style={{
          fontWeight: "bold",
          ...props.labelStyle
        }}
      >
        {label}
      </div>
      <div>
        <DatePicker
          selected={date}
          onChange={date => {
            setDate(date);
            onChange(date);
          }}
          dateFormat="dd/MM/yyyy"
          className={"datepicker"}
        />
      </div>
    </div>
  );
}
