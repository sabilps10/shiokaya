import React,{useState,useEffect} from "react";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import {Button} from "@material-ui/core";

export default function LabelAndAttachment(props) {
  const { label, value } = props;
    const [files, setFiles] = useState([]);
    useEffect(()=>{
        if(typeof value === 'undefined') return;
        if(value === null) return;
        setFiles(JSON.parse(value));
    },[
        value
    ])
  return (
    <div
      style={{
        marginBottom: "10px",
      }}
    >
      <div
        style={{
          fontWeight: "bold",
        }}
      >
        {label}
      </div>
      <div>
        <Table aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>Judul Penelitian</TableCell>
              <TableCell align="right">Download</TableCell>
             
            </TableRow>
          </TableHead>
          <TableBody>
            {files.map((row) => (
              <TableRow key={row.name}>
                <TableCell component="th" scope="row">
                  {row.name}
                </TableCell>
                <TableCell align="right"><Button variant={'contained'} onClick={()=>{
                    document.location = row.value;
                }}>Download</Button></TableCell>
               
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </div>
    </div>
  );
}
