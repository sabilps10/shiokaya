import React,{Suspense, lazy }  from "react";
import {
 
  CircularProgress,
  Snackbar,
} from "@material-ui/core";


import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import { Actions } from "../redux/reducers";
import CallAction from "../redux/actions/CallAction";
//import LoginAction from "../redux/actions/login";
import LoginContainerStyles from "./styles/LoginContainerStyles";

//import Top from "./common/Top";
//import Footer from "./common/Footer";
const Home = lazy(()=>import("./screens/home"));
const LoadingScreen = lazy(()=>import("./screens/loading"));
const Footer = lazy(()=>import('./common/Footer'));
class LandingContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      fetching: false,
      success: false,
      session_id: "",
      snackbar: false,
      snackbarText: "",
      content: null,
    };
    this.handleHomeContent = this.handleHomeContent.bind(this);
    this.renderLoading = this.renderLoading.bind(this);
  }
  componentDidMount() {
    const { call_action, callAction } = this.props;
    callAction(call_action, 'home', {
      endpoint: '/page/content/home',
      scenario: "get"
    })
  }
  componentDidUpdate(_props, _states) {
    const { login, call_action } = this.props;
    if (_props.call_action.home !== call_action.home) {
      this.handleHomeContent(call_action.home);
    }
  }
  handleHomeContent(payload) {
    if (typeof payload === 'undefined') return;
    if (payload === null) return;
    this.setState({
      content: payload
    })
  }
  renderLoading() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
       <Suspense fallback={''}>
         <LoadingScreen/>
       </Suspense>
        <Suspense fallback={<div>Loading...</div>}>
          <Footer />
        </Suspense>
      </div>
    );
  }
  render() {
    const { classes } = this.props;
    const {
      email,
      password,
      fetching,
      success,
      session_id,
      snackbar,
      snackbarText,
      content
    } = this.state;
    if (content === null) return this.renderLoading();

    return (
      <div className={classes.root}>
        <Suspense fallback={<CircularProgress/>}>
          <Home content={content}/>
        </Suspense>
        <Suspense fallback={<div>Loading...</div>}>
          <Footer />
        </Suspense>
        
        <Snackbar
          open={snackbar}
          message={snackbarText}
          autoHideDuration={6000}
        ></Snackbar>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  const { login, call_action } = state;
  return {
    login,
    call_action
  };
};

const mapDispatchToProps = (dispatch) => ({
  doLogin: (data) => dispatch(Actions.loginAction(data)),
  callAction: (store, name, opts) => dispatch(CallAction(store, name, opts)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(LoginContainerStyles)(LandingContainer));
