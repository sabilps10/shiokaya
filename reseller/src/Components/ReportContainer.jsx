import React from "react";
import {
  Grid,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Button,
  CircularProgress,
  Snackbar,
} from "@material-ui/core";

import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";

import DashboardStyle from "./styles/DashboardStyles";
import TopLoggedInNav from "./common/TopLoggedInNav";
import Sidebar from "./common/Sidebar";

import Footer from "./common/Footer";
import DashboardBreadcrumbs from "./common/DashboardBreadcrumbs";

import CallAction from "../redux/actions/CallAction";
import { Actions } from "../redux/reducers";
import { userHasAccess } from "../libs/helper";
import LabelAndText from "./widgets/LabelAndText";
import LabelAndTextArea from "./widgets/LabelAndTextArea";
import StatsBox from "./widgets/StatsBox";

class ReportContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      snackbar: false,
      snackbarText: "",
      showSidebar: window.innerWidth < 600 ? false : true,
      pendings: [],
    };
  }
  componentDidMount() {
   
  }
  componentDidUpdate(_props, _states) {
    const { call_action } = this.props;
    /* if (_props.call_action.pending_request !== call_action.pending_request) {
      this.handlePendingRequests(call_action.pending_request);
    }*/
  }
  componentWillUnmount() {
    
  }

  render() {
    const {
      classes,
      moduleProps,
      modules,
      history,
      getSummary,
      summary,
    } = this.props;
    const {
      pendings,
      confirm_approve,
      item,
      approve_progress,
      confirm_reject,
      reject_progress,
      snackbar,
      snackbarText,
    } = this.state;
    return (
      <div className={classes.root}>
        <TopLoggedInNav
          history={history}
        
          modules={modules}
        />
        <Grid container>
          
          <Grid item xs={12} md={12}>
            <div
              style={{
                width: "100%",
                marginTop: 80,
                paddingLeft: 15,
                paddingRight: 15,
              }}
            >
              <Grid container spacing={2}>
                <Grid item md={12}>
                  <DashboardBreadcrumbs />
                </Grid>
                <Grid item md={12}>
                  <h1>Reports</h1>
                </Grid>
                <Grid item md={12}>
                  <p>Laporan belum tersedia.</p>
                </Grid>
                
              </Grid>
            </div>
          </Grid>
        </Grid>

        <Snackbar
          open={snackbar}
          message={snackbarText}
          autoHideDuration={6000}
        ></Snackbar>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  const {  call_action } = state;
  return {
  
    call_action,
  };
};

const mapDispatchToProps = (dispatch) => ({
  callAction: (state, name, opts) => dispatch(CallAction(state, name, opts)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(DashboardStyle)(ReportContainer));
