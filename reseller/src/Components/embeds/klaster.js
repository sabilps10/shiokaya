import React from "react";
import {Grid} from "@material-ui/core";

export default function KlasterDetail(props) {
  const { data, parent, transforms } = props;

  return (
    <div>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <h4>Klaster</h4>
          {data.klaster}
        </Grid>
      </Grid>
    </div>
  );
}
