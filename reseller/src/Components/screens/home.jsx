import React, { lazy, Suspense } from "react";
import {
  Container,
  Grid,
  Button,
  TextField,
  CircularProgress,
  Snackbar,
} from "@material-ui/core";

export default function (props) {
  const { content } = props
  return (
    <div>
    <div
      style={{
        zIndex: 10,
        backgroundColor: "white",
        height: 50,
        padding: "4px 10px",
      }}
    >
      <Grid container>
        <Grid item xs={6} style={{ textAlign: "left" }}>
          <img src={'https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/logo-shiokaya.png'} height={50} />
        </Grid>
        <Grid item xs={6}>
          <div style={{ textAlign: "right", marginTop: 10 }}>

            <a href="/login" style={{ fontWeight: 700 }}>
              Login
            </a>
          </div>
        </Grid>
      </Grid>
   </div>
    <div
      style={{
        textAlign: "center",
        backgroundRepeat: "no-repeat",
        background: `url(${content.blocks['affiliate-banner'].image_url})`,
        backgroundSize: "cover",
        position: "relative",
        width: "100%",
        height: window.innerHeight,
      }}
    >
      <Grid container>
        <Grid item xs={12}>
          <div
            style={{
              textAlign: "center",
              color: "white",
              marginTop: window.innerHeight * 0.5,
            }}
          >
            <h1 style={{ margin: 0, fontSize: "400%" }}>{content.blocks['affiliate-banner'].title}</h1>
            <h2 style={{ margin: 0, fontSize: "300%" }}>
              {content.blocks['affiliate-banner'].summary}
            </h2>
            <div dangerouslySetInnerHTML={{ __html: content.blocks['affiliate-banner'].content }} style={{ maxWidth: 420, textAlign: "center", margin: '0 auto' }} />


          </div>
        </Grid>
        <Grid item xs={12}></Grid>
      </Grid>
    </div>
    <div
      style={{
        paddingTop: 60,
        paddingBottom: 60,
        paddingLeft: 15,
        paddingRight: 15,
        backgroundColor: "#000000",
        color: "#ffffff",
      }}
    >
      <Grid container spacing={2}>
        <Grid item md={12}>
          <div style={{ textAlign: "center" }}>
            <h1 style={{ margin: 0 }}>
              {content.blocks['affiliate-highlight'].title}
            </h1>
            <p dangerouslySetInnerHTML={{ __html: content.blocks['affiliate-highlight'].content }} />

          </div>
        </Grid>
      </Grid>
    </div>
    <div style={{ paddingLeft: 0, paddingRight: 0 }}>
      <Grid container spacing={2}>
        <Grid item md={5}>
          <div style={{
            width: '100%',
            height: '100%',
            backgroundRepeat: "no-repeat",
            background: `url(${content.blocks['affiliate-howto-image'].image_url})`,
            backgroundSize: "cover",
          }}>

          </div>

        </Grid>
        <Grid item md={7}>
          <div
            style={{
              paddingTop: 15,
              paddingBottom: 15,
              paddingLeft: 15,
              paddingRight: 15,
            }}
          >
            <h3 style={{ margin: 0 }}>{content.blocks['affiliate-how'].title}</h3>
            <p dangerouslySetInnerHTML={{ __html: content.blocks['affiliate-how'].content }} />


            <h3 style={{ margin: 0 }}>{content.blocks['affiliate-how-to-join'].title}</h3>
            <p dangerouslySetInnerHTML={{ __html: content.blocks['affiliate-how-to-join'].content }} />

          </div>
          <div style={{ margin: 30, textAlign: "center" }}>

            <Button variant={"contained"} color={"primary"} onClick={() => {
              this.props.history.push("/register")
            }}>BERGABUNG</Button>


          </div>
        </Grid>
      </Grid>
    </div>
    </div>
    )
}