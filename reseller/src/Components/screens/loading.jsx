import React,{lazy,Suspense} from "react";
import {
    Container,
    Grid,
    Button,
    TextField,
    CircularProgress,
    Snackbar,
  } from "@material-ui/core";
import Skeleton from '@material-ui/lab/Skeleton';

export default function(props){
   
    return (
        <div>
        <div
        style={{
          zIndex: 10,
          backgroundColor: "white",
          height: 50,
          padding: "4px 10px",
        }}
      >
        <Grid container>
          <Grid item xs={6} style={{ textAlign: "left" }}>
            <img src={'https://mastery.sgp1.cdn.digitaloceanspaces.com/shiokaya/logo-shiokaya.png'} height={50} />
          </Grid>
          <Grid item xs={6}>
            <div style={{ textAlign: "right", marginTop: 10 }}>

              <a href="/login" style={{ fontWeight: 700 }}>
                Login
              </a>
            </div>
          </Grid>
        </Grid>
      </div>
      <div>
        <Skeleton variant="rect" width={"100%"} height={window.innerHeight * 0.5} />

      </div>
      <div
        style={{
          paddingTop: 60,
          paddingBottom: 60,
          paddingLeft: 15,
          paddingRight: 15,

        }}
      >
        <Grid container spacing={2}>
          <Grid item md={12}>
            <div style={{ textAlign: "center" }}>
              <Skeleton variant="text" width={'50%'} />
              <Skeleton variant="text" width={'100%'} />
              <Skeleton variant="text" width={'100%'} />
              <Skeleton variant="text" width={'100%'} />
            </div>
          </Grid>
        </Grid>
      </div>
      <div style={{ paddingLeft: 0, paddingRight: 0 }}>
        <Grid container spacing={2}>
          <Grid item md={5}>

            <Skeleton variant="text" width={'100%'} height={window.innerHeight * 0.5} />

          </Grid>
          <Grid item md={7}>
            <div
              style={{
                paddingTop: 30,
                paddingBottom: 30,
                paddingLeft: 15,
                paddingRight: 15,
              }}
            >
              <Skeleton variant="text" width={'50%'} />

              <Skeleton variant="text" width={'100%'} />

              <Skeleton variant="text" width={'100%'} />

              <Skeleton variant="text" width={'100%'} style={{ marginBottom: 10 }} />
              <Skeleton variant="text" width={'50%'} />

              <Skeleton variant="text" width={'100%'} />

              <Skeleton variant="text" width={'100%'} />

              <Skeleton variant="text" width={'100%'} />

            </div>
            <div style={{ margin: 30, textAlign: "center" }}>

              <Skeleton variant="box" width={'100'} />



            </div>
          </Grid>
        </Grid>
      </div>
      </div>
    )
}