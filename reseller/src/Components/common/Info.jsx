import React from "react";
import { Alert, AlertTitle } from '@material-ui/lab';

export default function Info(props){
    return (
        <Alert severity="info">
                <AlertTitle>Informasi</AlertTitle>
                Input Indeks Inovasi diberikan perpanjangan waktu menjadi 20
                Juni sampai 21 Agustus 2019 pukul 24:00 WIB
              </Alert>
    )
}