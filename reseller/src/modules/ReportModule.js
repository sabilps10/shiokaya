
import ReportContainer from "../Components/ReportContainer";

export default {
    name: "Reports",
    slug: "reports",
    crud: false,
    component: ReportContainer,
    menu:true,
  }