
import BuyContainer from "../Components/BuyContainer";

export default {
    name: "Buy Vouchers",
    slug: "buy",
    crud: false,
    component: BuyContainer,
    menu:true,
}