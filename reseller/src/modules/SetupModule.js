
import SetupContainer from "../Components/SetupContainer";

export default {
    name: "Link Banners",
    slug: "setup",
    crud: false,
    component: SetupContainer,
    menu:true,
}