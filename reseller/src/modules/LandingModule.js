
import LandingPageContainer from "../Components/LandingPageContainer";

export default {
    name: "Landing Page",
    slug: "landing-page",
    crud: false,
    component: LandingPageContainer,
    menu:true,
}