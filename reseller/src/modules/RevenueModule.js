
import RevenueContainer from "../Components/RevenueContainer";

export default {
    name: "Your Revenue",
    slug: "revenue",
    crud: false,
    component: RevenueContainer,
    menu:true,
}