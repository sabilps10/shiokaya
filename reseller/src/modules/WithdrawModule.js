
import WithdrawalContainer from "../Components/WithdrawalContainer";

export default {
    name: "Withdrawal History",
    slug: "withdrawals",
    crud: false,
    component: WithdrawalContainer,
    menu:true,
}