
let INITIAL_STATE = {
    data:null,
    payload:null,
    fetching:false,
    error:false,
    message:"",
}
export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case "REGISTER":
      return {
        data: action.data,
        fetching:true,
        message:"",
    };
    case "REGISTER_SUCCESS":
      return {
        payload: action.payload,
        fetching:false
    };
    case "REGISTER_ERROR":
      return {
        data:null,
        error: true,
        fetching:false,
        payload:null,
    };
    default:
      return state;
  }
};
