
let INITIAL_STATE = {
    data:null,
    payload:null,
    fetching:false,
    error:false,
    message:"",
}
export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case "QUESTIONS":
      return {
        data: action.data,
        fetching:true,
    };
    case "QUESTIONS_SUCCESS":
      return {
        payload: action.payload,
        fetching:false
    };
    case "QUESTIONS_ERROR":
      return {
        data:null,
        error: true,
        fetching:false,
        payload:null,
        message: action.message
    };
    default:
      return state;
  }
};
