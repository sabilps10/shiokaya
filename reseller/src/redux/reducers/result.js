
let INITIAL_STATE = {
    data:null,
    payload:null,
    fetching:false,
    error:false,
    message:"",
}
export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case "RESULT":
      return {
        data: action.data,
        fetching:true,
    };
    case "RESULT_SUCCESS":
      return {
        payload: action.payload,
        fetching:false
    };
    case "RESULT_ERROR":
      return {
        data:null,
        error: true,
        fetching:false,
        payload:null,
        message: action.message
    };
    default:
      return state;
  }
};
