import React, { useState, useEffect } from "react";
import styled from "styled-components";

const Avatar = styled.div`
width:100px;
height:100px;
border-radius:200px;
margin-right:30px;
.center-cropped {
    width: 100%;
    height: 100%;
    background-position: center center;
    background-repeat: no-repeat;
    background-size:cover;
    overflow: hidden;
    margin-bottom: 14px;
   
  }
  .center-cropped img {
    min-height: 100%;
    min-width: 100%;
    /* IE 8 */
    -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
    /* IE 5-7 */
    filter: alpha(opacity=0);
    /* modern browsers */
    opacity: 0;
  }
`
export default function(props){
    const {photo_url, style} = props;
    return <Avatar style={style}>
       <div className={`center-cropped`}
                      style={{
                        backgroundImage: `url('${photo_url}')`,
                      }}>
      <img
        className="card-img-top"
        style={{ marginBottom: 20 }}
        src={photo_url}
        
      />
      </div>
    </Avatar>
}