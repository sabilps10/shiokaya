import React,{useEffect} from "react";
import styled from "styled-components";


const Snackbar = styled.div`
    margin-bottom:0;
    margin-top:10px;
    background:#f1c40f;
    color:#000;
    padding:30px 10px;
    text-align:center;
    position:fixed;
    top:0;
    width:100%;
    z-index:999999;
`
export default function(props){
    const {
        text,
        open,
        onClose
    } = props;

    useEffect(()=>{
        if(open){
            setTimeout(()=>{
                onClose();
            },3000);
        }
    },[
        open
    ])
    return open ?  (
        <Snackbar>
            <span dangerouslySetInnerHTML={{__html:text}}></span>
        </Snackbar>
    ) : null;
}