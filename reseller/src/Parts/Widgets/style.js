import styled from "styled-components";

export const Section = styled.section`
font-family: 'Gilroy Bold';
p{
  font-family: 'Gilroy Regular';
}
h3{
  font-family: 'Gilroy Bold';
}
.loginbox{
  width:400px;
  margin:0 auto;
  marginTop:60;
}
.action{
  padding:15px;
}
.btn-primary{
  box-shadow: 1px 3px 5px grey;
  border-radius: 10px;
  background-color: var(--main-yellow-color);
  font-size: 110%;
}
`