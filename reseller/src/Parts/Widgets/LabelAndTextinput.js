import React from "react";
import styled from "styled-components";


const Input = styled.div`
    margin-bottom:10px;
    h4{
        margin:0;
        font-size:120%;
    }
    input[type=text]{
        width:100%;
        padding: 4px 10px;
    }
    input[type=password]{
        width:100%;
        padding: 4px 10px;
    }
`
export default function(props){
    const {
        label,
        placeholder,
        onChange,
        password
    } = props;
    return (
        <Input>
            <h4>{label}</h4>
            <input type={password ? 'password' : 'text'} placeholder={placeholder} onBlur={(evt)=>{
                onChange(evt.target.value);
            }}/>
        </Input>
    );
}