import React from "react";
import styled from "styled-components";
import { Card, Date, FlexContainer, Section } from "./style";

const ResponsiveMenu = styled.div`
  margin-bottom: 10px;
  padding:10px;
  text-align:center;
  h4 {
    margin: 0;
    font-size: 120%;
  }
  input[type="text"] {
    width: 100%;
    padding: 4px 10px;
  }
  input[type="password"] {
    width: 100%;
    padding: 4px 10px;
  }
  select {
    width: 100%;
    padding: 10px 10px;
  }
  .mobile {
    display: none;
  }
  button{
      margin-left:10px;
  }
  @media screen and (max-width: 600px) {
    .desktop {
      display: none;
    }
    .mobile {
      display: block;
    }
  }
`;
export default function (props) {
  const { onChange, menus } = props;

  return (
    <Section>
      <ResponsiveMenu>
        <div class="row">
          <div class="col-md-12">
            <div className={`desktop`}>
                {menus.map((menu,idx)=>{
                    return <button key={idx}
                    type="button"
                    class="btn btn-primary"
                    onClick={() => {
                      onChange(menu.value);
                    }}
                  >
                    {menu.label}
                  </button>;
                })}
              
            </div>
            <div className={`mobile`}>
              <select
                onChange={(evt) => {
                  onChange(evt.target.value);
                }}
              >
                   {menus.map((menu,idx)=>{
                    return  <option key={idx} value={menu.value}>{menu.label}</option>;
                })}
               
              
              </select>
            </div>
          </div>
        </div>
      </ResponsiveMenu>
    </Section>
  );
}
