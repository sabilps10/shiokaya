var assert = require("chai").assert;
const nock = require("nock");
const axios = require("axios");
const qs = require("qs");
const getAllResponse = {
  status: 1,
  data: [
    {
      id: 1,
      account_id: 1,
      amount: 0
    }
  ],
  page: 0,
  total: 1
};


const accountSaldo = {
  status: 1,
  data:{
    amount:1
  },
};

const invalidAccount = {
  status: 0,
  error: "Invalid user account !"
};

const saldoOK = {
  "status": 1,
  "data": {
      "amount": "0.00"
  }
};

const API = {
  getAll: data => {
    return axios
      .get(
        "http://localhost:3090/saldo" +
          (typeof data !== "undefined" ? "?" + qs.stringify(data) : "")
      )
      .then(res => res.data)
      .catch(error => console.log(error.message));
  },
  getSaldo: data => {
    return axios
      .get("http://localhost:3090/account/1/saldo")
      .then(res => res.data)
      .catch(error => console.log(error.message));
  },
  getWrongUserSaldo: data => {
    return axios
      .get("http://localhost:3090/account/2/saldo")
      .then(res => res.data)
      .catch(error => console.log(error.message));
  }
};

describe("Saldo", function() {
  beforeEach(() => {
    nock("http://localhost:3090")
      .get("/saldo?page=0&per_page=10")
      .reply(200, getAllResponse);

    nock("http://localhost:3090")
      .get("/saldo")
      .reply(200, getAllResponse);

    nock("http://localhost:3090")
    .get("/account/1/saldo")
    .reply(200, saldoOK);
    
    nock("http://localhost:3090")
      .get("/account/2/saldo")
      .reply(200, invalidAccount);
  });
  describe("retrieve all", function() {
    it("can getAll with page, per_page", async function() {
      let arr = await API.getAll({
        page: 0,
        per_page: 10
      });

      assert.exists(arr.status, "OK");
      assert.exists(arr.data, "OK");
      assert.exists(arr.page, "OK");
      assert.exists(arr.total, "OK");
    });
    it("can getAll without parameters", async function() {
      let arr = await API.getAll();
      assert.exists(arr.status, "OK");
      assert.exists(arr.data, "OK");
      assert.exists(arr.page, "OK");
      assert.exists(arr.total, "OK");
    });
  });
  describe("account saldo", () => {
    it("can get user current saldo ", async function() {
      let arr = await API.getSaldo();
      assert.exists(arr.status, "OK");
      assert.exists(arr.data, "OK");
      assert.equal(arr.status, 1);
    });
    it("should fail because account is wrong ", async function() {
      let arr = await API.getWrongUserSaldo();
      assert.exists(arr.status, "OK");
      assert.equal(arr.status, 0);
    });
  });
});
